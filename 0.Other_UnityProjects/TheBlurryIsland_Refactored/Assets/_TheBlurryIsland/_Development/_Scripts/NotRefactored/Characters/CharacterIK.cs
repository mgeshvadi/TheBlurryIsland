﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;


[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(MainCharacterController))]
public class CharacterIK : MonoBehaviour
{
    public Transform ikDefaultPointsParents;
    public List<IKClass> iks;

    private const float FootIkRayLength = 0.2f;

    private Animator animator;
    private Transform ikDynamicPointsParents;
    private MainCharacterController mainCharacterControllerOwner;
    private IKClass footIKClass;
    private bool checkFootIk;
    private bool ikParentIsAttached;

    private void Awake()
    {
        Init();
    }

    private void Init()
    {
        GetComponents();
        InitIkDynamicParent();
        InitIkPoints();
        InitFootIk();
    }

    private void GetComponents()
    {
        animator = GetComponent<Animator>();
        mainCharacterControllerOwner = GetComponent<MainCharacterController>();
    }

    private void InitIkDynamicParent()
    {
        ikDynamicPointsParents = new GameObject("IkDynamicPoints").transform;
        ikDynamicPointsParents.parent = ikDefaultPointsParents;
    }

    private void InitIkPoints()
    {
        foreach (IKClass ik in iks)
        {
            GameObject ikPoint = null;
            GameObject ikPoint2 = null;
            if (ik.defaultIkPoint)
            {
                ikPoint = Instantiate(ik.defaultIkPoint, ikDynamicPointsParents).gameObject;
                ikPoint.name = ik.type + "_DynamicIKPoint";
                if (ik.needsTwoDynamicIkPoints)
                {
                    ikPoint2 = Instantiate(ik.defaultIkPoint, ikDynamicPointsParents).gameObject;
                    ikPoint2.name = ik.type + "_DynamicIKPoint_2";
                }
            }

            ik.Init(this, ikPoint, ikPoint2);
        }
    }

    private void InitFootIk()
    {
        footIKClass = GetIkClassByType(IkType.FootOnFloor);
        if (footIKClass == null)
            return;
        checkFootIk = true;
        footIKClass.leftFoot = animator.GetBoneTransform(HumanBodyBones.LeftFoot);
        footIKClass.rightFoot = animator.GetBoneTransform(HumanBodyBones.RightFoot);

        footIKClass.leftFootIkRot = footIKClass.leftFoot.rotation;
        footIKClass.rightFootIkRot = footIKClass.rightFoot.rotation;
    }

    private void OnAnimatorIK(int layerIndex)
    {
        foreach (IKClass ik in iks.Where(ik => ik.type != IkType.Intractable || iks.FindAll(ikClass => ikClass.type == IkType.Climb && ikClass.points.Count > 0).Count == 0))
            ik.HandleIK();
    }

    private void Update()
    {
        if (!checkFootIk || GetMainCharacterController().characterIKFootBlocked)
            return;
        HandleFootIk();
    }

    private void HandleFootIk()
    {
        Debug.DrawRay(footIKClass.leftFoot.TransformPoint(Vector3.zero), Vector3.down, Color.green, 0.1f);
        if (Physics.Raycast(footIKClass.leftFoot.TransformPoint(Vector3.zero), Vector3.down, out RaycastHit leftFootHit, FootIkRayLength, footIKClass.layerMask))
        {
            footIKClass.leftFootIkPos = leftFootHit.point + new Vector3(0, footIKClass.footIkOffset, 0);
            footIKClass.leftFootIkRot = Quaternion.FromToRotation(transform.up, leftFootHit.normal) * transform.rotation;
        }

        Debug.DrawRay(footIKClass.rightFoot.TransformPoint(Vector3.zero), Vector3.down, Color.green, 0.1f);
        if (Physics.Raycast(footIKClass.rightFoot.TransformPoint(Vector3.zero), Vector3.down, out RaycastHit rightFootHit, FootIkRayLength, footIKClass.layerMask))
        {
            footIKClass.rightFootIkPos = rightFootHit.point + new Vector3(0, footIKClass.footIkOffset, 0);
            footIKClass.rightFootIkRot = Quaternion.FromToRotation(transform.up, rightFootHit.normal) * transform.rotation;
        }
    }

    public void AttachIkDynamicParent()
    {
        if (ikParentIsAttached)
            return;
        ikDynamicPointsParents.parent = ikDefaultPointsParents.parent;
        ikParentIsAttached = true;
    }

    public void DetachIkDynamicParent()
    {
        if (!ikParentIsAttached)
            return;
        ikDynamicPointsParents.parent = null;
        ikParentIsAttached = false;
    }

    public MainCharacterController GetMainCharacterController()
    {
        return mainCharacterControllerOwner;
    }

    public int GetIkClassesCount()
    {
        return iks.Count;
    }

    public IKClass GetIkClassByType(IkType type)
    {
        return iks.Find(c => c.GetIkType() == type);
    }

    public void AddIkClass(IKClass value)
    {
        IKClass ikClass = GetIkClassByType(value.type);
        if (ikClass == null)
            return;
        ikClass.AddPointOfInterest(value.points);
        if (value.overrideIKSettings)
            ikClass.Override(value);
        else
            ikClass.GetToDefault();
    }

    public void RemoveIkClass(IKClass value)
    {
        IKClass ikClass = GetIkClassByType(value.type);
        ikClass.RemovePointOfInterest(value.points);
    }

    public void IKTriggerEntered(List<IKClass> values)
    {
        foreach (IKClass value in values.Where(value => value.type != IkType.Intractable || !mainCharacterControllerOwner.characterIntractableIkBlock))
            AddIkClass(value);
    }

    public void IKTriggerExit(List<IKClass> values)
    {
        foreach (IKClass value in values)
        {
            RemoveIkClass(value);
        }
    }

    public void SetLookAtWeight(float ikWeight, float bodyWeight, float headWeight, float eysWeight, float clampWeight)
    {
        animator.SetLookAtWeight(ikWeight, bodyWeight, headWeight, eysWeight, clampWeight);
    }

    public void SetLookAtPosition(Vector3 point)
    {
        animator.SetLookAtPosition(point);
    }

    public void SetIKPositionWeight(AvatarIKGoal avatarIKGoal, float weight)
    {
        animator.SetIKPositionWeight(avatarIKGoal, weight);
    }

    public void SetIKPosition(AvatarIKGoal avatarIKGoal, Vector3 point)
    {
        animator.SetIKPosition(avatarIKGoal, point);
    }

    private void SetIKRotationWeight(AvatarIKGoal avatarIKGoal, float weight)
    {
        animator.SetIKRotationWeight(avatarIKGoal, weight);
    }

    private void SetIKRotation(AvatarIKGoal avatarIKGoal, Quaternion rotation)
    {
        animator.SetIKRotation(avatarIKGoal, rotation);
    }

    public void SetPositionAndRotation(AvatarIKGoal avatarIKGoal, float weight, Vector3 position, Quaternion rotation)
    {
        SetIKPositionWeight(avatarIKGoal, weight);
        SetIKRotationWeight(avatarIKGoal, weight);
        SetIKPosition(avatarIKGoal, position);
        SetIKRotation(avatarIKGoal, rotation);
    }
}
