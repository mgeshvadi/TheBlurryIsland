﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;


[RequireComponent(typeof(MainCharacterController))]
public class CharacterActions : MonoBehaviour
{
    public List<ActionClass> actions;

    [HideInInspector] public UnityEvent onCharacterActionsControlTypeChanged = new UnityEvent(); // TODO : notice this if realtime changing actions control type
    private List<ActionClass> userControlledActions = new List<ActionClass>();
    private List<ActionClass> triggerBasedActions = new List<ActionClass>();
    private List<ActionClass> constantValueActions = new List<ActionClass>();

    private bool alreadyBind;
    private MainCharacterController mainCharacterController;
    public ModuleEventCaller currentActionCaller;


    public void SetActionValue<TValue>(ActionType actionType, TValue value) where TValue : struct
    {
        ActionClass action = GetActionClassByType(actionType);
        if (action.actionControlMode != ActionControlMode.UserInput)
            action.SetActionValue(value);
    }

    public T GetActionValue<T>(ActionType actionType) where T : struct
    {
        ActionClass action = GetActionClassByType(actionType);
        return action.GetActionValue<T>();
    }

    public List<ActionClass> GetUserControlledActions()
    {
        return actions.FindAll(action => action.actionControlMode == ActionControlMode.UserInput);
    }

    private List<ActionClass> GetTriggerBasedActions()
    {
        return actions.FindAll(action => action.actionControlMode == ActionControlMode.TriggerBased);
    }

    private List<ActionClass> GetConstantValueActions()
    {
        return actions.FindAll(action => action.actionControlMode == ActionControlMode.ConstantValue);
    }

    public ActionClass GetActionClassByType(ActionType actionType)
    {
        return actions.Find(input => input.actionType.Equals(actionType));
    }


    private void Awake()
    {
        Init();
    }

    private void Init()
    {
        InitParameters();
        UpdateActionsLists();
        onCharacterActionsControlTypeChanged.AddListener(UpdateActionsLists);
        SetConstantActionsValue();
    }

    private void InitParameters()
    {
        mainCharacterController = GetComponent<MainCharacterController>();
    }


    private void SetConstantActionsValue()
    {
        foreach (ActionClass action in constantValueActions)
            action.SetActionValue(action.defaultValue);
    }

    private void UpdateActionsLists()
    {
        ClearBinding();
        userControlledActions = new List<ActionClass>(GetUserControlledActions());
        triggerBasedActions = new List<ActionClass>(GetTriggerBasedActions());
        constantValueActions = new List<ActionClass>(GetConstantValueActions());
        BindUserControlledActions();
    }

    private void ClearBinding()
    {
        foreach (ActionClass action in userControlledActions)
        {
            if (action.actionHandlerPerform == null)
                continue;
            ActionsInputTypeHolder.instance.actionsInputActionsDic[action.actionType].performed -= action.actionHandlerPerform;
            ActionsInputTypeHolder.instance.actionsInputActionsDic[action.actionType].canceled -= action.actionHandlerCancel;
        }

        alreadyBind = false;
    }


    private void BindUserControlledActions()
    {
        if (alreadyBind)
            return;
        alreadyBind = true;

        foreach (ActionClass action in userControlledActions)
        {
            action.actionHandlerPerform = context => ActionPerformed(action, context);
            action.actionHandlerCancel = context => ActionCanceled(action, context);
            ActionsInputTypeHolder.instance.actionsInputActionsDic[action.actionType].performed += action.actionHandlerPerform;
            ActionsInputTypeHolder.instance.actionsInputActionsDic[action.actionType].canceled += action.actionHandlerCancel;
        }
    }

    private static void ActionCanceled(ActionClass action, InputAction.CallbackContext context)
    {
        switch (ActionsInputTypeHolder.instance.actionsInputTypeDic[action.actionType])
        {
            case InputValueVariableType.Button:
                if (action.actionType == ActionType.Action)
                    action.SetActionValue(false);
                break;
            case InputValueVariableType.BooleanToggle:
                break;
            case InputValueVariableType.BooleanContinues:
                break;
            case InputValueVariableType.Vector1:
                break;
            case InputValueVariableType.Vector2:
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    private void ActionPerformed(ActionClass action, InputAction.CallbackContext context)
    {
        switch (ActionsInputTypeHolder.instance.actionsInputTypeDic[action.actionType])
        {
            case InputValueVariableType.Button:
                if (action.actionType == ActionType.Action)
                {
                    action.SetActionValue(true);
                    DoAction();
                }

                if (action.actionType == ActionType.Climb)
                {
                    action.SetActionValue(true);
                    Climb();
                }

                if (action.actionType == ActionType.ClimbCancel)
                {
                    action.SetActionValue(true);
                    ClimbCancel();
                }

                break;
            case InputValueVariableType.BooleanToggle:
                action.SetActionValue(!action.GetActionValue<bool>());
                break;
            case InputValueVariableType.BooleanContinues:
                action.SetActionValue(context.ReadValue<float>() > 0.1f);
                break;
            case InputValueVariableType.Vector1:
                action.SetActionValue(context.ReadValue<float>());
                break;
            case InputValueVariableType.Vector2:
                action.SetActionValue(context.ReadValue<Vector2>());
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    public void ActionZoneTriggerEnter(ZoneAction zone)
    {
        ActionClass action = GetActionClassByType(zone.actionType);
        if (zone.overrideActionControlType)
        {
            action.previousActionControlMode = action.actionControlMode;
            action.actionControlMode = ActionControlMode.TriggerBased;
            onCharacterActionsControlTypeChanged.Invoke();
        }

        if (action.actionControlMode != ActionControlMode.TriggerBased)
            return;
        action.TriggerEntered(zone.GetZoneEnteredValue());
    }

    public void ActionZoneTriggerExit(ZoneAction zone)
    {
        ActionClass action = GetActionClassByType(zone.actionType);
        if (action.actionControlMode != ActionControlMode.TriggerBased)
            return;
        if (zone.overrideActionControlType)
        {
            action.actionControlMode = action.previousActionControlMode;
            onCharacterActionsControlTypeChanged.Invoke();
        }

        if (action.actionControlMode == ActionControlMode.ConstantValue)
            action.SetActionValue(action.defaultValue);
        else
            action.TriggerExit(zone.GetZoneExitValue());
    }

    private void DoAction()
    {
        if (currentActionCaller)
            currentActionCaller.DoAction();
    }

    private void Climb()
    {
        mainCharacterController.ClimbRequested();
    }

    private void ClimbCancel()
    {
        mainCharacterController.ClimbCancelRequested();
    }
}
