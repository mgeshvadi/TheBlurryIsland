﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(CharacterActions))]
[RequireComponent(typeof(CharacterController))]
[RequireComponent(typeof(CharacterIK))]
public class MainCharacterController : MonoBehaviour
{
    [Space] [SerializeField] private float idleTimeout = 5f; // How long before Character starts considering random idles.
    [SerializeField] private ToggleState animationBasedTurn; // Should the character Turn with animation or without

    [Space] [Header("Note: Changing 2 below values need also changes in animator")] [Tooltip("Change 'Landing blend tree' + 'Airborne BlendTree' + 'Movement BlendTree' + 'Turn Blend Tree'")] [SerializeField]
    private float maxForwardSpeedWalk = 1.5f; // How fast Character can run.

    [SerializeField] private float maxForwardSpeedRun = 7f; // How fast Character can run.

    [Space] [Header("Note: Changing below value need also change in animator")] [Tooltip("Change 'Landing blend tree and hard transition' + 'Airborne BlendTree'")] [SerializeField]
    private float jumpSpeed = 5f; // How fast Character takes off when jumping.

    [SerializeField] private float gravity = 20f; // How fast Character accelerates downwards when airborne.
    [SerializeField] private LayerMask layerMaskMovement;

    [Space] [Header("Push And Pull")] [SerializeField]
    private LayerMask layerMaskPushAndPull;

    [SerializeField] private float characterPowerInPushAndPull = 400;

    [Space] [Header("Climb")] [SerializeField]
    private LayerMask layerMaskClimb;

    [Header("Crouch Ceiling Check")] [Tooltip("Check 'CrouchCeilingCheckModes.cs' enum for more detail")] [SerializeField]
    private CrouchCeilingCheckModes crouchCeilingCheckMode = CrouchCeilingCheckModes.CheckWithBug;

    [SerializeField] private LayerMask layerMaskCrouchCeilingCheck;

    // These constants are used to ensure Character moves and behaves properly.
    // It is advised you don't change them without fully understanding what they do in code.
    private const float GroundedRayDistance = 1f;
    private const float JumpAbortSpeed = 10f;
    private const float CrouchSpeed = 10f;
    private const float StickingGravityProportion = 0.3f;
    private const float GroundAcceleration = 10f;
    private const float GroundDeceleration = 10f;

    private const float TurnSpeedWithoutAnimation = 800;
    private const float TurnSpeedWithAnimation = 500;
    private const float CrouchColliderSizePercent = 0.5f;

    private const float MovingToSplineSpeed = 4f;
    private const float MovingToSplineClampTolerance = 0.02f;
    private const float PushAndPull_Side_ObjectVelocityToAnimatorSpeedProportion = 1f;
    private const float PushAndPull_Front_ObjectVelocityToAnimatorSpeedProportion = 1f;
    private const float PushAndPullBlendSpeed = 4;
    private const float PushAndPullExtraOffsetTweenDuration = 0.3f;

    private const float RayCastPushAndPull_Side_Size = 1f;
    private const float RayCastPushAndPull_Front_Size = 0.4f;
    private const float RayCastClimb_Side_Size = 0.4f;
    private const float RayCastClimb_Front_Size = 0.2f;
    private const float PushAndPullMaxTolerableDistance = 0.5f; // If the Movable object Position and Character get beyond this value the Push and Pull ends
    private const float PushAndPullMaxTolerableVelocity = 20f; // If the Movable object Velocity get beyond this value the Push and Pull ends

    private const float RayCastPushAndPull_Side_HeightProportion = 0.4f; // 1 means cast from head of the character and 0 from her/his feet
    private const float RayCastPushAndPull_Front_HeightProportion = 0.4f; // 1 means cast from head of the character and 0 from her/his feet
    private const float RayCastPushAndPull_Side_WidthProportion = 0f; // 1 means cast from edge of the character and 0 from center of her/him
    private const float RayCastPushAndPull_Front_WidthProportion = 1f; // 1 means cast from edge of the character and 0 from center of her/him
    private const float RayCastClimb_Side_Down_HeightProportion = 0.23f; // 1 means cast from head of the character and 0 from her/his feet
    private const float RayCastClimb_Side_Top_HeightProportion = 0.71f; // 1 means cast from head of the character and 0 from her/his feet
    private const float RayCastClimb_Side_WidthProportion = 1f; // 1 means cast from edge of the character and 0 from center of her/him
    private const float RayCastClimb_Side_DepthProportion = 1f; // 1 means cast from edge of the character and 0 from center of her/him
    private const float RayCastClimb_Front_Down_HeightProportion = 0.23f; // 1 means cast from head of the character and 0 from her/his feet
    private const float RayCastClimb_Front_Top_HeightProportion = 0.71f; // 1 means cast from head of the character and 0 from her/his feet
    private const float RayCastClimb_Front_WidthProportion = 1f; // 1 means cast from edge of the character and 0 from center of her/him
    private const float RayCastClimb_Front_DepthProportion = 0f; // 1 means cast from edge of the character and 0 from center of her/him

    private const float ClimbIdlePosBlendSpeed = 10;
    private const float ClimbingUpFinalPosBlendSpeed = 2; // Note: if the speed gets to low it may cause states where the character pos at the end of climb is not still the same as the climbable orders, this speed should be high enough so the character reach that Point
    private const float ClimbReachHeight = 0.2f;
    private const float MaxClimbHeightForAutomaticClimb = 0.8f; // A value between 1 and 0
    private const float ClimbIkEndAnimPercent = 0.6f;
    private const float ClimbColliderRadiusProportion = 0.3f; // 1 means the same side az default collider size
    private const float ClimbCancelComebackAmount = 0.3f;
    private const float ClimbCancelComebackBlendSpeed = 2f;
    private const float ClimbOffsetBlendSpeed = 4f;
    private const float ClimbColliderSizeChangeDuration = 0.5f;
    private const string LayerForBeingInClimb = "InClimb";


    private Vector3 rayCastPushAndPull_Side_Origin;
    private Vector3 rayCastPushAndPull_Front_Origin;
    private Vector3 rayCastClimb_Side_Down_Origin;
    private Vector3 rayCastClimb_Front_Down_Origin;
    private Vector3 rayCastClimb_Side_Top_Origin;
    private Vector3 rayCastClimb_Front_Top_Origin;
    private float originalCharacterHeight;
    private float originalCharacterRadius;
    private float pushAndPullBlendSpeedValue;
    private float forwardSpeed; // How fast Character is currently going along the ground.
    private float desiredForwardSpeed; // How fast Character aims be going along the ground based on input.
    private float crouchAmount;
    private float lastCrouchAmount;
    private float idleTimer; // Used to count up to Character considering a random idle.
    private float verticalSpeed; // How fast Character is currently moving up or down.
    private float movementX;


    private float alertModeValue;
    private bool isGrounded = true; // Whether or not Character is currently standing on the ground.
    private bool previouslyGrounded; // Whether or not Character was standing on the ground last frame.
    private bool readyToJump; // Whether or not the input state and Character are correct to allow jumping.
    [HideInInspector] public CharacterDirection direction = CharacterDirection.Right; // Whether Character is currently Going to right or not
    private bool onPushAndPull;
    private bool isCrouch;
    private bool isUnderCeiling;
    private bool crouchRayCastCheckIsNeeded;
    private Vector3 climbCancelAppliedComeback;
    private Vector3 climbCancelComebackInitial;
    private bool climbCancelComebackExist;
    private Vector3 climbInitialOffset;
    private Vector3 climbAppliedOffset;
    private int climbColliderRadiusTweenId;
    private RespawnPoint respawnPoint;
    private bool climbIkEnd;


    private bool characterInputBlocked;
    private bool characterJumpBlocked;
    private bool characterTurnBlocked;
    [HideInInspector] public bool characterIKBlocked;
    [HideInInspector] public bool characterIKBlockedBecauseOfCrouch;
    [HideInInspector] public bool characterIKFootBlocked;
    [HideInInspector] public bool characterIntractableIkBlock;

    private Animator animator;
    private CharacterController characterController;
    private CharacterActions characterActions;
    private CharacterIK characterIK;
    private AnimatorStateInfo currentStateInfo; // Information about the base layer of the animator cached.
    private AnimatorStateInfo nextStateInfo;
    private float currentStateTime;
    private bool isAnimatorTransitioning;
    private AnimatorStateInfo previousCurrentStateInfo; // Information about the base layer of the animator from last frame.
    private AnimatorStateInfo previousNextStateInfo;
    private bool previousIsAnimatorTransitioning;
    private Movable currentMovable;
    private float extraPositionDiffPushAndPull;
    private int pushAndPullExtraOffsetTweenId;
    private InteractionSide currentMovableInteractionSide;
    private InteractionSide currentClimbInteractionSide;
    private ClimbState currentClimbState = ClimbState.NotOnClimb;
    private ClimbState readyClimbState = ClimbState.NotOnClimb; // it means we are reedy for which climb state
    private Climbable currentClimbable;
    private Climbable previousClimbable;
    private GameObject currentClimbBlendTarget;
    private float currentClimbHeight;
    private float climbLastPositionOffset;
    private readonly List<Spline> avoidablesSplinesInRange = new List<Spline>();
    private IKClass climbIkClass = new IKClass();
    private int layerForWhileInClimb;
    private int originalLayer;
    private bool layerChanged;

    private static readonly int Hash_AlertMode = Animator.StringToHash("AlertMode");
    private static readonly int Hash_PushAndPull_Side = Animator.StringToHash("PushAndPull_Side");
    private static readonly int Hash_PushAndPull_Front = Animator.StringToHash("PushAndPull_Front");
    private static readonly int Hash_ClimbIdle = Animator.StringToHash("ClimbIdle");
    private static readonly int Hash_Climb = Animator.StringToHash("Climb");
    private static readonly int Hash_StateTime = Animator.StringToHash("StateTime");
    private static readonly int Hash_ForwardSpeed = Animator.StringToHash("ForwardSpeed");
    private static readonly int Hash_PushAndPullSpeedValue = Animator.StringToHash("PushAndPullSpeed");
    private static readonly int Hash_PushAndPullWeightValue = Animator.StringToHash("PushAndPullWeight");
    private static readonly int Hash_TimeoutToIdle = Animator.StringToHash("TimeoutToIdle");
    private static readonly int Hash_InputDetected = Animator.StringToHash("InputDetected");
    private static readonly int Hash_IsGrounded = Animator.StringToHash("IsGrounded");
    private static readonly int Hash_AirborneVerticalSpeed = Animator.StringToHash("AirborneVerticalSpeed");
    private static readonly int Hash_CrouchAmount = Animator.StringToHash("CrouchAmount");
    private static readonly int Hash_Turn = Animator.StringToHash("Turn");
    private static readonly int Hash_CancelTurn = Animator.StringToHash("CancelTurn");
    private static readonly int Hash_ClimbFromSide = Animator.StringToHash("ClimbFromSide");
    private static readonly int Hash_ClimbHeight = Animator.StringToHash("ClimbHeight");
    private static readonly int Hash_Die = Animator.StringToHash("Die");
    private static readonly int Hash_Respawn = Animator.StringToHash("Respawn");
    private static readonly int Hash_Swim = Animator.StringToHash("Swim");
    private static readonly int Hash_IsSwimming = Animator.StringToHash("IsSwimming");

    // Tags
    private static readonly int Hash_ClimbTag = Animator.StringToHash("ClimbTag");
    private static readonly int Hash_ClimbIdleTag = Animator.StringToHash("ClimbIdleTag");
    private static readonly int Hash_BlockInputTag = Animator.StringToHash("BlockInput"); // TODO : Notice this
    private static readonly int Hash_LandingTag = Animator.StringToHash("Landing"); // TODO : Notice this
    private static readonly int Hash_LandingHardTag = Animator.StringToHash("LandingHard"); // TODO : Notice this
    private static readonly int Hash_AirborneTag = Animator.StringToHash("AirBorne"); // TODO : Notice this
    private static readonly int Hash_TurnTag = Animator.StringToHash("Turn"); // TODO : Notice this
    private static readonly int Hash_RandomIdleTag = Animator.StringToHash("RandomIdle"); // TODO : Notice this
    private static readonly int Hash_PushAndPullTag = Animator.StringToHash("PushAndPull"); // TODO : Notice this
    private static readonly int Hash_MovementTag = Animator.StringToHash("Movement"); // TODO : Notice this
    private static readonly int Hash_BeginRespawnTag = Animator.StringToHash("BeginRespawn");


    private void Start()
    {
        Init();
    }

    private void Init()
    {
        GetComponents();
        SetParameters();
        SetRayCastsOrigins();
    }

    private void GetComponents()
    {
        animator = GetComponent<Animator>();
        characterController = GetComponent<CharacterController>();
        characterActions = GetComponent<CharacterActions>();
        characterIK = GetComponent<CharacterIK>();
    }

    private void SetParameters()
    {
        climbIkClass = new IKClass {type = IkType.Climb};
        originalCharacterHeight = characterController.height;
        originalCharacterRadius = characterController.radius;
        originalLayer = gameObject.layer;
        layerForWhileInClimb = LayerMask.NameToLayer(LayerForBeingInClimb);
    }

    private void SetRayCastsOrigins()
    {
        float radius = characterController.radius;
        rayCastPushAndPull_Side_Origin = new Vector3(0, originalCharacterHeight * RayCastPushAndPull_Side_HeightProportion, radius * RayCastPushAndPull_Side_WidthProportion);
        rayCastPushAndPull_Front_Origin = new Vector3(0, originalCharacterHeight * RayCastPushAndPull_Front_HeightProportion, radius * RayCastPushAndPull_Front_WidthProportion);

        rayCastClimb_Side_Down_Origin = new Vector3(-radius * RayCastClimb_Side_DepthProportion, originalCharacterHeight * RayCastClimb_Side_Down_HeightProportion, radius * RayCastClimb_Side_WidthProportion);
        rayCastClimb_Front_Down_Origin = new Vector3(-radius * RayCastClimb_Front_DepthProportion, originalCharacterHeight * RayCastClimb_Front_Down_HeightProportion, radius * RayCastClimb_Front_WidthProportion);
        rayCastClimb_Side_Top_Origin = new Vector3(-radius * RayCastClimb_Side_DepthProportion, originalCharacterHeight * RayCastClimb_Side_Top_HeightProportion, radius * RayCastClimb_Side_WidthProportion);
        rayCastClimb_Front_Top_Origin = new Vector3(-radius * RayCastClimb_Front_DepthProportion, originalCharacterHeight * RayCastClimb_Front_Top_HeightProportion, radius * RayCastClimb_Front_WidthProportion);
    }

    public void AddAvoidableSpline(List<Spline> splines)
    {
        foreach (Spline spline in splines)
            avoidablesSplinesInRange.Add(spline);
    }

    public void RemoveAvoidableSpline(List<Spline> splines)
    {
        foreach (Spline spline in splines)
            avoidablesSplinesInRange.Remove(spline);
    }

    public void OnAnimatorMove()
    {
        HandleMovement();
        HandleRotation();

        HandleIsGrounded();
        CheckForPushAndPullRequested();
        HandlePushAndPull();
        HandleVerticalSpeed();
    }

    private void FixedUpdate()
    {
        CacheAnimatorState();

        UpdateInputBlocking();
        UpdateJumpBlocking();
        UpdateTurnBlocking();
        UpdateIKBlockingAndParenting();

        CalculateForwardMovement();
        CalculateVerticalMovement();
        CalculateCrouchMovement();
        CalculateColliderSize();

        CheckForPushAndPullAddForce();
        HandleAlertMode();

        SetStateTimeInAnimator();
        TimeoutToIdle();
        CheckForClimbIfAirborne();
        CheckClimbPhaseChange();
        CheckForRespawn();
        previouslyGrounded = isGrounded;
    }

    private void HandleMovement()
    {
        Vector3 movement = CalculateMovement();
        float goalX = CalculateGoalX();

        if (IsOnClimb())
            movementX = movement.x;
        else if (!isGrounded || Math.Abs(transform.position.x - goalX) < MovingToSplineClampTolerance)
            movementX = 0;
        else
        {
            Vector3 position = transform.position;
            movementX = Mathf.MoveTowards(movementX, goalX - position.x, Time.deltaTime * MovingToSplineSpeed * Math.Abs(movement.z));
            movementX = Mathf.Clamp(movementX, -Math.Abs(goalX - position.x), Math.Abs(goalX - position.x));
        }

        movement.Set(movementX, movement.y, movement.z);
        characterController.Move(movement); // Move the character controller.
    }

    private Vector3 CalculateMovement()
    {
        Vector3 movement;
        if (IsOnClimb())
            movement = CalculateMovementClimb();
        else
        {
            movement = isGrounded ? onPushAndPull ? CalculateAndHandleMovementPushAndPull() : CalculateMovementGroundedNotClimbNotPushAndPull() : forwardSpeed * Time.deltaTime * transform.forward;
            movement += verticalSpeed * Time.deltaTime * Vector3.up; // Add to the movement with the calculated vertical speed.
        }

        if (climbCancelComebackExist)
        {
            Vector3 movementOffset = CalculateClimbCancelComeback();
            if (movementOffset.magnitude <= 0.005f)
                TurnOffClimbComeBack();
            movement += movementOffset;
        }

        return movement;
    }

    private Vector3 CalculateMovementGroundedNotClimbNotPushAndPull()
    {
        Vector3 origin = transform.position + 0.5f * GroundedRayDistance * Vector3.up;
        // ... rayCast into the ground...
        // if RayCast hit then get the movement of the root motion rotated to lie along the plane of the ground.
        // else If no ground is hit just get the movement as the root motion. Theoretically this should rarely happen as when grounded the ray should always hit.
        DrawDebugRay(origin, -Vector3.up * GroundedRayDistance);
        return Physics.Raycast(origin, -Vector3.up, out RaycastHit hit, GroundedRayDistance, layerMaskMovement, QueryTriggerInteraction.Ignore) ? Vector3.ProjectOnPlane(animator.deltaPosition, hit.normal) : animator.deltaPosition;
    }

    private Vector3 CalculateAndHandleMovementPushAndPull()
    {
        float diff = currentMovable.GetCharacterIdealPositionZ() + ((direction == CharacterDirection.Right || direction == CharacterDirection.GoingToRight ? -1 : 1) * extraPositionDiffPushAndPull * (!IsInteractionFromSide(currentMovableInteractionSide) && (direction == CharacterDirection.Right || direction == CharacterDirection.GoingToRight) ? -1 : 1)) - transform.position.z;
        Vector3 movement = new Vector3(0, 0, diff);
        if (diff > PushAndPullMaxTolerableDistance)
            EndPushAndPull();
        return movement;
    }

    private Vector3 CalculateMovementClimb()
    {
        Vector3 movement;
        switch (currentClimbState)
        {
            case ClimbState.ClimbIdle:
                movement = MoveTowardsVector3(Vector3.zero, currentClimbBlendTarget.transform.position - transform.position, Time.deltaTime * ClimbIdlePosBlendSpeed * (currentClimbable.GetVelocityMagnitude()+1));
                readyClimbState = (movement - Vector3.zero).magnitude < 0.02f ? ClimbState.ClimbingUp : ClimbState.ClimbIdle;
                return movement;
            case ClimbState.ClimbingUp:
                movement = CalculateClimbingUpOffset();
                if (readyClimbState != ClimbState.ClimbingEnd)
                {
                    if (movement.magnitude <= 0.005f)
                        readyClimbState = ClimbState.ClimbingEnd;
                }
                return currentStateInfo.tagHash == Hash_ClimbTag || (currentStateInfo.tagHash == Hash_ClimbIdleTag && !isAnimatorTransitioning) ? animator.deltaPosition * 1.2f + movement : movement; // TODO : after correcting climb animation remove this Multiplier value
            case ClimbState.ClimbingEnd:
                movement = MoveTowardsVector3(Vector3.zero, currentClimbBlendTarget.transform.position - transform.position, Time.deltaTime * ClimbingUpFinalPosBlendSpeed* (currentClimbable.GetVelocityMagnitude()+1));
                readyClimbState = (movement - Vector3.zero).magnitude < 0.02f ? ClimbState.NotOnClimb : readyClimbState;
                return movement;
            case ClimbState.NotOnClimb:
                return Vector3.zero;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    private float CalculateGoalX()
    {
        return avoidablesSplinesInRange.Where(spline => spline.ContainsCharacter(transform.position)).Select(spline => spline.CalculateX(transform.position.z)).Concat(new float[] {0}).Max();
    }

    private void HandleRotation()
    {
        if (ShouldTurnByAnimation())
            Turn();
        if (!isGrounded && !climbCancelComebackExist) // it means jumping while turing has happened
            HandleTurnRotationInOneFrame();
        else if (!onPushAndPull)
            HandleTurnRotation();
    }

    private bool ShouldTurnByAnimation()
    {
        if (animationBasedTurn == ToggleState.Off)
            return false;
        if (!IsTurningByAnimation() || AreInputAndCharacterAtSameDirection())
            return isGrounded && !characterTurnBlocked && !AreInputAndCharacterAtSameDirection() && (direction == CharacterDirection.Left || direction == CharacterDirection.Right);
        animator.SetTrigger(Hash_CancelTurn);
        return false;
    }

    private void Turn()
    {
        animator.ResetTrigger(Hash_CancelTurn);
        if (!animator.GetBool(Hash_InputDetected))
            animator.SetTrigger(Hash_Turn);
    }

    private bool IsTurningByAnimation()
    {
        return ((currentStateInfo.tagHash == Hash_TurnTag && !isAnimatorTransitioning) || nextStateInfo.tagHash == Hash_TurnTag);
    }

    private bool AreInputAndCharacterAtSameDirection()
    {
        return (GetMovementInput().x <= 0 || IsOnRightRoughly()) && (GetMovementInput().x >= 0 || !IsOnRightRoughly());
    }

    private void HandleTurnRotation()
    {
        if (IsTurningByAnimation())
            characterController.transform.rotation *= animator.deltaRotation; // Rotate the transform of the character controller by the animation's root rotation while turn states.
        else if (ShouldLerpRotation())
            LerpRotation();
        direction = ComputeDirection(transform.eulerAngles.y);
    }

    private bool ShouldLerpRotation()
    {
        return (!isAnimatorTransitioning || currentStateInfo.tagHash == Hash_TurnTag || currentStateInfo.tagHash == Hash_ClimbIdleTag || currentStateInfo.tagHash == Hash_ClimbTag) && currentStateInfo.tagHash != Hash_RandomIdleTag && nextStateInfo.tagHash != Hash_RandomIdleTag && currentStateInfo.tagHash != Hash_Respawn && currentStateInfo.tagHash != Hash_BeginRespawnTag && nextStateInfo.tagHash != Hash_Respawn && nextStateInfo.tagHash != Hash_BeginRespawnTag;
    }

    private void LerpRotation()
    {
        float inputX = GetMovementInput().x;
        float goalRotationY;
        if (IsOnClimb() && IsInteractionFromSide(currentClimbInteractionSide))
            goalRotationY = 270;
        else
            goalRotationY = !Mathf.Approximately(inputX, 0) ? inputX >= 0 ? 0 : 180 : IsOnRightRoughly() ? 0 : 180;

        if (!Mathf.Approximately(goalRotationY, transform.eulerAngles.y))
            goalRotationY = Mathf.MoveTowardsAngle(transform.eulerAngles.y, goalRotationY, Time.deltaTime * (animationBasedTurn == ToggleState.On ? TurnSpeedWithAnimation : TurnSpeedWithoutAnimation));
        SetRotationY(goalRotationY);
    }

    private void HandleTurnRotationInOneFrame()
    {
        SetRotationY(IsOnRightRoughly() ? 0 : 180);
    }

    private void SetRotationY(float goalRotationY)
    {
        Transform transformTemp = transform;
        Vector3 eulerAngles = transformTemp.eulerAngles;
        eulerAngles = new Vector3(eulerAngles.x, goalRotationY, eulerAngles.z);
        transformTemp.eulerAngles = eulerAngles;
    }

    private void HandleIsGrounded()
    {
        isGrounded = IsOnClimb() || characterController.isGrounded; // After the movement store whether or not the character controller is grounded.
        animator.SetBool(Hash_IsGrounded, isGrounded); // Send whether or not 'Five' is on the ground to the animator.
    }

    private void CheckForPushAndPullRequested()
    {
        if (!onPushAndPull && GetPushAndPullInput() && isGrounded)
        {
            if (CheckForPushAndPull_Front(out RaycastHit hitInfo))
                PushPullStarted(hitInfo, InteractionSide.Right); // For push and pull Right or Left is no different
            else if (CheckForPushAndPull_Side(out hitInfo))
                PushPullStarted(hitInfo, InteractionSide.Front); // For push and pull Front or back is no different
        }
        else if (onPushAndPull && (!GetPushAndPullInput() || !isGrounded || currentMovable.GetVelocity().magnitude > PushAndPullMaxTolerableVelocity))
            EndPushAndPull();
    }

    private bool CheckForPushAndPull_Front(out RaycastHit hitInfoResult)
    {
        Vector3 rayCastPosition = new Vector3(rayCastPushAndPull_Front_Origin.x, rayCastPushAndPull_Front_Origin.y, rayCastPushAndPull_Front_Origin.z * (IsOnRightRoughly() ? 1 : -1)) + transform.position;
        return EmitPushAndPullRayCast(rayCastPosition, IsOnRightRoughly() ? Vector3.forward : Vector3.back, RayCastPushAndPull_Front_Size, out hitInfoResult);
    }

    private bool CheckForPushAndPull_Side(out RaycastHit hitInfoResult)
    {
        Vector3 rayCastPosition = new Vector3(rayCastPushAndPull_Side_Origin.x, rayCastPushAndPull_Side_Origin.y, rayCastPushAndPull_Side_Origin.z * (IsOnRightRoughly() ? 1 : -1)) + transform.position;
        return EmitPushAndPullRayCast(rayCastPosition, Vector3.left, RayCastPushAndPull_Side_Size, out hitInfoResult);
    }


    private bool EmitPushAndPullRayCast(Vector3 rayCastPosition, Vector3 rayDirection, float rayCastPushAndPullSize, out RaycastHit hitInfo)
    {
        DrawDebugRay(rayCastPosition, rayDirection * rayCastPushAndPullSize, Color.yellow);
        return !(!Physics.Raycast(rayCastPosition, rayDirection, out hitInfo, rayCastPushAndPullSize, layerMaskPushAndPull, QueryTriggerInteraction.Ignore) || (!hitInfo.collider.CompareTag("Movable") && !hitInfo.collider.CompareTag("MovableAndClimbable")));
    }


    private void PushPullStarted(RaycastHit hitInfo, InteractionSide interactionSide)
    {
        currentMovableInteractionSide = interactionSide;
        HandleTurnRotationInOneFrame();
        InitializeNewMovable(hitInfo.collider.transform.parent.GetComponent<Movable>());
        CheckForPushAndPullExtraOffset();

        onPushAndPull = true;
        HandlePushAndPullAnimator();
    }

    private void InitializeNewMovable(Movable movable)
    {
        currentMovable = movable;
        currentMovable.GetReadyForMove(transform.position.z);
        animator.SetFloat(Hash_PushAndPullWeightValue, currentMovable.GetBlendTreeValue());
    }

    private void CheckForPushAndPullExtraOffset()
    {
        float pushAndPullInitialDiff = currentMovable.GetInitialPositionDifference(currentMovableInteractionSide);
        Vector2 sizeZs;
        switch (currentMovableInteractionSide)
        {
            case InteractionSide.Front:
            case InteractionSide.Back:
                sizeZs = currentMovable.GetSideZs();
                if ((direction == CharacterDirection.Right || direction == CharacterDirection.GoingToRight) && sizeZs.y < transform.position.z + pushAndPullInitialDiff)
                    PushAndPullExtraOffsetNeeded(Mathf.Abs(sizeZs.y - (transform.position.z + pushAndPullInitialDiff)));
                else if ((direction == CharacterDirection.Left || direction == CharacterDirection.GoingToLeft) && sizeZs.x > transform.position.z - pushAndPullInitialDiff)
                    PushAndPullExtraOffsetNeeded(Mathf.Abs(sizeZs.x - (transform.position.z - pushAndPullInitialDiff)));
                else
                    extraPositionDiffPushAndPull = 0;
                break;

            case InteractionSide.Left:
            case InteractionSide.Right:
                sizeZs = currentMovable.GetSideZs();
                if (direction == CharacterDirection.Right || direction == CharacterDirection.GoingToRight)
                    PushAndPullExtraOffsetNeeded(sizeZs.x - (transform.position.z + pushAndPullInitialDiff));
                else
                    PushAndPullExtraOffsetNeeded(sizeZs.y - (transform.position.z - pushAndPullInitialDiff));
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    private void PushAndPullExtraOffsetNeeded(float offsetAmount)
    {
        LeanTween.cancel(pushAndPullExtraOffsetTweenId);
        pushAndPullExtraOffsetTweenId = LeanTween.value(0, offsetAmount, PushAndPullExtraOffsetTweenDuration).setOnUpdate(f => extraPositionDiffPushAndPull = f).id;
    }

    private void HandlePushAndPullAnimator()
    {
        switch (currentMovableInteractionSide)
        {
            case InteractionSide.Right:
            case InteractionSide.Left:
                animator.SetBool(Hash_PushAndPull_Side, onPushAndPull);
                break;
            case InteractionSide.Front:
            case InteractionSide.Back:
                animator.SetBool(Hash_PushAndPull_Front, onPushAndPull);
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(currentMovableInteractionSide), currentMovableInteractionSide, null);
        }
    }

    private void EndPushAndPull()
    {
        currentMovable.MoveDone();
        onPushAndPull = false;
        animator.SetBool(Hash_PushAndPull_Side, onPushAndPull);
        animator.SetBool(Hash_PushAndPull_Front, onPushAndPull);
    }

    private void HandlePushAndPull()
    {
        if (onPushAndPull && currentStateInfo.tagHash == Hash_PushAndPullTag && !isAnimatorTransitioning && Mathf.Abs(GetMovementInput().x) > 0.15f)
        {
            pushAndPullBlendSpeedValue = Mathf.MoveTowards(pushAndPullBlendSpeedValue, GetMovementInput().x * (IsOnRightRoughly() ? 1 : -1), Time.deltaTime * PushAndPullBlendSpeed);
            animator.speed = Math.Abs(currentMovable.GetVelocity().z * (!IsInteractionFromSide(currentMovableInteractionSide) ? PushAndPull_Front_ObjectVelocityToAnimatorSpeedProportion : PushAndPull_Side_ObjectVelocityToAnimatorSpeedProportion));
        }
        else if (!onPushAndPull || Mathf.Abs(GetMovementInput().x) < 0.15f)
        {
            pushAndPullBlendSpeedValue = Mathf.MoveTowards(pushAndPullBlendSpeedValue, 0, Time.deltaTime * PushAndPullBlendSpeed);
            animator.speed = 1;
        }

        animator.SetFloat(Hash_PushAndPullSpeedValue, pushAndPullBlendSpeedValue);
    }

    public void ClimbCancelRequested()
    {
        if (currentClimbState != ClimbState.ClimbIdle || !GetClimbCancelInput())
            return;
        SetClimbCancelComeback();
        ClimbGoingForEnd();
        ClimbIkEnd();
        ClimbEnd();
    }

    public void DieFunc()
    {
        animator.SetTrigger(Hash_Die);
    }

    private void Reset()
    {
        animator.SetTrigger(Hash_Respawn);
        ResetAnimator();
        ResetPosition();
    }

    private void ResetAnimator()
    {
        animator.SetBool(Hash_IsGrounded, true);
        animator.ResetTrigger(Hash_CancelTurn);
        animator.SetBool(Hash_InputDetected, false);
        animator.ResetTrigger(Hash_Turn);
        animator.ResetTrigger(Hash_TimeoutToIdle);
        animator.ResetTrigger(Hash_Respawn);
        animator.ResetTrigger(Hash_Swim);
        animator.ResetTrigger(Hash_Die);
        animator.SetBool(Hash_PushAndPull_Side, false);
        animator.SetBool(Hash_PushAndPull_Front, false);
        animator.SetBool(Hash_IsSwimming, false);
        animator.SetBool(Hash_ClimbIdle, false);
        animator.ResetTrigger(Hash_Climb);
        animator.SetBool(Hash_ClimbFromSide, false);
    }

    private void ResetPosition()
    {
        if (respawnPoint)
            transform.position = respawnPoint.transform.position;
    }

    public void SetRespawn(RespawnPoint respawnPointTriggered)
    {
        respawnPoint = respawnPointTriggered;
    }

    private void SetClimbCancelComeback()
    {
        climbCancelComebackExist = true;

        switch (currentClimbInteractionSide)
        {
            case InteractionSide.Right:
            case InteractionSide.Left:
                climbCancelComebackInitial = new Vector3(0, 0, (IsOnRightRoughly() ? -1 : 1) * ClimbCancelComebackAmount);
                break;
            case InteractionSide.Front:
            case InteractionSide.Back:
                climbCancelComebackInitial = new Vector3(-ClimbCancelComebackAmount, 0, 0);
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }

        climbCancelAppliedComeback = Vector3.zero;
    }

    private Vector3 CalculateClimbCancelComeback()
    {
        Vector3 preAppliedOffset = climbCancelAppliedComeback;
        climbCancelAppliedComeback = MoveTowardsVector3(climbCancelAppliedComeback, climbCancelComebackInitial, Time.deltaTime * ClimbCancelComebackBlendSpeed);
        return climbCancelAppliedComeback - preAppliedOffset;
    }

    private void TurnOffClimbComeBack()
    {
        climbCancelAppliedComeback = Vector3.zero;
        climbCancelComebackExist = false;
        SetColliderRadius(originalCharacterRadius);
        previousClimbable.ClimbEnd();
        ChangeLayerToOriginal();
    }

    public void ClimbRequested()
    {
        if (currentClimbState == ClimbState.ClimbIdle)
            CheckForClimbingUp();
        else
            CheckForClimb();
    }

    private void CheckForClimbingUp() // step 2 Means character is in climb idle, waiting for user to ask for doing the climb itself
    {
        if (readyClimbState != ClimbState.ClimbingUp || !GetClimbInput() || !currentClimbable || currentClimbable.GetClimbType() == ClimbType.SinglePhaseClimb || climbCancelComebackExist) // if current climbable climb type is one step base, then second step will happen automatically
            return;
        ClimbUp();
    }

    private void CheckForClimb() // step 1 Means character is starting climb, it may be a complete climb or only to climbIdle
    {
        if ((!GetClimbInput() && isGrounded) || IsOnClimb() || currentClimbable || climbCancelComebackExist)
            return;

        if (CheckForClimb_Front(out RaycastHit hitInfo))
            CheckIfClimbIsPossible(hitInfo);
        else if (CheckForClimb_Side(out hitInfo))
            CheckIfClimbIsPossible(hitInfo);
    }

    private bool CheckForClimb_Front(out RaycastHit hitInfoResult)
    {
        Vector3 position = transform.position;
        Vector3 rayCastPositionDown = new Vector3(rayCastClimb_Front_Down_Origin.x, rayCastClimb_Front_Down_Origin.y, rayCastClimb_Front_Down_Origin.z * (IsOnRightRoughly() ? 1 : -1)) + position;
        Vector3 rayCastPositionTop = new Vector3(rayCastClimb_Front_Top_Origin.x, rayCastClimb_Front_Top_Origin.y, rayCastClimb_Front_Top_Origin.z * (IsOnRightRoughly() ? 1 : -1)) + position;
        return EmitClimbRayCast(rayCastPositionDown, IsOnRightRoughly() ? Vector3.forward : Vector3.back, RayCastClimb_Front_Size, out hitInfoResult)
               || EmitClimbRayCast(rayCastPositionTop, IsOnRightRoughly() ? Vector3.forward : Vector3.back, RayCastClimb_Front_Size, out hitInfoResult);
    }

    private bool CheckForClimb_Side(out RaycastHit hitInfoResult)
    {
        Vector3 position = transform.position;
        Vector3 rayCastPositionDown = new Vector3(rayCastClimb_Side_Down_Origin.x, rayCastClimb_Side_Down_Origin.y, rayCastClimb_Side_Down_Origin.z * (IsOnRightRoughly() ? 1 : -1)) + position;
        Vector3 rayCastPositionTop = new Vector3(rayCastClimb_Side_Top_Origin.x, rayCastClimb_Side_Top_Origin.y, rayCastClimb_Side_Top_Origin.z * (IsOnRightRoughly() ? 1 : -1)) + position;
        return EmitClimbRayCast(rayCastPositionDown, Vector3.left, RayCastClimb_Side_Size, out hitInfoResult)
               || EmitClimbRayCast(rayCastPositionTop, Vector3.left, RayCastClimb_Side_Size, out hitInfoResult);
    }

    private bool EmitClimbRayCast(Vector3 rayCastPosition, Vector3 rayDirection, float rayCastClimbSize, out RaycastHit hitInfo)
    {
        DrawDebugRay(rayCastPosition, rayDirection * rayCastClimbSize, Color.red);
        return !(!Physics.Raycast(rayCastPosition, rayDirection, out hitInfo, rayCastClimbSize, layerMaskClimb, QueryTriggerInteraction.Ignore) || (!hitInfo.collider.CompareTag("Climbable") && !hitInfo.collider.CompareTag("MovableAndClimbable")));
    }

    private void CheckIfClimbIsPossible(RaycastHit hitInfo)
    {
        currentClimbable = hitInfo.collider.GetComponent<Climbable>();
        if (!currentClimbable)
        {
            Debug.LogError("An object with climbable tag but without script on it detected");
            return;
        }


        float lowestClimbPointY = currentClimbable.GetToppestSide().GetGlobalPosition().y;
        if (!CanReachClimbPoint(lowestClimbPointY))
        {
            currentClimbable = null;
            return;
        }

        currentClimbInteractionSide = currentClimbable.InteractionHappened(transform.position, originalCharacterHeight, originalCharacterRadius);
        animator.SetBool(Hash_ClimbFromSide, IsInteractionFromSide(currentClimbInteractionSide));
        CalculateClimbHeight(lowestClimbPointY);

        Climb();
    }

    private bool CanReachClimbPoint(float climbPointY) // if climb Point is so high do not climb
    {
        return climbPointY < transform.position.y + originalCharacterHeight + ClimbReachHeight;
    }

    private Vector3 CalculateClimbingUpOffset()
    {
        SetClimbingUpOffset();
        Vector3 preAppliedOffset = climbAppliedOffset;
        climbAppliedOffset = MoveTowardsVector3(climbAppliedOffset, climbInitialOffset, Time.deltaTime * ClimbOffsetBlendSpeed * (currentClimbable.GetVelocityMagnitude()+1));
        return climbAppliedOffset - preAppliedOffset;
    }

    private void CalculateClimbHeight(float lowestClimbPointY)
    {
        // 1 :  lowestClimbPointY - transform.position.y = originalCharacterHeight + ClimbReachHeight
        // 0 : lowestClimbPointY - transform.position.y = 0
        currentClimbHeight = Mathf.Clamp(Math.Abs((lowestClimbPointY - transform.position.y) / (originalCharacterHeight + ClimbReachHeight)), 0f, 1f);
    }

    private void Climb()
    {
        SetColliderRadius(ClimbColliderRadiusProportion * originalCharacterRadius);
        AddClimbIks();
        Climb(currentClimbable.GetClimbType());
    }

    private void AddClimbIks()
    {
        if(!climbIkEnd)
            return;
        climbIkEnd = false;
        climbIkClass.points = currentClimbable.GetClimbIkGameObjects(currentClimbInteractionSide);
        characterIK.AddIkClass(climbIkClass);
    }

    private void Climb(ClimbType climbType)
    {
        HandleTurnRotationInOneFrame();
        climbLastPositionOffset = (currentClimbable.transform.position - transform.position).magnitude;
        currentClimbState = ClimbState.ClimbIdle;
        ChangeLayerToClimbMode();
        switch (climbType) // TODO : other types of climbs may be needed to be added here
        {
            case ClimbType.AutomaticClimb when currentClimbHeight < MaxClimbHeightForAutomaticClimb:
            case ClimbType.SinglePhaseClimb:
                ClimbUp();
                break;
            case ClimbType.AutomaticClimb when currentClimbHeight >= MaxClimbHeightForAutomaticClimb:
            case ClimbType.DoublePhaseClimb:
                ClimbIdle();
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(climbType), climbType, null);
        }

        animator.SetFloat(Hash_ClimbHeight, currentClimbHeight);
    }

    private void ClimbIdle()
    {
        currentClimbBlendTarget = currentClimbable.GetCharacterIdealPosition(currentClimbInteractionSide, ClimbState.ClimbIdle);
        currentClimbHeight = 1;
        // ChangeLayerToClimbMode();
        animator.SetBool(Hash_ClimbIdle, true);
    }

    private void ClimbUp()
    {
        currentClimbState = ClimbState.ClimbingUp;
        SetClimbingUpOffsetInitial();
        animator.SetBool(Hash_ClimbIdle, true);
        animator.SetTrigger(Hash_Climb);
    }

    private Vector3 climbingUpStartCharacterPosition;

    private void SetClimbingUpOffsetInitial()
    {
        climbingUpStartCharacterPosition = transform.position;
        currentClimbBlendTarget = currentClimbable.GetCharacterIdealPosition(currentClimbInteractionSide, ClimbState.ClimbingUp);
        climbAppliedOffset = Vector3.zero;
        SetClimbingUpOffset();
    }

    private void SetClimbingUpOffset()
    {
        Vector3 position = currentClimbBlendTarget.transform.position;
        // climbInitialOffset = new Vector3(position.x - climbingUpStartCharacterPosition.x, 0, position.z - climbingUpStartCharacterPosition.z);
        climbInitialOffset = position - climbingUpStartCharacterPosition;
    }

    private void HandleVerticalSpeed()
    {
        // If 'Five' is not on the ground then send the vertical speed to the animator.
        // This is so the vertical speed is kept when landing so the correct landing animation is played.
        if (!isGrounded)
            animator.SetFloat(Hash_AirborneVerticalSpeed, verticalSpeed);
    }

    private void CacheAnimatorState()
    {
        previousCurrentStateInfo = currentStateInfo;
        previousNextStateInfo = nextStateInfo;
        previousIsAnimatorTransitioning = isAnimatorTransitioning;

        currentStateInfo = animator.GetCurrentAnimatorStateInfo(0);
        nextStateInfo = animator.GetNextAnimatorStateInfo(0);
        isAnimatorTransitioning = animator.IsInTransition(0);
    }

    private void UpdateInputBlocking()
    {
        // Input while states with block tag, climb Idle and climbing ARE BLOCKED
        characterInputBlocked = currentStateInfo.tagHash == Hash_BlockInputTag && !isAnimatorTransitioning;
        characterInputBlocked |= nextStateInfo.tagHash == Hash_BlockInputTag;
        characterInputBlocked |= currentStateInfo.tagHash == Hash_ClimbTag || nextStateInfo.tagHash == Hash_ClimbTag;
        characterInputBlocked |= currentStateInfo.tagHash == Hash_ClimbIdleTag || nextStateInfo.tagHash == Hash_ClimbIdleTag;
        characterInputBlocked |= currentStateInfo.tagHash == Hash_ClimbTag || nextStateInfo.tagHash == Hash_ClimbTag;
        characterInputBlocked |= currentStateInfo.tagHash == Hash_BeginRespawnTag || nextStateInfo.tagHash == Hash_BeginRespawnTag;
        characterInputBlocked |= IsOnClimb();
    }

    private void UpdateJumpBlocking()
    {
        // Jump In HardLanding ( not its blending out ) ARE BLOCKED
        characterJumpBlocked = (currentStateInfo.tagHash == Hash_LandingHardTag && !isAnimatorTransitioning) || nextStateInfo.tagHash == Hash_LandingHardTag;
        characterJumpBlocked |= isUnderCeiling;
    }

    private void UpdateTurnBlocking()
    {
        // Turn in turn && turn while jumping && turn while landing && turn while push or pull ARE BLOCKED
        // Turn in landing ( except the hard one ) && Turn in transition of airborne to landing is now ALLOWED
        characterTurnBlocked = currentStateInfo.tagHash == Hash_TurnTag || nextStateInfo.tagHash == Hash_TurnTag;
        // characterTurnBlocked |= currentStateInfo.tagHash == Hash_LandingTag || nextStateInfo.tagHash == Hash_LandingTag;
        characterTurnBlocked |= currentStateInfo.tagHash == Hash_LandingHardTag || nextStateInfo.tagHash == Hash_LandingHardTag;
        characterTurnBlocked |= ((currentStateInfo.tagHash == Hash_AirborneTag && !isAnimatorTransitioning) || nextStateInfo.tagHash == Hash_AirborneTag);
        characterTurnBlocked |= currentStateInfo.tagHash == Hash_PushAndPullTag || nextStateInfo.tagHash == Hash_PushAndPullTag;
        characterTurnBlocked |= currentStateInfo.tagHash == Hash_BlockInputTag || nextStateInfo.tagHash == Hash_BlockInputTag;
        characterTurnBlocked |= currentStateInfo.tagHash == Hash_BlockInputTag || nextStateInfo.tagHash == Hash_BlockInputTag;
        characterTurnBlocked |= currentStateInfo.tagHash == Hash_ClimbIdleTag || nextStateInfo.tagHash == Hash_ClimbIdleTag;
        characterTurnBlocked |= currentStateInfo.tagHash == Hash_ClimbTag || nextStateInfo.tagHash == Hash_ClimbTag;
        characterTurnBlocked |= currentStateInfo.tagHash == Hash_Respawn || nextStateInfo.tagHash == Hash_Respawn;
        characterTurnBlocked |= currentStateInfo.tagHash == Hash_BeginRespawnTag || nextStateInfo.tagHash == Hash_BeginRespawnTag;
        characterTurnBlocked |= IsOnClimb();
    }

    private void UpdateIKBlockingAndParenting()
    {
        characterIKBlocked = ((currentStateInfo.tagHash != Hash_MovementTag || isAnimatorTransitioning) && nextStateInfo.tagHash != Hash_MovementTag);
        characterIKBlocked &= (currentStateInfo.tagHash != Hash_PushAndPullTag || isAnimatorTransitioning);
        characterIKBlocked = (direction != CharacterDirection.Left && direction != CharacterDirection.Right);

        characterIntractableIkBlock = onPushAndPull;

        characterIKBlockedBecauseOfCrouch = characterIKBlocked || crouchAmount > 0.2f;
        characterIKFootBlocked = currentStateInfo.tagHash != Hash_MovementTag || isAnimatorTransitioning || Math.Abs(forwardSpeed) > 0.001f; // currently foot ik is only allowed in idle

        if (characterIK.GetIkClassesCount() > 0)
        {
            if (direction == CharacterDirection.Right || direction == CharacterDirection.Left)
                characterIK.AttachIkDynamicParent();
            else
                characterIK.DetachIkDynamicParent();
        }
    }

    private void CalculateForwardMovement()
    {
        Vector2 moveInput = GetMovementInput(); // Cache the move input and cap it's magnitude at 1.
        if (moveInput.sqrMagnitude > 1f)
            moveInput.Normalize();

        if (AreInputAndCharacterAtSameDirection()) // Calculate the speed intended by input.
            desiredForwardSpeed = moveInput.magnitude * (IsRunInput() ? maxForwardSpeedRun : maxForwardSpeedWalk);
        else
            desiredForwardSpeed = 0;

        float acceleration = IsMovementInput() ? GroundAcceleration : GroundDeceleration; // Determine change to speed based on whether there is currently any move input.
        forwardSpeed = Mathf.MoveTowards(forwardSpeed, desiredForwardSpeed, acceleration * Time.fixedDeltaTime); // Adjust the forward speed towards the desired speed.
        animator.SetFloat(Hash_ForwardSpeed, forwardSpeed); // Set the animator parameter to control what animation is being played.
    }

    private void CalculateVerticalMovement()
    {
        if (IsOnClimb() && currentClimbState != ClimbState.ClimbingEnd)
        {
            verticalSpeed = 0;
            return;
        }

        if (!IsJumpInput() && isGrounded) // If jump is not currently held and Character is on the ground then she is ready to jump.
            readyToJump = true;

        if (isGrounded)
        {
            verticalSpeed = -gravity * StickingGravityProportion; // When grounded we apply a slight negative vertical speed to make Character "stick" to the ground.

            if (IsJumpInput() && readyToJump) // If jump is held, Character is ready to jump and not currently in the middle of a melee combo...
            {
                // ... then override the previously set vertical speed and make sure she cannot jump again.
                verticalSpeed = jumpSpeed;
                isGrounded = false;
                readyToJump = false;
                HandleTurnRotationInOneFrame();
            }
        }
        else
        {
            if (!IsJumpInput() && verticalSpeed > 0.0f) // If Character is airborne, the jump button is not held and Character is currently moving upwards...
                verticalSpeed -= JumpAbortSpeed * Time.fixedDeltaTime; // ... decrease Character's vertical speed. This is what causes holding jump to jump higher that tapping jump.

            if (Mathf.Approximately(verticalSpeed, 0f)) // If a jump is approximately peaking, make it absolute.
                verticalSpeed = 0f;

            verticalSpeed -= gravity * Time.fixedDeltaTime; // If Character is airborne, apply gravity.
        }
    }

    private void CalculateCrouchMovement()
    {
        isCrouch = GetCrouchInput() >= 0.5f;
        if (isCrouch)
            crouchRayCastCheckIsNeeded = true;
        float goal = crouchCeilingCheckMode != CrouchCeilingCheckModes.NoCeilingCheck &&
                     (!isCrouch || crouchCeilingCheckMode == CrouchCeilingCheckModes.FullCheck) &&
                     crouchRayCastCheckIsNeeded && IsUnderCeiling()
            ? 1
            : GetCrouchInput();
        if (Mathf.Approximately(crouchAmount, goal))
            return;

        crouchAmount = Mathf.MoveTowards(crouchAmount, goal, CrouchSpeed * Time.fixedDeltaTime);
        animator.SetFloat(Hash_CrouchAmount, crouchAmount); // Set the animator parameter to control what animation is being played.
    }

    private bool IsUnderCeiling()
    {
        Vector3 offset = new Vector3(0, 0, characterController.radius);
        Vector3 position = transform.position;
        DrawDebugRay(position + offset, direction: 1.03f * originalCharacterHeight * Vector3.up, Color.red);
        DrawDebugRay(position - offset, 1.03f * originalCharacterHeight * Vector3.up, Color.red);
        if (Physics.Raycast(transform.position + offset, Vector3.up, originalCharacterHeight * 1.03f, layerMaskCrouchCeilingCheck, QueryTriggerInteraction.Ignore)
            || Physics.Raycast(transform.position - offset, Vector3.up, originalCharacterHeight * 1.03f, layerMaskCrouchCeilingCheck, QueryTriggerInteraction.Ignore))
        {
            isUnderCeiling = true;
            return true;
        }

        isUnderCeiling = false;
        crouchRayCastCheckIsNeeded = false;
        return false;
    }

    private void CalculateColliderSize()
    {
        if (Math.Abs(lastCrouchAmount - crouchAmount) < 0.01f)
            return;
        lastCrouchAmount = crouchAmount;
        float value = Mathf.Lerp(originalCharacterHeight, CrouchColliderSizePercent * originalCharacterHeight, crouchAmount);
        characterController.height = value;

        Vector3 center = characterController.center;
        center = new Vector3(center.x, value / 2, center.z);
        characterController.center = center;
    }

    private void CheckForPushAndPullAddForce()
    {
        if (!onPushAndPull || currentStateInfo.tagHash != Hash_PushAndPullTag)
            return;
        currentMovable.AddForce(new Vector3(0, 0, characterPowerInPushAndPull * GetMovementInput().x));
    }


    private void HandleAlertMode()
    {
        float goal = GetAlertModeInput() ? 1 : 0;
        alertModeValue = !Mathf.Approximately(alertModeValue, goal) ? Mathf.MoveTowards(alertModeValue, goal, Time.fixedDeltaTime * 10) : goal;
        animator.SetFloat(Hash_AlertMode, alertModeValue);
    }

    private void SetStateTimeInAnimator()
    {
        currentStateTime = Mathf.Repeat(animator.GetCurrentAnimatorStateInfo(0).normalizedTime, 1f);
        animator.SetFloat(Hash_StateTime, currentStateTime);
    }


    private void TimeoutToIdle()
    {
        bool inputDetected = IsMovementInput() || IsJumpInput();
        if (isGrounded && !inputDetected)
        {
            idleTimer += Time.fixedDeltaTime;
            if (idleTimer >= idleTimeout)
            {
                idleTimer = 0f;
                animator.SetTrigger(Hash_TimeoutToIdle);
            }
        }
        else
        {
            idleTimer = 0f;
            animator.ResetTrigger(Hash_TimeoutToIdle);
        }

        animator.SetBool(Hash_InputDetected, inputDetected);
    }

    private void CheckForClimbIfAirborne()
    {
        if (!isGrounded)
            CheckForClimb();
    }

    private void CheckClimbPhaseChange()
    {
        switch (currentClimbState)
        {
            case ClimbState.ClimbIdle:
                break;
            case ClimbState.ClimbingUp:
                if (currentStateInfo.tagHash == Hash_ClimbTag && currentStateTime > ClimbIkEndAnimPercent)
                    ClimbIkEnd();
                if (readyClimbState == ClimbState.ClimbingEnd && currentStateInfo.tagHash == Hash_ClimbTag && isAnimatorTransitioning)
                    ClimbGoingForEnd();
                break;

            case ClimbState.ClimbingEnd when readyClimbState == ClimbState.NotOnClimb && currentStateInfo.tagHash != Hash_ClimbTag && currentStateInfo.tagHash != Hash_ClimbIdleTag:
                ClimbEnd();
                break;
            case ClimbState.NotOnClimb:
                return;
            default:
                return;
        }
    }

    private void CheckForRespawn()
    {
        if (currentStateInfo.tagHash == Hash_BeginRespawnTag)
            Reset();
    }

    private void ClimbGoingForEnd() // the last Positional blend
    {
        currentClimbState = ClimbState.ClimbingEnd;
        currentClimbBlendTarget = currentClimbable.GetCharacterIdealPosition(currentClimbInteractionSide, ClimbState.ClimbingEnd);
        animator.SetBool(Hash_ClimbIdle, false);
    }

    private void ClimbEnd()
    {
        movementX = 0;
        readyClimbState = ClimbState.NotOnClimb;
        currentClimbState = ClimbState.NotOnClimb;
        if (climbCancelComebackExist) // if the climb end is called from climb releasing we do not Recover ColliderSize Immediately, because doing so will cause the climbable physics make character collider Pops back suddenly, so instead we let it be finished when "ClimbComebackExist" gets to false
            previousClimbable = currentClimbable;
        else
        {
            SetColliderRadius(originalCharacterRadius);
            currentClimbable.ClimbEnd();
            ChangeLayerToOriginal();
        }

        currentClimbable = null;
    }

    private void ClimbIkEnd()
    {
        if (climbIkEnd)
            return;
        climbIkEnd = true;
        climbIkClass.points = currentClimbable.GetClimbIkGameObjects(currentClimbInteractionSide);
        characterIK.RemoveIkClass(climbIkClass);
    }

    private CharacterDirection ComputeDirection(float rotationY)
    {
        if (Mathf.Approximately(rotationY, 0))
            return CharacterDirection.Right;
        if (Mathf.Approximately(rotationY, 180))
            return CharacterDirection.Left;
        return lastNotZeroMovementInput.x > 0 ? CharacterDirection.GoingToRight : CharacterDirection.GoingToLeft;
    }

    private bool IsInteractionFromSide(InteractionSide interactionSide)
    {
        return interactionSide == InteractionSide.Front || interactionSide == InteractionSide.Back;
    }

    private bool IsOnClimb()
    {
        return currentClimbState != ClimbState.NotOnClimb;
    }

    private bool IsOnRightRoughly()
    {
        return direction == CharacterDirection.Right || direction == CharacterDirection.GoingToRight;
    }

    private bool IsJumpInput()
    {
        return characterActions.GetActionValue<bool>(ActionType.Jump) && !characterJumpBlocked && !characterInputBlocked;
    }

    private bool IsMovementInput()
    {
        return !Mathf.Approximately(GetMovementInput().sqrMagnitude, 0f);
    }

    private Vector2 lastNotZeroMovementInput;

    private Vector2 GetMovementInput()
    {
        Vector3 result = characterInputBlocked ? Vector2.zero : new Vector2(characterActions.GetActionValue<float>(ActionType.MovementX), 0);
        if (!Mathf.Approximately(result.x, 0))
            lastNotZeroMovementInput = result;
        return result;
    }

    private bool IsRunInput()
    {
        return characterActions.GetActionValue<bool>(ActionType.Run) && !characterInputBlocked;
    }

    private float GetCrouchInput()
    {
        return Mathf.Clamp(characterActions.GetActionValue<float>(ActionType.Crouch), -1, 0) * -1;
    }

    private bool GetPushAndPullInput()
    {
        return characterActions.GetActionValue<bool>(ActionType.PushAndPull);
    }

    private bool GetAlertModeInput()
    {
        return characterActions.GetActionValue<bool>(ActionType.AlertMode);
    }

    private bool GetClimbInput()
    {
        return characterActions.GetActionValue<bool>(ActionType.Climb);
    }

    private bool GetClimbCancelInput()
    {
        return characterActions.GetActionValue<bool>(ActionType.ClimbCancel);
    }

    private void ChangeLayerToClimbMode()
    {
        if (layerChanged)
            return;
        layerChanged = true;
        gameObject.layer = layerForWhileInClimb;
    }

    private void ChangeLayerToOriginal()
    {
        layerChanged = false;
        gameObject.layer = originalLayer;
    }

    private void SetColliderRadius(float goal)
    {
        LeanTween.cancel(climbColliderRadiusTweenId);
        climbColliderRadiusTweenId = LeanTween.value(gameObject, characterController.radius, goal, ClimbColliderSizeChangeDuration).setOnUpdate(f => characterController.radius = f).id;
    }

    private static Vector3 MoveTowardsVector3(Vector3 from, Vector3 to, float time)
    {
        float x = Mathf.MoveTowards(from.x, to.x, time);
        float y = Mathf.MoveTowards(from.y, to.y, time);
        float z = Mathf.MoveTowards(from.z, to.z, time);
        return new Vector3(x, y, z);
    }

    private static void DrawDebugRay(Vector3 origin, Vector3 direction)
    {
        DrawDebugRay(origin, direction, Color.yellow);
    }

    private static void DrawDebugRay(Vector3 origin, Vector3 direction, Color color)
    {
        #if UNITY_EDITOR
        Debug.DrawRay(origin, direction, color, 0.3f);
        #endif
    }
}
