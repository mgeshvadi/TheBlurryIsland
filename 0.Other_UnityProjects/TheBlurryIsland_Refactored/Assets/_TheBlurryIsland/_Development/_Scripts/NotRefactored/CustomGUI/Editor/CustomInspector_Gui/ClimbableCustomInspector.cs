﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Climbable))]
public class ClimbableCustomInspector : Editor
{
    private Climbable targetScript;

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        serializedObject.Update();
        Climbable climbable = (Climbable) target;
        if (GUILayout.Button("Apply"))
            climbable.Init();
        serializedObject.ApplyModifiedProperties();
    }
}
