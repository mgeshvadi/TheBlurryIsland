﻿#if UNITY_EDITOR

using UnityEditor;
using UnityEngine;

[InitializeOnLoad]
public class WelcomeWindowFirstTimeInitializer : ScriptableObject
{
    private static WelcomeWindowFirstTimeInitializer instance;

    static WelcomeWindowFirstTimeInitializer()
    {
        instance = null;
        EditorApplication.update += OnInit;
    }
    static void OnInit()
    {
        // EditorApplication.update -= OnInit;
        // instance = FindObjectOfType<WelcomeWindowFirstTimeInitializer>();
        // if (instance == null)
        // {
        //     instance = CreateInstance<WelcomeWindowFirstTimeInitializer>();
        //     if(!EditorApplication.isPlaying)
        //         EditorWindow.GetWindow<WelcomeWindow>("Welcome");
        // }
    }


public class WelcomeWindow : EditorWindow
{
    [MenuItem("Window/Welcome")]
    public static void ShowWindow()
    {
        GetWindow<WelcomeWindow>("Welcome");
    }

    private void OnGUI()
    {
        GUIStyle style = new GUIStyle();
        GUIStyle styleBack = new GUIStyle();
        styleBack.normal.background = Resources.Load<Texture2D>("WelcomeWindow/Welcome1");
        style.normal.textColor=Color.white;
        style.fontSize=23;

        GUILayout.BeginArea(new Rect(0, 0, Screen.width, Screen.height),styleBack);
            GUILayout.FlexibleSpace();

            GUILayout.BeginVertical("BOX");
                GUILayout.BeginHorizontal();
                    GUILayout.FlexibleSpace();
                    GUILayout.Label("Welcome To ",style);
                    style.normal.textColor=new Color(0.86f, 0.2f, 0.11f);
                    GUILayout.Label("BlurryIsland!",style);
                    GUILayout.FlexibleSpace();
                GUILayout.EndHorizontal();

                GUILayout.Space(50);

                GUILayout.BeginHorizontal();
                    GUILayout.FlexibleSpace();
                    style.fontSize=14;
                    style.normal.textColor=Color.white;
                    GUILayout.Label("This Project is Under Construction By ",style);
                    GUILayout.FlexibleSpace();
                GUILayout.EndHorizontal();

                GUILayout.Space(10);
                GUILayout.BeginHorizontal();
                    GUILayout.FlexibleSpace();
                    style.fontSize=17;
                    style.normal.textColor=new Color(0.17f, 0.18f, 0.81f);
                    GUILayout.Label(" Mahsa Geshvadi & Jamasb Samia",style);
                    GUILayout.FlexibleSpace();
                GUILayout.EndHorizontal();

                GUILayout.Space(30);

                GUILayout.BeginHorizontal();
                    GUILayout.FlexibleSpace();
                    style.fontSize=10;
                    style.normal.textColor=Color.black;
                    GUILayout.Label("@All Rights Are Reserved, 2020 ",style);
                GUILayout.FlexibleSpace();
                GUILayout.EndHorizontal();

            GUILayout.EndVertical();
            GUILayout.FlexibleSpace();
        GUILayout.EndArea();


    }
}

}
#endif
