﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.Experimental.SceneManagement;
using UnityEditor.SceneManagement;
using UnityEngine;


public static class EditorListIKTriggers
{
    public static void DrawList(IKTriggers targetScript, SerializedProperty list, bool showListSize, bool showListTitle, string listTitle, string sizeLabel)
    {
        List<string> ikTypeItems = new List<string>();
        int selectedIndex = 0;

        GUIStyle style = new GUIStyle(GUI.skin.button);
        GUIStyle titleStyle = new GUIStyle();
        titleStyle.fontStyle = FontStyle.Bold;
        List<IKClass> ikClasses = new List<IKClass>();
        if (targetScript.iks != null && targetScript.iks.Count != 0)
            ikClasses = new List<IKClass>(targetScript.iks);

        EditorGUILayout.Space();

        // Showing list title
        if (showListTitle)
        {
            EditorGUILayout.LabelField(listTitle);
            EditorGUILayout.Space();
        }

        // Showing list size
        if (showListSize && list.isExpanded)
        {
            EditorGUILayout.PropertyField(list.FindPropertyRelative("Array.size"), new GUIContent(sizeLabel));
            EditorGUILayout.Space();
        }

        EditorGUILayout.Space();

        // Drawing List elements ( actions )
        for (int i = 0; i < list.arraySize; i++)
        {
            SerializedProperty item = list.GetArrayElementAtIndex(i);

            EditorGUILayout.LabelField("IK #" + (i + 1));
            EditorGUI.indentLevel += 1;

            EditorGUIUtility.labelWidth = 190;
            EditorGUIUtility.fieldWidth = 150;
            GUILayout.BeginHorizontal("BOX");


            GUILayout.BeginVertical();


            ikTypeItems = Enum.GetNames(typeof(IkType)).ToList();
            foreach (IKClass m in ikClasses.Where(m => ikClasses[i].GetIkType() != m.GetIkType()))
                ikTypeItems.Remove(m.GetIkType().ToString());
            selectedIndex = ikTypeItems.FindIndex(s => ikClasses[i].GetIkType().ToString() == s);
            int temp = EditorGUILayout.Popup("IK Type", selectedIndex, ToCamelCase(ikTypeItems.ToArray()), GUILayout.ExpandWidth(false));
            if (selectedIndex != temp)
                SetDirty(targetScript);
            selectedIndex = temp;
            ikClasses[i].SetIkType((IkType) Enum.Parse(typeof(IkType), ikTypeItems[selectedIndex]));

            if (ikClasses[i].GetIkType() == IkType.PointOfInterest || ikClasses[i].GetIkType() == IkType.Intractable)
                EditorGUILayout.PropertyField(item.FindPropertyRelative(nameof(IKClass.points)), GUILayout.ExpandWidth(false));

            EditorGUILayout.Space();
            EditorGUILayout.Space();
            EditorGUILayout.PropertyField(item.FindPropertyRelative(nameof(IKClass.overrideIKSettings)), GUILayout.ExpandWidth(false));
            EditorGUILayout.Space();
            if (ikClasses[i].overrideIKSettings)
            {
                if (ikClasses[i].GetIkType() == IkType.Intractable)
                {
                    EditorGUILayout.PropertyField(item.FindPropertyRelative(nameof(IKClass.avatarIKGoalsWhenOnRight)), GUILayout.ExpandWidth(false));
                    EditorGUILayout.PropertyField(item.FindPropertyRelative(nameof(IKClass.avatarIKGoalsWhenOnLeft)), GUILayout.ExpandWidth(false));
                    EditorGUILayout.Space();
                }
                else if (ikClasses[i].GetIkType() != IkType.PointOfInterest)
                    EditorGUILayout.PropertyField(item.FindPropertyRelative(nameof(IKClass.avatarIKGoals)), GUILayout.ExpandWidth(false));

                EditorGUILayout.Space();

                EditorGUILayout.PropertyField(item.FindPropertyRelative(nameof(IKClass.ikWeight)));
                EditorGUILayout.PropertyField(item.FindPropertyRelative(nameof(IKClass.ikWeightChangeSpeed)));

                EditorGUILayout.Space();
                EditorGUILayout.Space();

                EditorGUILayout.PropertyField(item.FindPropertyRelative(nameof(IKClass.blockInCrouch)));
                EditorGUILayout.PropertyField(item.FindPropertyRelative(nameof(IKClass.noIkBehindOfCharacter)));
                if (ikClasses[i].noIkBehindOfCharacter)
                    EditorGUILayout.PropertyField(item.FindPropertyRelative(nameof(IKClass.zOffsetForCheckingBehind)));
            }

            GUILayout.EndVertical();

            style.normal.textColor = Color.red;
            if (GUILayout.Button("Remove", style, GUILayout.ExpandWidth(false)))
            {
                targetScript.iks?.Remove(ikClasses[i]);
                SetDirty(targetScript);
            }

            GUILayout.EndHorizontal();

            EditorGUILayout.Space();
            EditorGUI.indentLevel -= 1;
        }


        if (ikTypeItems.Count > 1 || ikClasses.Count == 0)
        {
            EditorGUILayout.Space(10);
            style.normal.textColor = Color.blue;

            if (GUILayout.Button("Add IK", style))
            {
                IKClass newModule = new IKClass();
                for (int i = 0; i < ikTypeItems.Count; i++)
                {
                    if (i == selectedIndex)
                        continue;
                    newModule.SetIkType((IkType) Enum.Parse(typeof(IkType), ikTypeItems[i]));
                    break;
                }
                targetScript.iks?.Add(newModule);
                SetDirty(targetScript);
            }
        }
        EditorGUILayout.Space(30);
    }

    private static void SetDirty(Component targetScript)
    {
        EditorUtility.SetDirty(targetScript);
        EditorUtility.SetDirty(targetScript.gameObject);
        var prefabStage = PrefabStageUtility.GetCurrentPrefabStage();
        if (prefabStage != null)
            EditorSceneManager.MarkSceneDirty(prefabStage.scene);
    }

    private static string[] ToCamelCase(string[] texts)
    {
        return texts.Select(ToCamelCase).ToArray();
    }

    private static string ToCamelCase(string text)
    {
        text = text.Substring(0, 1).ToUpper() + text.Substring(1);
        return System.Text.RegularExpressions.Regex.Replace(text, "(\\B[A-Z])", " $1");
    }

}

[CustomEditor(typeof(IKTriggers))]
public class IKTriggerCustomInspector : Editor
{
    private IKTriggers targetScript;

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        IKTriggers ikTriggers = (IKTriggers) target;
        EditorListIKTriggers.DrawList(ikTriggers, serializedObject.FindProperty(nameof(IKTriggers.iks)), false, false, "Iks", "Number Of Iks");
        serializedObject.ApplyModifiedProperties();
    }
}
