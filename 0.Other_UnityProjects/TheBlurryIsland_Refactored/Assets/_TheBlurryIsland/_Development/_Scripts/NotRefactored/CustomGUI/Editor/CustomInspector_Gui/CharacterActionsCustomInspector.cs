﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.Experimental.SceneManagement;
using UnityEditor.SceneManagement;
using UnityEngine;


public static class EditorListCharacterActions
{
    public static void DrawList(CharacterActions targetScript, SerializedProperty list, bool showListSize, bool showListTitle, string listTitle, string sizeLabel)
    {
        List<string> actionTypesItems = new List<string>();
        int selectedIndex = 0;
        List<ActionClass> actions = new List<ActionClass>(targetScript.actions);
        GUIStyle style = new GUIStyle(GUI.skin.button);

        EditorGUILayout.Space();

        // Showing list title
        if (showListTitle)
        {
            EditorGUILayout.LabelField(listTitle);
            EditorGUILayout.Space();
        }

        // Showing list size
        if (showListSize && list.isExpanded)
        {
            EditorGUILayout.PropertyField(list.FindPropertyRelative("Array.size"), new GUIContent(sizeLabel));
            EditorGUILayout.Space();
        }

        EditorGUILayout.Space();

        // Drawing List elements ( actions )
        for (int i = 0; i < list.arraySize; i++)
        {
            SerializedProperty item = list.GetArrayElementAtIndex(i);

            EditorGUILayout.LabelField("Action #" + (i + 1));
            EditorGUI.indentLevel += 1;

            EditorGUIUtility.labelWidth = 150;
            EditorGUIUtility.fieldWidth = 120;
            GUILayout.BeginHorizontal("BOX");

            GUILayout.BeginVertical();

            actionTypesItems = Enum.GetNames(typeof(ActionType)).ToList();
            foreach (ActionClass m in actions.Where(m => actions[i].actionType != m.actionType))
                actionTypesItems.Remove(m.actionType.ToString());
            selectedIndex = actionTypesItems.FindIndex(s => actions[i].actionType.ToString() == s);
            int temp = EditorGUILayout.Popup("Action Type", selectedIndex, actionTypesItems.ToArray(), GUILayout.ExpandWidth(false));
            if (selectedIndex != temp)
                SetDirty(targetScript);
            selectedIndex = temp;
            actions[i].actionType = (ActionType) Enum.Parse(typeof(ActionType), actionTypesItems[selectedIndex]);

            EditorGUILayout.PropertyField(item.FindPropertyRelative(nameof(ActionClass.actionControlMode)), GUILayout.ExpandWidth(false));
            EditorGUIUtility.labelWidth = 150;
            EditorGUIUtility.fieldWidth = 100;

            if (actions[i].actionControlMode == ActionControlMode.ConstantValue)
            {
                switch (ActionsInputTypeHolder.instance.actionsInputTypeDic[actions[i].actionType])
                {
                    case InputValueVariableType.BooleanToggle:
                    case InputValueVariableType.BooleanContinues:
                        bool tempBool = EditorGUILayout.Toggle(ToCamelCase(nameof(ActionClass.defaultValue)), actions[i].defaultValue.GetValue<bool>(), GUILayout.ExpandWidth(false));
                        if (tempBool != actions[i].defaultValue.GetValue<bool>())
                            SetDirty(targetScript);
                        actions[i].defaultValue.SetActionValue(tempBool);
                        break;
                    case InputValueVariableType.Vector1:
                        float tempFloat = EditorGUILayout.FloatField(ToCamelCase(nameof(ActionClass.defaultValue)), actions[i].defaultValue.GetValue<float>(), GUILayout.ExpandWidth(false));
                        if (Math.Abs(tempFloat - actions[i].defaultValue.GetValue<float>()) > 0.0001f)
                            SetDirty(targetScript);
                        actions[i].defaultValue.SetActionValue(tempFloat);
                        break;
                    case InputValueVariableType.Vector2:
                        Vector2 tempVector = EditorGUILayout.Vector2Field(ToCamelCase(nameof(ActionClass.defaultValue)), actions[i].defaultValue.GetValue<Vector2>(), GUILayout.ExpandWidth(false));
                        if (tempVector != actions[i].defaultValue.GetValue<Vector2>())
                            SetDirty(targetScript);
                        actions[i].defaultValue.SetActionValue(tempVector);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
            GUILayout.EndVertical();

            style.normal.textColor = Color.red;
            if (GUILayout.Button("Remove", style, GUILayout.ExpandWidth(false)))
            {
                targetScript.actions.Remove(actions[i]);
                SetDirty(targetScript);
            }

            GUILayout.EndHorizontal();

            EditorGUILayout.Space();
            EditorGUI.indentLevel -= 1;
        }


        if (actionTypesItems.Count > 1 || actions.Count == 0)
        {
            EditorGUILayout.Space(10);
            style.normal.textColor = Color.blue;

            if (GUILayout.Button("Add Action", style))
            {
                ActionClass actionClass = new ActionClass();
                for (int i = 0; i < actionTypesItems.Count; i++)
                {
                    if (i == selectedIndex)
                        continue;
                    actionClass.actionType = (ActionType) Enum.Parse(typeof(ActionType), actionTypesItems[i]);
                    break;
                }
                targetScript.actions.Add(actionClass);
                SetDirty(targetScript);
            }
        }

    }

    private static string ToCamelCase(string text)
    {
        text = text.Substring(0, 1).ToUpper() + text.Substring(1);
        return System.Text.RegularExpressions.Regex.Replace(text, "(\\B[A-Z])", " $1");
    }

    private static void SetDirty(Component targetScript)
    {
        EditorUtility.SetDirty(targetScript);
        EditorUtility.SetDirty(targetScript.gameObject);
        var prefabStage = PrefabStageUtility.GetCurrentPrefabStage();
        if (prefabStage != null)
            EditorSceneManager.MarkSceneDirty(prefabStage.scene);
    }
}

[CustomEditor(typeof(CharacterActions))]
public class CharacterActionsCustomInspector : Editor
{
    private CharacterActions targetScript;

    public override void OnInspectorGUI()
    {
        if (ActionsInputTypeHolder.instance == null)
            FindObjectOfType<ActionsInputTypeHolder>().GetComponent<ActionsInputTypeHolder>().Init();
        serializedObject.Update();
        EditorListCharacterActions.DrawList((CharacterActions) target, serializedObject.FindProperty(nameof(CharacterActions.actions)), false, false, "Actions", "Number Of Actions");
        serializedObject.ApplyModifiedProperties();
    }
}
