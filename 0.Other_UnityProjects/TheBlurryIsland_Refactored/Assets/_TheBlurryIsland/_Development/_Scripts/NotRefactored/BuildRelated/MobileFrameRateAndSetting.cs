﻿using System;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;

public class MobileFrameRateAndSetting : MonoBehaviour
{
    // TODO : add #if Android || IOS
    public static MobileFrameRateAndSetting instance;


    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(gameObject);
            return;
        }

        instance = this;

        InitFrameRate();
        InitOther();
    }

    private static void InitFrameRate()
    {
        Application.targetFrameRate = 1000;
        OnDemandRendering.renderFrameInterval = 2;
    }

    private static void InitOther()
    {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
    }
}
