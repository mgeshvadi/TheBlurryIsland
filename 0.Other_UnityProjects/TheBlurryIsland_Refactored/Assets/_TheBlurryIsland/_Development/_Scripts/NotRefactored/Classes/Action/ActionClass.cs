﻿using System;
using UnityEngine;
using UnityEngine.InputSystem;


[Serializable]
public class ActionClass
{
    public ActionType actionType;
    public ActionControlMode actionControlMode;
    public ActionValue defaultValue;

    [HideInInspector] public ActionValue actionValue;
    [HideInInspector] public ActionControlMode previousActionControlMode;
    [HideInInspector] public Action<InputAction.CallbackContext> actionHandlerPerform;
    [HideInInspector] public Action<InputAction.CallbackContext> actionHandlerCancel;

    public TValue GetActionValue<TValue>() where TValue : struct
    {
        return actionValue.GetValue<TValue>();
    }

    public void SetActionValue<TValue>(TValue tValue) where TValue : struct
    {
        actionValue.SetActionValue(tValue);
    }

    public void SetActionValue(ActionValue value)
    {
        actionValue = new ActionValue(value.GetValue<bool>(), value.GetValue<float>(), value.GetValue<Vector2>());
    }

    public void TriggerEntered(ActionValue value)
    {
        SetActionValue(value);
    }

    public void TriggerExit(ActionValue value)
    {
        SetActionValue(value);
    }

}
