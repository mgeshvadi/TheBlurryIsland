﻿using System;
using System.Collections.Generic;
using System.Linq;

[Serializable]
public class ClimbPoint
{
    public ClimbState climbState;
    public List<ObjectSidesPosition> objectSidesPositions;

    public ClimbPoint(ClimbState climbState,ObjectSidesPosition[] objectSidesPositions)
    {
        this.climbState = climbState;
        this.objectSidesPositions = objectSidesPositions.ToList();
    }

    public ClimbPoint(ObjectSidesPosition[] objectSidesPositions)
    {
        this.objectSidesPositions = objectSidesPositions.ToList();
    }
}
