﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class DeathFall : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Character"))
            other.GetComponent<MainCharacterController>().DieFunc();
    }
}
