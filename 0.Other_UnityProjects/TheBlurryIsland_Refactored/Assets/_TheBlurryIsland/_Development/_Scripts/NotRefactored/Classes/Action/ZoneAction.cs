﻿using System;
using UnityEngine;


[Serializable]
public class ZoneAction
{
    [Tooltip("This Means even if the control mode of this action is set to 'userInput' this trigger will changed it to 'triggerBased' as long as the character is in this zone trigger")]
    public bool overrideActionControlType;
    public ActionType actionType;


    public ActionValue zoneEnterValue;
    public ActionValue zoneExitValue;

    public ActionValue GetZoneEnteredValue()
    {
        return new ActionValue(zoneEnterValue.GetValue<bool>(), zoneEnterValue.GetValue<float>(), zoneEnterValue.GetValue<Vector2>());
    }

    public ActionValue GetZoneExitValue()
    {
        return new ActionValue(zoneExitValue.GetValue<bool>(), zoneExitValue.GetValue<float>(), zoneExitValue.GetValue<Vector2>());
    }
}
