﻿using System;
using UnityEngine;

[Serializable]
public class ActionValue
{
    public bool booleanValue;
    public  float vector1Value;
    public  Vector2 vector2Value;

    public ActionValue(bool booleanValue,float vector1Value,Vector2 vector2Value)
    {
        this.booleanValue = booleanValue;
        this.vector1Value = vector1Value;
        this.vector2Value = vector2Value;
    }

    public void SetActionValue<TValue>(TValue tValue) where TValue : struct
    {
        if (typeof(TValue) == typeof(bool))
            booleanValue = (bool) (object) tValue;
        else if (typeof(TValue) == typeof(float))
            vector1Value = (float) (object) tValue;
        else if (typeof(TValue) == typeof(Vector2))
            vector2Value = (Vector2) (object) tValue;
    }


    public TValue GetValue<TValue>() where TValue : struct
    {
        if (typeof(TValue) == typeof(bool))
            return (TValue) (object) booleanValue;
        if (typeof(TValue) == typeof(float))
            return (TValue) (object) vector1Value;
        if (typeof(TValue) == typeof(Vector2))
            return (TValue) (object) vector2Value;

        return (TValue) new object();
    }


}
