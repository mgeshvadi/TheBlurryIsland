﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(DroneActions))]
[RequireComponent(typeof(Rigidbody))]
public class DroneController : MonoBehaviour
{
    private const float ForceFactor = 150;
    private const int RotationSpeed = 100;
    private const float MaxRotation = 20;
    private const int MaxSpeedLimitation = 40;

    private DroneActions droneActionComponent;
    private Rigidbody droneRigidBody;

    private void Awake()
    {
        Init();
    }

    private void Init()
    {
        droneActionComponent = GetComponent<DroneActions>();
        droneRigidBody = GetComponent<Rigidbody>();

    }

    private void FixedUpdate()
    {
        Vector2 inputForce = droneActionComponent.GetActionValue<Vector2>(ActionType.MovementDrone);
      Debug.Log(inputForce);

      ApplyForce(inputForce);
        Tilt(inputForce);
    }


    private void Tilt(Vector2 inputForce)
    {
        float r = Mathf.MoveTowardsAngle(droneRigidBody.rotation.eulerAngles.x, MaxRotation * inputForce.x, RotationSpeed * Time.fixedDeltaTime);
        droneRigidBody.rotation = Quaternion.Euler(new Vector3(r, 0, 0));

    }

    private void ApplyForce(Vector2 inputForce)
    {
        if (droneRigidBody.velocity.magnitude < MaxSpeedLimitation)
            droneRigidBody.AddForce(new Vector3(0f, inputForce.y, inputForce.x) * ForceFactor);

    }

    private void DroneFall()
    {
        droneRigidBody.useGravity = true;

    }

}
