﻿using UnityEngine;
using Random = UnityEngine.Random;


public class RandomIdle : StateMachineBehaviour
{
    [Header("Action")]
    [SerializeField]private int numberOfStatesStandAction = 3;
    [SerializeField]private int numberOfStatesCrouchAction = 3;
    [Header("Quiet")]
    [SerializeField]private int numberOfStatesStandQuiet = 3;
    [SerializeField]private int numberOfStatesCrouchQuiet = 3;

    private bool randomIntSelected;

    private static readonly int Hash_RandomIdle_Stand_Quiet = Animator.StringToHash("RandomIdle_Stand_Quiet");
    private static readonly int Hash_RandomIdle_Stand_Action = Animator.StringToHash("RandomIdle_Stand_Action");
    private static readonly int Hash_RandomIdle_Crouch_Quiet = Animator.StringToHash("RandomIdle_Crouch_Quiet");
    private static readonly int Hash_RandomIdle_Crouch_Action = Animator.StringToHash("RandomIdle_Crouch_Action");


//    OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        randomIntSelected = false;
    }

//     OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (randomIntSelected || animator.IsInTransition(0))
            return;
        animator.SetFloat(Hash_RandomIdle_Stand_Quiet, Random.Range(0, numberOfStatesStandQuiet));
        animator.SetFloat(Hash_RandomIdle_Stand_Action, Random.Range(0, numberOfStatesStandAction));
        animator.SetFloat(Hash_RandomIdle_Crouch_Quiet, Random.Range(0, numberOfStatesCrouchQuiet));
        animator.SetFloat(Hash_RandomIdle_Crouch_Action, Random.Range(0, numberOfStatesCrouchAction));
        randomIntSelected = true;
    }
}
