﻿using System.Collections.Generic;
using UnityEngine;


public class IKTriggers : MonoBehaviour
{
    public List<IKClass> iks;

    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Character"))
            return;
        other.GetComponent<CharacterIK>().IKTriggerEntered(iks);
    }

    private void OnTriggerExit(Collider other)
    {
        if (!other.CompareTag("Character"))
            return;
        other.GetComponent<CharacterIK>().IKTriggerExit(iks);
    }
}
