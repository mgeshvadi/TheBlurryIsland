﻿public enum InteractionSide
{
    Left,  // z-
    Right, // z+
    Front, // x+
    Back,  // x-
    Top,   // y+
    Bottom // y-
}
