﻿public enum IkType
{
    PointOfInterest,
    Intractable,
    FootOnFloor,
    PushAndPull,
    Climb
}
