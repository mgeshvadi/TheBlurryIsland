﻿
public enum CharacterDirection
{
  Right,
  GoingToRight,
  GoingToLeft,
  Left
}
