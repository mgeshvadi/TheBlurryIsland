﻿
public enum ActionControlMode
{
    UserInput,
    TriggerBased,
    AI,
    ConstantValue
}
