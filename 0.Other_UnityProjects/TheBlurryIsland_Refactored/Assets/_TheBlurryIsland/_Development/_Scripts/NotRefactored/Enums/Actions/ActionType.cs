﻿public enum ActionType
{
    MovementX,
    MovementSwim,
    Run,
    Jump,
    Crouch,
    SwimMode,
    AlertMode,
    Action,
    PushAndPull,
    Climb,
    ClimbCancel,
    DroneMode,
    MovementDrone
}
