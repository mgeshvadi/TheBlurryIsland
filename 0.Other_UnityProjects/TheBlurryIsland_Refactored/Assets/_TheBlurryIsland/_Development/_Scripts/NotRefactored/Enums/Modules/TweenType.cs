﻿
public enum TweenType
{
    Position,
    Rotation,
    Scale,
    Color,
    Alpha
}
