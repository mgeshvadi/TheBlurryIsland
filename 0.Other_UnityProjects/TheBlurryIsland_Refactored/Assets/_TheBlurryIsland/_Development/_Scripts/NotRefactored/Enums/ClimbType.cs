﻿public enum ClimbType
{
    SinglePhaseClimb, // character do the climb completely
    DoublePhaseClimb, // character first goes to climb idle and waits for user to order another climb action for the rest of climb
    AutomaticClimb, // character script chose between "SinglePhaseClimb" and "DoublePhaseClimb" based on its air height and the climbable Position
    Valut,
    Ladder,
    Rope
}


public enum ClimbState
{
    NotOnClimb,
    ClimbIdle,
    ClimbingUp,
    ClimbingEnd
}
