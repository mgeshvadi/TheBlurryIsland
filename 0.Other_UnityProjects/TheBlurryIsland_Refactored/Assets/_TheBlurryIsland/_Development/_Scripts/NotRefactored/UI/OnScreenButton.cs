﻿using System;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem.Layouts;


namespace UnityEngine.InputSystem.OnScreen
{
    /// <summary>
    /// A button that is visually represented on-screen and triggered by touch or other pointer
    /// input.
    /// </summary>
    public class OnScreenButton : OnScreenControl, IPointerDownHandler, IPointerUpHandler, IDragHandler
    {
        [InputControl(layout = "Button")]
        [SerializeField]
        private string actionControlPath;

        private RectTransform rectTransform;

        private void Awake()
        {
            rectTransform = transform.GetComponent<RectTransform>();
        }

        // ReSharper disable once ConvertToAutoProperty
        protected override string controlPathInternal
        {
            get => actionControlPath;
            set => actionControlPath = value;
        }

        public void OnDrag(PointerEventData eventData)
        {
            if (eventData == null)
                throw new ArgumentNullException(nameof(eventData));

            RectTransformUtility.ScreenPointToLocalPointInRectangle(rectTransform, eventData.position, eventData.pressEventCamera, out var position);
            if (!rectTransform.rect.Contains(position))
                OnPointerUp(eventData);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            SendValueToControl(0.0f);
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            SendValueToControl(1.0f);
        }
    }
}
