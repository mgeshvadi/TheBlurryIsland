﻿using System;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem.Layouts;
using UnityEngine.UI;


namespace UnityEngine.InputSystem.OnScreen
{
    /// <summary>
    /// A stick control displayed on screen and moved around by touch or other pointer
    /// input.
    /// </summary>
    public class OnScreenStick : OnScreenControl, IPointerDownHandler, IPointerUpHandler, IDragHandler
    {
        // ReSharper disable once Unity.RedundantAttributeOnTarget

        [Header("Visual Stick")]
        [SerializeField] private RectTransform stick;
        [SerializeField] private ToggleState showingVisual;
        [SerializeField] private Text debugText; // TODO : remove this line, Test only
        [Space]
        [InputControl(layout = "Vector2")]
        [SerializeField] private string actionControlPath;

        private const int MovementRange = 50;

        private RectTransform rectTransform;
        private Vector3 startPos;
        private Vector2 pointerDownPos;

        // ReSharper disable once ConvertToAutoProperty
        protected override string controlPathInternal
        {
            get => actionControlPath;
            set => actionControlPath = value;
        }

        private void Awake()
        {
            stick.gameObject.SetActive(false);
            rectTransform = transform.GetComponent<RectTransform>();
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            if (eventData == null)
                throw new ArgumentNullException(nameof(eventData));

            RectTransformUtility.ScreenPointToLocalPointInRectangle(rectTransform, eventData.position, eventData.pressEventCamera, out pointerDownPos);

            stick.gameObject.SetActive(showingVisual == ToggleState.On);

            stick.position = eventData.position;
            startPos = stick.anchoredPosition;
        }

        public void OnDrag(PointerEventData eventData)
        {
            if (eventData == null)
                throw new ArgumentNullException(nameof(eventData));

            RectTransformUtility.ScreenPointToLocalPointInRectangle(rectTransform, eventData.position, eventData.pressEventCamera, out var position);
            if (!rectTransform.rect.Contains(position))
            {
                OnPointerUp(eventData);
                return;
            }

            var delta = position - pointerDownPos;
            delta = Vector2.ClampMagnitude(delta, MovementRange);
            stick.anchoredPosition = startPos + (Vector3) delta;

            debugText.text = new Vector2(delta.x / MovementRange, delta.y / MovementRange).ToString(); // TODO : remove this line, Test only
            SendValueToControl(new Vector2(delta.x / MovementRange, delta.y / MovementRange));
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            stick.anchoredPosition = startPos;
            stick.gameObject.SetActive(false);
            SendValueToControl(Vector2.zero);
        }

    }
}
