﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class DataVariation
{
    public GameObject prefab;
    public List<PhysicalProperty> physicalProperties;
}
