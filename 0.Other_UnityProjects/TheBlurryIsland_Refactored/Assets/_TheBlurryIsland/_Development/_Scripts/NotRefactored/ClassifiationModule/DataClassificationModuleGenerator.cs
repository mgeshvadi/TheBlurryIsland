﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Random = UnityEngine.Random;

public class DataClassificationModuleGenerator : MonoBehaviour
{
    public TimeDuration instantiationDelayTime;
    public List<DataVariation> dataVariations;

    private ToggleState currentAutoState = ToggleState.Off;

    public void ToggleAutoGenerating()
    {
        switch (currentAutoState)
        {
            case ToggleState.On:
                TurnOffAutoGenerating();
                break;
            case ToggleState.Off:
                TurnOnAutoGenerating();
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    public void Generate(int count)
    {
        for (int i = 0; i < count; i++)
            Invoke(nameof(InstantiateLabelTuple), instantiationDelayTime.GetTime());

    }

    private void TurnOnAutoGenerating()
    {
        currentAutoState = ToggleState.On;
        StartCoroutine(AutoInstantiateDataVisuals());
    }

    private void TurnOffAutoGenerating()
    {
        currentAutoState = ToggleState.Off;
        StopCoroutine(AutoInstantiateDataVisuals());
    }


    private IEnumerator AutoInstantiateDataVisuals()
    {
        while (currentAutoState == ToggleState.On)
        {
            InstantiateLabelTuple();
            yield return new WaitForSeconds(instantiationDelayTime.GetTime());
        }
    }

    private void InstantiateLabelTuple()
    {
        DataVariation selected = dataVariations[Random.Range(0, dataVariations.Count)];
        GameObject instantiated = Instantiate(selected.prefab, transform).gameObject;
        instantiated.transform.localPosition = Vector3.zero;
        instantiated.name = "Instantiated";

        PhysicalModule tupleComponent = instantiated.AddComponent<PhysicalModule>();
        tupleComponent.physicalProperties = selected.physicalProperties;

        tupleComponent.startEffect.Invoke();
    }
}
