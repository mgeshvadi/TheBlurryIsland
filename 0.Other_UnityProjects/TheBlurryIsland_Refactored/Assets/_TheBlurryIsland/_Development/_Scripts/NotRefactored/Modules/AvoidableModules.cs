﻿using System.Collections.Generic;
using UnityEngine;


// ReSharper disable Unity.InefficientPropertyAccess

[ExecuteInEditMode]
public class AvoidableModules : MonoBehaviour
{
    public Vector3 meshSize = Vector3.one;
    [Space]
    public float characterWidth = 0.35f;
    [SerializeField] private float splineOffsetZ = 0.3f;
    [SerializeField] private float sidePointsOffsetZ = 0.45f;

    private readonly List<GameObject> points = new List<GameObject>();
    private readonly Vector3 triggerToMeshProportion = new Vector3(1, 0.4f, 0.05f);
    private readonly List<Spline> splines = new List<Spline>();

#if UNITY_EDITOR
    private void Update()
    {
        if (!Application.isPlaying)
            Init();
    }
#endif

    private void Awake()
    {
        Init();
    }

    private void Init()
    {
        CheckValid();
        UnifyScales();
        SetTriggerScaleAndPosition();
        SetPoints();
        CalculateSplines();
    }

    private void CheckValid()
    {
        if (!transform.parent)
        {
            Debug.LogError("Avoidables should be child of sth");
            return;
        }
        if (transform.childCount == 4)
            return;
        Debug.LogError("Trigger should have exactly 4 points as child");
    }

    private void UnifyScales()
    {
        List<Transform> transforms = new List<Transform>();
        Transform t = transform;
        while (t.parent)
        {
            t = t.parent;
            transforms.Add(t);
        }
        transforms.Reverse();
        foreach (Transform tr in transforms)
            tr.localScale = Vector3.one;
    }

    private void SetTriggerScaleAndPosition()
    {

        transform.localScale = new Vector3(triggerToMeshProportion.x * characterWidth*2 + characterWidth*2 + meshSize.x, triggerToMeshProportion.y * meshSize.y, triggerToMeshProportion.z * splineOffsetZ*2 + meshSize.z + splineOffsetZ*2);
        transform.localPosition = new Vector3(0, -triggerToMeshProportion.y * meshSize.y, 0);
    }


    private void SetPoints()
    {
        points.Clear();
        for (int i = 0; i < 4; i++)
            points.Add(transform.GetChild(i).gameObject);

        SetPoint(points[0], "Point1", 0, -meshSize.z/2 - splineOffsetZ);
        SetPoint(points[1], "Point2", meshSize.x / 2 + characterWidth, -0.5f * meshSize.z - sidePointsOffsetZ);
        SetPoint(points[2], "Point3", meshSize.x / 2 + characterWidth, 0.5f * meshSize.z + sidePointsOffsetZ);
        SetPoint(points[3], "Point4", 0, meshSize.z/2 + splineOffsetZ);
    }

    private void SetPoint(GameObject go, string nameValue, float x, float z)
    {
        go.name = nameValue;
        SetPositionCorrespondingToParentRotation(go,new Vector3(transform.parent.position.x + x, transform.position.y, transform.parent.position.z + z));
        go.transform.localScale = Vector3.one;
    }

    private void SetPositionCorrespondingToParentRotation(GameObject go,Vector3 globalPosition)
    {
        Transform parent = go.transform.parent;

        Vector3 tempEuler = parent.eulerAngles;
        parent.eulerAngles= new Vector3(0,0,0);

        GameObject temp = new GameObject();
        temp.transform.parent = null;
        temp.transform.position = globalPosition;
        temp.transform.parent = parent;
        parent.eulerAngles= tempEuler;

        go.transform.localPosition = temp.transform.localPosition;
        DestroyImmediate(temp);
    }

    private void CalculateSplines()
    {
        splines.Clear();
        for (int i = 0; i < points.Count - 1; i++)
            splines.Add(new Spline(points[i].transform, points[i + 1].transform));
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Character"))
            return;
        other.GetComponent<MainCharacterController>().AddAvoidableSpline(splines);
    }

    private void OnTriggerExit(Collider other)
    {
        if (!other.CompareTag("Character"))
            return;
        other.GetComponent<MainCharacterController>().RemoveAvoidableSpline(splines);
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        if (points.Count != 4)
            return;
        Gizmos.color = Color.green;
        for (int i = 0; i < points.Count - 1; i++)
            Gizmos.DrawLine(points[i].transform.position, points[i + 1].transform.position);
    }
#endif
}
