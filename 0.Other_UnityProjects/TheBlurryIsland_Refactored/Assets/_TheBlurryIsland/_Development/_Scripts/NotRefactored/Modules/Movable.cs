﻿using UnityEngine;

[ExecuteInEditMode]
public class Movable : MonoBehaviour
{
    public Rigidbody mainRigidBody;
    public AvoidableModules avoidableModules;
    public Transform sideZMax;
    public Transform sideZMin;

    [Header("bound Settings")] public bool hasBounds;
    public GameObject startBound;
    public GameObject endBound;
    public float boundExtraOffset = 1;

    [Header("IK Settings")] public bool hasIk;
    public GameObject iKPoint1;
    public GameObject iKPoint2;
    public IKTriggers ikTrigger;
    public float ikPointExtraOffset = 2;
    public float ikTriggerExtraOffset = 1;

    public Vector3 meshSize;
    public float characterWidth = 0.35f;


    // Character blend amount and Positioning setting
    private const float MaxMass = 80;
    private const float MinMass = 10;
    private const float MinMassEffectCharacterOffset_Side = 0.05f;
    private const float MaxMassEffectCharacterOffset_Side = 0.5f;
    private const float MinMassEffectCharacterOffset_Front = 0.1f;
    private const float MaxMassEffectCharacterOffset_Front = 1f;
    private const float MinMassBlendTreeAmount = 0f; // what is the value of Push and Pull blend tree Parameter For the easy Push and Pull
    private const float MaxMassBlendTreeAmount = 1f; // what is the value of Push and Pull blend tree Parameter For the hard Push and Pull


    private Vector3 meshPosition;
    private float pushAndPullStartOffsetZ;
    private float characterMassEffectOffset_Side;
    private float characterMassEffectOffset_Front;
    private float mass;
    private float blendTreeValue;

    public void GetReadyForMove(float characterPositionZ)
    {
        pushAndPullStartOffsetZ = mainRigidBody.position.z - characterPositionZ;
        mainRigidBody.isKinematic = false;
    }

    public void MoveDone()
    {
    }

    public void AddForce(Vector3 vector3)
    {
        mainRigidBody.AddForce(vector3);
    }

    public float GetCharacterIdealPositionZ()
    {
        return mainRigidBody.position.z - pushAndPullStartOffsetZ;
    }

    public float GetInitialPositionDifference(InteractionSide interactionSide)
    {
        return interactionSide == InteractionSide.Right || interactionSide == InteractionSide.Left ? characterMassEffectOffset_Front : characterMassEffectOffset_Side;
    }

    public float GetBlendTreeValue()
    {
        return blendTreeValue;
    }

    public Vector3 GetVelocity()
    {
        return mainRigidBody.velocity;
    }

    public Vector2 GetSideZs()
    {
        return new Vector2(sideZMin.position.z, sideZMax.position.z);
    }

    private void Awake()
    {
        Init();
    }

    #if UNITY_EDITOR
    private void Update()
    {
        if (Application.isPlaying)
            return;
        Init();
    }
    #endif

    private void Init()
    {
        InitParameters();
        CalculateCharacterMassEffectOffset();
        SetIkPoints();
        SetIkTriggerSize();
        SetBounds();
    }

    private void InitParameters()
    {
        mass = mainRigidBody.mass;
    }

    private void CalculateCharacterMassEffectOffset()
    {
        characterMassEffectOffset_Side = (mass - MinMass) / (MaxMass - MinMass) * (MaxMassEffectCharacterOffset_Side - MinMassEffectCharacterOffset_Side) + MinMassEffectCharacterOffset_Side;
        characterMassEffectOffset_Side = Mathf.Clamp(characterMassEffectOffset_Side, MinMassEffectCharacterOffset_Side, MaxMassEffectCharacterOffset_Side);

        characterMassEffectOffset_Front = (mass - MinMass) / (MaxMass - MinMass) * (MaxMassEffectCharacterOffset_Front - MinMassEffectCharacterOffset_Front) + MinMassEffectCharacterOffset_Front;
        characterMassEffectOffset_Front = Mathf.Clamp(characterMassEffectOffset_Front, MinMassEffectCharacterOffset_Front, MaxMassEffectCharacterOffset_Front);

        blendTreeValue = (mass - MinMass) / (MaxMass - MinMass) * (MaxMassBlendTreeAmount - MinMassBlendTreeAmount) + MinMassBlendTreeAmount;
        blendTreeValue = Mathf.Clamp(blendTreeValue, MinMassBlendTreeAmount, MaxMassBlendTreeAmount);
    }

    private void SetIkPoints()
    {
        if (!hasIk)
            return;
        if (hasBounds)
        {
            meshSize = avoidableModules.meshSize / 2;
            meshPosition = avoidableModules.transform.position;
        }
        else
            meshPosition = transform.parent.position;

        SetGameObjectPos(iKPoint1, meshPosition + new Vector3(0, 0, meshSize.z + Mathf.Abs(ikPointExtraOffset)));
        SetGameObjectPos(iKPoint2, meshPosition + new Vector3(0, 0, -meshSize.z - Mathf.Abs(ikPointExtraOffset)));
    }

    private void SetBounds()
    {
        if (!hasBounds)
            return;
        Transform transform1 = avoidableModules.transform;
        Vector3 triggerSize = transform1.localScale / 2;
        Vector3 avoidablePosition = transform1.position;

        SetGameObjectPos(startBound, avoidablePosition + new Vector3(0, 0, triggerSize.z + Mathf.Abs(boundExtraOffset)));
        SetGameObjectPos(endBound, avoidablePosition + new Vector3(0, 0, -triggerSize.z - Mathf.Abs(boundExtraOffset)));
    }

    private void SetIkTriggerSize()
    {
        if (!hasIk)
            return;
        if (hasBounds)
            ikTrigger.transform.localScale = avoidableModules.meshSize + new Vector3(avoidableModules.characterWidth * 2, 0, ikTriggerExtraOffset);
        else
            ikTrigger.transform.localScale = meshSize + new Vector3(characterWidth * 2, 0, ikTriggerExtraOffset);
    }

    private void SetGameObjectPos(GameObject go, Vector3 globalPos)
    {
        SetPositionCorrespondingToParentRotation(go, globalPos);
    }

    private void SetPositionCorrespondingToParentRotation(GameObject go, Vector3 globalPosition)
    {
        Transform parent = go.transform.parent;

        Vector3 eulerAngles = parent.eulerAngles;
        Vector3 tempEuler = eulerAngles;

        GameObject temp = new GameObject();
        temp.transform.parent = null;
        temp.transform.position = globalPosition;
        temp.transform.parent = parent;
        eulerAngles = tempEuler;
        parent.eulerAngles = eulerAngles;

        go.transform.localPosition = temp.transform.localPosition;
        DestroyImmediate(temp);
    }
}
