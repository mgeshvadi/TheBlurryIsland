﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;


public class ModuleEventCaller : MonoBehaviour
{
    [SerializeField] private UnityEvent correspondingEvents;
    [SerializeField] private List<ToggleModuleElement> correspondingModuleElements;
    [Space]
    private readonly List<CharacterActions> charactersInRange = new List<CharacterActions>();

    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Character"))
            return;
        CharacterActions characterActions = other.GetComponent<CharacterActions>();
        charactersInRange.Add(characterActions);
        characterActions.currentActionCaller = this;
    }

    private void OnTriggerExit(Collider other)
    {
        if (!other.CompareTag("Character"))
            return;
        CharacterActions characterActions = other.GetComponent<CharacterActions>();
        charactersInRange.Remove(characterActions);
        characterActions.currentActionCaller = null;
    }

    public void DoAction()
    {
        correspondingEvents.Invoke();
        if (correspondingModuleElements.Any(element => !element.ReadyForAction()))
            return;
        foreach (ToggleModuleElement element in correspondingModuleElements)
            element.ToggleStates();
    }

}
