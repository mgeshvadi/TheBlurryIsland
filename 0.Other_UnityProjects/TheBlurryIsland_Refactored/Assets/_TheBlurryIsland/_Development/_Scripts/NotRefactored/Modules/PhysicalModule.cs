﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PhysicalModule : MonoBehaviour
{
    public List<PhysicalProperty> physicalProperties;
    [HideInInspector] public UnityEvent startEffect = new UnityEvent();

    private Rigidbody rb;

    private void Awake()
    {
        Init();
    }

    private void Init()
    {
        GetComponents();
        startEffect.AddListener(AdoptEffects);
    }

    private void GetComponents()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void AdoptEffects()
    {
        foreach (PhysicalProperty effect in physicalProperties)
            AdoptEffect(effect);
    }

    private void AdoptEffect(PhysicalProperty property) // TODO: Complete adopting effects
    {
        switch (property.type)
        {
            case PhysicalPropertyType.ForceInitial:
                break;
            case PhysicalPropertyType.ForceContinues:
                break;
            case PhysicalPropertyType.ForceOnCollide:
                break;
            case PhysicalPropertyType.AngularVelocity:
                break;
            case PhysicalPropertyType.Velocity:
                rb.velocity = property.value.GetValue<Vector3>();
                break;
            case PhysicalPropertyType.Rotation:
                break;
            case PhysicalPropertyType.Position:
                break;
            case PhysicalPropertyType.Mass:
                break;
            case PhysicalPropertyType.Acceleration:
                break;
            case PhysicalPropertyType.Transparency:
                break;
            case PhysicalPropertyType.Resistance:
                break;
            case PhysicalPropertyType.Vibration:
                break;
            case PhysicalPropertyType.Color:
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }
}
