﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Climbable : MonoBehaviour
{
    [SerializeField] private ClimbType climbType;

    [Tooltip("Only add values for which you wish to override for this specific gameObject")] [SerializeField]
    private List<ClimbPoint> climbPointsOverride = new List<ClimbPoint>();

    [Tooltip("For ik points climb state should be set to 'NotOnClimb',Only add values for which you wish to override for this specific gameObject")] [SerializeField]
    private List<ClimbPoint> climbIkPointsOverride = new List<ClimbPoint>();

    [Space] [Tooltip("This field is only required if the object will move, and the rigidbody is not on this object itself")] [Header("Optional")]
    private Rigidbody mainRigidbody;

    private List<ClimbPoint> climbPoints = new List<ClimbPoint>();
    private List<ClimbPoint> climbIkPoints = new List<ClimbPoint>();

    private const float CharacterClimbHandsDistance = 0.3f; //the distance between character hands while she/he is climbing

    private List<ObjectSidesPosition> sides = new List<ObjectSidesPosition>();
    private GameObject templateGameObject;

    public float GetVelocityMagnitude()
    {
        return mainRigidbody ? mainRigidbody.velocity.magnitude : 0f;
    }

    public ClimbType GetClimbType()
    {
        return climbType;
    }

    public GameObject GetCharacterIdealPosition(InteractionSide climbSide, ClimbState climbState)
    {
        return climbPoints.Find(cp => cp.climbState == climbState).objectSidesPositions.Find(osp => osp.interactionSide == climbSide).gameObjects[0];
    }

    public List<GameObject> GetClimbIkGameObjects(InteractionSide climbSide)
    {
        ObjectSidesPosition ikPoint = climbIkPoints[0].objectSidesPositions.Find(osp => osp.interactionSide == climbSide);
        return new List<GameObject>(ikPoint.GetGameObjects());
    }

    public InteractionSide InteractionHappened(Vector3 characterPosition, float characterHeight, float characterRadius)
    {
        InteractionSide climbSide = ConvertCharacterPositionToInteractionSide(characterPosition, characterRadius);
        SetObjectSidePosition(GetObjectSidePosition(ClimbState.ClimbIdle, climbSide), characterPosition, characterRadius, characterHeight);
        SetObjectSidePosition(GetObjectSidePosition(ClimbState.ClimbingUp, climbSide), characterPosition, characterRadius, characterHeight);
        SetObjectSidePosition(GetObjectSidePosition(ClimbState.ClimbingEnd, climbSide), characterPosition, characterRadius, characterHeight);

        SetObjectSidePosition(GetIkObjectSidePosition(climbSide), characterPosition, characterRadius, null);
        AdoptAsIkPoint(GetIkObjectSidePosition(climbSide), characterRadius);
        return climbSide;
    }

    public ObjectSidesPosition GetToppestSide()
    {
        return GetSide(InteractionSide.Top);
    }


    public void ClimbEnd()
    {
        SetClimbPointsPositions(); // In climb happening we mines the y of climb points as the "characterHeight" amount, and if we not restore it back, each climb it gets lower and lower while its not adorable
    }


    private void Awake()
    {
        GetComponents();
        Init();
    }

    private void GetComponents()
    {
        if (!mainRigidbody)
            mainRigidbody = GetComponent<Rigidbody>();
    }

    public void Init()
    {
        NewTheLists();
        DestroyGameObjects();
        FillClimbPoints();
        FillClimbIkPoints();
        FillAndInstantiateSides();
        InstantiateClimbPointsGameObjects();
        DestroyImmediate(templateGameObject);

        SetSidesPositions();
        SetClimbPointsPositions();
        SetClimbIkPointsPositions();
    }

    private void NewTheLists()
    {
        if (climbPoints == null)
            climbPoints = new List<ClimbPoint>();

        if (climbIkPoints == null)
            climbIkPoints = new List<ClimbPoint>();

        if (sides == null)
            sides = new List<ObjectSidesPosition>();
    }

    private void DestroyGameObjects()
    {
        foreach (GameObject go in from point in climbPoints from osp in point.objectSidesPositions from go in osp.gameObjects select go)
            DestroyGameObject(go);
        foreach (GameObject go in from point in climbIkPoints from osp in point.objectSidesPositions from go in osp.gameObjects select go)
            DestroyGameObject(go);
        foreach (GameObject go in sides.SelectMany(osp => osp.gameObjects))
            DestroyGameObject(go);

        for (int i = 0; i < transform.childCount; i++)
            DestroyGameObject(transform.GetChild(i).gameObject);

        if (templateGameObject)
            DestroyImmediate(templateGameObject);
        templateGameObject = new GameObject("Template Game object temporary");
    }

    private void FillClimbPoints()
    {
        climbPoints.Clear();

        climbPoints.Add(new ClimbPoint(ClimbState.ClimbIdle, new[]
        {
            new ObjectSidesPosition(InteractionSide.Right, new Vector3(0, -0.2f, 0.03f)),
            new ObjectSidesPosition(InteractionSide.Left, new Vector3(0, -0.2f, -0.03f)),
            new ObjectSidesPosition(InteractionSide.Front, new Vector3(0.03f, -0.17f, 0)),
            new ObjectSidesPosition(InteractionSide.Back, new Vector3(-0.03f, -0.17f, 0))
        }));

        climbPoints.Add(new ClimbPoint(ClimbState.ClimbingUp, new[]
        {
            new ObjectSidesPosition(InteractionSide.Right, new Vector3(0, 0.2f, 0.2f)),
            new ObjectSidesPosition(InteractionSide.Left, new Vector3(0, 0.2f, -0.2f)),
            new ObjectSidesPosition(InteractionSide.Front, new Vector3(0.2f, 0.2f, 0)),
            new ObjectSidesPosition(InteractionSide.Back, new Vector3(-0.2f, 0.2f, 0))
        }));

        climbPoints.Add(new ClimbPoint(ClimbState.ClimbingEnd, new[]
        {
            new ObjectSidesPosition(InteractionSide.Right, new Vector3(0f, 1.65f, -0.15f)),
            new ObjectSidesPosition(InteractionSide.Left, new Vector3(0, 1.65f, 0.15f)),
            new ObjectSidesPosition(InteractionSide.Front, new Vector3(-0.1f, 1.65f, 0)),
            new ObjectSidesPosition(InteractionSide.Back, new Vector3(0.1f, 1.65f, 0))
        }));


        CheckForClimbPointOverride();
    }

    private void FillClimbIkPoints()
    {
        climbIkPoints.Clear();

        climbIkPoints.Add(new ClimbPoint(new[]
        {
            new ObjectSidesPosition(InteractionSide.Right, new Vector3(0, 0, 0)),
            new ObjectSidesPosition(InteractionSide.Left, new Vector3(0, 0, 0)),
            new ObjectSidesPosition(InteractionSide.Front, new Vector3(0, 0, 0)),
            new ObjectSidesPosition(InteractionSide.Back, new Vector3(0, 0, 0))
        }));

        CheckForClimbIkPointOverride();
    }

    private void CheckForClimbPointOverride()
    {
        foreach (ClimbPoint point in climbPointsOverride)
        {
            List<ObjectSidesPosition> objectSidesPositions = climbPoints.Find(climbPoint => climbPoint.climbState == point.climbState).objectSidesPositions;
            foreach (ObjectSidesPosition sidesPosition in objectSidesPositions)
                point.objectSidesPositions.Find(position => position.interactionSide == sidesPosition.interactionSide).offset = sidesPosition.offset;
        }
    }

    private void CheckForClimbIkPointOverride()
    {
        foreach (ClimbPoint point in climbIkPointsOverride)
        {
            List<ObjectSidesPosition> objectSidesPositions = climbIkPoints.Find(climbPoint => climbPoint.climbState == point.climbState).objectSidesPositions;
            foreach (ObjectSidesPosition sidesPosition in objectSidesPositions)
                point.objectSidesPositions.Find(position => position.interactionSide == sidesPosition.interactionSide).offset = sidesPosition.offset;
        }
    }

    private void FillAndInstantiateSides()
    {
        sides.Clear();
        sides.Add(new ObjectSidesPosition(InteractionSide.Right, InstantiateGameObject("Side_" + InteractionSide.Right)));
        sides.Add(new ObjectSidesPosition(InteractionSide.Left, InstantiateGameObject("Side_" + InteractionSide.Left)));
        sides.Add(new ObjectSidesPosition(InteractionSide.Front, InstantiateGameObject("Side_" + InteractionSide.Front)));
        sides.Add(new ObjectSidesPosition(InteractionSide.Back, InstantiateGameObject("Side_" + InteractionSide.Back)));
        sides.Add(new ObjectSidesPosition(InteractionSide.Top, InstantiateGameObject("Side_" + InteractionSide.Top)));
        sides.Add(new ObjectSidesPosition(InteractionSide.Bottom, InstantiateGameObject("Side_" + InteractionSide.Bottom)));
    }

    private void InstantiateClimbPointsGameObjects()
    {
        foreach (ClimbPoint point in climbPoints)
        {
            foreach (ObjectSidesPosition objectSidesPosition in point.objectSidesPositions)
                objectSidesPosition.SetGameObject(InstantiateGameObject(point.climbState + "_" + objectSidesPosition.interactionSide));
        }

        foreach (ObjectSidesPosition objectSidesPosition in climbIkPoints.SelectMany(point => point.objectSidesPositions))
            objectSidesPosition.SetGameObjects(new[] {InstantiateGameObject("IK_1_" + objectSidesPosition.interactionSide), InstantiateGameObject("IK_2_" + objectSidesPosition.interactionSide)});
    }

    private void SetSidesPositions()
    {
        GetSide(InteractionSide.Right).SetLocalPosition(new Vector3(0, 0, 0.5f));
        GetSide(InteractionSide.Left).SetLocalPosition(new Vector3(0, 0, -0.5f));
        GetSide(InteractionSide.Front).SetLocalPosition(new Vector3(0.5f, 0, 0));
        GetSide(InteractionSide.Back).SetLocalPosition(new Vector3(-0.5f, 0, 0));
        GetSide(InteractionSide.Top).SetLocalPosition(new Vector3(0, 0.5f, 0));
        GetSide(InteractionSide.Bottom).SetLocalPosition(new Vector3(0, -0.5f, 0));
    }

    private ObjectSidesPosition GetSide(InteractionSide interactionSide)
    {
        return sides.Find(side => side.interactionSide == interactionSide);
    }

    private void SetClimbPointsPositions()
    {
        foreach (ObjectSidesPosition osp in climbPoints.SelectMany(climbPoint => climbPoint.objectSidesPositions))
            SetObjectSidePosition(osp);
    }

    private void SetClimbIkPointsPositions()
    {
        foreach (ObjectSidesPosition osp in climbIkPoints.SelectMany(ikPoint => ikPoint.objectSidesPositions))
        {
            SetObjectSidePosition(osp);
            AdoptAsIkPoint(osp, 0);
        }
    }

    private void SetObjectSidePosition(ObjectSidesPosition osp)
    {
        osp.SetGlobalPosition(GetSide(osp.interactionSide).GetGlobalPosition() + osp.offset);
        osp.SetGlobalPosition(new Vector3(osp.GetGlobalPosition().x, GetToppestSide().GetGlobalPosition().y, osp.GetGlobalPosition().z) + new Vector3(0, osp.offset.y, 0));
    }

    private void SetObjectSidePosition(ObjectSidesPosition osp, Vector3 characterPosition, float characterRadius, float? characterHeight)
    {
        Vector3 ospPos = osp.GetGlobalPosition();
        switch (osp.interactionSide)
        {
            case InteractionSide.Left:
            case InteractionSide.Right:
                osp.SetGlobalPosition(new Vector3(characterPosition.x, ospPos.y, ospPos.z));
                break;
            case InteractionSide.Front:
            case InteractionSide.Back:
                osp.SetGlobalPosition(new Vector3(ospPos.x, ospPos.y, characterPosition.z));
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }

        osp.SetGlobalPosition(ClampBetweenSides(osp.interactionSide, characterRadius, osp.GetGlobalPosition()));

        if (characterHeight != null)
            osp.SetGlobalPosition(osp.GetGlobalPosition() - new Vector3(0, (float) characterHeight, 0));
    }

    private void AdoptAsIkPoint(ObjectSidesPosition osp, float characterRadius)
    {
        Vector3 hand1 = osp.GetGlobalPosition();
        switch (osp.interactionSide)
        {
            case InteractionSide.Left:
                osp.SetGlobalPositions(new[] {ClampBetweenSides(osp.interactionSide, characterRadius, hand1 + new Vector3(CharacterClimbHandsDistance / 2, 0, 0)), ClampBetweenSides(osp.interactionSide, characterRadius, hand1 - new Vector3(CharacterClimbHandsDistance / 2, 0, 0))});
                break;
            case InteractionSide.Right:
                osp.SetGlobalPositions(new[] {ClampBetweenSides(osp.interactionSide, characterRadius, hand1 - new Vector3(CharacterClimbHandsDistance / 2, 0, 0)), ClampBetweenSides(osp.interactionSide, characterRadius, hand1 + new Vector3(CharacterClimbHandsDistance / 2, 0, 0))});
                break;
            case InteractionSide.Front:
                osp.SetGlobalPositions(new[] {ClampBetweenSides(osp.interactionSide, characterRadius, hand1 + new Vector3(0, 0, CharacterClimbHandsDistance / 2)), ClampBetweenSides(osp.interactionSide, characterRadius, hand1 - new Vector3(0, 0, CharacterClimbHandsDistance / 2))});
                break;
            case InteractionSide.Back:
                osp.SetGlobalPositions(new[] {ClampBetweenSides(osp.interactionSide, characterRadius, hand1 - new Vector3(0, 0, CharacterClimbHandsDistance / 2)), ClampBetweenSides(osp.interactionSide, characterRadius, hand1 + new Vector3(0, 0, CharacterClimbHandsDistance / 2))});
                break;
            case InteractionSide.Top:
                break;
            case InteractionSide.Bottom:
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    private ObjectSidesPosition GetObjectSidePosition(ClimbState climbState, InteractionSide interactionSide)
    {
        return climbPoints.Find(point => point.climbState == climbState).objectSidesPositions.Find(osp => osp.interactionSide == interactionSide);
    }

    private ObjectSidesPosition GetIkObjectSidePosition(InteractionSide interactionSide)
    {
        return climbIkPoints[0].objectSidesPositions.Find(osp => osp.interactionSide == interactionSide);
    }

    private InteractionSide ConvertCharacterPositionToInteractionSide(Vector3 characterPosition, float characterRadius)
    {
        if (characterPosition.z >= GetSide(InteractionSide.Right).GetGlobalPosition().z && characterPosition.x <= GetSide(InteractionSide.Front).GetGlobalPosition().x && characterPosition.x >= GetSide(InteractionSide.Back).GetGlobalPosition().x)
            return InteractionSide.Right;

        if (characterPosition.z <= GetSide(InteractionSide.Right).GetGlobalPosition().z && characterPosition.x <= GetSide(InteractionSide.Front).GetGlobalPosition().x && characterPosition.x >= GetSide(InteractionSide.Back).GetGlobalPosition().x)
            return InteractionSide.Left;

        if (characterPosition.x >= GetSide(InteractionSide.Front).GetGlobalPosition().x && characterPosition.z <= GetSide(InteractionSide.Right).GetGlobalPosition().z + characterRadius && characterPosition.z >= GetSide(InteractionSide.Left).GetGlobalPosition().z - characterRadius)
            return InteractionSide.Front;

        if (characterPosition.x <= GetSide(InteractionSide.Back).GetGlobalPosition().x && characterPosition.z <= GetSide(InteractionSide.Right).GetGlobalPosition().z + characterRadius && characterPosition.z >= GetSide(InteractionSide.Left).GetGlobalPosition().z - characterRadius)
            return InteractionSide.Back;

        Debug.LogError("That wasn't supposed to happen");
        return InteractionSide.Front;
    }

    private Vector3 ClampBetweenSides(InteractionSide activeSide, float characterRadius, Vector3 value)
    {
        switch (activeSide)
        {
            case InteractionSide.Left:
            case InteractionSide.Right:
                return new Vector3(Mathf.Clamp(value.x, GetSide(InteractionSide.Back).GetGlobalPosition().x + characterRadius / 2, GetSide(InteractionSide.Front).GetGlobalPosition().x- characterRadius / 2) , value.y, value.z);
            case InteractionSide.Front:
            case InteractionSide.Back:
                return new Vector3(value.x, value.y, Mathf.Clamp(value.z, GetSide(InteractionSide.Left).GetGlobalPosition().z + characterRadius / 2, GetSide(InteractionSide.Right).GetGlobalPosition().z - characterRadius / 2));
            default:
                throw new ArgumentOutOfRangeException(nameof(activeSide), activeSide, null);
        }
    }


    private static void DestroyGameObject(GameObject go)
    {
        if (Application.isPlaying)
            Destroy(go);
        else
            DestroyImmediate(go);
    }

    private GameObject InstantiateGameObject(string nameValue)
    {
        GameObject go = Instantiate(templateGameObject, transform, false);
        go.name = nameValue;
        return go;
    }
}
