﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;


public class ToggleModuleElement : MonoBehaviour
{

    public List<ModuleValue> moduleValues = new List<ModuleValue>();


    [HideInInspector] public UnityEvent onAllTogglesFinished = new UnityEvent();
    private readonly UnityEvent onOneToggleFinished = new UnityEvent();
    private static readonly int Hash_BaseColor = Shader.PropertyToID("_BaseColor");


    private Material material;
    private Color defaultColor;

    public void ToggleStates()
    {
        foreach (ModuleValue moduleValue in moduleValues)
            ToggleState(moduleValue);
    }

    public bool ReadyForAction()
    {
        return moduleValues.All(moduleValue => moduleValue.toggleDone);
    }

    private void ToggleState(ModuleValue moduleValue)
    {
        moduleValue.currentToggleState = InvertToggleState(moduleValue.currentToggleState);
        AdaptCurrentState(moduleValue);
    }

    private void Awake()
    {
        GetComponents();
        SetInitialState();
        onOneToggleFinished.AddListener(CheckAllTogglesFinished);
    }

    public void GetComponents()
    {
        if (material)
            return;
        if (!GetComponent<MeshRenderer>())
            return;
        material = GetComponent<MeshRenderer>().materials[0];
        defaultColor = material.color;
    }

    private void CheckAllTogglesFinished()
    {
        if (ReadyForAction())
            onAllTogglesFinished.Invoke();
    }

    public void SetInitialState()
    {
        var transform1 = transform;
        transform1.localPosition = new Vector3(0, 0, 0);
        transform1.localEulerAngles = new Vector3(0, 0, 0);
        transform1.localScale = new Vector3(1, 1, 1);
        if(material)
            material.SetColor(Hash_BaseColor, defaultColor);
        foreach (ModuleValue moduleValue in moduleValues)
        {
            moduleValue.currentToggleState = moduleValue.initialState;
            AdaptCurrentState(moduleValue, false);
            moduleValue.toggleDone = true;
        }
    }

    private void AdaptCurrentState(ModuleValue moduleValue)
    {
        AdaptCurrentState(moduleValue, true,moduleValue.isOnRecursion? moduleValue.toggleDurationAtRecursion: moduleValue.toggleDurationAtFirst,moduleValue.isOnRecursion? moduleValue.toggleDelayAtRecursion : moduleValue.toggleDelayAtFirst);
    }

    private void AdaptCurrentState(ModuleValue moduleValue, bool withTween, float duration = 0, float delay = 0)
    {
        switch (moduleValue.toggleType)
        {
            case TweenType.Position:
                if (withTween)
                    TweenPosition(moduleValue.CalculateGoalVector(), duration, delay, moduleValue);
                else
                    SetPosition(moduleValue.CalculateGoalVector(), moduleValue);
                break;
            case TweenType.Rotation:
                if (withTween)
                    TweenRotation(moduleValue.CalculateGoalVector(), duration, delay, moduleValue);
                else
                    SetRotation(moduleValue.CalculateGoalVector(), moduleValue);
                break;
            case TweenType.Scale:
                if (withTween)
                    TweenScale(moduleValue.CalculateGoalVector(), duration, delay, moduleValue);
                else
                    SetScale(moduleValue.CalculateGoalVector(), moduleValue);
                break;
            case TweenType.Color:
                if (withTween)
                    TweenColor(moduleValue.CalculateGoalColor(), duration, delay, moduleValue);
                else
                    SetColor(moduleValue.CalculateGoalColor(), moduleValue);
                break;
            case TweenType.Alpha:
                if (withTween)
                    TweenAlpha(moduleValue.CalculateGoalAlpha(), duration, delay, moduleValue);
                else
                    SetAlpha(moduleValue.CalculateGoalAlpha(), moduleValue);
                break;

            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    private void TweenPosition(Vector3 to, float duration, float delay, ModuleValue moduleValue)
    {
        LeanTween.cancel(moduleValue.tweenId);
        moduleValue.toggleDone = false;
        moduleValue.tweenId = LeanTween.moveLocal(gameObject, to, duration).setEase(moduleValue.toggleEasingTypeAtFirst).setDelay(delay).setOnUpdate((vector3, o) => { moduleValue.currentValue = vector3; }).setOnComplete(() =>
                                                                                                                                                                                                                            {
                                                                                                                                                                                                                                moduleValue.currentValue = to;
                                                                                                                                                                                                                                transform.localPosition = to;
                                                                                                                                                                                                                                ModuleTweenFinished(moduleValue);
                                                                                                                                                                                                                            }).id;
    }

    private void SetPosition(Vector3 to, ModuleValue moduleValue)
    {
        transform.localPosition = to;
        moduleValue.currentValue = to;
    }

    private void TweenRotation(Vector3 to, float duration, float delay, ModuleValue moduleValue)
    {
        LeanTween.cancel(moduleValue.tweenId);
        moduleValue.toggleDone = false;
        moduleValue.tweenId = LeanTween.value(gameObject, moduleValue.currentValue, to, duration).setEase(moduleValue.toggleEasingTypeAtFirst).setDelay(delay).setOnUpdate((vector3, o) =>
                                                                                                                                                                           {
                                                                                                                                                                               moduleValue.currentValue = vector3;
                                                                                                                                                                               transform.localEulerAngles = vector3;
                                                                                                                                                                           }).setOnComplete(() =>
                                                                                                                                                                                            {
                                                                                                                                                                                                moduleValue.currentValue = to;
                                                                                                                                                                                                transform.localEulerAngles = to;
                                                                                                                                                                                                ModuleTweenFinished(moduleValue);
                                                                                                                                                                                            }).id;
    }

    private void SetRotation(Vector3 to, ModuleValue moduleValue)
    {
        transform.localEulerAngles = to;
        moduleValue.currentValue = to;
    }

    private void TweenScale(Vector3 to, float duration, float delay, ModuleValue moduleValue)
    {
        LeanTween.cancel(moduleValue.tweenId);
        moduleValue.toggleDone = false;
        moduleValue.tweenId = LeanTween.scale(gameObject, to, duration).setEase(moduleValue.toggleEasingTypeAtFirst).setDelay(delay).setOnUpdate((vector3, o) => { moduleValue.currentValue = vector3; }).setOnComplete(() =>
                                                                                                                                                                                                                        {
                                                                                                                                                                                                                            moduleValue.currentValue = to;
                                                                                                                                                                                                                            transform.localScale = to;
                                                                                                                                                                                                                            ModuleTweenFinished(moduleValue);
                                                                                                                                                                                                                        }).id;
    }

    private void SetScale(Vector3 to, ModuleValue moduleValue)
    {
        transform.localScale = to;
        moduleValue.currentValue = to;
    }

    private void TweenColor(Color to, float duration, float delay, ModuleValue moduleValue)
    {
        LeanTween.cancel(moduleValue.tweenId);
        moduleValue.toggleDone = false;
        moduleValue.tweenId = LeanTween.value(gameObject, moduleValue.currentColorValue, to, duration).setEase(moduleValue.toggleEasingTypeAtFirst).setDelay(delay).setOnUpdate((color, o) =>
                                                                                                                                                                                {
                                                                                                                                                                                    moduleValue.currentColorValue = color;
                                                                                                                                                                                    material.SetColor(Hash_BaseColor, color);
                                                                                                                                                                                }).setOnComplete(() =>
                                                                                                                                                                                                 {
                                                                                                                                                                                                     moduleValue.currentColorValue = to;
                                                                                                                                                                                                     material.SetColor(Hash_BaseColor, to);
                                                                                                                                                                                                     ModuleTweenFinished(moduleValue);
                                                                                                                                                                                                 }).id;
    }

    private void SetColor(Color to, ModuleValue moduleValue)
    {
        moduleValue.currentColorValue = to;
        material.SetColor(Hash_BaseColor, to);
    }

    private void TweenAlpha(float to, float duration, float delay, ModuleValue moduleValue)
    {
        LeanTween.cancel(moduleValue.tweenId);
        moduleValue.toggleDone = false;
        Color color = material.color;
        moduleValue.tweenId = LeanTween.value(gameObject, moduleValue.currentAlphaValue, to, duration).setEase(moduleValue.toggleEasingTypeAtFirst).setDelay(delay).setOnUpdate((f, o) =>
                                                                                                                                                                                {
                                                                                                                                                                                    moduleValue.currentAlphaValue = f;
                                                                                                                                                                                    material.SetColor(Hash_BaseColor, new Color(color.r, color.g, color.b, f));
                                                                                                                                                                                }).setOnComplete(() =>
                                                                                                                                                                                                 {
                                                                                                                                                                                                     moduleValue.currentAlphaValue = to;
                                                                                                                                                                                                     material.SetColor(Hash_BaseColor, new Color(color.r, color.g, color.b, to));
                                                                                                                                                                                                     ModuleTweenFinished(moduleValue);
                                                                                                                                                                                                 }).id;
    }

    private void SetAlpha(float to, ModuleValue moduleValue)
    {
        Color color = material.color;
        moduleValue.currentAlphaValue = to;
        material.SetColor(Hash_BaseColor, new Color(color.r, color.g, color.b, to));
    }

    private void ModuleTweenFinished(ModuleValue moduleValue)
    {
        if (moduleValue.recursion == global::ToggleState.On && !moduleValue.isOnRecursion)
        {
            moduleValue.isOnRecursion = true;
            ToggleState(moduleValue);
        }
        else if ((moduleValue.recursion == global::ToggleState.On && moduleValue.isOnRecursion) || moduleValue.recursion == global::ToggleState.Off)
        {
            moduleValue.isOnRecursion = false;
            moduleValue.toggleDone = true;
            onOneToggleFinished.Invoke();
        }
    }

    private ModuleToggleState InvertToggleState(ModuleToggleState state)
    {
        switch (state)
        {
            case ModuleToggleState.OnMin:
                return ModuleToggleState.OnMax;
            case ModuleToggleState.OnMax:
                return ModuleToggleState.OnMin;
            default:
                throw new ArgumentOutOfRangeException(nameof(state), state, null);
        }
    }
}
