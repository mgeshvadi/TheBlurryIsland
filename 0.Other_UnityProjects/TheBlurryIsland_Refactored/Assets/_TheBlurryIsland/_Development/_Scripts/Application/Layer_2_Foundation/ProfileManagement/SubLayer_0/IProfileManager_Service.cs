using Blurry.Application.Layer_0_Base.ServiceLocating.SubLayer_0;


namespace Blurry.Application.Layer_2_Foundation.ProfileManagement.SubLayer_0
{
    public interface IProfileManager_Service : IService
    {
        string GlobalUserId { get; }

        T LoadData<T>(string key, T defaultValue);
        void SaveData<T>(string key, T value);
    }
}