using Blurry.Application.Layer_1_Ground.Behaviour.SubLayer_0;
using Blurry.Application.Layer_1_Ground.Behaviour.SubLayer_3;
using Blurry.Application.Layer_6_CoreMechanics.SubLayer_3_BehaviourData;


namespace Blurry.Application.Layer_6_CoreMechanics.SubLayer_5_Behaviour
{
    public interface IMechanic_Behaviour : IBehaviour
    {
    }

    public abstract class AMechanic_Behaviour<TData> : ABehaviour<TData>, IMechanic_Behaviour where TData : IMechanic_BehaviourData
    {
        protected AMechanic_Behaviour(TData data) : base(data)
        {
        }
    }
}