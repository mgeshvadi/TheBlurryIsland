using Blurry.Application.Layer_1_Ground.Presentation.SubLayer_0;
using Blurry.Application.Layer_1_Ground.Presentation.SubLayer_4;
using Blurry.Application.Layer_6_CoreMechanics.SubLayer_0_PresentationData;


namespace Blurry.Application.Layer_6_CoreMechanics.SubLayer_2_Presentation
{
    public interface IMechanicController_Presentation : IPresentationM
    {
    }

    public abstract class AMechanicController_Presentation<TData> : APresentation<TData>, IMechanicController_Presentation where TData : IMechanicController_PresentationData
    {
        protected AMechanicController_Presentation(TData data) : base(data)
        {
        }
    }
}