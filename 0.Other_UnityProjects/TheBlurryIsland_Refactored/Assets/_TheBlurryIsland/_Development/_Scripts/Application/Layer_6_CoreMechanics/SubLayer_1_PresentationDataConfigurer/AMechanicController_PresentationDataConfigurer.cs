using Blurry.Application.Layer_1_Ground.Configurer.SubLayer_2;
using Blurry.Application.Layer_6_CoreMechanics.SubLayer_0_Config;
using Blurry.Application.Layer_6_CoreMechanics.SubLayer_0_Entity;
using Blurry.Application.Layer_6_CoreMechanics.SubLayer_0_PresentationData;


namespace Blurry.Application.Layer_6_CoreMechanics.SubLayer_1_PresentationDataConfigurer
{
    public abstract class AMechanicController_PresentationDataConfigurer<TData, TConfig, TEntity> : AConfigurer<TData, TConfig, TEntity> where TData : IMechanicController_PresentationData
                                                                                                                                         where TConfig : IMechanicController_Config
                                                                                                                                         where TEntity : AMechanicController_Entity
    {
        protected AMechanicController_PresentationDataConfigurer(TConfig config, TEntity entity) : base(config, entity)
        {
        }
    }
}