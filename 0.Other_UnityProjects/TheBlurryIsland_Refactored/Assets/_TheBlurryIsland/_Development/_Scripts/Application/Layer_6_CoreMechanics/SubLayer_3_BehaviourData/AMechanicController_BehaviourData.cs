using Blurry.Application.Layer_1_Ground.Data.SubLayer_2;
using Blurry.Application.Layer_1_Ground.Data.SubLayer_3;
using Blurry.Application.Layer_6_CoreMechanics.SubLayer_2_Presentation;


namespace Blurry.Application.Layer_6_CoreMechanics.SubLayer_3_BehaviourData
{
    public interface IMechanicController_BehaviourData : IBehaviour_Data
    {
    }

    public abstract class AMechanicController_BehaviourData<TPresentation> : ABehaviour_Data<TPresentation>, IMechanicController_BehaviourData where TPresentation : IMechanicController_Presentation
    {
        protected AMechanicController_BehaviourData(TPresentation presentation) : base(presentation)
        {
        }
    }
}