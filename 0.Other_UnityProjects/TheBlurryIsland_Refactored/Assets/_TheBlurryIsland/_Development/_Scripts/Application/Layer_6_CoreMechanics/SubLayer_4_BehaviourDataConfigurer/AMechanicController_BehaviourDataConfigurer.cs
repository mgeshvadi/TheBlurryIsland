using Blurry.Application.Layer_1_Ground.Configurer.SubLayer_2;
using Blurry.Application.Layer_6_CoreMechanics.SubLayer_0_Config;
using Blurry.Application.Layer_6_CoreMechanics.SubLayer_0_Entity;
using Blurry.Application.Layer_6_CoreMechanics.SubLayer_3_BehaviourData;


namespace Blurry.Application.Layer_6_CoreMechanics.SubLayer_4_BehaviourDataConfigurer
{
    public abstract class AMechanicController_BehaviourDataConfigurer<TData, TConfig, TEntity> : AConfigurer<TData, TConfig, TEntity> where TData : IMechanicController_BehaviourData
                                                                                                                                      where TConfig : IMechanicController_Config
                                                                                                                                      where TEntity : AMechanicController_Entity
    {
        protected AMechanicController_BehaviourDataConfigurer(TConfig config, TEntity entity) : base(config, entity)
        {
        }
    }
}