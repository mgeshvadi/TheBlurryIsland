using Blurry.Application.Layer_1_Ground.Presentation.SubLayer_0;
using Blurry.Application.Layer_1_Ground.Presentation.SubLayer_4;
using Blurry.Application.Layer_6_CoreMechanics.SubLayer_0_PresentationData;


namespace Blurry.Application.Layer_6_CoreMechanics.SubLayer_2_Presentation
{
    public interface IMechanic_Presentation : IPresentationM
    {
    }

    public abstract class AMechanic_Presentation<TData> : APresentation<TData>, IMechanic_Presentation where TData : IMechanic_PresentationData
    {
        protected AMechanic_Presentation(TData data) : base(data)
        {
        }
    }
}