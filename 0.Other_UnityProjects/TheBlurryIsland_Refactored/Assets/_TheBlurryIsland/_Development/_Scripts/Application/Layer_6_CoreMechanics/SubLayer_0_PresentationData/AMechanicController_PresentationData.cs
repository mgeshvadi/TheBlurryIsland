using Blurry.Application.Layer_1_Ground.Data.SubLayer_2;
using Blurry.Application.Layer_1_Ground.Data.SubLayer_3;


namespace Blurry.Application.Layer_6_CoreMechanics.SubLayer_0_PresentationData
{
    public interface IMechanicController_PresentationData : IPresentation_Data
    {
    }

    public abstract class AMechanicController_PresentationData : APresentation_Data, IMechanicController_PresentationData
    {
    }
}