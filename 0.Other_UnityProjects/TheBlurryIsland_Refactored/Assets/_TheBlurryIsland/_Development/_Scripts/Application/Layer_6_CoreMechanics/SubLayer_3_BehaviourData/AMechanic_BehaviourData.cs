using Blurry.Application.Layer_1_Ground.Data.SubLayer_2;
using Blurry.Application.Layer_1_Ground.Data.SubLayer_3;
using Blurry.Application.Layer_6_CoreMechanics.SubLayer_2_Presentation;


namespace Blurry.Application.Layer_6_CoreMechanics.SubLayer_3_BehaviourData
{
    public interface IMechanic_BehaviourData : IBehaviour_Data
    {
    }

    public abstract class AMechanic_BehaviourData<TPresentation> : ABehaviour_Data<TPresentation>, IMechanic_BehaviourData where TPresentation : IMechanic_Presentation
    {
        protected AMechanic_BehaviourData(TPresentation presentation) : base(presentation)
        {
        }
    }
}