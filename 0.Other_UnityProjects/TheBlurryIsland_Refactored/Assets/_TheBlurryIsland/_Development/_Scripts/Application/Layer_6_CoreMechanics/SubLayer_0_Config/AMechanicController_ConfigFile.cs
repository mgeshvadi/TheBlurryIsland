using Blurry.Application.Layer_1_Ground.Config.SubLayer_0;
using Blurry.Application.Layer_1_Ground.Config.SubLayer_1;


namespace Blurry.Application.Layer_6_CoreMechanics.SubLayer_0_Config
{
    public interface IMechanicController_Config : IConfig
    {
    }

    public abstract class AMechanicController_ConfigFile<TConfig> : AConfig_File<TConfig> where TConfig : IMechanicController_Config
    {
    }
}