using System.Collections.Generic;
using Blurry.Application.Layer_1_Ground.Behaviour.SubLayer_1;
using Blurry.Application.Layer_1_Ground.Behaviour.SubLayer_5;
using Blurry.Application.Layer_6_CoreMechanics.SubLayer_3_BehaviourData;
using Blurry.Application.Layer_6_CoreMechanics.SubLayer_5_Behaviour;


namespace Blurry.Application.Layer_6_CoreMechanics.SubLayer_6_ControllerBehaviour
{
    public interface IMechanicController_Behaviour : IBehaviourController_Behaviour
    {
    }

    public abstract class AMechanicController_Behaviour<TData, TBehaviours> : ABehaviourController_BehaviourList<TData, TBehaviours>, IMechanicController_Behaviour where TData : IMechanicController_BehaviourData
                                                                                                                                                                    where TBehaviours : IMechanic_Behaviour

    {
        protected AMechanicController_Behaviour(TData data, List<TBehaviours> behaviours) : base(data, behaviours)
        {
        }
    }
}