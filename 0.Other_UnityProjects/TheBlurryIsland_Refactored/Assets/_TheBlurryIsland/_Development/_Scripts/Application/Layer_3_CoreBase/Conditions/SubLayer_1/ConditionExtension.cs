using System.Collections.Generic;
using System.Linq;
using Blurry.Application.Layer_3_CoreBase.Conditions.SubLayer_0;


namespace Blurry.Application.Layer_3_CoreBase.Conditions.SubLayer_1
{
    public static class ConditionExtension
    {
        public static bool AreAllSatisfied<T>(this IEnumerable<T> conditions) where T : ICondition
        {
            return conditions.All(condition => condition.IsSatisfied());
        }

        public static bool IsAnySatisfied<T>(this IEnumerable<T> conditions) where T : ICondition
        {
            return conditions.Any(condition => condition.IsSatisfied());
        }
    }
}
