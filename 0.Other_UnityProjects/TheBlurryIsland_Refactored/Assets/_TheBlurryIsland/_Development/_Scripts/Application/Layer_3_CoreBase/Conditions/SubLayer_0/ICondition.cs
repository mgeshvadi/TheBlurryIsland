namespace Blurry.Application.Layer_3_CoreBase.Conditions.SubLayer_0
{
    public interface ICondition
    {
        public bool IsSatisfied();
    }
}
