using Blurry.Application.Layer_1_Ground.Data.SubLayer_2;
using Blurry.Application.Layer_1_Ground.Data.SubLayer_3;
using Blurry.Application.Layer_7_Levels.SubLayer_2_Presentation;


namespace Blurry.Application.Layer_7_Levels.SubLayer_3_BehaviourData
{
    public interface ILevel_BehaviourData : IBehaviour_Data
    {
    }

    public abstract class ALevel_BehaviourData<TPresentation> : ABehaviour_Data<TPresentation>, ILevel_BehaviourData where TPresentation : ILevel_Presentation
    {
        protected ALevel_BehaviourData(TPresentation presentation) : base(presentation)
        {
        }
    }
}