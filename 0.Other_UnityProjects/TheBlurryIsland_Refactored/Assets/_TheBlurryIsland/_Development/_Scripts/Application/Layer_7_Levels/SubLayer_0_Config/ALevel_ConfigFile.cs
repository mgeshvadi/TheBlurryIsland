using Blurry.Application.Layer_1_Ground.Config.SubLayer_0;
using Blurry.Application.Layer_1_Ground.Config.SubLayer_1;


namespace Blurry.Application.Layer_7_Levels.SubLayer_0_Config
{
    public interface ILevel_Config : IConfig
    {
    }

    public abstract class ALevel_ConfigFile<TConfig> : AConfig_File<TConfig> where TConfig : ILevel_Config
    {
    }
}