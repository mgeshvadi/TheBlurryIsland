using System;
using System.Collections.Generic;
using Blurry.Application.Layer_1_Ground.Behaviour.SubLayer_1;
using Blurry.Application.Layer_1_Ground.Behaviour.SubLayer_5;
using Blurry.Application.Layer_6_CoreMechanics.SubLayer_6_ControllerBehaviour;
using Blurry.Application.Layer_7_Levels.SubLayer_3_BehaviourData;


namespace Blurry.Application.Layer_7_Levels.SubLayer_5_Behaviour
{
    public interface ILevel_ControllerBehaviour : IBehaviourController_Behaviour
    {

    }

    public abstract class ALevel_ControllerBehaviour<TData> : ABehaviourController_BehaviourDictionary<TData, IMechanicController_Behaviour>, ILevel_ControllerBehaviour where TData : ILevel_BehaviourData
    {
        protected ALevel_ControllerBehaviour(TData data, Dictionary<Type, IMechanicController_Behaviour> behaviours) : base(data, behaviours)
        {
        }

        protected T GetLevelController<T>() where T : class, IMechanicController_Behaviour
        {
            return behaviours[typeof(T)] as T;
        }
    }
}