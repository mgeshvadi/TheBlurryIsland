using Blurry.Application.Layer_1_Ground.Data.SubLayer_2;
using Blurry.Application.Layer_1_Ground.Data.SubLayer_3;


namespace Blurry.Application.Layer_7_Levels.SubLayer_0_PresentationData
{
    public interface ILevel_PresentationData : IPresentation_Data
    {
    }

    public abstract class ALevel_PresentationData : APresentation_Data, ILevel_PresentationData
    {
    }
}