using Blurry.Application.Layer_1_Ground.Presentation.SubLayer_0;
using Blurry.Application.Layer_1_Ground.Presentation.SubLayer_4;
using Blurry.Application.Layer_7_Levels.SubLayer_0_PresentationData;


namespace Blurry.Application.Layer_7_Levels.SubLayer_2_Presentation
{
    public interface ILevel_Presentation : IPresentationM
    {
    }

    public abstract class ALevel_Presentation<TData> : APresentation<TData>, ILevel_Presentation where TData : ILevel_PresentationData
    {
        protected ALevel_Presentation(TData data) : base(data)
        {
        }
    }
}