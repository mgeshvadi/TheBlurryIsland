using Blurry.Application.Layer_1_Ground.Configurer.SubLayer_2;
using Blurry.Application.Layer_7_Levels.SubLayer_0_Config;
using Blurry.Application.Layer_7_Levels.SubLayer_0_Entity;
using Blurry.Application.Layer_7_Levels.SubLayer_3_BehaviourData;


namespace Blurry.Application.Layer_7_Levels.SubLayer_4_BehaviourDataConfigurer
{
    public abstract class ALevel_BehaviourDataConfigurer<TData, TConfig, TEntity> : AConfigurer<TData, TConfig, TEntity> where TData : ILevel_BehaviourData
                                                                                                                         where TConfig : ILevel_Config
                                                                                                                         where TEntity : ALevel_Entity
    {
        protected ALevel_BehaviourDataConfigurer(TConfig config, TEntity entity) : base(config, entity)
        {
        }
    }
}