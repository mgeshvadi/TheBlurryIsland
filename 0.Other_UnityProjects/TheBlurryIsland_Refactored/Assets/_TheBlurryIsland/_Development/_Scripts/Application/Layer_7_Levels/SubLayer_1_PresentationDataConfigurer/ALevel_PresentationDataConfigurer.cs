using Blurry.Application.Layer_1_Ground.Configurer.SubLayer_2;
using Blurry.Application.Layer_7_Levels.SubLayer_0_Config;
using Blurry.Application.Layer_7_Levels.SubLayer_0_Entity;
using Blurry.Application.Layer_7_Levels.SubLayer_0_PresentationData;


namespace Blurry.Application.Layer_7_Levels.SubLayer_1_PresentationDataConfigurer
{
    public abstract class ALevel_PresentationDataConfigurer<TData, TConfig, TEntity> : AConfigurer<TData, TConfig, TEntity> where TData : ILevel_PresentationData
                                                                                                                            where TConfig : ILevel_Config
                                                                                                                            where TEntity : ALevel_Entity
    {
        protected ALevel_PresentationDataConfigurer(TConfig config, TEntity entity) : base(config, entity)
        {
        }
    }
}