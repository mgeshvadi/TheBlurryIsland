using Blurry.Application.Layer_1_Ground.Presentation.SubLayer_0;
using Blurry.Application.Layer_1_Ground.Presentation.SubLayer_4;
using Blurry.Application.Layer_4_CharacterBehaviours.SubLayer_0_PresentationData;


namespace Blurry.Application.Layer_4_CharacterBehaviours.SubLayer_2_Presentation
{
    public interface ICharacterBehaviour_Presentation : IPresentationM
    {
    }

    public abstract class ACharacterBehaviour_Presentation<TData> : APresentation<TData>, ICharacterBehaviour_Presentation where TData : ICharacterBehaviour_PresentationData
    {
        protected ACharacterBehaviour_Presentation(TData data) : base(data)
        {
        }
    }
}