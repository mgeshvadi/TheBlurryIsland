using Blurry.Application.Layer_1_Ground.Behaviour.SubLayer_0;
using Blurry.Application.Layer_1_Ground.Behaviour.SubLayer_3;
using Blurry.Application.Layer_4_CharacterBehaviours.SubLayer_3_BehaviourData;


namespace Blurry.Application.Layer_4_CharacterBehaviours.SubLayer_5_Behaviour
{
    public interface ICharacterBehaviour_Behaviour : IBehaviour
    {
    }

    public abstract class ACharacterBehaviour_Behaviour<TData> : ABehaviour<TData>, ICharacterBehaviour_Behaviour where TData : ICharacterBehaviour_BehaviourData
    {
        protected ACharacterBehaviour_Behaviour(TData data) : base(data)
        {
        }
    }
}