using Blurry.Application.Layer_1_Ground.Config.SubLayer_0;
using Blurry.Application.Layer_1_Ground.Config.SubLayer_1;


namespace Blurry.Application.Layer_4_CharacterBehaviours.SubLayer_0_Config
{
    public interface ICharacterBehaviour_Config : IConfig
    {
    }

    public abstract class ACharacterBehaviour_ConfigFile<TConfig> : AConfig_File<TConfig> where TConfig : ICharacterBehaviour_Config
    {
    }
}