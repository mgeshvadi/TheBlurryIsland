using Blurry.Application.Layer_1_Ground.Data.SubLayer_2;
using Blurry.Application.Layer_1_Ground.Data.SubLayer_3;
using Blurry.Application.Layer_4_CharacterBehaviours.SubLayer_2_Presentation;


namespace Blurry.Application.Layer_4_CharacterBehaviours.SubLayer_3_BehaviourData
{
    public interface ICharacterBehaviour_BehaviourData : IBehaviour_Data
    {
    }

    public abstract class ACharacterBehaviour_BehaviourData<TPresentation> : ABehaviour_Data<TPresentation>, ICharacterBehaviour_BehaviourData where TPresentation : ICharacterBehaviour_Presentation
    {
        protected ACharacterBehaviour_BehaviourData(TPresentation presentation) : base(presentation)
        {
        }
    }
}