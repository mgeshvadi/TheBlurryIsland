using UnityEngine;
using Blurry.Application.Layer_1_Ground.Data.SubLayer_2;
using Blurry.Application.Layer_1_Ground.Data.SubLayer_3;


namespace Blurry.Application.Layer_4_CharacterBehaviours.SubLayer_0_PresentationData
{
    public interface ICharacterBehaviour_PresentationData : IPresentation_Data
    {
        public Animator Animator { get; set; }
    }

    public abstract class ACharacterBehaviour_PresentationData : APresentation_Data, ICharacterBehaviour_PresentationData
    {
        public Animator Animator { get; set; }
    }
}