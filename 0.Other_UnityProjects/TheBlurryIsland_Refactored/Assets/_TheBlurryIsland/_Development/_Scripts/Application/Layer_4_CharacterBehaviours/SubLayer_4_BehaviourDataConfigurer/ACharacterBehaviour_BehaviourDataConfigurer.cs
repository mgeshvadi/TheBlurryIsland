using Blurry.Application.Layer_1_Ground.Configurer.SubLayer_2;
using Blurry.Application.Layer_4_CharacterBehaviours.SubLayer_0_Config;
using Blurry.Application.Layer_4_CharacterBehaviours.SubLayer_3_BehaviourData;


namespace Blurry.Application.Layer_4_CharacterBehaviours.SubLayer_4_BehaviourDataConfigurer
{
    public abstract class ACharacterBehaviour_BehaviourDataConfigurer<TData, TConfig> : AConfigurer<TData, TConfig> where TData : ICharacterBehaviour_BehaviourData
                                                                                                                    where TConfig : ICharacterBehaviour_Config
    {
        protected ACharacterBehaviour_BehaviourDataConfigurer(TConfig config) : base(config)
        {
        }
    }
}