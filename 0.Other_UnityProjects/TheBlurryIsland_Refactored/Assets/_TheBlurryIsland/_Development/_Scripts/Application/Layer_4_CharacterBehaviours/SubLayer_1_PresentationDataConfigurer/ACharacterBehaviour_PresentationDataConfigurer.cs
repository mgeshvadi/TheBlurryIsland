using UnityEngine;
using Blurry.Application.Layer_1_Ground.Configurer.SubLayer_2;
using Blurry.Application.Layer_4_CharacterBehaviours.SubLayer_0_Config;
using Blurry.Application.Layer_4_CharacterBehaviours.SubLayer_0_PresentationData;


namespace Blurry.Application.Layer_4_CharacterBehaviours.SubLayer_1_PresentationDataConfigurer
{
    public abstract class ACharacterBehaviour_PresentationDataConfigurer<TData, TConfig> : AConfigurer<TData, TConfig> where TData : ICharacterBehaviour_PresentationData
                                                                                                                       where TConfig : ICharacterBehaviour_Config
    {
        private readonly Animator animator;

        protected ACharacterBehaviour_PresentationDataConfigurer(Animator animator, TConfig config) : base(config)
        {
            this.animator = animator;
        }

        protected override void Configure_(TData toConfigure)
        {
            toConfigure.Animator = animator;
            Configure__(toConfigure);
        }

        protected abstract void Configure__(TData toConfigure);
    }
}