namespace Blurry.Application.Layer_1_Ground.Configurer.SubLayer_0
{
    public interface IConfigurable
    {
        public bool IsConfigured { set; get; }
    }
}