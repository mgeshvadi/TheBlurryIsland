using System;
using Blurry.Application.Layer_1_Ground.Behaviour.SubLayer_0;
using Blurry.Application.Layer_1_Ground.Behaviour.SubLayer_1;
using Blurry.Application.Layer_1_Ground.Data.SubLayer_2;
using Blurry.Application.Layer_1_Ground.Behaviour.SubLayer_3;


namespace Blurry.Application.Layer_1_Ground.Behaviour.SubLayer_4
{
    public abstract class ABehaviourController_Behaviour<TData, TBehaviours> : ABehaviour<TData>, IBehaviourController_Behaviour where TData : IBehaviour_Data
                                                                                                                                 where TBehaviours : IBehaviour
    {
        protected ABehaviourController_Behaviour(TData data) : base(data)
        {
        }

        protected override void Activate_()
        {
            DoForEachBehaviour(behaviour => behaviour.TryToActivate());
            Activate__();
        }

        protected override void Start_()
        {
            DoForEachBehaviour(behaviour => behaviour.TryToStart());
            StartInner__();
        }

        protected override void Stop_()
        {
            DoForEachBehaviour(behaviour => behaviour.TryToStop());
            StopInner__();
        }

        protected override void Reset_()
        {
            DoForEachBehaviour(behaviour => behaviour.Reset());
            ResetInner__();
        }

        protected override void Restart_()
        {
            DoForEachBehaviour(behaviour => behaviour.TryToRestart());
            RestartInner__();
        }

        protected override void Deactivate_()
        {
            DoForEachBehaviour(behaviour => behaviour.TryToDeactivate());
            Deactivate__();
        }

        public override void Update(float deltaTime)
        {
            DoForEachBehaviour(behaviour => behaviour.Update(deltaTime));
            Update_(deltaTime);
        }

        public override void FixedUpdate(float fixedDeltaTime)
        {
            DoForEachBehaviour(behaviour => behaviour.FixedUpdate(fixedDeltaTime));
            FixedUpdate_(fixedDeltaTime);
        }

        protected abstract void DoForEachBehaviour(Action<TBehaviours> action);

        protected abstract void Activate__();
        protected abstract void StartInner__();
        protected abstract void StopInner__();
        protected abstract void ResetInner__();
        protected abstract void RestartInner__();
        protected abstract void Deactivate__();

        protected abstract void Update_(float deltaTime);
        protected abstract void FixedUpdate_(float fixedDeltaTime);
    }
}