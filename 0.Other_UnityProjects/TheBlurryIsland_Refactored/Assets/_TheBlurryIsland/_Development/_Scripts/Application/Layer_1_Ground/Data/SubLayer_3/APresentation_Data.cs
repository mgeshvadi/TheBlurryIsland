using Blurry.Application.Layer_1_Ground.Data.SubLayer_2;


namespace Blurry.Application.Layer_1_Ground.Data.SubLayer_3
{
    public abstract class APresentation_Data : IPresentation_Data
    {
        public bool IsConfigured { get; set; }
    }
}