using Blurry.Application.Layer_0_Base.ServiceLocating.SubLayer_0;
using Blurry.Application.Layer_1_Ground.EventManagement.SubLayer_0;
using Blurry.Application.Layer_1_Ground.EventManagement.SubLayer_1;


namespace Blurry.Application.Layer_1_Ground.EventManagement.SubLayer_2
{
    public interface IEventManager_Service : IService
    {
        void Propagate(IGameEvent evt, object sender);
        void Register(IEventListener listener);
        void UnRegister(IEventListener listener);
        bool Has(IEventListener listener);
        void Clear();
    }
}