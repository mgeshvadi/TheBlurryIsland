using System;
using System.Collections.Generic;
using Blurry.Application.Layer_1_Ground.Behaviour.SubLayer_0;
using Blurry.Application.Layer_1_Ground.Data.SubLayer_2;
using Blurry.Application.Layer_1_Ground.Behaviour.SubLayer_4;


namespace Blurry.Application.Layer_1_Ground.Behaviour.SubLayer_5
{
    public abstract class ABehaviourController_BehaviourDictionary<TData, TBehaviours> : ABehaviourController_Behaviour<TData, TBehaviours> where TData : IBehaviour_Data
                                                                                                                                            where TBehaviours : IBehaviour
    {
        protected readonly Dictionary<Type, TBehaviours> behaviours;

        protected ABehaviourController_BehaviourDictionary(TData data, Dictionary<Type, TBehaviours> behaviours) : base(data)
        {
            this.behaviours = behaviours;
        }

        protected override void DoForEachBehaviour(Action<TBehaviours> action)
        {
            foreach (KeyValuePair<Type, TBehaviours> behaviour in behaviours)
                action.Invoke(behaviour.Value);
        }
    }
}