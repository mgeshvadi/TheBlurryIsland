using UnityEngine;


namespace Blurry.Application.Layer_1_Ground.Entity.SubLayer_0
{
    public abstract class AEntity : MonoBehaviour
    {
        public TTargetsEntity[] FindChildEntities<TTargetsEntity>() where TTargetsEntity : AEntity
        {
            return GetComponentsInChildren<TTargetsEntity>();
        }
    }
}