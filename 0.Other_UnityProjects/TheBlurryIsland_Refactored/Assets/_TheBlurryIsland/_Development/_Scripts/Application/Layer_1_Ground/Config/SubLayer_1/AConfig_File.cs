using System.Collections.Generic;
using UnityEngine;
using Blurry.Application.Layer_1_Ground.Config.SubLayer_0;


namespace Blurry.Application.Layer_1_Ground.Config.SubLayer_1
{
    public abstract class AConfig_File<TConfig> : ScriptableObject where TConfig : IConfig
    {
        [SerializeField] private int activePresetIndex = 0;
        [SerializeField] private List<TConfig> presets = new List<TConfig>();

        public TConfig GetConfig()
        {
            return presets[activePresetIndex];
        }
    }
}