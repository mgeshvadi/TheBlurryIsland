using Blurry.Application.Layer_1_Ground.Configurer.SubLayer_0;


namespace Blurry.Application.Layer_1_Ground.Configurer.SubLayer_1
{
    public interface IConfigurer<in TConfigurable> where TConfigurable : IConfigurable
    {
        public void Configure(TConfigurable toConfigure);
    }
}