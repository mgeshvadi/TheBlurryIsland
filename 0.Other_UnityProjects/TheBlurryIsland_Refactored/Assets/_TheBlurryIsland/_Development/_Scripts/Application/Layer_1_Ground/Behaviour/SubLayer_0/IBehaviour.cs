namespace Blurry.Application.Layer_1_Ground.Behaviour.SubLayer_0
{
    public interface IBehaviour
    {
        public void TryToActivate();
        public void TryToStart();
        public void TryToStop();
        public void TryToRestart();
        public void TryToDeactivate();

        public void Reset();
        public void Update(float deltaTime);
        public void FixedUpdate(float fixedDeltaTime);
    }
}