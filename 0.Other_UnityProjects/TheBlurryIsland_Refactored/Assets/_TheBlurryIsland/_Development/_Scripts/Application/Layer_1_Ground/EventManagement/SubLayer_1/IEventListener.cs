using Blurry.Application.Layer_1_Ground.EventManagement.SubLayer_0;


namespace Blurry.Application.Layer_1_Ground.EventManagement.SubLayer_1
{
    public interface IEventListener
    {
        void OnEvent(IGameEvent evt, object sender);
    }
}