using Blurry.Application.Layer_1_Ground.Config.SubLayer_0;
using Blurry.Application.Layer_1_Ground.Configurer.SubLayer_0;
using Blurry.Application.Layer_1_Ground.Entity.SubLayer_0;
using Blurry.Application.Layer_1_Ground.Configurer.SubLayer_1;


namespace Blurry.Application.Layer_1_Ground.Configurer.SubLayer_2
{
    public abstract class AConfigurer<TConfigurable> : IConfigurer<TConfigurable> where TConfigurable : IConfigurable
    {
        public void Configure(TConfigurable toConfigure)
        {
            Configure_(toConfigure);
            toConfigure.IsConfigured = true;
        }

        protected abstract void Configure_(TConfigurable toConfigure);
    }

    public abstract class AConfigurer<TConfigurable, TConfig> : AConfigurer<TConfigurable> where TConfigurable : IConfigurable
                                                                                           where TConfig : IConfig
    {
        protected TConfig config;

        protected AConfigurer(TConfig config)
        {
            this.config = config;
        }
    }

    public abstract class AConfigurer<TConfigurable, TConfig, TEntity> : AConfigurer<TConfigurable, TConfig> where TConfigurable : IConfigurable
                                                                                                             where TConfig : IConfig
                                                                                                             where TEntity : AEntity
    {
        protected TEntity entity;

        protected AConfigurer(TConfig config, TEntity entity) : base(config)
        {
            this.entity = entity;
        }
    }
}