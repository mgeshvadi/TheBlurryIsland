using System;
using System.Collections.Generic;
using Blurry.Application.Layer_1_Ground.Behaviour.SubLayer_0;
using Blurry.Application.Layer_1_Ground.Data.SubLayer_2;
using Blurry.Application.Layer_1_Ground.Behaviour.SubLayer_4;


namespace Blurry.Application.Layer_1_Ground.Behaviour.SubLayer_5
{
    public abstract class ABehaviourController_BehaviourList<TData, TBehaviours> : ABehaviourController_Behaviour<TData, TBehaviours> where TData : IBehaviour_Data
                                                                                                                                      where TBehaviours : IBehaviour
    {
        protected readonly List<TBehaviours> behaviours;

        protected ABehaviourController_BehaviourList(TData data, List<TBehaviours> behaviours) : base(data)
        {
            this.behaviours = behaviours;
        }

        protected override void DoForEachBehaviour(Action<TBehaviours> action)
        {
            foreach (TBehaviours behaviour in behaviours)
                action.Invoke(behaviour);
        }
    }
}