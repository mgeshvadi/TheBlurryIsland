using UnityEngine;
using Blurry.Application.Layer_1_Ground.Presentation.SubLayer_0;
using Blurry.Application.Layer_1_Ground.Data.SubLayer_2;


namespace Blurry.Application.Layer_1_Ground.Presentation.SubLayer_4
{
    public abstract class APresentation<TData> : IPresentationM where TData : IPresentation_Data
    {
        protected readonly TData data;

        protected APresentation(TData data)
        {
            if (!data.IsConfigured)
                Debug.LogError(message: $"Not Configured Data | Expected the data {data.GetType()} to be configured before passed into Presentation: {GetType()}"); // Todo : user our logging system
            this.data = data;
        }
    }
}