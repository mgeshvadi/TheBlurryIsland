using Blurry.Application.Layer_1_Ground.Presentation.SubLayer_0;
using Blurry.Application.Layer_1_Ground.Data.SubLayer_2;


namespace Blurry.Application.Layer_1_Ground.Data.SubLayer_3
{
    public abstract class ABehaviour_Data<TPresentation> : IBehaviour_Data where TPresentation : IPresentationM
    {
        private readonly TPresentation presentation;

        public TPresentation Presentation => presentation;
        public bool IsConfigured { get; set; }

        protected ABehaviour_Data(TPresentation presentation)
        {
            this.presentation = presentation;
        }
    }
}