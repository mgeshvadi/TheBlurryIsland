using System;
using UnityEngine;
using Blurry.Application.Layer_1_Ground.Behaviour.SubLayer_0;
using Blurry.Application.Layer_1_Ground.Data.SubLayer_2;


namespace Blurry.Application.Layer_1_Ground.Behaviour.SubLayer_3
{
    public abstract class ABehaviour<TData> : IBehaviour where TData : IBehaviour_Data
    {
        protected readonly TData data;

        private bool isActive;
        private bool isStarted;

        protected ABehaviour(TData data)
        {
            if (!data.IsConfigured)
                Debug.LogError(message: $"Not Configured Data | Expected the data {data.GetType()} to be configured before passed into behaviour: {GetType()}"); // Todo : user our logging system
            this.data = data;
        }

        public void TryToActivate()
        {
            if (CanActivate())
                Activate();
            else
                LogAboutBehavior(LogType.Error, "Couldn't Get Activated"); // todo: Even move these strings to Log System
        }

        public void TryToStart()
        {
            if (CanStart())
                Start();
            else
                LogAboutBehavior(LogType.Error, "Couldn't Get Started");
        }

        public void TryToStop()
        {
            if (CanStop())
                Stop();
            else
                LogAboutBehavior(LogType.Error, "Couldn't Get Stopped");
        }

        public void TryToRestart()
        {
            if (CanRestart())
                Restart();
            else
                LogAboutBehavior(LogType.Error, "Couldn't Get ReStarted");
        }

        public void TryToDeactivate()
        {
            if (CanDeactivate())
                Deactivate();
            else
                LogAboutBehavior(LogType.Error, "Couldn't Get Deactivated");
        }

        private bool CanActivate() { return !isActive; }
        private bool CanStart() { return isActive && !isStarted; }
        private bool CanStop() { return isActive && isStarted; }
        private bool CanRestart() { return isActive; }
        private bool CanDeactivate() { return isActive; }

        private void Activate()
        {
            isActive = true;
            Activate_();
            LogAboutBehavior(LogType.Normal, "Activated");
        }

        private void Start()
        {
            isStarted = true;
            Start_();
            LogAboutBehavior(LogType.Normal, "Got Started");
        }

        private void Stop()
        {
            isStarted = false;
            Stop_();
            LogAboutBehavior(LogType.Normal, "Got Stopped");
        }

        private void Restart()
        {
            Stop();
            Reset();
            Restart_();
            LogAboutBehavior(LogType.Normal, "Got ReStarted");
            Start();
        }

        public void Reset()
        {
            Reset_();
            LogAboutBehavior(LogType.Normal, "Got Reseted");
        }

        private void Deactivate()
        {
            isActive = false;
            Deactivate_();
            LogAboutBehavior(LogType.Normal, "Got Deactivated");
        }

        private enum LogType // TODO : Move this to loggerSystem
        {
            Error,
            Warning,
            Normal
        }

        private void LogAboutBehavior(LogType logType, string message)
        {
            message = $"System Of Type {GetType()} {message}";
            switch (logType)
            {
                case LogType.Error:
                    Debug.LogError(message);
                    break;
                case LogType.Warning:
                    Debug.LogWarning(message);
                    break;
                case LogType.Normal:
                    Debug.Log(message);
                    break;
                default: throw new ArgumentOutOfRangeException(nameof(logType), logType, null);
            }
        }

        protected abstract void Activate_();
        protected abstract void Start_();
        protected abstract void Stop_();
        protected abstract void Reset_();
        protected abstract void Restart_();
        protected abstract void Deactivate_();

        public abstract void Update(float deltaTime);
        public abstract void FixedUpdate(float fixedDeltaTime);
    }
}