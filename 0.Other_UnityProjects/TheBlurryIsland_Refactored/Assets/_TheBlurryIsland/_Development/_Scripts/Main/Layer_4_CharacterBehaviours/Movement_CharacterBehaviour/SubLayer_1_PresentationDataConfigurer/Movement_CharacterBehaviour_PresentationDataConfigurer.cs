using Blurry.Application.Layer_4_CharacterBehaviours.SubLayer_1_PresentationDataConfigurer;
using Blurry.Main.Layer_4_CharacterBehaviours.Movement_CharacterBehaviour.SubLayer_0_Config;
using Blurry.Main.Layer_4_CharacterBehaviours.Movement_CharacterBehaviour.SubLayer_0_PresentationData;
using UnityEngine;


namespace Blurry.Main.Layer_4_CharacterBehaviours.Sample_CharacterBehaviour.SubLayer_1_PresentationDataConfigurer
{
    public class Movement_CharacterBehaviour_PresentationDataConfigurer : ACharacterBehaviour_PresentationDataConfigurer<Movement_CharacterBehaviour_PresentationData, Movement_CharacterBehaviour_Config>
    {
        public Movement_CharacterBehaviour_PresentationDataConfigurer(Animator animator, Movement_CharacterBehaviour_Config config) : base(animator, config)
        {
        }

        protected override void Configure__(Movement_CharacterBehaviour_PresentationData toConfigure)
        {
            toConfigure.animatorData = new Movement_CharacterBehaviour_PresentationData.AnimatorData();

            toConfigure.animatorData.movementState.parameterId = Animator.StringToHash(name: config.AnimatorConfigs.MovementState.ParameterName);
            toConfigure.animatorData.movementState.idleValue = config.AnimatorConfigs.MovementState.IdleValue;
            toConfigure.animatorData.movementState.walkValidThreshold = config.AnimatorConfigs.MovementState.WalkValidThreshold;
            toConfigure.animatorData.movementState.walkAndIdleTransitionSpeed = config.AnimatorConfigs.MovementState.WalkAndIdleTransitionSpeed;
        }
    }
}