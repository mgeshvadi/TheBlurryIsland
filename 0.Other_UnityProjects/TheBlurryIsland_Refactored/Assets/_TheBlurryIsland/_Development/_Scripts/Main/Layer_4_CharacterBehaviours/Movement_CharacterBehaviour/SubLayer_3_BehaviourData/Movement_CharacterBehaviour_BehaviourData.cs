using Blurry.Application.Layer_4_CharacterBehaviours.SubLayer_3_BehaviourData;
using Blurry.Main.Layer_4_CharacterBehaviours.Movement_CharacterBehaviour.SubLayer_2_Presentation;


namespace Blurry.Main.Layer_4_CharacterBehaviours.Movement_CharacterBehaviour.SubLayer_3_BehaviourData
{
    public class Movement_CharacterBehaviour_BehaviourData : ACharacterBehaviour_BehaviourData<Movement_CharacterBehaviour_Presentation>
    {
        protected Movement_CharacterBehaviour_BehaviourData(Movement_CharacterBehaviour_Presentation presentation) : base(presentation)
        {
        }
    }
}