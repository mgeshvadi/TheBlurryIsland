using Blurry.Application.Layer_4_CharacterBehaviours.SubLayer_1_PresentationDataConfigurer;
using Blurry.Main.Layer_4_CharacterBehaviours.Sample_CharacterBehaviour.SubLayer_0_Config;
using Blurry.Main.Layer_4_CharacterBehaviours.Sample_CharacterBehaviour.SubLayer_0_PresentationData;
using UnityEngine;


namespace Blurry.Main.Layer_4_CharacterBehaviours.Sample_CharacterBehaviour.SubLayer_1_PresentationDataConfigurer
{
    public class Sample_CharacterBehaviour_PresentationDataConfigurer : ACharacterBehaviour_PresentationDataConfigurer<Sample_CharacterBehaviour_PresentationData, Sample_CharacterBehaviour_Config>
    {
        public Sample_CharacterBehaviour_PresentationDataConfigurer(Animator animator, Sample_CharacterBehaviour_Config config) : base(animator, config)
        {
        }

        protected override void Configure__(Sample_CharacterBehaviour_PresentationData toConfigure)
        {
            throw new System.NotImplementedException();
        }
    }
}