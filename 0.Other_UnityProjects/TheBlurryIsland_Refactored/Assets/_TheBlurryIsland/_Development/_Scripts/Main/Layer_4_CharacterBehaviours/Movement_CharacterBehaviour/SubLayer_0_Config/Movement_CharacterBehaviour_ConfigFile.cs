using System;
using Blurry.Application.Layer_4_CharacterBehaviours.SubLayer_0_Config;
using Blurry.Main.Layer_4_CharacterBehaviours.General.SubLayer_1_ConfigFileLoader;
using UnityEngine;


namespace Blurry.Main.Layer_4_CharacterBehaviours.Movement_CharacterBehaviour.SubLayer_0_Config
{
    [Serializable]
    public class Movement_CharacterBehaviour_Config : ICharacterBehaviour_Config
    {
        [Serializable]
        public class AnimatorConfig
        {
            [Serializable]
            public class AnimatorMovementState
            {
                [SerializeField] private string parameterName;
                [SerializeField] private float idleValue;
                [SerializeField] private Threshold walkValidThreshold;
                [SerializeField] private float walkAndIdleTransitionSpeed;

                public string ParameterName => parameterName;
                public float IdleValue => idleValue;
                public Threshold WalkValidThreshold => walkValidThreshold;
                public float WalkAndIdleTransitionSpeed => walkAndIdleTransitionSpeed;
            }

            [SerializeField] private AnimatorMovementState movementState;

            public AnimatorMovementState MovementState => movementState;
        }

        [SerializeField] private AnimatorConfig animatorConfigs;

        public AnimatorConfig AnimatorConfigs => animatorConfigs;
    }

    [CreateAssetMenu(fileName = nameof(Movement_CharacterBehaviour_ConfigFile), menuName = CharacterBehaviours_ConfigFileLoader.CONFIG_FILES_CONTEXT_MENU + nameof(Movement_CharacterBehaviour_ConfigFile))]
    public class Movement_CharacterBehaviour_ConfigFile : ACharacterBehaviour_ConfigFile<Movement_CharacterBehaviour_Config>
    {
    }
}