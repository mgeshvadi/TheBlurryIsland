using Blurry.Application.Layer_4_CharacterBehaviours.SubLayer_5_Behaviour;
using Blurry.Main.Layer_4_CharacterBehaviours.Movement_CharacterBehaviour.SubLayer_3_BehaviourData;


namespace Blurry.Main.Layer_4_CharacterBehaviours.Movement_CharacterBehaviour.SubLayer_5_Behaviour
{
    public class Movement_CharacterBehaviour_Behaviour : ACharacterBehaviour_Behaviour<Movement_CharacterBehaviour_BehaviourData>
    {
        public Movement_CharacterBehaviour_Behaviour(Movement_CharacterBehaviour_BehaviourData data) : base(data)
        {
        }

        protected override void Activate_()
        {
        }

        protected override void Start_()
        {
        }

        protected override void Stop_()
        {
        }

        protected override void Reset_()
        {
        }

        protected override void Restart_()
        {
        }

        protected override void Deactivate_()
        {
        }

        public override void Update(float deltaTime)
        {
        }

        public override void FixedUpdate(float fixedDeltaTime)
        {
        }
    }
}