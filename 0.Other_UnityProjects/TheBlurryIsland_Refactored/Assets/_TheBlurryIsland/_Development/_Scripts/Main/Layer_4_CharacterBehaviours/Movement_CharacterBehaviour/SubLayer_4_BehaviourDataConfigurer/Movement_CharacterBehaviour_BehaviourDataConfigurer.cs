using Blurry.Application.Layer_4_CharacterBehaviours.SubLayer_4_BehaviourDataConfigurer;
using Blurry.Main.Layer_4_CharacterBehaviours.Movement_CharacterBehaviour.SubLayer_0_Config;
using Blurry.Main.Layer_4_CharacterBehaviours.Movement_CharacterBehaviour.SubLayer_3_BehaviourData;


namespace Blurry.Main.Layer_4_CharacterBehaviours.Movement_CharacterBehaviour.SubLayer_4_BehaviourDataConfigurer
{
    public class Movement_CharacterBehaviour_BehaviourDataConfigurer : ACharacterBehaviour_BehaviourDataConfigurer<Movement_CharacterBehaviour_BehaviourData, Movement_CharacterBehaviour_Config>
    {
        public Movement_CharacterBehaviour_BehaviourDataConfigurer(Movement_CharacterBehaviour_Config config) : base(config)
        {
        }

        protected override void Configure_(Movement_CharacterBehaviour_BehaviourData toConfigure)
        {
        }
    }
}