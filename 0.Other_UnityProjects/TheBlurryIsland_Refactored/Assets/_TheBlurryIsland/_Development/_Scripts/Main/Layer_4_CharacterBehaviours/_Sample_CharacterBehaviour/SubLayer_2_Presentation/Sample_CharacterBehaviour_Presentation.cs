using Blurry.Application.Layer_4_CharacterBehaviours.SubLayer_2_Presentation;
using Blurry.Main.Layer_4_CharacterBehaviours.Sample_CharacterBehaviour.SubLayer_0_PresentationData;


namespace Blurry.Main.Layer_4_CharacterBehaviours.Sample_CharacterBehaviour.SubLayer_2_Presentation
{
    public class Sample_CharacterBehaviour_Presentation : ACharacterBehaviour_Presentation<Sample_CharacterBehaviour_PresentationData>
    {
        protected Sample_CharacterBehaviour_Presentation(Sample_CharacterBehaviour_PresentationData data) : base(data)
        {
        }
    }
}