using Blurry.Application.Layer_4_CharacterBehaviours.SubLayer_3_BehaviourData;
using Blurry.Main.Layer_4_CharacterBehaviours.Sample_CharacterBehaviour.SubLayer_2_Presentation;


namespace Blurry.Main.Layer_4_CharacterBehaviours.Sample_CharacterBehaviour.SubLayer_3_BehaviourData
{
    public class Sample_CharacterBehaviour_BehaviourData : ACharacterBehaviour_BehaviourData<Sample_CharacterBehaviour_Presentation>
    {
        protected Sample_CharacterBehaviour_BehaviourData(Sample_CharacterBehaviour_Presentation presentation) : base(presentation)
        {
        }
    }
}