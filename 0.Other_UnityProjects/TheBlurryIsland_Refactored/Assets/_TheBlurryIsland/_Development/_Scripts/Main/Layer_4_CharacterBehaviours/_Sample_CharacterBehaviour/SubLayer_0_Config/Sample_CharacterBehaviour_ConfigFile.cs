using System;
using UnityEngine;
using Blurry.Application.Layer_4_CharacterBehaviours.SubLayer_0_Config;
using Blurry.Main.Layer_4_CharacterBehaviours.General.SubLayer_1_ConfigFileLoader;


namespace Blurry.Main.Layer_4_CharacterBehaviours.Sample_CharacterBehaviour.SubLayer_0_Config
{
    [Serializable]
    public class Sample_CharacterBehaviour_Config : ICharacterBehaviour_Config
    {
    }

    [CreateAssetMenu(fileName = nameof(Sample_CharacterBehaviour_ConfigFile), menuName = CharacterBehaviours_ConfigFileLoader.CONFIG_FILES_CONTEXT_MENU + nameof(Sample_CharacterBehaviour_ConfigFile))]
    public class Sample_CharacterBehaviour_ConfigFile : ACharacterBehaviour_ConfigFile<Sample_CharacterBehaviour_Config>
    {
    }
}