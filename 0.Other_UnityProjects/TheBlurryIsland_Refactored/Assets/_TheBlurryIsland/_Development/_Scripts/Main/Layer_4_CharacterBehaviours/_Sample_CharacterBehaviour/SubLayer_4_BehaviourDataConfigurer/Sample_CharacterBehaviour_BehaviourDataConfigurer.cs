using Blurry.Application.Layer_4_CharacterBehaviours.SubLayer_4_BehaviourDataConfigurer;
using Blurry.Main.Layer_4_CharacterBehaviours.Sample_CharacterBehaviour.SubLayer_0_Config;
using Blurry.Main.Layer_4_CharacterBehaviours.Sample_CharacterBehaviour.SubLayer_3_BehaviourData;


namespace Blurry.Main.Layer_4_CharacterBehaviours.Sample_CharacterBehaviour.SubLayer_4_BehaviourDataConfigurer
{
    public class Sample_CharacterBehaviour_BehaviourDataConfigurer : ACharacterBehaviour_BehaviourDataConfigurer<Sample_CharacterBehaviour_BehaviourData, Sample_CharacterBehaviour_Config>
    {
        public Sample_CharacterBehaviour_BehaviourDataConfigurer(Sample_CharacterBehaviour_Config config) : base(config)
        {
        }

        protected override void Configure_(Sample_CharacterBehaviour_BehaviourData toConfigure)
        {
            throw new System.NotImplementedException();
        }
    }
}