using Blurry.Application.Layer_4_CharacterBehaviours.SubLayer_5_Behaviour;
using Blurry.Main.Layer_4_CharacterBehaviours.Sample_CharacterBehaviour.SubLayer_3_BehaviourData;


namespace Blurry.Layer_4_CharacterBehaviours.Sample_CharacterBehaviour.SubLayer_5_Behaviour
{
    public class Sample_CharacterBehaviour_Behaviour : ACharacterBehaviour_Behaviour<Sample_CharacterBehaviour_BehaviourData>
    {
        public Sample_CharacterBehaviour_Behaviour(Sample_CharacterBehaviour_BehaviourData data) : base(data)
        {
        }

        protected override void Activate_()
        {
            throw new System.NotImplementedException();
        }

        protected override void Start_()
        {
            throw new System.NotImplementedException();
        }

        protected override void Stop_()
        {
            throw new System.NotImplementedException();
        }

        protected override void Reset_()
        {
            throw new System.NotImplementedException();
        }

        protected override void Restart_()
        {
            throw new System.NotImplementedException();
        }

        protected override void Deactivate_()
        {
            throw new System.NotImplementedException();
        }

        public override void Update(float deltaTime)
        {
            throw new System.NotImplementedException();
        }

        public override void FixedUpdate(float fixedDeltaTime)
        {
            throw new System.NotImplementedException();
        }
    }
}