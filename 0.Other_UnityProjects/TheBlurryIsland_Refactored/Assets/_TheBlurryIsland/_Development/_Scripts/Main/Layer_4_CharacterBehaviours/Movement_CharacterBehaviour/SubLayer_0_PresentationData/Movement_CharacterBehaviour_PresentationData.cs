using Blurry.Application.Layer_4_CharacterBehaviours.SubLayer_0_PresentationData;


namespace Blurry.Main.Layer_4_CharacterBehaviours.Movement_CharacterBehaviour.SubLayer_0_PresentationData
{
    public class Movement_CharacterBehaviour_PresentationData : ACharacterBehaviour_PresentationData
    {
        public class AnimatorData
        {
            public class AnimatorMovementState
            {
                public int parameterId;
                public float idleValue;
                public Threshold walkValidThreshold;
                public float walkAndIdleTransitionSpeed;

            }

            public AnimatorMovementState movementState;
        }

        public AnimatorData animatorData;
    }
}