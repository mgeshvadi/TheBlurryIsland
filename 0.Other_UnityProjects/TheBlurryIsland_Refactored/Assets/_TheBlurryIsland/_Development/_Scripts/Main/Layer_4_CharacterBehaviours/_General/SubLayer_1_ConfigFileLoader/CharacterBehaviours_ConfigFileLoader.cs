using Blurry.Application.Layer_4_CharacterBehaviours.SubLayer_0_Config;
using Blurry.Main.Layer_1_Ground.Config.SubLayer_2;


namespace Blurry.Main.Layer_4_CharacterBehaviours.General.SubLayer_1_ConfigFileLoader
{
    public static class CharacterBehaviours_ConfigFileLoader
    {
        public const string CONFIG_FILES_CONTEXT_MENU = ConfigFileLoader.CONFIG_FILES_CONTEXT_MENU + "CharacterBehaviours/";
        private const string CONFIG_FILES_DIRECTORY = ConfigFileLoader.CONFIG_FILES_DIRECTORY + "CharacterBehaviours/";

        public static T LoadConfigFile<T>() where T : ACharacterBehaviour_ConfigFile<ICharacterBehaviour_Config>
        {
            return LoadConfigFile<T, ICharacterBehaviour_Config>(CONFIG_FILES_DIRECTORY);
        }

        private static T LoadConfigFile<T, G>(string directory) where T : ACharacterBehaviour_ConfigFile<G> where G : ICharacterBehaviour_Config
        {
            return ConfigFileLoader.LoadConfigFile<T, G>(directory);
        }
    }
}