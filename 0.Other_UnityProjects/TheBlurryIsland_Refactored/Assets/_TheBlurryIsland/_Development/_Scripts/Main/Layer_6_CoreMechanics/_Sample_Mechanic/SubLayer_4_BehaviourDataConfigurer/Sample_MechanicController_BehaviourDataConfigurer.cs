using Blurry.Application.Layer_6_CoreMechanics.SubLayer_4_BehaviourDataConfigurer;
using Blurry.Main.Layer_6_CoreMechanics.Sample_Mechanic.SubLayer_0_Config;
using Blurry.Main.Layer_6_CoreMechanics.Sample_Mechanic.SubLayer_0_Entity;
using Blurry.Main.Layer_6_CoreMechanics.Sample_Mechanic.SubLayer_3_BehaviourData;


namespace Blurry.Main.Layer_6_CoreMechanics.Sample_Mechanic.SubLayer_4_BehaviourDataConfigurer
{
    public class Sample_MechanicController_BehaviourDataConfigurer : AMechanicController_BehaviourDataConfigurer<Sample_MechanicController_BehaviourData, Sample_MechanicController_Config, Sample_MechanicController_Entity>
    {
        public Sample_MechanicController_BehaviourDataConfigurer(Sample_MechanicController_Config config, Sample_MechanicController_Entity entity) : base(config, entity)
        {
        }

        protected override void Configure_(Sample_MechanicController_BehaviourData toConfigure)
        {
            throw new System.NotImplementedException();
        }
    }
}