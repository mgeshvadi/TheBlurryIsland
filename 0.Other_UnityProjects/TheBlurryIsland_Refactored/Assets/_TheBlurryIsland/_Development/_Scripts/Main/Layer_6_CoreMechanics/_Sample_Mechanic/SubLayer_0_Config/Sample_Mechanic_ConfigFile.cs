using System;
using UnityEngine;
using Blurry.Application.Layer_6_CoreMechanics.SubLayer_0_Config;
using Blurry.Main.Layer_6_CoreMechanics.General.SubLayer_1_ConfigFileLoader;


namespace Blurry.Main.Layer_6_CoreMechanics.Sample_Mechanic.SubLayer_0_Config
{
    [Serializable]
    public class Sample_Mechanic_Config : IMechanic_Config
    {
    }

    [CreateAssetMenu(fileName = nameof(Sample_Mechanic_ConfigFile), menuName = Mechanics_ConfigFileLoader.CONFIG_FILES_CONTEXT_MENU + nameof(Sample_Mechanic_ConfigFile))]
    public class Sample_Mechanic_ConfigFile : AMechanic_ConfigFile<Sample_Mechanic_Config>
    {
    }
}