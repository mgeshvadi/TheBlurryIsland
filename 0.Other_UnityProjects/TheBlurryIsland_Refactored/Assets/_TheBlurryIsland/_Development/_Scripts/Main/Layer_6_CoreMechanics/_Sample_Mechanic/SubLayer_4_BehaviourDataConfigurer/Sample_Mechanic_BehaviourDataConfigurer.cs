using Blurry.Application.Layer_6_CoreMechanics.SubLayer_4_BehaviourDataConfigurer;
using Blurry.Main.Layer_6_CoreMechanics.Sample_Mechanic.SubLayer_0_Config;
using Blurry.Main.Layer_6_CoreMechanics.Sample_Mechanic.SubLayer_0_Entity;
using Blurry.Main.Layer_6_CoreMechanics.Sample_Mechanic.SubLayer_3_BehaviourData;


namespace Blurry.Main.Layer_6_CoreMechanics.Sample_Mechanic.SubLayer_4_BehaviourDataConfigurer
{
    public class Sample_Mechanic_BehaviourDataConfigurer : AMechanic_BehaviourDataConfigurer<Sample_Mechanic_BehaviourData, Sample_Mechanic_Config, Sample_Mechanic_Entity>
    {
        public Sample_Mechanic_BehaviourDataConfigurer(Sample_Mechanic_Config config, Sample_Mechanic_Entity entity) : base(config, entity)
        {
        }

        protected override void Configure_(Sample_Mechanic_BehaviourData toConfigure)
        {
            throw new System.NotImplementedException();
        }
    }
}