using Blurry.Application.Layer_6_CoreMechanics.SubLayer_2_Presentation;
using Blurry.Main.Layer_6_CoreMechanics.Sample_Mechanic.SubLayer_0_PresentationData;


namespace Blurry.Main.Layer_6_CoreMechanics.Sample_Mechanic.SubLayer_2_Presentation
{
    public class Sample_Mechanic_Presentation : AMechanic_Presentation<Sample_Mechanic_PresentationData>
    {
        public Sample_Mechanic_Presentation(Sample_Mechanic_PresentationData data) : base(data)
        {
        }
    }
}