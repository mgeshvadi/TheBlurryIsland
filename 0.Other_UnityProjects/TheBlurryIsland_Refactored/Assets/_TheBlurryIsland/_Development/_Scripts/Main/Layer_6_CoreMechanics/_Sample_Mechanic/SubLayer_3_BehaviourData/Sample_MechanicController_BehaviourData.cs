using Blurry.Application.Layer_6_CoreMechanics.SubLayer_3_BehaviourData;
using Blurry.Main.Layer_6_CoreMechanics.Sample_Mechanic.SubLayer_2_Presentation;


namespace Blurry.Main.Layer_6_CoreMechanics.Sample_Mechanic.SubLayer_3_BehaviourData
{
    public class Sample_MechanicController_BehaviourData : AMechanicController_BehaviourData<Sample_MechanicController_Presentation>
    {
        public Sample_MechanicController_BehaviourData(Sample_MechanicController_Presentation presentation) : base(presentation)
        {
        }
    }
}