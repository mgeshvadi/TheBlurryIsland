using Blurry.Application.Layer_6_CoreMechanics.SubLayer_2_Presentation;
using Blurry.Main.Layer_6_CoreMechanics.Sample_Mechanic.SubLayer_0_PresentationData;


namespace Blurry.Main.Layer_6_CoreMechanics.Sample_Mechanic.SubLayer_2_Presentation
{
    public class Sample_MechanicController_Presentation : AMechanicController_Presentation<Sample_MechanicController_PresentationData>
    {
        public Sample_MechanicController_Presentation(Sample_MechanicController_PresentationData data) : base(data)
        {
        }
    }
}