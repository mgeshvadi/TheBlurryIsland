using System;
using UnityEngine;
using Blurry.Application.Layer_6_CoreMechanics.SubLayer_0_Config;
using Blurry.Main.Layer_6_CoreMechanics.General.SubLayer_1_ConfigFileLoader;


namespace Blurry.Main.Layer_6_CoreMechanics.Sample_Mechanic.SubLayer_0_Config
{
    [Serializable]
    public class Sample_MechanicController_Config : IMechanicController_Config
    {
    }

    [CreateAssetMenu(fileName = nameof(Sample_MechanicController_ConfigFile), menuName = MechanicsControllers_ConfigFileLoader.CONFIG_FILES_CONTEXT_MENU + nameof(Sample_MechanicController_ConfigFile))]
    public class Sample_MechanicController_ConfigFile : AMechanicController_ConfigFile<Sample_MechanicController_Config>
    {
    }
}