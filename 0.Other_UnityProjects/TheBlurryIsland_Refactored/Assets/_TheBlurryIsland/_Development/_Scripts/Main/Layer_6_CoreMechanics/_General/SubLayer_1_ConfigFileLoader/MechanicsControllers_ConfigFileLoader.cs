using Blurry.Application.Layer_6_CoreMechanics.SubLayer_0_Config;
using Blurry.Main.Layer_1_Ground.Config.SubLayer_2;


namespace Blurry.Main.Layer_6_CoreMechanics.General.SubLayer_1_ConfigFileLoader
{
    public static class MechanicsControllers_ConfigFileLoader
    {
        public const string CONFIG_FILES_CONTEXT_MENU = ConfigFileLoader.CONFIG_FILES_CONTEXT_MENU + "Mechanics/";
        private const string CONFIG_FILES_DIRECTORY = ConfigFileLoader.CONFIG_FILES_DIRECTORY + "Mechanics/";

        public static T LoadConfigFile<T>() where T : AMechanicController_ConfigFile<IMechanicController_Config>
        {
            return LoadConfigFile<T, IMechanicController_Config>(CONFIG_FILES_DIRECTORY);
        }

        private static T LoadConfigFile<T, G>(string directory) where T : AMechanicController_ConfigFile<G> where G : IMechanicController_Config
        {
            return ConfigFileLoader.LoadConfigFile<T, G>(directory);
        }
    }
}