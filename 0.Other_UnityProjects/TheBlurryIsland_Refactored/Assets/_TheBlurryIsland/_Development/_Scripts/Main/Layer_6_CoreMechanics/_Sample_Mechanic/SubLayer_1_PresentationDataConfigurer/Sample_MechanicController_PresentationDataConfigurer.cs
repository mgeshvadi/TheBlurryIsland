using Blurry.Application.Layer_6_CoreMechanics.SubLayer_1_PresentationDataConfigurer;
using Blurry.Main.Layer_6_CoreMechanics.Sample_Mechanic.SubLayer_0_Config;
using Blurry.Main.Layer_6_CoreMechanics.Sample_Mechanic.SubLayer_0_Entity;
using Blurry.Main.Layer_6_CoreMechanics.Sample_Mechanic.SubLayer_0_PresentationData;


namespace Blurry.Main.Layer_6_CoreMechanics.Sample_Mechanic.SubLayer_1_PresentationDataConfigurer
{
    public class Sample_MechanicController_PresentationDataConfigurer : AMechanicController_PresentationDataConfigurer<Sample_MechanicController_PresentationData, Sample_MechanicController_Config, Sample_MechanicController_Entity>
    {
        public Sample_MechanicController_PresentationDataConfigurer(Sample_MechanicController_Config config, Sample_MechanicController_Entity entity) : base(config, entity)
        {
        }

        protected override void Configure_(Sample_MechanicController_PresentationData toConfigure)
        {
            throw new System.NotImplementedException();
        }
    }
}