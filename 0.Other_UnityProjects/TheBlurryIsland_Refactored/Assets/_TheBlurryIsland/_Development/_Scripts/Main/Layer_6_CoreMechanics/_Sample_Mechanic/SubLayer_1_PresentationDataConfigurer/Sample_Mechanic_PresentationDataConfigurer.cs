using Blurry.Application.Layer_6_CoreMechanics.SubLayer_1_PresentationDataConfigurer;
using Blurry.Main.Layer_6_CoreMechanics.Sample_Mechanic.SubLayer_0_Config;
using Blurry.Main.Layer_6_CoreMechanics.Sample_Mechanic.SubLayer_0_Entity;
using Blurry.Main.Layer_6_CoreMechanics.Sample_Mechanic.SubLayer_0_PresentationData;


namespace Blurry.Main.Layer_6_CoreMechanics.Sample_Mechanic.SubLayer_1_PresentationDataConfigurer
{
    public class Sample_Mechanic_PresentationDataConfigurer : AMechanic_PresentationDataConfigurer<Sample_Mechanic_PresentationData, Sample_Mechanic_Config, Sample_Mechanic_Entity>
    {
        public Sample_Mechanic_PresentationDataConfigurer(Sample_Mechanic_Config config, Sample_Mechanic_Entity entity) : base(config, entity)
        {
        }

        protected override void Configure_(Sample_Mechanic_PresentationData toConfigure)
        {
            throw new System.NotImplementedException();
        }
    }
}