using Blurry.Application.Layer_6_CoreMechanics.SubLayer_5_Behaviour;
using Blurry.Main.Layer_6_CoreMechanics.Sample_Mechanic.SubLayer_3_BehaviourData;


namespace Blurry.Main.Layer_6_CoreMechanics.Sample_Mechanic.SubLayer_5_Behaviour
{
    public class Sample_Mechanic_Behaviour : AMechanic_Behaviour<Sample_Mechanic_BehaviourData>
    {
        public Sample_Mechanic_Behaviour(Sample_Mechanic_BehaviourData data) : base(data)
        {
        }

        protected override void Activate_()
        {
            throw new System.NotImplementedException();
        }

        protected override void Start_()
        {
            throw new System.NotImplementedException();
        }

        protected override void Stop_()
        {
            throw new System.NotImplementedException();
        }

        protected override void Reset_()
        {
            throw new System.NotImplementedException();
        }

        protected override void Restart_()
        {
            throw new System.NotImplementedException();
        }

        protected override void Deactivate_()
        {
            throw new System.NotImplementedException();
        }

        public override void Update(float deltaTime)
        {
            throw new System.NotImplementedException();
        }

        public override void FixedUpdate(float fixedDeltaTime)
        {
            throw new System.NotImplementedException();
        }
    }
}