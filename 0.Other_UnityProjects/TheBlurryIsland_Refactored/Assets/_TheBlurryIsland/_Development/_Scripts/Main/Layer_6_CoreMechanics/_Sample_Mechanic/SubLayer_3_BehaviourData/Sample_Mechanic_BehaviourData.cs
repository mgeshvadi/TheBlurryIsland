using Blurry.Application.Layer_6_CoreMechanics.SubLayer_3_BehaviourData;
using Blurry.Main.Layer_6_CoreMechanics.Sample_Mechanic.SubLayer_2_Presentation;


namespace Blurry.Main.Layer_6_CoreMechanics.Sample_Mechanic.SubLayer_3_BehaviourData
{
    public class Sample_Mechanic_BehaviourData : AMechanic_BehaviourData<Sample_Mechanic_Presentation>
    {
        public Sample_Mechanic_BehaviourData(Sample_Mechanic_Presentation presentation) : base(presentation)
        {
        }
    }
}