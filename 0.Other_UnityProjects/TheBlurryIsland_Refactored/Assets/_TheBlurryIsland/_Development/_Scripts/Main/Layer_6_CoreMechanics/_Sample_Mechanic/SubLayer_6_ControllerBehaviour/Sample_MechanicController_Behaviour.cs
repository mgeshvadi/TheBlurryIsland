using System.Collections.Generic;
using Blurry.Application.Layer_6_CoreMechanics.SubLayer_6_ControllerBehaviour;
using Blurry.Main.Layer_6_CoreMechanics.Sample_Mechanic.SubLayer_3_BehaviourData;
using Blurry.Main.Layer_6_CoreMechanics.Sample_Mechanic.SubLayer_5_Behaviour;


namespace Blurry.Main.Layer_6_CoreMechanics.Sample_Mechanic.SubLayer_6_ControllerBehaviour
{
    public class Sample_MechanicController_Behaviour : AMechanicController_Behaviour<Sample_MechanicController_BehaviourData, Sample_Mechanic_Behaviour>
    {
        public Sample_MechanicController_Behaviour(Sample_MechanicController_BehaviourData data, List<Sample_Mechanic_Behaviour> behaviours) : base(data, behaviours)
        {
        }

        protected override void Activate__()
        {
            throw new System.NotImplementedException();
        }

        protected override void StartInner__()
        {
            throw new System.NotImplementedException();
        }

        protected override void StopInner__()
        {
            throw new System.NotImplementedException();
        }

        protected override void ResetInner__()
        {
            throw new System.NotImplementedException();
        }

        protected override void RestartInner__()
        {
            throw new System.NotImplementedException();
        }

        protected override void Deactivate__()
        {
            throw new System.NotImplementedException();
        }

        protected override void Update_(float deltaTime)
        {
            throw new System.NotImplementedException();
        }

        protected override void FixedUpdate_(float fixedDeltaTime)
        {
            throw new System.NotImplementedException();
        }
    }
}