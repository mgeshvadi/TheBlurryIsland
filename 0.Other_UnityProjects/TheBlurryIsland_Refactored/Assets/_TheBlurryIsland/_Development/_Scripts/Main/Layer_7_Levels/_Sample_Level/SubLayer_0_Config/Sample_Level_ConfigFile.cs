using System;
using UnityEngine;
using Blurry.Application.Layer_7_Levels.SubLayer_0_Config;
using Blurry.Main.Layer_7_Levels.General.SubLayer_1_ConfigFileLoader;


namespace Blurry.Main.Layer_7_Levels.Sample_level.SubLayer_0_Config
{
    [Serializable]
    public class Sample_Level_Config : ILevel_Config
    {
    }

    [CreateAssetMenu(fileName = nameof(Sample_Level_ConfigFile), menuName = Levels_ConfigFileLoader.CONFIG_FILES_CONTEXT_MENU + nameof(Sample_Level_ConfigFile))]
    public class Sample_Level_ConfigFile : ALevel_ConfigFile<Sample_Level_Config>
    {
    }
}