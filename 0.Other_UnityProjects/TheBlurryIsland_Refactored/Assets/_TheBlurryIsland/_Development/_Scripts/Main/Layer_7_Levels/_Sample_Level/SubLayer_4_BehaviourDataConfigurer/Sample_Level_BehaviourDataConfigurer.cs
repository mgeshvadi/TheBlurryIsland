using Blurry.Application.Layer_7_Levels.SubLayer_4_BehaviourDataConfigurer;
using Blurry.Main.Layer_7_Levels.Sample_level.SubLayer_0_Config;
using Blurry.Main.Layer_7_Levels.Sample_level.SubLayer_0_Entity;
using Blurry.Main.Layer_7_Levels.Sample_level.SubLayer_3_BehaviourData;


namespace Blurry.Main.Layer_7_Levels.Sample_level.SubLayer_4_BehaviourDataConfigurer
{
    public class Sample_Level_BehaviourDataConfigurer : ALevel_BehaviourDataConfigurer<Sample_Level_BehaviourData, Sample_Level_Config, Sample_Level_Entity>
    {
        public Sample_Level_BehaviourDataConfigurer(Sample_Level_Config config, Sample_Level_Entity entity) : base(config, entity)
        {
        }

        protected override void Configure_(Sample_Level_BehaviourData toConfigure)
        {
            throw new System.NotImplementedException();
        }
    }
}