using System;
using System.Collections.Generic;
using Blurry.Application.Layer_6_CoreMechanics.SubLayer_6_ControllerBehaviour;
using Blurry.Application.Layer_7_Levels.SubLayer_5_Behaviour;
using Blurry.Main.Layer_7_Levels.Sample_level.SubLayer_3_BehaviourData;


namespace Blurry.Main.Layer_7_Levels.Sample_level.SubLayer_5_Behaviour
{
    public class Sample_Level_ControllerBehaviour : ALevel_ControllerBehaviour<Sample_Level_BehaviourData>
    {
        public Sample_Level_ControllerBehaviour(Sample_Level_BehaviourData data, Dictionary<Type, IMechanicController_Behaviour> behaviours) : base(data, behaviours)
        {
        }

        protected override void Activate__()
        {
            throw new NotImplementedException();
        }

        protected override void StartInner__()
        {
            throw new NotImplementedException();
        }

        protected override void StopInner__()
        {
            throw new NotImplementedException();
        }

        protected override void ResetInner__()
        {
            throw new NotImplementedException();
        }

        protected override void RestartInner__()
        {
            throw new NotImplementedException();
        }

        protected override void Deactivate__()
        {
            throw new NotImplementedException();
        }

        protected override void Update_(float deltaTime)
        {
            throw new NotImplementedException();
        }

        protected override void FixedUpdate_(float fixedDeltaTime)
        {
            throw new NotImplementedException();
        }
    }
}