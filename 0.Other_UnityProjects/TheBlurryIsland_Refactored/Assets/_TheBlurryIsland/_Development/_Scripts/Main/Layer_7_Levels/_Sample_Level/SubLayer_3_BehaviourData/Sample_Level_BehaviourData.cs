using Blurry.Application.Layer_7_Levels.SubLayer_3_BehaviourData;
using Blurry.Main.Layer_7_Levels.Sample_level.SubLayer_2_Presentation;


namespace Blurry.Main.Layer_7_Levels.Sample_level.SubLayer_3_BehaviourData
{
    public class Sample_Level_BehaviourData : ALevel_BehaviourData<Sample_Level_Presentation>
    {
        public Sample_Level_BehaviourData(Sample_Level_Presentation presentation) : base(presentation)
        {
        }
    }
}