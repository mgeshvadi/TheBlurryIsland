using Blurry.Main.Layer_1_Ground.Config.SubLayer_2;
using Blurry.Application.Layer_7_Levels.SubLayer_0_Config;


namespace Blurry.Main.Layer_7_Levels.General.SubLayer_1_ConfigFileLoader
{
    public static class Levels_ConfigFileLoader
    {
        public const string CONFIG_FILES_CONTEXT_MENU = ConfigFileLoader.CONFIG_FILES_CONTEXT_MENU + "Levels/";
        private const string CONFIG_FILES_DIRECTORY = ConfigFileLoader.CONFIG_FILES_DIRECTORY + "Levels/";

        public static T LoadConfigFile<T>() where T : ALevel_ConfigFile<ILevel_Config>
        {
            return LoadConfigFile<T, ILevel_Config>(CONFIG_FILES_DIRECTORY);
        }

        private static T LoadConfigFile<T, G>(string directory) where T : ALevel_ConfigFile<G> where G : ILevel_Config
        {
            return ConfigFileLoader.LoadConfigFile<T, G>(directory);
        }
    }
}