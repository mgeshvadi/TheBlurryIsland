using Blurry.Application.Layer_7_Levels.SubLayer_1_PresentationDataConfigurer;
using Blurry.Main.Layer_7_Levels.Sample_level.SubLayer_0_Config;
using Blurry.Main.Layer_7_Levels.Sample_level.SubLayer_0_Entity;
using Blurry.Main.Layer_7_Levels.Sample_level.SubLayer_0_PresentationData;


namespace Blurry.Main.Layer_7_Levels.Sample_level.SubLayer_1_PresentationDataConfigurer
{
    public abstract class Sample_Level_PresentationDataConfigurer : ALevel_PresentationDataConfigurer<Sample_Level_PresentationData, Sample_Level_Config, Sample_Level_Entity>
    {
        protected Sample_Level_PresentationDataConfigurer(Sample_Level_Config config, Sample_Level_Entity entity) : base(config, entity)
        {
        }
    }
}