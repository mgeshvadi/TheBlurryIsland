using Blurry.Application.Layer_1_Ground.Config.SubLayer_0;
using Blurry.Application.Layer_1_Ground.Config.SubLayer_1;
using Blurry.Main.Layer_0_Base.AssetLoader.SubLayer_0;


namespace Blurry.Main.Layer_1_Ground.Config.SubLayer_2
{
    public static class ConfigFileLoader
    {
        public const string CONFIG_FILES_CONTEXT_MENU = "Blurry/Resources/ConfigFiles/";
        public const string CONFIG_FILES_DIRECTORY = "ConfigFiles/";

        public static T LoadConfigFile<T>() where T : AConfig_File<IConfig>
        {
            return LoadConfigFile<T, IConfig>(CONFIG_FILES_DIRECTORY);
        }

        public static T LoadConfigFile<T, G>(string directory) where T : AConfig_File<G> where G : IConfig
        {
            return ResourceEx.Load<T>(directory + typeof(T));
        }
    }
}