using System.Collections.Generic;
using Blurry.Application.Layer_1_Ground.EventManagement.SubLayer_0;
using Blurry.Application.Layer_1_Ground.EventManagement.SubLayer_1;
using Blurry.Application.Layer_1_Ground.EventManagement.SubLayer_2;


namespace Blurry.Main.Layer_1_Ground.EventManagement.SubLayer_3
{
    public class EventManager_Service : IEventManager_Service
    {
        private readonly List<IEventListener> listeners = new List<IEventListener>();

        public void Register(IEventListener listener)
        {
            if (listeners.Contains(listener) == false)
                listeners.Add(listener);
        }

        public void UnRegister(IEventListener listener)
        {
            listeners.Remove(listener);
        }

        public void Propagate(IGameEvent evt, object sender)
        {
            // Todo: Iman Said: without making a copy of list, we may have Problem "when we try propagate an event during another propagation operation", is that correct?
            foreach (var listener in listeners)
                listener.OnEvent(evt, sender);
        }

        public bool Has(IEventListener listener)
        {
            return listeners.Contains(listener);
        }

        public void Clear()
        {
            listeners.Clear();
        }
    }
}