using System;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;


// TODO: Remove Unused methods (when proven that they are not needed)
namespace Blurry.Main.Layer_0_Base.AssetLoader.SubLayer_0
{
    public class ResourceEx
    {
        [Serializable]
        public class File { }

        public static T Load<T>(string path) where T : Object
        {
            return Resources.Load<T>(path);
        }

        public static T Load<T>(string dire, int id) where T : Object
        {
            throw new NotImplementedException();
        }

        public static List<File> LoadAll(string dire, bool subfolders)
        {
            throw new NotImplementedException();
        }

        public static List<File> LoadAll(string dire, bool subfolders, System.Predicate<File> match)
        {
            throw new NotImplementedException();
        }

        public static List<T> LoadAll<T>(string dire, bool subfolders, System.Predicate<File> match) where T : Object
        {
            throw new NotImplementedException();
        }
    }
}
