using System.Collections.Generic;
using System.Linq;
using Blurry.Application.Layer_0_Base.ServiceLocating.SubLayer_0;


namespace Blurry.Main.Layer_0_Base.ServiceLocating.SubLayer_1
{
    public class ServiceLocator
    {
        public static ServiceLocator instance;

        private readonly List<IService> services = new List<IService>();

        public static void Initialize()
        {
            if (instance == null)
                instance = new ServiceLocator();
        }

        public static bool IsInitialized()
        {
            return instance != null;
        }

        public static void Clear()
        {
            instance = null;
        }

        public static IService Register(IService service)
        {
            instance.services.Add(service);
            return service;
        }

        public static void UnRegister<T>() where T : IService
        {
            var service = Find<T>();
            instance.services.Remove(service);
        }

        public static void Replace<T>(T service) where T : IService
        {
            if (Has<T>())
                UnRegister<T>();
            Register(service);
        }

        public static T Find<T>() where T : IService
        {
            foreach (T service in instance.services.OfType<T>())
                return service;

            throw new System.Exception($"Service of type '{typeof(T)}' could not be found."); // TODO: Use Global way
        }

        public static bool Has<T>()
        {
            return instance.services.OfType<T>().Any();
        }
    }
}