using UnityEngine;
using Blurry.Application.Layer_2_Foundation.ProfileManagement.SubLayer_0;


namespace Blurry.Main.Layer_2_Foundation.ProfileManagement.SubLayer_1
{
    public class ProfileManager_Service : IProfileManager_Service
    {
        public string GlobalUserId
        {
            get => PlayerPrefs.GetString("GlobalUserId", "0");
            private set => PlayerPrefs.SetString("GlobalUserId", value);
        }


        public T LoadData<T>(string key, T defaultValue)
        {
            if (typeof(T) == typeof(int))
            {
                var value = PlayerPrefs.GetInt(key, (int) (object) defaultValue);
                return (T) (object) value;
            }
            if (typeof(T) == typeof(float))
            {
                var value = PlayerPrefs.GetFloat(key, (float) (object) defaultValue);
                return (T) (object) value;
            }
            if (typeof(T) == typeof(string))
            {
                var value = PlayerPrefs.GetString(key, (string) (object) defaultValue);
                return (T) (object) value;
            }

            return defaultValue;
        }

        public void SaveData<T>(string key, T value)
        {
            if (typeof(T) == typeof(int))
                PlayerPrefs.SetInt(key, (int) (object) value);
            else if (typeof(T) == typeof(float))
                PlayerPrefs.SetFloat(key, (float) (object) value);
            else
                PlayerPrefs.SetString(key, value.ToString());
        }
    }
}