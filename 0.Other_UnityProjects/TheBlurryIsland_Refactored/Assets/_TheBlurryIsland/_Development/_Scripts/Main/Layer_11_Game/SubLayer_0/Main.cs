using Blurry.Main.Layer_0_Base.ServiceLocating.SubLayer_1;
// using Blurry.Layer_1_Ground.ConfigurationManagement.Implementations.SubLayer_3;
using UnityEngine;


namespace Blurry.Main.layer_11_Game.SubLayer_0
{
    public class Main : MonoBehaviour
    {
        // [SerializeField] private ConfigurationManager_Service configurationManager;

        private void Awake()
        {
            DontDestroyOnLoad(gameObject);
            Initialize();
        }

        private void Initialize()
        {
            Initialize_Layer_0_Base_SubLayer_0();

            Initialize_Layer_1_Ground_SubLayer_0();

            Initialize_Layer_2_Foundation_SubLayer_0();
            Initialize_Layer_2_Foundation_SubLayer_1();

            Initialize_Layer_3_Character_SubLayer_0();

            Initialize_Layer_4_CoreMechanics_SubLayer_0();

            Initialize_Layer_5_Levels_SubLayer_0();

            Initialize_Layer_6_UI_SubLayer_0();
        }

        private void Initialize_Layer_0_Base_SubLayer_0()
        {
            ServiceLocator.Initialize();
        }

        private void Initialize_Layer_1_Ground_SubLayer_0()
        {
            // ServiceLocator.Register(new EventManagerService());
            // ServiceLocator.Register(configurationManager.Initialize());
        }

        private void Initialize_Layer_2_Foundation_SubLayer_0()
        {
            // ServiceLocator.Register(new ProfileManager_Service());
        }

        private void Initialize_Layer_2_Foundation_SubLayer_1()
        {
            // ServiceLocator.Register(new Main_InventoryManager_Service());
            // ServiceLocator.Register(new UserInputManager_Service());
        }

        private void Initialize_Layer_3_Character_SubLayer_0()
        {
        }

        private void Initialize_Layer_4_CoreMechanics_SubLayer_0()
        {
            // ServiceLocator.Register(new GamePlayService());
        }

        private void Initialize_Layer_5_Levels_SubLayer_0()
        {

        }

        private void Initialize_Layer_6_UI_SubLayer_0()
        {

        }
    }
}