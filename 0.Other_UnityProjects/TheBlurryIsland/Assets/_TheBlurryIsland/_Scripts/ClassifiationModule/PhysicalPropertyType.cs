﻿public enum PhysicalPropertyType
{
    // Vector 3
    ForceInitial,
    ForceContinues,
    ForceOnCollide,
    AngularVelocity,
    Velocity,
    Rotation,
    Position,

    // float
    Mass,
    Acceleration,
    Transparency,
    Resistance,

    // vector 2
    Vibration,

    // Color
    Color
}
