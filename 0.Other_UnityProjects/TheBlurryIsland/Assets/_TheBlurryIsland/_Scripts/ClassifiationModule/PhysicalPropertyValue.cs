﻿using System;
using UnityEngine;


[Serializable]
public class PhysicalPropertyValue
{
    public Vector3 vector3Value;
    public Vector2 vector2Value;
    public float floatValue;
    public Color colorValue;

    public void SetValue<TValue>(TValue tValue) where TValue : struct
    {
        if (typeof(TValue) == typeof(Vector3))
            vector3Value = (Vector3) (object) tValue;
        if (typeof(TValue) == typeof(Vector2))
            vector2Value = (Vector2) (object) tValue;
        else if (typeof(TValue) == typeof(float))
            floatValue = (float) (object) tValue;
        else if (typeof(TValue) == typeof(Color))
            colorValue = (Color) (object) tValue;
    }

    public TValue GetValue<TValue>() where TValue : struct
    {
        if (typeof(TValue) == typeof(Vector3))
            return (TValue) (object) vector3Value;
        if (typeof(TValue) == typeof(Vector2))
            return (TValue) (object) vector2Value;
        if (typeof(TValue) == typeof(float))
            return (TValue) (object) floatValue;
        if (typeof(TValue) == typeof(Color))
            return (TValue) (object) colorValue;

        return (TValue) new object();
    }




}
