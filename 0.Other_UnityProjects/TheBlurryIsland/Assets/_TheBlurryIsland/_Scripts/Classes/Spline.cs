﻿using System;
using UnityEngine;


[Serializable]
public class Spline
{
    private readonly Transform startPoint;
    private readonly Transform endPoint;


    public Spline(Transform startPoint, Transform endPoint)
    {
        this.startPoint = startPoint;
        this.endPoint = endPoint;
    }

    public bool ContainsCharacter(Vector3 globalPosition)
    {
        return startPoint.position.z < globalPosition.z && endPoint.position.z > globalPosition.z;
    }

    public float CalculateX(float globalPositionZ)
    {
        var position = endPoint.position;
        var position1 = startPoint.position;
        float m = (position.x - position1.x) / (position.z - position1.z);
        return m * (globalPositionZ - position1.z) + position1.x;
    }
}
