﻿using System;
using UnityEngine;

[Serializable]
public class PhysicalProperty
{
    public PhysicalPropertyType type;
    public PhysicalPropertyValue value;
}
