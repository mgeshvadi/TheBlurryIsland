﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

[Serializable]
public class TimeDuration
{
    [SerializeField] private float time = 4;

    [Tooltip("This value gets Plus and Minus to and from time value, In Seconds")] [SerializeField]
    private float randomness = 1;


    public float GetTime()
    {
        float min = Mathf.Clamp(time, 0, time - randomness);
        return Random.Range(min, time + randomness);
    }
}
