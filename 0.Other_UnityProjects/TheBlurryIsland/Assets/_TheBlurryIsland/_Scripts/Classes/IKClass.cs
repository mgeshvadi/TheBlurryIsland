﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class IKClass
{
    public IkType type;
    public Transform defaultIkPoint;
    public bool needsTwoDynamicIkPoints;
    public List<AvatarIKGoal> avatarIKGoals;
    public List<AvatarIKGoal> avatarIKGoalsWhenOnRight;
    public List<AvatarIKGoal> avatarIKGoalsWhenOnLeft;


    [Range(0, 1)] public float ikWeight = 1;
    [Range(0, 1)] public float ikBodyWeight = 0.5f;
    [Range(0, 1)] public float ikHeadWeight = 0.9f;
    [Range(0, 1)] public float ikEyesWeight = 0.5f;
    [Range(0, 1)] public float ikClampWeight = 1;
    public float ikWeightChangeSpeed = 1;
    public float ikPointChangeSpeed = 30;

    public bool blockInCrouch;
    public bool noIkBehindOfCharacter;
    public float zOffsetForCheckingBehind = 1f;
    public float footIkOffset;
    public LayerMask layerMask;

    public List<GameObject> points = new List<GameObject>();
    public bool overrideIKSettings;

    private readonly UnityEvent onPointsChanged = new UnityEvent();
    private bool pointsAdopted;
    private CharacterIK characterIKOwner;
    private float currentIkWeight;
    private float currentBodyWeight;
    private float currentHeadWeight;
    private float currentEyeWeight;
    private float currentClampWeight;
    private Transform currentIkPointOne;
    private Transform currentIkPointTwo;
    private Transform finalSelectedIkPointOne;
    private Transform finalSelectedIkPointTwo;
    private IKClass defaultSettings;
    [HideInInspector] public Transform leftFoot;
    [HideInInspector] public Transform rightFoot;
    [HideInInspector] public Vector3 leftFootIkPos;
    [HideInInspector] public Vector3 rightFootIkPos;
    [HideInInspector] public Quaternion leftFootIkRot;
    [HideInInspector] public Quaternion rightFootIkRot;


    public IkType GetIkType()
    {
        return type;
    }

    public void SetIkType(IkType value)
    {
        type = value;
    }

    public void Init(CharacterIK ownerCharacterIK, GameObject ikPoint, GameObject ikPoint2)
    {
        currentIkPointOne = ikPoint ? ikPoint.transform : null;
        currentIkPointTwo = ikPoint2 ? ikPoint2.transform : null;
        defaultSettings = new IKClass();
        characterIKOwner = ownerCharacterIK;
        onPointsChanged.AddListener(() => { pointsAdopted = false; });

        defaultSettings.ikWeight = ikWeight;
        defaultSettings.ikWeightChangeSpeed = ikWeightChangeSpeed;

        defaultSettings.avatarIKGoals = new List<AvatarIKGoal>(avatarIKGoals);
        defaultSettings.avatarIKGoalsWhenOnLeft = new List<AvatarIKGoal>(avatarIKGoalsWhenOnLeft);
        defaultSettings.avatarIKGoalsWhenOnRight = new List<AvatarIKGoal>(avatarIKGoalsWhenOnRight);

        defaultSettings.blockInCrouch = blockInCrouch;
        defaultSettings.noIkBehindOfCharacter = noIkBehindOfCharacter;
        defaultSettings.zOffsetForCheckingBehind = zOffsetForCheckingBehind;
    }

    public void AddPointOfInterest(List<GameObject> gos)
    {
        foreach (GameObject go in gos)
            points.Add(go);
        onPointsChanged.Invoke();
    }

    public void RemovePointOfInterest(List<GameObject> gos)
    {
        foreach (GameObject go in gos)
            points.Remove(go);
        onPointsChanged.Invoke();
    }

    public void Override(IKClass overrideValues)
    {
        ikWeight = overrideValues.ikWeight;
        ikWeightChangeSpeed = overrideValues.ikWeightChangeSpeed;

        avatarIKGoals = new List<AvatarIKGoal>(overrideValues.avatarIKGoals);
        avatarIKGoalsWhenOnLeft = new List<AvatarIKGoal>(overrideValues.avatarIKGoalsWhenOnLeft);
        avatarIKGoalsWhenOnRight = new List<AvatarIKGoal>(overrideValues.avatarIKGoalsWhenOnRight);

        blockInCrouch = overrideValues.blockInCrouch;
        noIkBehindOfCharacter = overrideValues.noIkBehindOfCharacter;
        zOffsetForCheckingBehind = overrideValues.zOffsetForCheckingBehind;
    }

    public void GetToDefault()
    {
        ikWeight = defaultSettings.ikWeight;
        ikWeightChangeSpeed = defaultSettings.ikWeightChangeSpeed;

        avatarIKGoals = new List<AvatarIKGoal>(defaultSettings.avatarIKGoals);
        avatarIKGoalsWhenOnLeft = new List<AvatarIKGoal>(defaultSettings.avatarIKGoalsWhenOnLeft);
        avatarIKGoalsWhenOnRight = new List<AvatarIKGoal>(defaultSettings.avatarIKGoalsWhenOnRight);

        blockInCrouch = defaultSettings.blockInCrouch;
        noIkBehindOfCharacter = defaultSettings.noIkBehindOfCharacter;
        zOffsetForCheckingBehind = defaultSettings.zOffsetForCheckingBehind;
    }

    public void HandleIK()
    {
        if (pointsAdopted)
            return;
        ToggleIK((points.Count != 0 || type == IkType.FootOnFloor) ? ToggleState.On : ToggleState.Off);
    }

    private void ToggleIK(ToggleState value)
    {
        switch (value)
        {
            case ToggleState.On:
                if (characterIKOwner.GetMainCharacterController().characterIKBlocked ||
                    (blockInCrouch && characterIKOwner.GetMainCharacterController().characterIKBlockedBecauseOfCrouch) ||
                    (type == IkType.FootOnFloor && characterIKOwner.GetMainCharacterController().characterIKFootBlocked)||
                    (type==IkType.Intractable && characterIKOwner.GetMainCharacterController().characterIntractableIkBlock))
                    LerpWeightAndPointTo(0, true);
                else
                {
                    if (type == IkType.FootOnFloor)
                        LerpWeightAndPointTo(ikWeight, false);
                    else
                    {
                        if (type == IkType.Climb)
                        {
                            finalSelectedIkPointOne = points[0].transform;
                            finalSelectedIkPointTwo = points[1].transform;
                        }
                        else
                            finalSelectedIkPointOne = FindNearestObject(points);

                        if (finalSelectedIkPointOne == null)
                            LerpWeightAndPointTo(0, true);
                        else
                            LerpWeightAndPointTo(ikWeight, false, ikBodyWeight, ikHeadWeight, ikEyesWeight, ikClampWeight);
                    }
                }

                break;
            case ToggleState.Off:
                LerpWeightAndPointTo(0, false);
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(value), value, null);
        }
    }

    private void LerpWeightAndPointTo(float goalIkValue, bool isTemporary, float goalBodyValue = 0, float goalHeadValue = 0, float goalEyesValue = 0, float goalClampValue = 0)
    {
        if (type == IkType.FootOnFloor)
            currentIkWeight = goalIkValue;
        else
        {
            if (Math.Abs(goalIkValue) < 0.005f && !isTemporary)
            {
                finalSelectedIkPointOne = defaultIkPoint;
                finalSelectedIkPointTwo = defaultIkPoint;
            }

            LerpSelectedPositionOne();
            LerpSelectedPositionTwo();

            currentIkWeight = !Mathf.Approximately(currentIkWeight, goalIkValue) ? Mathf.MoveTowards(currentIkWeight, goalIkValue, ikWeightChangeSpeed * Time.deltaTime) : goalIkValue;
        }

        if (type == IkType.PointOfInterest)
        {
            currentBodyWeight = !Mathf.Approximately(currentBodyWeight, goalBodyValue) ? Mathf.MoveTowards(currentBodyWeight, goalBodyValue, ikWeightChangeSpeed * Time.deltaTime) : goalBodyValue;
            currentHeadWeight = !Mathf.Approximately(currentHeadWeight, goalHeadValue) ? Mathf.MoveTowards(currentHeadWeight, goalHeadValue, ikWeightChangeSpeed * Time.deltaTime) : goalHeadValue;
            currentEyeWeight = !Mathf.Approximately(currentEyeWeight, goalEyesValue) ? Mathf.MoveTowards(currentEyeWeight, goalEyesValue, ikWeightChangeSpeed * Time.deltaTime) : goalEyesValue;
            currentClampWeight = !Mathf.Approximately(currentClampWeight, goalClampValue) ? Mathf.MoveTowards(currentClampWeight, goalClampValue, ikWeightChangeSpeed * Time.deltaTime) : goalClampValue;
        }


        if (type != IkType.FootOnFloor && !isTemporary && Mathf.Approximately(currentIkWeight, 0))
        {
            pointsAdopted = true;
            currentIkPointOne.transform.position = finalSelectedIkPointOne.position;
            if (currentIkPointTwo)
                currentIkPointTwo.transform.position = finalSelectedIkPointTwo.position;
        }

        switch (type)
        {
            case IkType.PointOfInterest:
                characterIKOwner.SetLookAtWeight(currentIkWeight, currentBodyWeight, currentHeadWeight, currentEyeWeight, currentClampWeight);
                characterIKOwner.SetLookAtPosition(currentIkPointOne.position);
                break;
            case IkType.PushAndPull:
                break;
            case IkType.FootOnFloor:
                characterIKOwner.SetPositionAndRotation(AvatarIKGoal.LeftFoot, currentIkWeight, leftFootIkPos, leftFootIkRot);
                characterIKOwner.SetPositionAndRotation(AvatarIKGoal.RightFoot, currentIkWeight, rightFootIkPos, rightFootIkRot);
                break;
            case IkType.Intractable when IsOnRightFromIkPointOfView():
            {
                foreach (AvatarIKGoal avatarIKGoal in avatarIKGoalsWhenOnRight)
                {
                    characterIKOwner.SetIKPositionWeight(avatarIKGoal, currentIkWeight);
                    characterIKOwner.SetIKPosition(avatarIKGoal, currentIkPointOne.position);
                }

                break;
            }
            case IkType.Intractable when !IsOnRightFromIkPointOfView():
            {
                foreach (AvatarIKGoal avatarIKGoal in avatarIKGoalsWhenOnLeft)
                {
                    characterIKOwner.SetIKPositionWeight(avatarIKGoal, currentIkWeight);
                    characterIKOwner.SetIKPosition(avatarIKGoal, currentIkPointOne.position);
                }

                break;
            }
            case IkType.Climb:
                characterIKOwner.SetIKPositionWeight(avatarIKGoals[0], currentIkWeight);
                characterIKOwner.SetIKPosition(avatarIKGoals[0], currentIkPointOne.position);

                characterIKOwner.SetIKPositionWeight(avatarIKGoals[1], currentIkWeight);
                characterIKOwner.SetIKPosition(avatarIKGoals[1], currentIkPointTwo.position);
                break;
        }
    }

    private void LerpSelectedPositionOne()
    {
        LerpPosition(currentIkPointOne, finalSelectedIkPointOne);
    }

    private void LerpSelectedPositionTwo()
    {
        LerpPosition(currentIkPointTwo, finalSelectedIkPointTwo);
    }

    private void LerpPosition(Transform currentIkPoint, Transform finalSelectedIkPoint)
    {
        if (!currentIkPoint || !finalSelectedIkPoint)
            return;
        Vector3 position = finalSelectedIkPoint ? finalSelectedIkPoint.position : defaultIkPoint.position;
        Vector3 position1 = currentIkPoint.position;
        position1 = position1 != position ? Vector3.MoveTowards(position1, position, ikPointChangeSpeed * Time.deltaTime) : position;
        currentIkPoint.position = position1;
    }

    private Transform FindNearestObject(IReadOnlyList<GameObject> list)
    {
        List<GameObject> verified = new List<GameObject>(list);
        if (noIkBehindOfCharacter)
            foreach (GameObject o in list.Where(IsBehindOfOwner))
                verified.Remove(o);
        if (verified.Count == 0)
            return null;

        GameObject nearest = verified[0];
        float minDistance = CalculateDistance(verified[0].transform.position);

        foreach (GameObject t in verified)
        {
            if (noIkBehindOfCharacter && IsBehindOfOwner(t))
                continue;
            float distance = CalculateDistance(t.transform.position);
            if (distance > minDistance)
                continue;
            minDistance = distance;
            nearest = t;
        }

        return nearest.transform;
    }

    private float CalculateDistance(Vector3 position)
    {
        return Vector3.Distance(position, characterIKOwner.transform.position);
    }

    private bool IsBehindOfOwner(GameObject go)
    {
        Vector3 position = go.transform.position;
        Vector3 position1 = characterIKOwner.transform.position;
        return position.z < position1.z + Mathf.Abs(zOffsetForCheckingBehind) && IsOnRightFromIkPointOfView() || position.z > position1.z - Mathf.Abs(zOffsetForCheckingBehind) && !IsOnRightFromIkPointOfView();
    }

    private bool IsOnRightFromIkPointOfView()
    {
        return characterIKOwner.GetMainCharacterController().direction == CharacterDirection.Right || characterIKOwner.GetMainCharacterController().direction == CharacterDirection.GoingToLeft;
    }
}
