﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ObjectSidesPosition
{
    public Vector3 offset;
    public InteractionSide interactionSide;
    [HideInInspector] public List<GameObject> gameObjects = new List<GameObject>();

    public ObjectSidesPosition(InteractionSide interactionSide, Vector3 offset)
    {
        this.interactionSide = interactionSide;
        this.offset = offset;
    }

    public ObjectSidesPosition(InteractionSide interactionSide, GameObject gameObject)
    {
        this.interactionSide = interactionSide;
        SetGameObject(gameObject);
    }

    public void SetGameObject(GameObject go)
    {
        gameObjects.Clear();
        gameObjects.Add(go);
    }

    public void SetGameObjects(GameObject[] gos)
    {
        gameObjects = new List<GameObject>(gos);
    }

    public void SetLocalPosition(Vector3 position)
    {
        gameObjects[0].transform.localPosition = position;
    }

    public void SetGlobalPosition(Vector3 position)
    {
        gameObjects[0].transform.position = position;
    }

    public void SetGlobalPositions(Vector3[] positions)
    {
        for (int i = 0; i < positions.Length; i++)
            gameObjects[i].transform.position = positions[i];
    }

    public Vector3 GetGlobalPosition()
    {
        return gameObjects[0].transform.position;
    }

    public List<GameObject> GetGameObjects()
    {
        return gameObjects;
    }
}
