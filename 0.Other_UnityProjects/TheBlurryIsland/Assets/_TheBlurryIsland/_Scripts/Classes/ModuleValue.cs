﻿using System;
using UnityEngine;


[Serializable]
public class ModuleValue
{
    public TweenType toggleType;
    public ToggleState recursion = ToggleState.Off;

    public ModuleToggleState initialState;
    public Vector3 minValue;
    public Vector3 maxValue;
    public Vector3 minValueScale = new Vector3(1, 1, 1);
    public Vector3 maxValueScale = new Vector3(1, 1, 1);
    public Color minColor = Color.magenta;
    public Color maxColor = Color.magenta;
    public float minAlpha = 1;
    public float maxAlpha = 1;

    public float toggleDurationAtFirst = 3;
    public float toggleDelayAtFirst;
    public LeanTweenType toggleEasingTypeAtFirst = LeanTweenType.easeInOutSine;
    public float toggleDurationAtRecursion = 3;
    public float toggleDelayAtRecursion;
    public LeanTweenType toggleEasingTypeAtRecursion = LeanTweenType.easeInOutSine;


    [HideInInspector] public Vector3 currentValue;
    [HideInInspector] public float currentAlphaValue;
    [HideInInspector] public Color currentColorValue;
    [HideInInspector] public ModuleToggleState currentToggleState;
    [HideInInspector] public int tweenId;
    [HideInInspector] public bool toggleDone;
    [HideInInspector] public bool isOnRecursion;

    public Vector3 CalculateGoalVector()
    {
        switch (currentToggleState)
        {
            case ModuleToggleState.OnMin:
                return toggleType == TweenType.Scale ? minValueScale : minValue;
            case ModuleToggleState.OnMax:
                return toggleType == TweenType.Scale ? maxValueScale : maxValue;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    public Color CalculateGoalColor()
    {
        switch (currentToggleState)
        {
            case ModuleToggleState.OnMin:
                return minColor;
            case ModuleToggleState.OnMax:
                return maxColor;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    public float CalculateGoalAlpha()
    {
        switch (currentToggleState)
        {
            case ModuleToggleState.OnMin:
                return minAlpha;
            case ModuleToggleState.OnMax:
                return maxAlpha;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

}
