﻿using UnityEngine;
using UnityEngine.InputSystem;


public class DebuggerTest : MonoBehaviour
{

#if UNITY_EDITOR

    private void Update()
    {
        Keyboard kb = InputSystem.GetDevice<Keyboard>();
        if (kb.numpad0Key.wasPressedThisFrame)
            Time.timeScale = 0f;
        else if (kb.numpad1Key.wasPressedThisFrame)
            Time.timeScale = 0.1f;
        else if (kb.numpad2Key.wasPressedThisFrame)
            Time.timeScale = 0.2f;
        else if (kb.numpad3Key.wasPressedThisFrame)
            Time.timeScale = 0.3f;
        else if (kb.numpad4Key.wasPressedThisFrame)
            Time.timeScale = 0.4f;
        else if (kb.numpad5Key.wasPressedThisFrame)
            Time.timeScale = 0.5f;
        else if (kb.numpad6Key.wasPressedThisFrame)
            Time.timeScale = 0.6f;
        else if (kb.numpad7Key.wasPressedThisFrame)
            Time.timeScale = 0.7f;
        else if (kb.numpad8Key.wasPressedThisFrame)
            Time.timeScale = 0.8f;
        else if (kb.numpad9Key.wasPressedThisFrame)
            Time.timeScale = 0.9f;
        else if (kb.pKey.wasPressedThisFrame)
            Time.timeScale = 1;
    }
#endif
}
