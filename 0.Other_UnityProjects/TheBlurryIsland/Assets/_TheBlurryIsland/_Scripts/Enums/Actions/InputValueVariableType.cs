﻿
public enum InputValueVariableType
{
    Button,
    BooleanToggle,
    BooleanContinues, // value is updated continually
    Vector1,
    Vector2
}
