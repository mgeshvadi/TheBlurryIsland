﻿
public enum CrouchCeilingCheckModes
{
    NoCeilingCheck,    // this is good for a game design where its not possible to get to a situation which the character is crouched but a object is above her/his head, weather by user input or by crouch zones
    CheckWithBug, // Only When the Character is getting up and out from crouch ( or a ceiling is detected ) 2 raycast emmit,
                    // Bug 1. if go under a ceiling in crouch mode without releasing crouch input and do a jump 1 jump will be done, its safe although.
                    // Bug 2. if go under a ceiling which ends to a crouch zone , the character will lose its jump power until getting out from that zone
                    // Bug 3. if go to crouch zone and it ends to a ceiling half in crouch zone and half outside , the if character goes under it in the half which is in crouch zone, then character can jump, although its not buggy
                    // *** NOTE BUGS NUMBER 1 and 3 will be fixed if we add ceiling check before jump.
                    //*** this case is good when it is guaranteed no ceiling is half in crouch zone half outside ( full out side / full inside is fine ), and also first time jump Bugs is ignorable
    FullCheck // in addition to Previous state Every Frame While Character is in crouch ( even when we know character is not getting up ) 2 ray casts emmit, no a very big deal and it is far from having bugs.
                    //*** this case is good when we are ok in performance, by the way i don't think that this option will cost a lot
}
