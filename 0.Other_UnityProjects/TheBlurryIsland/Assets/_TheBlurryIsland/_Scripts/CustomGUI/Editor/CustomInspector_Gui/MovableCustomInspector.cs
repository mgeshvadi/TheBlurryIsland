﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Movable))]
public class MovableCustomInspector : Editor
{
    private Movable targetScript;

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        Movable movable = (Movable) target;
        EditorGUILayout.PropertyField(serializedObject.FindProperty(nameof(Movable.mainRigidBody)), GUILayout.ExpandWidth(true));
        EditorGUILayout.PropertyField(serializedObject.FindProperty(nameof(Movable.sideZMin)), GUILayout.ExpandWidth(true));
        EditorGUILayout.PropertyField(serializedObject.FindProperty(nameof(Movable.sideZMax)), GUILayout.ExpandWidth(true));

        EditorGUILayout.PropertyField(serializedObject.FindProperty(nameof(Movable.hasBounds)), GUILayout.ExpandWidth(true));
        if (movable.hasBounds)
        {
            EditorGUILayout.PropertyField(serializedObject.FindProperty(nameof(Movable.avoidableModules)), GUILayout.ExpandWidth(true));
            EditorGUILayout.PropertyField(serializedObject.FindProperty(nameof(Movable.startBound)), GUILayout.ExpandWidth(true));
            EditorGUILayout.PropertyField(serializedObject.FindProperty(nameof(Movable.endBound)), GUILayout.ExpandWidth(true));
            EditorGUILayout.PropertyField(serializedObject.FindProperty(nameof(Movable.boundExtraOffset)), GUILayout.ExpandWidth(true));
        }

        EditorGUILayout.PropertyField(serializedObject.FindProperty(nameof(Movable.hasIk)), GUILayout.ExpandWidth(true));
        if (movable.hasIk)
        {
            EditorGUILayout.PropertyField(serializedObject.FindProperty(nameof(Movable.iKPoint1)), GUILayout.ExpandWidth(true));
            EditorGUILayout.PropertyField(serializedObject.FindProperty(nameof(Movable.iKPoint2)), GUILayout.ExpandWidth(true));
            EditorGUILayout.PropertyField(serializedObject.FindProperty(nameof(Movable.ikTrigger)), GUILayout.ExpandWidth(true));
            EditorGUILayout.PropertyField(serializedObject.FindProperty(nameof(Movable.ikPointExtraOffset)), GUILayout.ExpandWidth(true));
            EditorGUILayout.PropertyField(serializedObject.FindProperty(nameof(Movable.ikTriggerExtraOffset)), GUILayout.ExpandWidth(true));
        }

        EditorGUILayout.Space();
        if (movable.hasIk && !movable.hasBounds)
        {
            EditorGUILayout.PropertyField(serializedObject.FindProperty(nameof(Movable.meshSize)), GUILayout.ExpandWidth(true));
            EditorGUILayout.PropertyField(serializedObject.FindProperty(nameof(Movable.characterWidth)), GUILayout.ExpandWidth(true));
        }
        serializedObject.ApplyModifiedProperties();
    }
}
