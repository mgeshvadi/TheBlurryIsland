﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.Experimental.SceneManagement;
using UnityEditor.SceneManagement;
using UnityEngine;


public static class EditorListZoneTrigger
{
    public static void DrawList(ActionZoneTrigger targetScript, SerializedProperty list, bool showListSize, bool showListTitle, string listTitle, string sizeLabel)
    {
        List<string> actionTypesItems = new List<string>();
        int selectedIndex = 0;
        List<ZoneAction> zoneActions = new List<ZoneAction>(targetScript.zoneActions);
        GUIStyle style = new GUIStyle(GUI.skin.button);

        EditorGUILayout.Space();

        // Showing list title
        if (showListTitle)
        {
            EditorGUILayout.LabelField(listTitle);
            EditorGUILayout.Space();
        }

        // Showing list size
        if (showListSize && list.isExpanded)
        {
            EditorGUILayout.PropertyField(list.FindPropertyRelative("Array.size"), new GUIContent(sizeLabel));
            EditorGUILayout.Space();
        }

        EditorGUILayout.Space();

        // Drawing List elements ( actions )
        for (int i = 0; i < list.arraySize; i++)
        {
            SerializedProperty item = list.GetArrayElementAtIndex(i);

            EditorGUILayout.LabelField("Action #" + (i + 1));
            EditorGUI.indentLevel += 1;

            EditorGUIUtility.labelWidth = 100;
            EditorGUIUtility.fieldWidth = 120;
            GUILayout.BeginHorizontal("BOX");

            GUILayout.BeginVertical();

            actionTypesItems = Enum.GetNames(typeof(ActionType)).ToList();
            List<ActionType> notValidForZones = new List<ActionType>(ActionsInputTypeHolder.instance.actionsWhichCannotBeUsedForActionZones);
            foreach (ActionType type in notValidForZones)
                actionTypesItems.Remove(type.ToString());

            foreach (ZoneAction m in zoneActions.Where(m => zoneActions[i].actionType != m.actionType))
                actionTypesItems.Remove(m.actionType.ToString());
            selectedIndex = actionTypesItems.FindIndex(s => zoneActions[i].actionType.ToString() == s);
            int temp = EditorGUILayout.Popup("Action Type", selectedIndex, actionTypesItems.ToArray(), GUILayout.ExpandWidth(false));
            if (temp != selectedIndex)
                SetDirty(targetScript);
            selectedIndex = temp;
            zoneActions[i].actionType = (ActionType) Enum.Parse(typeof(ActionType), actionTypesItems[selectedIndex]);


            EditorGUIUtility.labelWidth = 210;
            EditorGUIUtility.fieldWidth = 55;
            EditorGUILayout.PropertyField(item.FindPropertyRelative(nameof(ZoneAction.overrideActionControlType)), GUILayout.ExpandWidth(false));
            EditorGUILayout.Space(10);
            EditorGUIUtility.labelWidth = 120;
            EditorGUIUtility.fieldWidth = 100;


            switch (ActionsInputTypeHolder.instance.actionsInputTypeDic[zoneActions[i].actionType])
            {
                case InputValueVariableType.BooleanToggle:
                case InputValueVariableType.BooleanContinues:
                    bool enterResult = EditorGUILayout.Toggle(ToCamelCase(nameof(ZoneAction.zoneEnterValue)), zoneActions[i].zoneEnterValue.GetValue<bool>(), GUILayout.ExpandWidth(false));
                    bool exitResult = EditorGUILayout.Toggle(ToCamelCase(nameof(ZoneAction.zoneExitValue)), zoneActions[i].zoneExitValue.GetValue<bool>(), GUILayout.ExpandWidth(false));
                    if (zoneActions[i].zoneEnterValue.GetValue<bool>() != enterResult || zoneActions[i].zoneExitValue.GetValue<bool>() != exitResult)
                        SetDirty(targetScript);
                    zoneActions[i].zoneEnterValue.SetActionValue(enterResult);
                    zoneActions[i].zoneExitValue.SetActionValue(exitResult);
                    break;
                case InputValueVariableType.Vector1:
                    float enterResultFloat = EditorGUILayout.FloatField(ToCamelCase(nameof(ZoneAction.zoneEnterValue)), zoneActions[i].zoneEnterValue.GetValue<float>(), GUILayout.ExpandWidth(false));
                    float exitResultFloat = EditorGUILayout.FloatField(ToCamelCase(nameof(ZoneAction.zoneExitValue)), zoneActions[i].zoneExitValue.GetValue<float>(), GUILayout.ExpandWidth(false));
                    if (Math.Abs(zoneActions[i].zoneEnterValue.GetValue<float>() - enterResultFloat) > 0.0001f || Math.Abs(zoneActions[i].zoneExitValue.GetValue<float>() - exitResultFloat) > 0.0001f)
                        SetDirty(targetScript);
                    zoneActions[i].zoneEnterValue.SetActionValue(enterResultFloat);
                    zoneActions[i].zoneExitValue.SetActionValue(exitResultFloat);
                    break;
                case InputValueVariableType.Vector2:
                    Vector2 enterResultV = EditorGUILayout.Vector2Field(ToCamelCase(nameof(ZoneAction.zoneEnterValue)), zoneActions[i].zoneEnterValue.GetValue<Vector2>(), GUILayout.ExpandWidth(false));
                    Vector2 exitResultV = EditorGUILayout.Vector2Field(ToCamelCase(nameof(ZoneAction.zoneExitValue)), zoneActions[i].zoneExitValue.GetValue<Vector2>(), GUILayout.ExpandWidth(false));
                    if (zoneActions[i].zoneEnterValue.GetValue<Vector2>() != enterResultV || zoneActions[i].zoneEnterValue.GetValue<Vector2>() != exitResultV)
                        SetDirty(targetScript);
                    zoneActions[i].zoneEnterValue.SetActionValue(enterResultV);
                    zoneActions[i].zoneExitValue.SetActionValue(exitResultV);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            GUILayout.EndVertical();

            style.normal.textColor = Color.red;
            if (GUILayout.Button("Remove", style, GUILayout.ExpandWidth(false)))
            {
                targetScript.zoneActions.Remove(zoneActions[i]);
                SetDirty(targetScript);
            }

            GUILayout.EndHorizontal();

            EditorGUILayout.Space();
            EditorGUI.indentLevel -= 1;
        }


        if (actionTypesItems.Count > 1 || zoneActions.Count == 0)
        {
            EditorGUILayout.Space(10);
            style.normal.textColor = Color.blue;

            if (GUILayout.Button("Add Action", style))
            {
                ZoneAction zoneAction = new ZoneAction();
                for (int i = 0; i < actionTypesItems.Count; i++)
                {
                    if (i == selectedIndex)
                        continue;
                    zoneAction.actionType = (ActionType) Enum.Parse(typeof(ActionType), actionTypesItems[i]);
                    break;
                }
                targetScript.zoneActions.Add(zoneAction);
                SetDirty(targetScript);
            }
        }
    }

    private static string ToCamelCase(string text)
    {
        text = text.Substring(0, 1).ToUpper() + text.Substring(1);
        return System.Text.RegularExpressions.Regex.Replace(text, "(\\B[A-Z])", " $1");
    }

    private static void SetDirty(Component targetScript)
    {
        EditorUtility.SetDirty(targetScript);
        EditorUtility.SetDirty(targetScript.gameObject);
        var prefabStage = PrefabStageUtility.GetCurrentPrefabStage();
        if (prefabStage != null)
            EditorSceneManager.MarkSceneDirty(prefabStage.scene);
    }
}

[CustomEditor(typeof(ActionZoneTrigger))]
public class ActionZoneTriggerCustomInspector : Editor
{
    private ActionZoneTrigger targetScript;

    public override void OnInspectorGUI()
    {
        if (ActionsInputTypeHolder.instance == null)
            FindObjectOfType<ActionsInputTypeHolder>().GetComponent<ActionsInputTypeHolder>().Init();
        serializedObject.Update();
        EditorListZoneTrigger.DrawList((ActionZoneTrigger) target, serializedObject.FindProperty(nameof(ActionZoneTrigger.zoneActions)), false, false, "Actions", "Number Of Actions");
        serializedObject.ApplyModifiedProperties();
    }

}
