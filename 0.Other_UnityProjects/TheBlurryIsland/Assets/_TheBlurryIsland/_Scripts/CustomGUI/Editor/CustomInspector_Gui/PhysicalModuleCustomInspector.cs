﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.Experimental.SceneManagement;
using UnityEditor.SceneManagement;
using UnityEngine;


public static class EditorListPhysicalProperty
{
    public static void DrawList(PhysicalModule targetScript, SerializedProperty list, bool showListSize, bool showListTitle, string listTitle, string sizeLabel)
    {
        List<string> physicalPropertiesTypesItems = new List<string>();
        int selectedIndex = 0;

        GUIStyle style = new GUIStyle(GUI.skin.button);
        List<PhysicalProperty> physicalProperties = new List<PhysicalProperty>();
        if (targetScript.physicalProperties != null && targetScript.physicalProperties.Count != 0)
            physicalProperties = new List<PhysicalProperty>(targetScript.physicalProperties);

        EditorGUILayout.Space();

        // Showing list title
        if (showListTitle)
        {
            EditorGUILayout.LabelField(listTitle);
            EditorGUILayout.Space();
        }

        // Showing list size
        if (showListSize && list.isExpanded)
        {
            EditorGUILayout.PropertyField(list.FindPropertyRelative("Array.size"), new GUIContent(sizeLabel));
            EditorGUILayout.Space();
        }

        EditorGUILayout.Space();

        style.wordWrap = true;
        style.normal.textColor = Color.black;

        // Drawing List elements ( actions )
        for (int i = 0; i < list.arraySize; i++)
        {

            EditorGUILayout.LabelField("Property #" + (i + 1));
            EditorGUI.indentLevel += 1;

            EditorGUIUtility.labelWidth = 120;
            EditorGUIUtility.fieldWidth = 100;
            GUILayout.BeginHorizontal("BOX");

            GUILayout.BeginVertical();


            physicalPropertiesTypesItems = Enum.GetNames(typeof(PhysicalPropertyType)).ToList();
            foreach (PhysicalProperty m in physicalProperties.Where(m => physicalProperties[i].type != m.type))
                physicalPropertiesTypesItems.Remove(m.type.ToString());

            selectedIndex = physicalPropertiesTypesItems.FindIndex(s => physicalProperties[i].type.ToString() == s);
            int temp = EditorGUILayout.Popup("Property Type", selectedIndex, physicalPropertiesTypesItems.ToArray(), GUILayout.ExpandWidth(false));
            if (selectedIndex != temp)
                SetDirty(targetScript);
            selectedIndex = temp;
            physicalProperties[i].type = (PhysicalPropertyType) Enum.Parse(typeof(PhysicalPropertyType), physicalPropertiesTypesItems[selectedIndex]);

            switch (physicalProperties[i].type)
            {
                case PhysicalPropertyType.ForceInitial:
                case PhysicalPropertyType.ForceContinues:
                case PhysicalPropertyType.ForceOnCollide:
                case PhysicalPropertyType.AngularVelocity:
                case PhysicalPropertyType.Velocity:
                case PhysicalPropertyType.Rotation:
                case PhysicalPropertyType.Position:
                    Vector3 tempVector3 = EditorGUILayout.Vector3Field(ToCamelCase(nameof(PhysicalProperty.value)), physicalProperties[i].value.GetValue<Vector3>(), GUILayout.ExpandWidth(false));
                    if (tempVector3 != physicalProperties[i].value.GetValue<Vector3>())
                        SetDirty(targetScript);
                    physicalProperties[i].value.SetValue(tempVector3);
                    break;

                case PhysicalPropertyType.Mass:
                case PhysicalPropertyType.Acceleration:
                case PhysicalPropertyType.Transparency:
                case PhysicalPropertyType.Resistance:
                    float tempFloat = EditorGUILayout.FloatField(ToCamelCase(nameof(PhysicalProperty.value)), physicalProperties[i].value.GetValue<float>(), GUILayout.ExpandWidth(false));
                    if (Math.Abs(tempFloat - physicalProperties[i].value.GetValue<float>()) > 0.001f)
                        SetDirty(targetScript);
                    physicalProperties[i].value.SetValue(tempFloat);
                    break;

                case PhysicalPropertyType.Vibration:
                    Vector2 tempVector2 = EditorGUILayout.Vector2Field(ToCamelCase(nameof(PhysicalProperty.value)), physicalProperties[i].value.GetValue<Vector2>(), GUILayout.ExpandWidth(false));
                    if (tempVector2 != physicalProperties[i].value.GetValue<Vector2>())
                        SetDirty(targetScript);
                    physicalProperties[i].value.SetValue(tempVector2);
                    break;

                case PhysicalPropertyType.Color:
                    Color tempColor = EditorGUILayout.ColorField(ToCamelCase(nameof(PhysicalProperty.value)), physicalProperties[i].value.GetValue<Color>(), GUILayout.ExpandWidth(false));
                    if (tempColor != physicalProperties[i].value.GetValue<Color>())
                        SetDirty(targetScript);
                    physicalProperties[i].value.SetValue(tempColor);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            EditorGUILayout.Space();
            EditorGUILayout.Space();


            GUILayout.EndVertical();

            style.normal.textColor = Color.red;
            if (GUILayout.Button("Remove", style, GUILayout.ExpandWidth(false)))
            {
                targetScript.physicalProperties?.Remove(physicalProperties[i]);
                SetDirty(targetScript);
            }

            GUILayout.EndHorizontal();
            EditorGUILayout.Space();
            EditorGUI.indentLevel -= 1;
        }


        if (physicalPropertiesTypesItems.Count > 1 || physicalProperties.Count == 0)
        {
            EditorGUILayout.Space(10);
            style.normal.textColor = Color.blue;

            if (GUILayout.Button("Add Physical Property", style))
            {
                PhysicalProperty newProperty = new PhysicalProperty();
                for (int i = 0; i < physicalPropertiesTypesItems.Count; i++)
                {
                    if (i == selectedIndex)
                        continue;
                    newProperty.type = (PhysicalPropertyType) Enum.Parse(typeof(PhysicalPropertyType), physicalPropertiesTypesItems[i]);
                    break;
                }

                if (targetScript.physicalProperties == null)
                    targetScript.physicalProperties = new List<PhysicalProperty>();
                targetScript.physicalProperties.Add(newProperty);
                SetDirty(targetScript);
            }
        }

        EditorGUILayout.Space(30);
    }

    private static string ToCamelCase(string text)
    {
        text = text.Substring(0, 1).ToUpper() + text.Substring(1);
        return System.Text.RegularExpressions.Regex.Replace(text, "(\\B[A-Z])", " $1");
    }

    private static void SetDirty(Component targetScript)
    {
        EditorUtility.SetDirty(targetScript);
        EditorUtility.SetDirty(targetScript.gameObject);
        var prefabStage = PrefabStageUtility.GetCurrentPrefabStage();
        if (prefabStage != null)
            EditorSceneManager.MarkSceneDirty(prefabStage.scene);
    }
}

[CustomEditor(typeof(PhysicalModule))]
public class PhysicalModuleCustomInspector : Editor
{
    private PhysicalModule targetScript;

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        PhysicalModule physicalModule = (PhysicalModule) target;
        EditorListPhysicalProperty.DrawList(physicalModule, serializedObject.FindProperty(nameof(PhysicalModule.physicalProperties)), false, false, "Physical Properties", "Number Of Properties");
        serializedObject.ApplyModifiedProperties();
    }
}
