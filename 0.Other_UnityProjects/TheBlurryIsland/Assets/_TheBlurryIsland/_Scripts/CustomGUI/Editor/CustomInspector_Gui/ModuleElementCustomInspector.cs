﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.Experimental.SceneManagement;
using UnityEditor.SceneManagement;
using UnityEngine;


public static class EditorListModuleElement
{
    public static void DrawList(ToggleModuleElement targetScript, SerializedProperty list, bool showListSize, bool showListTitle, string listTitle, string sizeLabel)
    {
        List<string> tweenTypeItems = new List<string>();
        int selectedIndex = 0;

        GUIStyle style = new GUIStyle(GUI.skin.button);
        GUIStyle titleStyle = new GUIStyle {fontStyle = FontStyle.Bold};
        List<ModuleValue> moduleValues = new List<ModuleValue>();
        if (targetScript.moduleValues!=null && targetScript.moduleValues.Count != 0)
            moduleValues = new List<ModuleValue>(targetScript.moduleValues);

        EditorGUILayout.Space();

        // Showing list title
        if (showListTitle)
        {
            EditorGUILayout.LabelField(listTitle);
            EditorGUILayout.Space();
        }

        // Showing list size
        if (showListSize && list.isExpanded)
        {
            EditorGUILayout.PropertyField(list.FindPropertyRelative("Array.size"), new GUIContent(sizeLabel));
            EditorGUILayout.Space();
        }

        EditorGUILayout.Space();

        style.normal.textColor = Color.magenta;
        style.wordWrap = true;
        if (moduleValues.FindAll(value => value.toggleType == TweenType.Alpha).Count > 0 && moduleValues.FindAll(value => value.toggleType == TweenType.Color).Count > 0)
            EditorGUILayout.LabelField("Warning: Using both color and alpha, if their toggle time overlaps, can cause color not working, you can instead use only color and change its alpha in color selection inspector", style);
        style.normal.textColor = Color.black;


        // Drawing List elements ( actions )
        for (int i = 0; i < list.arraySize; i++)
        {
            SerializedProperty item = list.GetArrayElementAtIndex(i);

            EditorGUILayout.LabelField("Tween #" + (i + 1));
            EditorGUI.indentLevel += 1;

            EditorGUIUtility.labelWidth = 210;
            EditorGUIUtility.fieldWidth = 100;
            GUILayout.BeginHorizontal("BOX");


            GUILayout.BeginVertical();


            tweenTypeItems = Enum.GetNames(typeof(TweenType)).ToList();
            foreach (ModuleValue m in moduleValues.Where(m => moduleValues[i].toggleType != m.toggleType))
                tweenTypeItems.Remove(m.toggleType.ToString());
            selectedIndex = tweenTypeItems.FindIndex(s => moduleValues[i].toggleType.ToString() == s);
            int temp = EditorGUILayout.Popup("Toggle Type", selectedIndex, tweenTypeItems.ToArray(), GUILayout.ExpandWidth(false));
            if (selectedIndex != temp)
                SetDirty(targetScript);
            selectedIndex = temp;
            moduleValues[i].toggleType = (TweenType) Enum.Parse(typeof(TweenType), tweenTypeItems[selectedIndex]);


            EditorGUILayout.PropertyField(item.FindPropertyRelative(nameof(ModuleValue.recursion)), GUILayout.ExpandWidth(false));
            EditorGUILayout.Space();


            EditorGUILayout.PropertyField(item.FindPropertyRelative(nameof(ModuleValue.initialState)));
            switch (moduleValues[i].toggleType)
            {
                case TweenType.Position:
                case TweenType.Rotation:
                    EditorGUILayout.PropertyField(item.FindPropertyRelative(nameof(ModuleValue.minValue)));
                    EditorGUILayout.PropertyField(item.FindPropertyRelative(nameof(ModuleValue.maxValue)));
                    break;
                case TweenType.Scale:
                    EditorGUILayout.PropertyField(item.FindPropertyRelative(nameof(ModuleValue.minValueScale)));
                    EditorGUILayout.PropertyField(item.FindPropertyRelative(nameof(ModuleValue.maxValueScale)));
                    break;
                case TweenType.Alpha:
                    EditorGUILayout.PropertyField(item.FindPropertyRelative(nameof(ModuleValue.minAlpha)));
                    EditorGUILayout.PropertyField(item.FindPropertyRelative(nameof(ModuleValue.maxAlpha)));
                    break;
                case TweenType.Color:
                    EditorGUILayout.PropertyField(item.FindPropertyRelative(nameof(ModuleValue.minColor)));
                    EditorGUILayout.PropertyField(item.FindPropertyRelative(nameof(ModuleValue.maxColor)));
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            EditorGUILayout.Space();
            EditorGUILayout.Space();

            EditorGUILayout.LabelField("Toggle Settings", titleStyle);
            EditorGUILayout.PropertyField(item.FindPropertyRelative(nameof(ModuleValue.toggleDurationAtFirst)));
            EditorGUILayout.PropertyField(item.FindPropertyRelative(nameof(ModuleValue.toggleDelayAtFirst)));
            EditorGUILayout.PropertyField(item.FindPropertyRelative(nameof(ModuleValue.toggleEasingTypeAtFirst)));

            if (moduleValues[i].recursion == ToggleState.On)
            {
                EditorGUILayout.Space();
                EditorGUILayout.LabelField("RecursionSettings", titleStyle);
                EditorGUILayout.PropertyField(item.FindPropertyRelative(nameof(ModuleValue.toggleDurationAtRecursion)));
                EditorGUILayout.PropertyField(item.FindPropertyRelative(nameof(ModuleValue.toggleDelayAtRecursion)));
                EditorGUILayout.PropertyField(item.FindPropertyRelative(nameof(ModuleValue.toggleEasingTypeAtRecursion)));
            }
            GUILayout.EndVertical();

            style.normal.textColor = Color.red;
            if (GUILayout.Button("Remove", style, GUILayout.ExpandWidth(false)))
            {
                targetScript.moduleValues?.Remove(moduleValues[i]);
                SetDirty(targetScript);
            }

            GUILayout.EndHorizontal();

            EditorGUILayout.Space();
            EditorGUI.indentLevel -= 1;
        }


        if (tweenTypeItems.Count > 1 || moduleValues.Count == 0)
        {
            EditorGUILayout.Space(10);
            style.normal.textColor = Color.blue;

            if (GUILayout.Button("Add Action", style))
            {
                ModuleValue newModule = new ModuleValue();
                for (int i = 0; i < tweenTypeItems.Count; i++)
                {
                    if (i == selectedIndex)
                        continue;
                    newModule.toggleType = (TweenType) Enum.Parse(typeof(TweenType), tweenTypeItems[i]);
                    break;
                }
                targetScript.moduleValues?.Add(newModule);
                SetDirty(targetScript);
            }
        }
        EditorGUILayout.Space(30);
    }

    private static void SetDirty(Component targetScript)
    {
        EditorUtility.SetDirty(targetScript);
        EditorUtility.SetDirty(targetScript.gameObject);
        var prefabStage = PrefabStageUtility.GetCurrentPrefabStage();
        if (prefabStage != null)
            EditorSceneManager.MarkSceneDirty(prefabStage.scene);
    }
}

[CustomEditor(typeof(ToggleModuleElement))]
public class ModuleElementCustomInspector : Editor
{
    private ToggleModuleElement targetScript;

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        ToggleModuleElement toggleModuleElement = (ToggleModuleElement) target;
        EditorListModuleElement.DrawList(toggleModuleElement, serializedObject.FindProperty(nameof(ToggleModuleElement.moduleValues)), false, false, "Module Tweens", "Number Of Tweens");
        if (!Application.isPlaying)
        {
            toggleModuleElement.GetComponents();
            toggleModuleElement.SetInitialState();
        }
        serializedObject.ApplyModifiedProperties();
    }
}
