﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEditor.Experimental.SceneManagement;
using UnityEditor.SceneManagement;
using UnityEngine;

public static class EditorListCharacterIK
{
    public static void DrawList(CharacterIK targetScript, SerializedProperty list, bool showListSize, bool showListTitle, string listTitle, string sizeLabel)
    {
        List<string> ikTypeItems = new List<string>();
        int selectedIndex = 0;

        GUIStyle style = new GUIStyle(GUI.skin.button);
        List<IKClass> ikClasses = new List<IKClass>();
        if (targetScript.iks != null && targetScript.iks.Count != 0)
            ikClasses = new List<IKClass>(targetScript.iks);

        EditorGUILayout.Space();

        // Showing list title
        if (showListTitle)
        {
            EditorGUILayout.LabelField(listTitle);
            EditorGUILayout.Space();
        }

        // Showing list size
        if (showListSize && list.isExpanded)
        {
            EditorGUILayout.PropertyField(list.FindPropertyRelative("Array.size"), new GUIContent(sizeLabel));
            EditorGUILayout.Space();
        }

        EditorGUILayout.Space();

        // Drawing List elements ( actions )
        for (int i = 0; i < list.arraySize; i++)
        {
            SerializedProperty item = list.GetArrayElementAtIndex(i);

            EditorGUILayout.LabelField("IK #" + (i + 1));
            EditorGUI.indentLevel += 1;

            EditorGUIUtility.labelWidth = 190;
            EditorGUIUtility.fieldWidth = 150;
            GUILayout.BeginHorizontal("BOX");


            GUILayout.BeginVertical();


            ikTypeItems = Enum.GetNames(typeof(IkType)).ToList();
            foreach (IKClass m in ikClasses.Where(m => ikClasses[i].GetIkType() != m.GetIkType()))
                ikTypeItems.Remove(m.GetIkType().ToString());
            selectedIndex = ikTypeItems.FindIndex(s => ikClasses[i].GetIkType().ToString() == s);
            int temp = EditorGUILayout.Popup("IK Type", selectedIndex, ToCamelCase(ikTypeItems.ToArray()), GUILayout.ExpandWidth(false));
            if (selectedIndex != temp)
                SetDirty(targetScript);
            selectedIndex = temp;
            ikClasses[i].SetIkType((IkType) Enum.Parse(typeof(IkType), ikTypeItems[selectedIndex]));

            if (ikClasses[i].type != IkType.FootOnFloor)
            {
                EditorGUILayout.PropertyField(item.FindPropertyRelative(nameof(IKClass.defaultIkPoint)), GUILayout.ExpandWidth(false));
                EditorGUILayout.PropertyField(item.FindPropertyRelative(nameof(IKClass.needsTwoDynamicIkPoints)), GUILayout.ExpandWidth(false));
            }

            if (ikClasses[i].type == IkType.Climb)
            {
                EditorGUILayout.Space();
                style.wordWrap = true;
                style.normal.textColor = Color.blue;
                EditorGUILayout.LabelField("For Climb it is important to first add Right hand and then second hand as Ik Points", style);
            }

            if (ikClasses[i].GetIkType() == IkType.Intractable)
            {
                EditorGUILayout.PropertyField(item.FindPropertyRelative(nameof(IKClass.avatarIKGoalsWhenOnRight)), GUILayout.ExpandWidth(false));
                EditorGUILayout.PropertyField(item.FindPropertyRelative(nameof(IKClass.avatarIKGoalsWhenOnLeft)), GUILayout.ExpandWidth(false));
                EditorGUILayout.Space();
            }
            else if (ikClasses[i].GetIkType() != IkType.PointOfInterest && ikClasses[i].type != IkType.FootOnFloor)
                EditorGUILayout.PropertyField(item.FindPropertyRelative(nameof(IKClass.avatarIKGoals)), GUILayout.ExpandWidth(false));

            EditorGUILayout.Space();

            EditorGUILayout.PropertyField(item.FindPropertyRelative(nameof(IKClass.ikWeight)));
            if (ikClasses[i].GetIkType() == IkType.PointOfInterest)
            {
                EditorGUILayout.PropertyField(item.FindPropertyRelative(nameof(IKClass.ikBodyWeight)));
                EditorGUILayout.PropertyField(item.FindPropertyRelative(nameof(IKClass.ikHeadWeight)));
                EditorGUILayout.PropertyField(item.FindPropertyRelative(nameof(IKClass.ikEyesWeight)));
                EditorGUILayout.PropertyField(item.FindPropertyRelative(nameof(IKClass.ikClampWeight)));
                EditorGUILayout.Space();
            }

            if (ikClasses[i].type != IkType.FootOnFloor)
            {
                EditorGUILayout.PropertyField(item.FindPropertyRelative(nameof(IKClass.ikWeightChangeSpeed)));
                EditorGUILayout.PropertyField(item.FindPropertyRelative(nameof(IKClass.ikPointChangeSpeed)));
            }

            EditorGUILayout.Space();
            EditorGUILayout.Space();

            if (ikClasses[i].type == IkType.FootOnFloor)
            {
                EditorGUILayout.PropertyField(item.FindPropertyRelative(nameof(IKClass.footIkOffset)));
                EditorGUILayout.PropertyField(item.FindPropertyRelative(nameof(IKClass.layerMask)));
            }
            else if (ikClasses[i].type != IkType.Climb)
            {
                EditorGUILayout.PropertyField(item.FindPropertyRelative(nameof(IKClass.blockInCrouch)));
                EditorGUILayout.PropertyField(item.FindPropertyRelative(nameof(IKClass.noIkBehindOfCharacter)));
            }

            if (ikClasses[i].noIkBehindOfCharacter)
                EditorGUILayout.PropertyField(item.FindPropertyRelative(nameof(IKClass.zOffsetForCheckingBehind)));


            GUILayout.EndVertical();

            style.normal.textColor = Color.red;
            if (GUILayout.Button("Remove", style, GUILayout.ExpandWidth(false)))
            {
                targetScript.iks?.Remove(ikClasses[i]);
                SetDirty(targetScript);
            }

            GUILayout.EndHorizontal();

            EditorGUILayout.Space();
            EditorGUI.indentLevel -= 1;
        }


        if (ikTypeItems.Count > 1 || ikClasses.Count == 0)
        {
            EditorGUILayout.Space(10);
            style.normal.textColor = Color.blue;

            if (GUILayout.Button("Add IK", style))
            {
                IKClass newModule = new IKClass();
                for (int i = 0; i < ikTypeItems.Count; i++)
                {
                    if (i == selectedIndex)
                        continue;
                    newModule.SetIkType((IkType) Enum.Parse(typeof(IkType), ikTypeItems[i]));
                    break;
                }

                targetScript.iks?.Add(newModule);
                SetDirty(targetScript);
            }
        }

        EditorGUILayout.Space(30);
    }

    private static void SetDirty(Component targetScript)
    {
        EditorUtility.SetDirty(targetScript);
        EditorUtility.SetDirty(targetScript.gameObject);
        var prefabStage = PrefabStageUtility.GetCurrentPrefabStage();
        if (prefabStage != null)
            EditorSceneManager.MarkSceneDirty(prefabStage.scene);
    }

    private static string[] ToCamelCase(string[] texts)
    {
        return texts.Select(ToCamelCase).ToArray();
    }

    private static string ToCamelCase(string text)
    {
        text = text.Substring(0, 1).ToUpper() + text.Substring(1);
        return Regex.Replace(text, "(\\B[A-Z])", " $1");
    }
}


[CustomEditor(typeof(CharacterIK))]
public class CharacterIKCustomInspector : Editor
{
    private CharacterIK targetScript;

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        CharacterIK characterIK = (CharacterIK) target;
        EditorGUILayout.PropertyField(serializedObject.FindProperty(nameof(CharacterIK.ikDefaultPointsParents)), GUILayout.ExpandWidth(true));
        EditorListCharacterIK.DrawList(characterIK, serializedObject.FindProperty(nameof(CharacterIK.iks)), false, false, "Iks", "Number Of Iks");
        serializedObject.ApplyModifiedProperties();
    }
}
