﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.Experimental.SceneManagement;
using UnityEditor.SceneManagement;
using UnityEngine;


public static class EditorListDataVariation
{
    public static void DrawList(DataClassificationModuleGenerator targetScript, SerializedProperty list, bool showListSize, bool showListTitle, string listTitle, string sizeLabel)
    {
        GUIStyle style = new GUIStyle(GUI.skin.button);
        List<DataVariation> dataVariations = new List<DataVariation>();
        if (targetScript.dataVariations != null && targetScript.dataVariations.Count != 0)
            dataVariations = new List<DataVariation>(targetScript.dataVariations);

        EditorGUILayout.Space();

        // Showing list title
        if (showListTitle)
        {
            EditorGUILayout.LabelField(listTitle);
            EditorGUILayout.Space();
        }

        // Showing list size
        if (showListSize && list.isExpanded)
        {
            EditorGUILayout.PropertyField(list.FindPropertyRelative("Array.size"), new GUIContent(sizeLabel));
            EditorGUILayout.Space();
        }

        EditorGUILayout.Space();

        style.wordWrap = true;
        style.normal.textColor = Color.black;

        // Drawing List elements ( actions )
        for (int i = 0; i < list.arraySize; i++)
        {
            SerializedProperty item = list.GetArrayElementAtIndex(i);

            EditorGUILayout.LabelField("Variation #" + (i + 1));
            EditorGUI.indentLevel += 1;

            EditorGUIUtility.labelWidth = 120;
            EditorGUIUtility.fieldWidth = 100;

            GUIStyle gs = new GUIStyle {normal = {background = MakeTex(600, 1, new Color(0.5f, 0.3f, 0.3f, 0.1f))}};
            GUILayout.BeginHorizontal(gs);

            GUILayout.BeginVertical();

            List<string> physicalPropertiesTypesItems = new List<string>();
            int selectedIndex = 0;

            style = new GUIStyle(GUI.skin.button);
            List<PhysicalProperty> physicalProperties = new List<PhysicalProperty>();
            if (targetScript.dataVariations != null && (targetScript.dataVariations[i].physicalProperties != null && targetScript.dataVariations[i].physicalProperties.Count != 0))
                physicalProperties = new List<PhysicalProperty>(targetScript.dataVariations[i].physicalProperties);


            EditorGUILayout.Space();

            style.wordWrap = true;
            style.normal.textColor = Color.black;

            EditorGUILayout.PropertyField(item.FindPropertyRelative(nameof(DataVariation.prefab)));

            for (int j = 0; j < physicalProperties.Count; j++)
            {

                EditorGUILayout.LabelField("Property #" + (j + 1));
                EditorGUI.indentLevel += 1;

                EditorGUIUtility.labelWidth = 120;
                EditorGUIUtility.fieldWidth = 100;
                GUILayout.BeginHorizontal("BOX");

                GUILayout.BeginVertical();


                physicalPropertiesTypesItems = Enum.GetNames(typeof(PhysicalPropertyType)).ToList();
                foreach (PhysicalProperty m in physicalProperties.Where(m => physicalProperties[j].type != m.type))
                    physicalPropertiesTypesItems.Remove(m.type.ToString());

                selectedIndex = physicalPropertiesTypesItems.FindIndex(s => physicalProperties[j].type.ToString() == s);
                int temp = EditorGUILayout.Popup("Property Type", selectedIndex, physicalPropertiesTypesItems.ToArray(), GUILayout.ExpandWidth(false));
                if (selectedIndex != temp)
                    SetDirty(targetScript);
                selectedIndex = temp;
                physicalProperties[j].type = (PhysicalPropertyType) Enum.Parse(typeof(PhysicalPropertyType), physicalPropertiesTypesItems[selectedIndex]);

                switch (physicalProperties[j].type)
                {
                    case PhysicalPropertyType.ForceInitial:
                    case PhysicalPropertyType.ForceContinues:
                    case PhysicalPropertyType.ForceOnCollide:
                    case PhysicalPropertyType.AngularVelocity:
                    case PhysicalPropertyType.Velocity:
                    case PhysicalPropertyType.Rotation:
                    case PhysicalPropertyType.Position:
                        Vector3 tempVector3 = EditorGUILayout.Vector3Field(ToCamelCase(nameof(PhysicalProperty.value)), physicalProperties[j].value.GetValue<Vector3>(), GUILayout.ExpandWidth(false));
                        if (tempVector3 != dataVariations[i].physicalProperties[j].value.GetValue<Vector3>())
                            SetDirty(targetScript);
                        dataVariations[i].physicalProperties[j].value.SetValue(tempVector3);
                        break;

                    case PhysicalPropertyType.Mass:
                    case PhysicalPropertyType.Acceleration:
                    case PhysicalPropertyType.Transparency:
                    case PhysicalPropertyType.Resistance:
                        float tempFloat = EditorGUILayout.FloatField(ToCamelCase(nameof(PhysicalProperty.value)), physicalProperties[j].value.GetValue<float>(), GUILayout.ExpandWidth(false));
                        if (Math.Abs(tempFloat - dataVariations[i].physicalProperties[j].value.GetValue<float>()) > 0.001f)
                            SetDirty(targetScript);
                        dataVariations[i].physicalProperties[j].value.SetValue(tempFloat);
                        break;

                    case PhysicalPropertyType.Vibration:
                        Vector2 tempVector2 = EditorGUILayout.Vector2Field(ToCamelCase(nameof(PhysicalProperty.value)), physicalProperties[j].value.GetValue<Vector2>(), GUILayout.ExpandWidth(false));
                        if (tempVector2 != dataVariations[i].physicalProperties[j].value.GetValue<Vector2>())
                            SetDirty(targetScript);
                        dataVariations[i].physicalProperties[j].value.SetValue(tempVector2);
                        break;

                    case PhysicalPropertyType.Color:
                        Color tempColor = EditorGUILayout.ColorField(ToCamelCase(nameof(PhysicalProperty.value)), physicalProperties[j].value.GetValue<Color>(), GUILayout.ExpandWidth(false));
                        if (tempColor != dataVariations[i].physicalProperties[j].value.GetValue<Color>())
                            SetDirty(targetScript);
                        physicalProperties[j].value.SetValue(tempColor);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                EditorGUILayout.Space();
                EditorGUILayout.Space();


                GUILayout.EndVertical();

                style.normal.textColor = Color.red;
                if (GUILayout.Button("Remove", style, GUILayout.ExpandWidth(false)))
                {
                    if (targetScript.dataVariations[i].physicalProperties != null)
                        targetScript.dataVariations[i].physicalProperties?.Remove(physicalProperties[j]);
                    SetDirty(targetScript);
                }

                GUILayout.EndHorizontal();
                EditorGUILayout.Space();
                EditorGUI.indentLevel -= 1;
            }

            if (physicalPropertiesTypesItems.Count > 1 || physicalProperties.Count == 0)
            {
                EditorGUILayout.Space(10);
                style.normal.textColor = Color.blue;

                if (GUILayout.Button("Add Physical Property", style))
                {
                    PhysicalProperty newProperty = new PhysicalProperty();
                    for (int j = 0; j < physicalPropertiesTypesItems.Count; j++)
                    {
                        if (j == selectedIndex)
                            continue;
                        newProperty.type = (PhysicalPropertyType) Enum.Parse(typeof(PhysicalPropertyType), physicalPropertiesTypesItems[j]);
                        break;
                    }

                    if (targetScript.dataVariations[i].physicalProperties == null)
                        targetScript.dataVariations[i].physicalProperties = new List<PhysicalProperty>();
                    targetScript.dataVariations[i].physicalProperties.Add(newProperty);
                    SetDirty(targetScript);
                }

                EditorGUILayout.Space(30);
            }



            EditorGUILayout.Space();
            EditorGUILayout.Space();

            GUILayout.EndVertical();

            style.normal.textColor = Color.red;
            if (GUILayout.Button("Remove", style, GUILayout.ExpandWidth(false)))
            {
                targetScript.dataVariations?.Remove(dataVariations[i]);
                SetDirty(targetScript);
            }

            GUILayout.EndHorizontal();
            EditorGUILayout.Space();
            EditorGUI.indentLevel -= 1;
        }

        EditorGUILayout.Space(10);
        style.normal.textColor = Color.blue;

        if (GUILayout.Button("Add Data Variation", style))
        {
            DataVariation newProperty = new DataVariation();
            if (targetScript.dataVariations == null)
                targetScript.dataVariations = new List<DataVariation>();
            targetScript.dataVariations.Add(newProperty);
            SetDirty(targetScript);
        }

        EditorGUILayout.Space(30);
    }

    private static Texture2D MakeTex(int width, int height, Color col)
    {
        Color[] pix = new Color[width*height];

        for(int i = 0; i < pix.Length; i++)
            pix[i] = col;

        Texture2D result = new Texture2D(width, height);
        result.SetPixels(pix);
        result.Apply();

        return result;
    }

    private static string ToCamelCase(string text)
    {
        text = text.Substring(0, 1).ToUpper() + text.Substring(1);
        return System.Text.RegularExpressions.Regex.Replace(text, "(\\B[A-Z])", " $1");
    }

    private static void SetDirty(Component targetScript)
    {
        EditorUtility.SetDirty(targetScript);
        EditorUtility.SetDirty(targetScript.gameObject);
        var prefabStage = PrefabStageUtility.GetCurrentPrefabStage();
        if (prefabStage != null)
            EditorSceneManager.MarkSceneDirty(prefabStage.scene);
    }
}


[CustomEditor(typeof(DataClassificationModuleGenerator))]
public class DataClassificationModuleManagerCustomInspector : Editor
{
    private DataClassificationModuleGenerator targetScript;

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        DataClassificationModuleGenerator dataClassificationModuleGenerator = (DataClassificationModuleGenerator) target;
        EditorGUILayout.PropertyField(serializedObject.FindProperty(nameof(DataClassificationModuleGenerator.instantiationDelayTime)), GUILayout.ExpandWidth(true));
        EditorListDataVariation.DrawList(dataClassificationModuleGenerator, serializedObject.FindProperty(nameof(DataClassificationModuleGenerator.dataVariations)), true, true, "Data Variations", "Number Of Variations");
        serializedObject.ApplyModifiedProperties();
    }
}
