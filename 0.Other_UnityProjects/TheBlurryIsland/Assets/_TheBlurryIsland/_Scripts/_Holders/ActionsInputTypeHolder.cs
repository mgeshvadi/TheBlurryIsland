﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;


[ExecuteInEditMode]
public class ActionsInputTypeHolder : MonoBehaviour
{
    public Dictionary<ActionType, InputAction> actionsInputActionsDic = new Dictionary<ActionType, InputAction>();
    public Dictionary<ActionType, InputValueVariableType> actionsInputTypeDic = new Dictionary<ActionType, InputValueVariableType>();
    [HideInInspector] public List<ActionType> actionsWhichCannotBeUsedForActionZones = new List<ActionType>();


    public static ActionsInputTypeHolder instance;

    private void Awake()
    {
        Init();
    }

    public void Init()
    {
        if (instance != null && instance != this)
            Destroy(gameObject);
        else
            instance = this;
        mainInputActions = new MainInputActions();
        mainInputActions.Enable();
        FillDictionaries();
        FillList();
    }

    private MainInputActions mainInputActions;

    private void FillDictionaries()
    {
        actionsInputActionsDic.Clear();
        actionsInputTypeDic.Clear();

        actionsInputTypeDic.Add(ActionType.MovementX, InputValueVariableType.Vector1);
        actionsInputActionsDic.Add(ActionType.MovementX, mainInputActions.CharacterNormal.MovementX);

        actionsInputTypeDic.Add(ActionType.MovementSwim, InputValueVariableType.Vector2);
        actionsInputActionsDic.Add(ActionType.MovementSwim, mainInputActions.CharacterSwim.MovementSwim);

        actionsInputTypeDic.Add(ActionType.MovementDrone, InputValueVariableType.Vector2);
        actionsInputActionsDic.Add(ActionType.MovementDrone, mainInputActions.Drone.MovementDrone);

        actionsInputTypeDic.Add(ActionType.Run, InputValueVariableType.BooleanContinues);
        actionsInputActionsDic.Add(ActionType.Run, mainInputActions.CharacterNormal.Run);

        actionsInputTypeDic.Add(ActionType.Jump, InputValueVariableType.BooleanContinues);
        actionsInputActionsDic.Add(ActionType.Jump, mainInputActions.CharacterNormal.Jump);

        actionsInputTypeDic.Add(ActionType.Crouch, InputValueVariableType.Vector1);
        actionsInputActionsDic.Add(ActionType.Crouch, mainInputActions.CharacterNormal.Crouch);

        actionsInputTypeDic.Add(ActionType.SwimMode, InputValueVariableType.BooleanToggle);
        actionsInputActionsDic.Add(ActionType.SwimMode, mainInputActions.CharacterSwim.SwimMode);

        actionsInputTypeDic.Add(ActionType.DroneMode, InputValueVariableType.BooleanToggle);
        actionsInputActionsDic.Add(ActionType.DroneMode, mainInputActions.Drone.DroneMode);

        actionsInputTypeDic.Add(ActionType.AlertMode, InputValueVariableType.BooleanToggle);
        actionsInputActionsDic.Add(ActionType.AlertMode, mainInputActions.CharacterNormal.AlertMode);

        actionsInputTypeDic.Add(ActionType.Action, InputValueVariableType.Button);
        actionsInputActionsDic.Add(ActionType.Action, mainInputActions.CharacterNormal.Action);

        actionsInputTypeDic.Add(ActionType.PushAndPull, InputValueVariableType.BooleanContinues);
        actionsInputActionsDic.Add(ActionType.PushAndPull, mainInputActions.CharacterNormal.PushAndPull);

        actionsInputTypeDic.Add(ActionType.Climb, InputValueVariableType.Button);
        actionsInputActionsDic.Add(ActionType.Climb, mainInputActions.CharacterNormal.Climb);

        actionsInputTypeDic.Add(ActionType.ClimbCancel, InputValueVariableType.Button);
        actionsInputActionsDic.Add(ActionType.ClimbCancel, mainInputActions.CharacterNormal.ClimbCancel);
    }

    private void FillList()
    {
        actionsWhichCannotBeUsedForActionZones.Clear();
        actionsWhichCannotBeUsedForActionZones.Add(ActionType.Action);
        actionsWhichCannotBeUsedForActionZones.Add(ActionType.PushAndPull);
        actionsWhichCannotBeUsedForActionZones.Add(ActionType.Climb);
        actionsWhichCannotBeUsedForActionZones.Add(ActionType.ClimbCancel);
    }
}
