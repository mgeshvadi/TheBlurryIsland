﻿using System.Collections.Generic;
using UnityEngine;



public class ActionZoneTrigger : MonoBehaviour
{
    public List<ZoneAction> zoneActions = new List<ZoneAction>();

    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Character"))
            return;
        CharacterActions characterActions = other.GetComponent<CharacterActions>();
        foreach (ZoneAction zone in zoneActions)
            characterActions.ActionZoneTriggerEnter(zone);
    }

    private void OnTriggerExit(Collider other)
    {
        if (!other.CompareTag("Character"))
            return;
        CharacterActions characterActions = other.GetComponent<CharacterActions>();
        foreach (ZoneAction zone in zoneActions)
            characterActions.ActionZoneTriggerExit(zone);
    }
}
