﻿using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;


[ExecuteInEditMode]
[RequireComponent(typeof(TextMeshPro))]
public class ZoneTextSetter : MonoBehaviour
{
    private ActionZoneTrigger zone;
    private TextMeshPro text;

    private void Awake()
    {
        GetTextComponent();
        GetZoneComponent();
    }

    private void Update()
    {
        if (!text)
            GetTextComponent();
        text.text = ComputeText();
    }

    private void GetTextComponent()
    {
        text = GetComponent<TextMeshPro>();
    }

    private void GetZoneComponent()
    {
        if (gameObject.transform.parent == null)
            return;
        zone = gameObject.transform.parent.GetComponent<ActionZoneTrigger>();
    }

    private string ComputeText()
    {
        if (!zone)
        {
            GetZoneComponent();
            if (!zone)
                return "ERROR: Component Not Found";
        }
        List<ZoneAction> zoneActions = new List<ZoneAction>(zone.zoneActions);
        return zoneActions.Count == 0 ? "Empty Zone" : zoneActions.Aggregate("Zone", (current, action) => current + "_" + action.actionType);
    }
}
