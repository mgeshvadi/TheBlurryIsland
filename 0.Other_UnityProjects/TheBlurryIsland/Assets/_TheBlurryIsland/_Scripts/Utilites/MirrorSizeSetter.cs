﻿using System;
using UnityEngine;
using UnityEngine.Events;


[ExecuteInEditMode]
public class MirrorSizeSetter : MonoBehaviour
{
    [SerializeField] private float mirrorSize = 1;
    [SerializeField] private float mirrorFarClip = 100;


    private Camera mirrorCamera;
    private UnityEvent onCameraSizeChanged;
    private float lastMirrorSize;

    private const float MirrorSizeToCameraDistanceProportionParameter1 = -1.59f;  // This value is calculated by testing on a simple mirror in unity scene
    private const float MirrorSizeToCameraDistanceProportionParameter2 = -1.375f; // This value is calculated by testing on a simple mirror in unity scene

    private void Awake()
    {
        FirstTimeSetup();
    }

    private void FirstTimeSetup()
    {
        InitComponents();
        AttachEvents();
        onCameraSizeChanged.Invoke();
        lastMirrorSize = mirrorSize;
        if (mirrorSize < 0)
            mirrorSize = 0;
    }

    private void AttachEvents()
    {
        onCameraSizeChanged?.RemoveAllListeners();
        onCameraSizeChanged = new UnityEvent();
        onCameraSizeChanged.AddListener(SetMirrorSize);
        onCameraSizeChanged.AddListener(SetCameraPosition);
        onCameraSizeChanged.AddListener(SetCameraFarClip);
    }

    private void InitComponents()
    {
        mirrorCamera = transform.GetChild(0).GetComponent<Camera>();
    }

    private void SetMirrorSize()
    {
        transform.localScale = new Vector3(1, mirrorSize, mirrorSize);
    }

    private void SetCameraPosition()
    {
        mirrorCamera.transform.localPosition = new Vector3(MirrorSizeToCameraDistanceProportionParameter1 + ((mirrorSize - 1) * MirrorSizeToCameraDistanceProportionParameter2), 0, 0);
    }

    private void SetCameraFarClip()
    {
        mirrorCamera.farClipPlane = mirrorFarClip + -1 * mirrorCamera.transform.localPosition.x + MirrorSizeToCameraDistanceProportionParameter1;
    }

#if UNITY_EDITOR // TODO : if real time change in size of mirrors are required remove this line
    private void Update()
    {
        // if (!mirrorCamera || onCameraSizeChanged.GetPersistentEventCount() == 0)
        FirstTimeSetup();
        AttachEvents();
        if (Math.Abs(mirrorSize - lastMirrorSize) > 0.005f)
        {
            onCameraSizeChanged.Invoke();
            if (mirrorSize < 0)
                mirrorSize = 0;

            lastMirrorSize = mirrorSize;
        }
    }
#endif
}
