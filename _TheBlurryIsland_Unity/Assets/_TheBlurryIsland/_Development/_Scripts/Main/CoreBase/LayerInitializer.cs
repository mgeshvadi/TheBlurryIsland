

namespace Blurry.Main.CoreBase
{
    public class LayerInitializationsResult : Application.CoreBase.LayerInitializationsResult
    {
    }

    public static class LayerInitializer
    {
        public static LayerInitializationsResult Initialize()
        {
            var applicationSideResult = Application.CoreBase.LayerInitializer.Initialize();
            LayerInitializationsResult result = new LayerInitializationsResult();
            return result;
        }
    }
}