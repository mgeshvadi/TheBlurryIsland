using System;
using UnityEngine;
using Blurry.Application.Base.AssetManagement.Base;
using Blurry.Application.Utility.Extensions;
using Object = UnityEngine.Object;


namespace Blurry.Main.Base.AssetManagement.UnityResourcesAssetLoader
{
    public class UnityResources_AssetLoader : IAssetLoader
    {
        public T Load<T>(Asset asset) where T : Object
        {
            return (T) Load(asset, typeof(T));
        }

        public T Load<T>(Asset asset, Type type) where T : Object
        {
            return (T) Load(asset, type);
        }

        public Object Load(Asset asset)
        {
            return Load(asset, typeof(Object));
        }

        public Object Load(Asset asset, Type type)
        {
            return Resources.Load(asset.Path, type);
        }


        public T[] LoadAll<T>(AssetFolder assetFolder) where T : Object
        {
            return LoadAll(assetFolder, typeof(T)).ConvertTo<Object, T>();
        }

        public T[] LoadAll<T>(AssetFolder assetFolder, Type type) where T : Object
        {
            return LoadAll(assetFolder, type).ConvertTo<Object, T>();
        }

        public Object[] LoadAll(AssetFolder assetFolder)
        {
            return LoadAll(assetFolder, typeof(Object));
        }

        public Object[] LoadAll(AssetFolder assetFolder, Type type)
        {
            return Resources.LoadAll(assetFolder.Path, type);
        }
    }
}