using UnityEngine;
using Blurry.Application.Base.AssetManagement.Base;
using Blurry.Main.Base.AssetManagement.UnityResourcesAssetLoader.Base;


namespace Blurry.Main.Base.AssetManagement.UnityResourcesAssetLoader
{
    public class UnityResources_AsyncAssetLoader : IAsyncAssetLoader
    {
        public IAssetRequest<T> Load<T>(Asset asset) where T : Object
        {
            return (UnityReseources_AssetRequest<T>) Resources.LoadAsync(asset.Path);
        }

        public IAssetRequest Load(Asset asset)
        {
            return (UnityReseources_AssetRequest) Resources.LoadAsync(asset.Path);
        }

        public IAssetRequest<T>[] LoadAll<T>(AssetFolder assetFolder) where T : Object
        {
            throw new System.NotImplementedException();
        }

        public IAssetRequest[] LoadAll(AssetFolder assetFolder)
        {
            throw new System.NotImplementedException();
        }
    }
}