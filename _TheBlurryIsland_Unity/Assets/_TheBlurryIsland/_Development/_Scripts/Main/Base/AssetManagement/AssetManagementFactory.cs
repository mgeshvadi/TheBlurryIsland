using Blurry.Application.Base.AssetManagement.Base;
using Blurry.Main.Base.AssetManagement.UnityResourcesAssetLoader;


namespace Blurry.Main.Base.AssetManagement
{
    public class AssetManagementFactory : IAssetManagementFactory
    {
        public IAssetLoader CreateActiveAssetLoader()
        {
            return new UnityResources_AssetLoader();
        }

        public IAsyncAssetLoader CreateActiveAsyncAssetLoader()
        {
            return new UnityResources_AsyncAssetLoader();
        }
    }
}