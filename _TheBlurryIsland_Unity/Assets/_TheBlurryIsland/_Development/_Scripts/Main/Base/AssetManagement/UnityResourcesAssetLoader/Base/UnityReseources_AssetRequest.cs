using UnityEngine;
using Blurry.Application.Base.AssetManagement.Base;


namespace Blurry.Main.Base.AssetManagement.UnityResourcesAssetLoader.Base
{
    public class UnityReseources_AssetRequest : ResourceRequest, IAssetRequest
    {
        public Object Asset => base.asset;
    }

    public class UnityReseources_AssetRequest<T> : ResourceRequest, IAssetRequest<T> where T : Object
    {
        public T Asset => (T) base.asset;
    }
}