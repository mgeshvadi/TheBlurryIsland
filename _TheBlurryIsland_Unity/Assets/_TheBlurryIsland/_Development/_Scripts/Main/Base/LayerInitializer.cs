using Blurry.Main.Base.AssetManagement;
using Blurry.Main.Base.ServiceManagement;


namespace Blurry.Main.Base
{
    public class LayerInitializationsResult : Application.Base.LayerInitializationsResult
    {
    }

    public static class LayerInitializer
    {
        public static LayerInitializationsResult Initialize()
        {
            var applicationSideResult = Application.Base.LayerInitializer.Initialize(new MicroServiceManagementFactory(), new AssetManagementFactory());
            LayerInitializationsResult result = new LayerInitializationsResult();
            return result;
        }
    }
}