using System;
using System.Collections.Generic;
using Blurry.Application.Base.ServiceManagement.Base;


namespace Blurry.Main.Base.ServiceManagement
{
    public class Broker_MicroServiceLocator : IMicroServiceLocator
    {
        public readonly Dictionary<Type, IMicroService> microServices = new Dictionary<Type, IMicroService>();


        public void Register<TBase>(IMicroService microService) where TBase : IMicroService
        {
            microServices.Add(typeof(TBase), microService);
        }

        public void UnRegister<TBase>() where TBase : IMicroService
        {
            microServices.Remove(typeof(TBase));
        }

        public TBase Find<TBase>() where TBase : IMicroService
        {
            if (!Has<TBase>())
                throw new Exception($"Service of type '{typeof(TBase)}' could not be found."); // TODO: Use our global way for logging and exception throwing
            return (TBase) microServices[typeof(TBase)];
        }

        public bool Has<TBase>() where TBase : IMicroService
        {
            return microServices.ContainsKey(typeof(TBase));
        }

        public void Replace<TBase>(TBase service) where TBase : IMicroService
        {
            if (Has<TBase>())
                UnRegister<TBase>();
            Register<TBase>(service);
        }
    }
}