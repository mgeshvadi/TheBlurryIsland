using Blurry.Application.Base.ServiceManagement.Base;


namespace Blurry.Main.Base.ServiceManagement
{
    public class MicroServiceManagementFactory : IMicroServiceManagementFactory
    {
        public IMicroServiceLocator CreateActiveServiceLocator()
        {
            return new Broker_MicroServiceLocator();
        }
    }
}