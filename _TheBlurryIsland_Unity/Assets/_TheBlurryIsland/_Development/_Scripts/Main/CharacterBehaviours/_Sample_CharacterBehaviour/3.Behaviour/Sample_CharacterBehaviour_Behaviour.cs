using Blurry.Application.CharacterBehaviours.Behaviour;
using Blurry.Main.CharacterBehaviours.Sample_CharacterBehaviour.BehaviourData;
using Blurry.Main.CharacterBehaviours.Sample_CharacterBehaviour.Presentation;
using UnityEngine;
using UnityEngine.Scripting;


namespace Blurry.Main.CharacterBehaviours.Sample_CharacterBehaviour.Behaviour
{
    [Preserve] [SampleCharacterBehaviour_ReflectionCreationBundle]
    public sealed class Sample_CharacterBehaviour_Behaviour : ACharacterBehaviour_Behaviour<ISample_CharacterBehaviour_Presentation, Sample_CharacterBehaviour_BehaviourData>
    {
        public Sample_CharacterBehaviour_Behaviour(ISample_CharacterBehaviour_Presentation presentation, Sample_CharacterBehaviour_BehaviourData data) : base(presentation, data)
        {
        }

        protected override void Activate_()
        {
            Debug.Log($"{nameof(Sample_CharacterBehaviour_Behaviour)}:{nameof(Activate_)}");
        }

        protected override void Start_()
        {
            Debug.Log($"{nameof(Sample_CharacterBehaviour_Behaviour)}:{nameof(Start_)}");
        }

        protected override void Stop_()
        {
            Debug.Log($"{nameof(Sample_CharacterBehaviour_Behaviour)}:{nameof(Stop_)}");
        }

        protected override void Reset_()
        {
            Debug.Log($"{nameof(Sample_CharacterBehaviour_Behaviour)}:{nameof(Reset_)}");
        }

        protected override void Restart_()
        {
            Debug.Log($"{nameof(Sample_CharacterBehaviour_Behaviour)}:{nameof(Restart_)}");
        }

        protected override void Deactivate_()
        {
            Debug.Log($"{nameof(Sample_CharacterBehaviour_Behaviour)}:{nameof(Deactivate_)}");
        }

        public override void Update(float deltaTime)
        {
            Debug.Log($"{nameof(Sample_CharacterBehaviour_Behaviour)}:{nameof(Update)}");
        }

        public override void FixedUpdate(float fixedDeltaTime)
        {
            Debug.Log($"{nameof(Sample_CharacterBehaviour_Behaviour)}:{nameof(FixedUpdate)}");
        }
    }
}