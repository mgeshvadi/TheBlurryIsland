using System;
using UnityEngine;
using Blurry.Application.CharacterBehaviours.Config;
using Blurry.Application.CharacterBehaviours.ConfigManagement;


namespace Blurry.Main.CharacterBehaviours.Sample_CharacterBehaviour.Config
{
    [Serializable]
    public sealed class Sample_CharacterBehaviour_Config : ICharacterBehaviour_Config
    {
    }

    [SampleCharacterBehaviour_ReflectionCreationBundle]
    [CreateAssetMenu(fileName = nameof(Sample_CharacterBehaviour_ConfigFile), menuName = CharacterBehaviours_ConfigFileLoader.CONFIG_FILES_CONTEXT_MENU + nameof(Sample_CharacterBehaviour_ConfigFile))]
    public sealed class Sample_CharacterBehaviour_ConfigFile : ACharacterBehaviour_ConfigFile<Sample_CharacterBehaviour_Config>
    {
    }
}