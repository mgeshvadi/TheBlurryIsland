using Blurry.Application.CharacterBehaviours.BehaviourData;
using Blurry.Main.CharacterBehaviours.Sample_CharacterBehaviour.Config;
using UnityEngine.Scripting;


namespace Blurry.Main.CharacterBehaviours.Sample_CharacterBehaviour.BehaviourData
{
    [Preserve] [SampleCharacterBehaviour_ReflectionCreationBundle]
    public sealed class Sample_CharacterBehaviour_BehaviourData : ACharacterBehaviour_BehaviourData<Sample_CharacterBehaviour_Config>
    {
        public Sample_CharacterBehaviour_BehaviourData(Sample_CharacterBehaviour_Config config) : base(config)
        {
        }
    }
}