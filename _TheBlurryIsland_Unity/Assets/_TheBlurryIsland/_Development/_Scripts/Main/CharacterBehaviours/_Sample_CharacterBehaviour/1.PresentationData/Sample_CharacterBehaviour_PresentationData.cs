using Blurry.Application.CharacterBehaviours.PresentationData;
using Blurry.Main.CharacterBehaviours.Sample_CharacterBehaviour.Config;
using UnityEngine.Scripting;


namespace Blurry.Main.CharacterBehaviours.Sample_CharacterBehaviour.PresentationData
{
    [Preserve] [SampleCharacterBehaviour_ReflectionCreationBundle]
    public sealed class Sample_CharacterBehaviour_PresentationData : ACharacterBehaviour_PresentationData<Sample_CharacterBehaviour_Config>
    {
        public Sample_CharacterBehaviour_PresentationData(Sample_CharacterBehaviour_Config config) : base(config)
        {
        }
    }
}