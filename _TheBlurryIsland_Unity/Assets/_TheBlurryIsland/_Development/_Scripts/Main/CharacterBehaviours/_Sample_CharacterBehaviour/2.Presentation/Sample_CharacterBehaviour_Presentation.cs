using Blurry.Application.CharacterBehaviours.Presentation;
using Blurry.Main.CharacterBehaviours.Sample_CharacterBehaviour.PresentationData;
using UnityEngine.Scripting;


namespace Blurry.Main.CharacterBehaviours.Sample_CharacterBehaviour.Presentation
{
    public interface ISample_CharacterBehaviour_Presentation : ICharacterBehaviour_Presentation
    {
    }

    [Preserve] [SampleCharacterBehaviour_ReflectionCreationBundle]
    public sealed class Sample_CharacterBehaviour_Presentation : ACharacterBehaviour_Presentation<Sample_CharacterBehaviour_PresentationData>, ISample_CharacterBehaviour_Presentation
    {
        public Sample_CharacterBehaviour_Presentation(Sample_CharacterBehaviour_PresentationData data) : base(data)
        {
        }
    }
}