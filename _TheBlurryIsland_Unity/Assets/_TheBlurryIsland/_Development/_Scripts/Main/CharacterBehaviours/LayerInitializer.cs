

namespace Blurry.Main.CharacterBehaviours
{
    public class LayerInitializationsResult : Application.CharacterBehaviours.LayerInitializationsResult
    {
    }

    public static class LayerInitializer
    {
        public static LayerInitializationsResult Initialize()
        {
            var applicationSileResult = Application.CharacterBehaviours.LayerInitializer.Initialize();
            LayerInitializationsResult result = new LayerInitializationsResult();
            return result;
        }
    }
}