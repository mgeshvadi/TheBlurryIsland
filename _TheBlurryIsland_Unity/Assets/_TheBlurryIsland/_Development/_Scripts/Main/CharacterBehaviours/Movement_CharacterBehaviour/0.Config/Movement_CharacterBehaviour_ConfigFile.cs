using System;
using UnityEngine;
using Blurry.Application.CharacterBehaviours.Config;
using Blurry.Application.CharacterBehaviours.ConfigManagement;


namespace Blurry.Main.CharacterBehaviours.Movement_CharacterBehaviour.Config
{
    [Serializable] [MovementCharacterBehaviour_ReflectionCreationBundle]
    public sealed class Movement_CharacterBehaviour_Config : ICharacterBehaviour_Config
    {
        [Serializable]
        public class AnimatorConfig
        {
            [Serializable]
            public class AnimatorMovementState
            {
                [SerializeField] private string parameterName;
                [SerializeField] private float idleValue;
                [SerializeField] private Threshold walkValidThreshold;
                [SerializeField] private float walkAndIdleTransitionSpeed;

                public string ParameterName => parameterName;
                public float IdleValue => idleValue;
                public Threshold WalkValidThreshold => walkValidThreshold;
                public float WalkAndIdleTransitionSpeed => walkAndIdleTransitionSpeed;
            }

            [SerializeField] private AnimatorMovementState movementState;

            public AnimatorMovementState MovementState => movementState;
        }

        [SerializeField] private AnimatorConfig animatorConfigs;

        public AnimatorConfig AnimatorConfigs => animatorConfigs;
    }

    [CreateAssetMenu(fileName = nameof(Movement_CharacterBehaviour_ConfigFile), menuName = CharacterBehaviours_ConfigFileLoader.CONFIG_FILES_CONTEXT_MENU + nameof(Movement_CharacterBehaviour_ConfigFile))]
    public class Movement_CharacterBehaviour_ConfigFile : ACharacterBehaviour_ConfigFile<Movement_CharacterBehaviour_Config>
    {
    }
}