using Blurry.Application.CharacterBehaviours.PresentationData;
using Blurry.Main.CharacterBehaviours.Movement_CharacterBehaviour.Config;
using UnityEngine.Scripting;


namespace Blurry.Main.CharacterBehaviours.Movement_CharacterBehaviour.PresentationData
{
    [Preserve] [MovementCharacterBehaviour_ReflectionCreationBundle]
    public sealed class Movement_CharacterBehaviour_PresentationData : ACharacterBehaviour_PresentationData<Movement_CharacterBehaviour_Config>
    {
        public class AnimatorData
        {
            public class AnimatorMovementState
            {
                public int parameterId;
                public float idleValue;
                public Threshold walkValidThreshold;
                public float walkAndIdleTransitionSpeed;

            }

            public AnimatorMovementState movementState;
        }

        public AnimatorData animatorData;

        public Movement_CharacterBehaviour_PresentationData(Movement_CharacterBehaviour_Config config) : base(config)
        {
        }
    }
}