using System;
using UnityEngine;
using Blurry.Application.CharacterBehaviours.Presentation;
using Blurry.Main.CharacterBehaviours.Movement_CharacterBehaviour.PresentationData;
using UnityEngine.Scripting;


namespace Blurry.Main.CharacterBehaviours.Movement_CharacterBehaviour.Presentation
{
    public interface IMovement_CharacterBehaviour_Presentation : ICharacterBehaviour_Presentation
    {

    }

    [Preserve] [MovementCharacterBehaviour_ReflectionCreationBundle]
    public sealed class Movement_CharacterBehaviour_Presentation : ACharacterBehaviour_Presentation<Movement_CharacterBehaviour_PresentationData>, IMovement_CharacterBehaviour_Presentation
    {
        public Movement_CharacterBehaviour_Presentation(Movement_CharacterBehaviour_PresentationData data) : base(data)
        {
        }

        private float movementFactorTargetValue;
        private float movementFactorCurrentValue;

        public void UpdateMovement(float deltaTime)
        {
            movementFactorCurrentValue = Mathf.Lerp(movementFactorCurrentValue, movementFactorTargetValue, deltaTime * data.animatorData.movementState.walkAndIdleTransitionSpeed);
            data.Animator.SetFloat(data.animatorData.movementState.parameterId, movementFactorCurrentValue);
        }

        public void SetMovementAmount(float rawTargetValue)
        {
            rawTargetValue = ValidateRequestedMovementFactorTargetValue(rawTargetValue);
            if (ShouldRequestedMovementFactorTargetValueResultInIdle(rawTargetValue))
                SetMovementFactorTargetValueToIdle();
            else
                SetMovementFactorTargetValueToWalk(rawTargetValue);
        }

        private float ValidateRequestedMovementFactorTargetValue(float requestedMovementFactorTargetValue)
        {
            if (requestedMovementFactorTargetValue.IsBetweenOrEqual(0, 1))
                Debug.LogError($"Movement Presentation expected a value between 0 and 1 as {nameof(movementFactorTargetValue)}. received {nameof(requestedMovementFactorTargetValue)}: {requestedMovementFactorTargetValue}");
            return Mathf.Clamp(requestedMovementFactorTargetValue, 0, 1);
        }

        private bool ShouldRequestedMovementFactorTargetValueResultInIdle(float requestedMovementFactorTrargetValue)
        {
            return requestedMovementFactorTrargetValue == 0;
        }

        private void SetMovementFactorTargetValueToIdle()
        {
            movementFactorTargetValue = data.animatorData.movementState.idleValue;
        }

        private void SetMovementFactorTargetValueToWalk(float rawTargetValue)
        {
            movementFactorTargetValue = rawTargetValue.Remap(new Threshold(0, 1), data.animatorData.movementState.walkValidThreshold);
        }
    }
}


public static class MathEx // TODO: Move this to lowest scripts level
{
    public static float Remap(this float value, Threshold oldThreshold, Threshold newThreshold)
    {
        return (value - oldThreshold.min) / (newThreshold.min - oldThreshold.min) * (newThreshold.max - oldThreshold.max) + oldThreshold.max;
    }

    public static float Remap(this float value, float oldLowBound, float oldHighBound, float newLowBound, float newHighBound)
    {
        return (value - oldLowBound) / (newLowBound - oldLowBound) * (newHighBound - oldHighBound) + oldHighBound;
    }

    public static bool IsBetween(this float value, float min, float max)
    {
        return value.IsBetween(new Threshold(min, max));
    }

    public static bool IsBetween(this float value, Threshold threshold)
    {
        return value > threshold.min && value < threshold.max;
    }

    public static bool IsBetweenOrMaxEqual(this float value, float min, float max)
    {
        return value.IsBetweenOrMaxEqual(new Threshold(min, max));
    }

    public static bool IsBetweenOrMaxEqual(this float value, Threshold threshold)
    {
        return value > threshold.min && value <= threshold.max;
    }

    public static bool IsBetweenOrMinEqual(this float value, float min, float max)
    {
        return value.IsBetweenOrMinEqual(new Threshold(min, max));
    }

    public static bool IsBetweenOrMinEqual(this float value, Threshold threshold)
    {
        return value >= threshold.min && value < threshold.max;
    }

    public static bool IsBetweenOrEqual(this float value, float min, float max)
    {
        return value.IsBetweenOrEqual(new Threshold(min, max));
    }

    public static bool IsBetweenOrEqual(this float value, Threshold threshold)
    {
        return value >= threshold.min && value <= threshold.max;
    }
}

[Serializable]
public struct Threshold // TODO: Move this to lowest scripts level
{
    public float min;
    public float max;

    public Threshold(float min, float max)
    {
        this.min = min;
        this.max = max;
    }
}