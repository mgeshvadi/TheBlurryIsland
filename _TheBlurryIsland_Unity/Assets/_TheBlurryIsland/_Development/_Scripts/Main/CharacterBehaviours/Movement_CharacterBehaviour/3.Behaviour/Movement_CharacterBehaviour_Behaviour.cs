using Blurry.Application.CharacterBehaviours.Behaviour;
using Blurry.Main.CharacterBehaviours.Movement_CharacterBehaviour.BehaviourData;
using Blurry.Main.CharacterBehaviours.Movement_CharacterBehaviour.Presentation;
using UnityEngine.Scripting;


namespace Blurry.Main.CharacterBehaviours.Movement_CharacterBehaviour.Behaviour
{
    [Preserve] [MovementCharacterBehaviour_ReflectionCreationBundle]
    public sealed class Movement_CharacterBehaviour_Behaviour : ACharacterBehaviour_Behaviour<IMovement_CharacterBehaviour_Presentation, Movement_CharacterBehaviour_BehaviourData>
    {
        public Movement_CharacterBehaviour_Behaviour(IMovement_CharacterBehaviour_Presentation presentation, Movement_CharacterBehaviour_BehaviourData data) : base(presentation, data)
        {
        }

        protected override void Activate_()
        {
        }

        protected override void Start_()
        {
        }

        protected override void Stop_()
        {
        }

        protected override void Reset_()
        {
        }

        protected override void Restart_()
        {
        }

        protected override void Deactivate_()
        {
        }

        public override void Update(float deltaTime)
        {
        }

        public override void FixedUpdate(float fixedDeltaTime)
        {
        }
    }
}