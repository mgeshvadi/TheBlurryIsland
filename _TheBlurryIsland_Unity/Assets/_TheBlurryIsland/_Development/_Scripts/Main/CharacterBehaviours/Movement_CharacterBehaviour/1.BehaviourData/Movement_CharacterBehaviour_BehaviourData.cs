using Blurry.Application.CharacterBehaviours.BehaviourData;
using Blurry.Main.CharacterBehaviours.Movement_CharacterBehaviour.Config;
using UnityEngine.Scripting;


namespace Blurry.Main.CharacterBehaviours.Movement_CharacterBehaviour.BehaviourData
{
    [Preserve] [MovementCharacterBehaviour_ReflectionCreationBundle]
    public sealed class Movement_CharacterBehaviour_BehaviourData : ACharacterBehaviour_BehaviourData<Movement_CharacterBehaviour_Config>
    {
        public Movement_CharacterBehaviour_BehaviourData(Movement_CharacterBehaviour_Config config) : base(config)
        {
        }
    }
}