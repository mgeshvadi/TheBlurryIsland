using Blurry.Application.CoreMechanics.Mechanics.PresentationData;
using Blurry.Main.CoreMechanics.Mechanics.Sample_Mechanic.Config;
using Blurry.Main.CoreMechanics.Mechanics.Sample_Mechanic.Entity;
using UnityEngine.Scripting;


namespace Blurry.Main.CoreMechanics.Mechanics.Sample_Mechanic.PresentationData
{
    [Preserve] [Sample_Mechanic_ReflectionCreationBundle]
    public sealed class Sample_Mechanic_PresentationData : AMechanic_PresentationData<Sample_Mechanic_Entity, Sample_Mechanic_Config>
    {
        public Sample_Mechanic_PresentationData(Sample_Mechanic_Entity entity, Sample_Mechanic_Config config) : base(entity, config)
        {
        }
    }
}