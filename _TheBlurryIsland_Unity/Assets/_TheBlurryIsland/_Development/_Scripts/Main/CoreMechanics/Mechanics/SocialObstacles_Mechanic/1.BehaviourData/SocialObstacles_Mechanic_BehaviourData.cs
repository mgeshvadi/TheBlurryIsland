using Blurry.Application.CoreMechanics.Mechanics.BehaviourData;
using Blurry.Application.Utility.DataStructures;
using Blurry.Main.CoreMechanics.Mechanics.SocialObstacles_Mechanic.Config;
using Blurry.Main.CoreMechanics.Mechanics.SocialObstacles_Mechanic.Entity;
using UnityEngine.Scripting;


namespace Blurry.Main.CoreMechanics.Mechanics.SocialObstacles_Mechanic.BehaviourData
{
    [Preserve] [SocialObstacles_Mechanic_ReflectionCreationBundle]
    public sealed class SocialObstacles_Mechanic_BehaviourData : AMechanic_BehaviourData<SocialObstacles_Mechanic_Entity, SocialObstacles_Mechanic_Config>
    {
        public IntMinMax EachWaveObstaclesCount { get; }
        internal FloatMinMax WavesIntervalSecond { get; }
        internal float ObstaclesWavesIntervalRandomnessNormalizedPercent { get; }
        internal FloatMinMax Speed { get; }

        public SocialObstacles_Mechanic_BehaviourData(SocialObstacles_Mechanic_Entity entity, SocialObstacles_Mechanic_Config config) : base(entity, config)
        {
            EachWaveObstaclesCount = config.EachWaveObstaclesCount;
            WavesIntervalSecond = config.WavesIntervalSecond;
            ObstaclesWavesIntervalRandomnessNormalizedPercent = config.ObstaclesWavesIntervalRandomnessNormalizedPercent;
            Speed = config.Speed;
        }
    }
}