using System;
using UnityEngine;
using Blurry.Application.CoreMechanics.Mechanics.Config;
using Blurry.Application.CoreMechanics.Mechanics.ConfigManagement;
using Blurry.Application.Utility.DataStructures;


namespace Blurry.Main.CoreMechanics.Mechanics.SocialObstacles_Mechanic.Config
{
    [Serializable]
    public sealed class SocialObstacles_Mechanic_Config : IMechanic_Config
    {
        [SerializeField] private IntMinMax eachWaveObstaclesCount;
        [SerializeField] private FloatMinMax wavesIntervalSecond;
        [Range(0f, 1f)] [SerializeField] private float obstaclesWavesIntervalRandomnessNormalizedPercent;
        [Space]
        [SerializeField] private float obstaclesAngularVelocityIntensity;
        [SerializeField] private FloatMinMax speed;
        [Range(0f, 1f)] [SerializeField] private float speedRandomnessNormalizedPercent;

        internal IntMinMax EachWaveObstaclesCount => eachWaveObstaclesCount;
        internal FloatMinMax WavesIntervalSecond => wavesIntervalSecond;
        internal float ObstaclesWavesIntervalRandomnessNormalizedPercent => obstaclesWavesIntervalRandomnessNormalizedPercent;

        internal float ObstaclesAngularVelocityIntensity => obstaclesAngularVelocityIntensity;
        internal FloatMinMax Speed => speed;
        internal float SpeedRandomnessNormalizedPercent => speedRandomnessNormalizedPercent;
    }

    [SocialObstacles_Mechanic_ReflectionCreationBundle]
    [CreateAssetMenu(fileName = nameof(SocialObstacles_Mechanic_ConfigFile), menuName = Mechanics_ConfigFileLoader.CONFIG_FILES_CONTEXT_MENU + nameof(SocialObstacles_Mechanic_ConfigFile))]
    public sealed class SocialObstacles_Mechanic_ConfigFile : AMechanic_ConfigFile<SocialObstacles_Mechanic_Config>
    {
    }
}