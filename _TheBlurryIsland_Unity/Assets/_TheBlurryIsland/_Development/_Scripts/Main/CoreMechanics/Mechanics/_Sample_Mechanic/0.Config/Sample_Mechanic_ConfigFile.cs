using System;
using UnityEngine;
using Blurry.Application.CoreMechanics.Mechanics.Config;
using Blurry.Application.CoreMechanics.Mechanics.ConfigManagement;


namespace Blurry.Main.CoreMechanics.Mechanics.Sample_Mechanic.Config
{
    [Serializable]
    public sealed class Sample_Mechanic_Config : IMechanic_Config
    {
        [SerializeField] private int sample;
    }

    [Sample_Mechanic_ReflectionCreationBundle]
    [CreateAssetMenu(fileName = nameof(Sample_Mechanic_ConfigFile), menuName = Mechanics_ConfigFileLoader.CONFIG_FILES_CONTEXT_MENU + nameof(Sample_Mechanic_ConfigFile))]
    public sealed class Sample_Mechanic_ConfigFile : AMechanic_ConfigFile<Sample_Mechanic_Config>
    {
    }
}