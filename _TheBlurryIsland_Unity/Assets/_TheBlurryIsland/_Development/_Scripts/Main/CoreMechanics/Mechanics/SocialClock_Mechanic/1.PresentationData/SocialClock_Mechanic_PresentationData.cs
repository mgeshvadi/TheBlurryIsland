using Blurry.Application.CoreMechanics.Mechanics.PresentationData;
using Blurry.Main.CoreMechanics.Mechanics.SocialClock_Mechanic.Config;
using Blurry.Main.CoreMechanics.Mechanics.SocialClock_Mechanic.Entity;
using UnityEngine;
using UnityEngine.Scripting;


namespace Blurry.Main.CoreMechanics.Mechanics.SocialClock_Mechanic.PresentationData
{
    [Preserve] [SocialClock_Mechanic_ReflectionCreationBundle]
    public sealed class SocialClock_Mechanic_PresentationData : AMechanic_PresentationData<SocialClock_Mechanic_Entity, SocialClock_Mechanic_Config>
    {
        internal readonly struct HandlesVisuals
        {
            internal readonly Transform hourHandle;
            internal readonly Transform minutesHandle;

            internal HandlesVisuals(Transform hourHandle, Transform minutesHandle)
            {
                this.hourHandle = hourHandle;
                this.minutesHandle = minutesHandle;
            }
        }

        internal HandlesVisuals Visuals { get; }
        internal bool IsContinues { get; }

        public SocialClock_Mechanic_PresentationData(SocialClock_Mechanic_Entity entity, SocialClock_Mechanic_Config config) : base(entity, config)
        {
            Visuals = new HandlesVisuals(entity.HourHandle, entity.MinutesHandle);
            IsContinues = config.TimePassingMode == SocialClock_Mechanic_Config.Mode.Continues;
        }
    }
}