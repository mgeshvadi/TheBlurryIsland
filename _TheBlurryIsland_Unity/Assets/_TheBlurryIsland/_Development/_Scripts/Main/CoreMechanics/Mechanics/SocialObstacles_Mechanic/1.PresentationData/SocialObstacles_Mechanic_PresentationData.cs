using System.Collections.Generic;
using Blurry.Application.CoreMechanics.Mechanics.PresentationData;
using Blurry.Application.Utility.Extensions;
using Blurry.Main.CoreMechanics.Mechanics.SocialObstacles_Mechanic.Config;
using Blurry.Main.CoreMechanics.Mechanics.SocialObstacles_Mechanic.Entity;
using UnityEngine;
using UnityEngine.Scripting;


namespace Blurry.Main.CoreMechanics.Mechanics.SocialObstacles_Mechanic.PresentationData
{
    [Preserve] [SocialObstacles_Mechanic_ReflectionCreationBundle]
    public sealed class SocialObstacles_Mechanic_PresentationData : AMechanic_PresentationData<SocialObstacles_Mechanic_Entity, SocialObstacles_Mechanic_Config>
    {
        internal readonly struct InstantiationPointTransforms
        {
            internal readonly Transform min;
            internal readonly Transform max;

            internal InstantiationPointTransforms(Transform min, Transform max)
            {
                this.min = min;
                this.max = max;
            }
        }

        internal float SpeedRandomnessNormalizedPercent { get; }
        internal float ObstaclesAngularVelocityIntensity { get; }

        internal Transform ObstaclesParent { get; }
        internal Rigidbody ObstaclePrefab => ObstaclePrefabs.RandomElement();
        internal InstantiationPointTransforms InstantiationPoints { get; }

        private List<Rigidbody> ObstaclePrefabs { get; }

        public SocialObstacles_Mechanic_PresentationData(SocialObstacles_Mechanic_Entity entity, SocialObstacles_Mechanic_Config config) : base(entity, config)
        {
            SpeedRandomnessNormalizedPercent = config.SpeedRandomnessNormalizedPercent;
            ObstaclesAngularVelocityIntensity = config.ObstaclesAngularVelocityIntensity;
            ObstaclesParent = entity.ObstaclesParent;
            ObstaclePrefabs = entity.ObstaclePrefabs;
            InstantiationPoints = new InstantiationPointTransforms(entity.MostBottomLeftInstantiationPoint, entity.MostTopRightInstantiationPoint);
        }

    }
}