using System.Collections.Generic;
using Blurry.Application.CoreMechanics.Mechanics.Entity;
using UnityEngine;


namespace Blurry.Main.CoreMechanics.Mechanics.SocialObstacles_Mechanic.Entity
{
    [SocialObstacles_Mechanic_ReflectionCreationBundle]
    public sealed class SocialObstacles_Mechanic_Entity : AMechanic_Entity
    {
        [SerializeField] private Transform obstaclesParent;
        [SerializeField] private List<Rigidbody> obstaclePrefabs;
        [SerializeField] private Transform mostBottomLeftInstantiationPoint;
        [SerializeField] private Transform mostTopRightInstantiationPoint;

        internal Transform ObstaclesParent => obstaclesParent;
        internal List<Rigidbody> ObstaclePrefabs => obstaclePrefabs;
        internal Transform MostBottomLeftInstantiationPoint => mostBottomLeftInstantiationPoint;
        internal Transform MostTopRightInstantiationPoint => mostTopRightInstantiationPoint;
    }
}