using System;
using Blurry.Application.CoreMechanics.Mechanics.Presentation;
using Blurry.Application.Utility.Extensions;
using Blurry.Main.CoreMechanics.Mechanics.SocialClock_Mechanic.PresentationData;
using UnityEngine;
using UnityEngine.Scripting;


namespace Blurry.Main.CoreMechanics.Mechanics.SocialClock_Mechanic.Presentation
{
    public interface ISocialClock_Mechanic_Presentation : IMechanic_Presentation
    {
        public void VisualizeTime(TimeSpan time);
    }

    [Preserve] [SocialClock_Mechanic_ReflectionCreationBundle]
    public sealed class SocialClock_Mechanic_Presentation : AMechanic_Presentation<SocialClock_Mechanic_PresentationData>, ISocialClock_Mechanic_Presentation
    {
        public SocialClock_Mechanic_Presentation(SocialClock_Mechanic_PresentationData data) : base(data)
        {
        }

        public void VisualizeTime(TimeSpan time)
        {
            float minutes = (time.Minutes % 60f) + (data.IsContinues ? time.Seconds / 60f : 0f);
            float hours = (time.Hours % 12f) + (data.IsContinues ? minutes / 60f : 0f);

            SetRotationFor(data.Visuals.minutesHandle, normalizedValue: minutes / 60f);
            SetRotationFor(data.Visuals.hourHandle, normalizedValue: hours / 12f);
        }

        private void SetRotationFor(Transform transform, float normalizedValue)
        {
            float rotationDegree = -normalizedValue.RemapTo(oldRange: new Vector2(0, 1f), newRange: new Vector2(0, 360f));
            Vector3 rotation = transform.localEulerAngles;
            rotation.z = rotationDegree;
            transform.localEulerAngles = rotation;
        }
    }
}