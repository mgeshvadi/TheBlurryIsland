using Blurry.Application.ServiceMiddle.InputManagement;
using Blurry.Application.ServiceMiddle.InputManagement.Base;
using UnityEngine.InputSystem;


namespace Blurry.Main.CoreMechanics.Mechanics.SocialObstacles_Mechanic.Input
{
    public class SocialObstacles_InputSubject : AInput_Subject<float>
    {
        protected override InputAction GetCorrespondingInputAction(InputController inputController)
        {
            return inputController.CoreMechanics.SocialObstacles;
        }
    }
}