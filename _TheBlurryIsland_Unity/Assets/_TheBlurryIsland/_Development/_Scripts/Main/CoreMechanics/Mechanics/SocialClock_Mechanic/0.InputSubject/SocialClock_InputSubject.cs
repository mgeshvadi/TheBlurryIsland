using Blurry.Application.ServiceMiddle.InputManagement;
using Blurry.Application.ServiceMiddle.InputManagement.Base;
using UnityEngine.InputSystem;


namespace Blurry.Main.CoreMechanics.Mechanics.SocialClock_Mechanic.Input
{
    public class SocialClock_InputSubject : AInput_Subject<float>
    {
        protected override InputAction GetCorrespondingInputAction(InputController inputController)
        {
            return inputController.CoreMechanics.SocialClock;
        }
    }
}