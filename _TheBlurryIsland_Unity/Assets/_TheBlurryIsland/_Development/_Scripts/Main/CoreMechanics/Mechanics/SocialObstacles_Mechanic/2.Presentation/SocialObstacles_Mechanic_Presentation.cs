using System.Collections.Generic;
using Blurry.Application.CoreMechanics.Mechanics.Presentation;
using Blurry.Application.Utility.StaticMethods;
using Blurry.Main.CoreMechanics.Mechanics.SocialObstacles_Mechanic.PresentationData;
using UnityEngine;
using UnityEngine.Scripting;


namespace Blurry.Main.CoreMechanics.Mechanics.SocialObstacles_Mechanic.Presentation
{
    public interface ISocialObstacles_Mechanic_Presentation : IMechanic_Presentation
    {
        void InstantiateObstacles(int count);
        void SetObstaclesSpeed(float speed);
    }

    [Preserve] [SocialObstacles_Mechanic_ReflectionCreationBundle]
    public sealed class SocialObstacles_Mechanic_Presentation : AMechanic_Presentation<SocialObstacles_Mechanic_PresentationData>, ISocialObstacles_Mechanic_Presentation
    {
        private readonly HashSet<Rigidbody> obstacles;

        public SocialObstacles_Mechanic_Presentation(SocialObstacles_Mechanic_PresentationData data) : base(data)
        {
            obstacles = new HashSet<Rigidbody>();
        }

        public void InstantiateObstacles(int count)
        {
            for (int i = 0; i < count; i++)
            {
                Rigidbody newObstacle = Object.Instantiate(data.ObstaclePrefab, CalculateInstantiationPosition(), Random.rotation, data.ObstaclesParent);
                newObstacle.angularVelocity = CalculateInitialAngularVelocity();
                newObstacle.GetComponent<MeshRenderer>().material.color = new Color(Random.value * 2, Random.value * 2, Random.value * 2); // TODO: remove this
                obstacles.Add(newObstacle);
            }
        }

        private Vector3 CalculateInstantiationPosition()
        {
            Vector3 result = Vector3.zero;
            result.x = Random.Range(data.InstantiationPoints.min.position.x, data.InstantiationPoints.max.position.x);
            result.y = Random.Range(data.InstantiationPoints.min.position.y, data.InstantiationPoints.max.position.y);
            result.z = Random.Range(data.InstantiationPoints.min.position.z, data.InstantiationPoints.max.position.z);
            return result;
        }

        private Vector3 CalculateInitialAngularVelocity()
        {
            return Random.insideUnitSphere * data.ObstaclesAngularVelocityIntensity;
        }

        public void SetObstaclesSpeed(float speed)
        {
            foreach (Rigidbody obstacle in obstacles)
            {
                speed = RandomnessUtilities.AddRandomness(value: speed, normalizedPercent: data.SpeedRandomnessNormalizedPercent);
                Vector3 force = new Vector3(-speed, 0);
                // obstacle.AddForce(force, ForceMode.Impulse);
                obstacle.velocity = force;
            }
        }
    }
}