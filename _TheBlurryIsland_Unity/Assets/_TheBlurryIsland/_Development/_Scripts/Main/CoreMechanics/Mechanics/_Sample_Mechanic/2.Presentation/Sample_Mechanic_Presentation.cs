using Blurry.Application.CoreMechanics.Mechanics.Presentation;
using Blurry.Main.CoreMechanics.Mechanics.Sample_Mechanic.PresentationData;
using UnityEngine.Scripting;


namespace Blurry.Main.CoreMechanics.Mechanics.Sample_Mechanic.Presentation
{
    public interface ISample_Mechanic_Presentation : IMechanic_Presentation
    {
    }

    [Preserve] [Sample_Mechanic_ReflectionCreationBundle]
    public sealed class Sample_Mechanic_Presentation : AMechanic_Presentation<Sample_Mechanic_PresentationData>, ISample_Mechanic_Presentation
    {
        public Sample_Mechanic_Presentation(Sample_Mechanic_PresentationData data) : base(data)
        {
        }
    }
}