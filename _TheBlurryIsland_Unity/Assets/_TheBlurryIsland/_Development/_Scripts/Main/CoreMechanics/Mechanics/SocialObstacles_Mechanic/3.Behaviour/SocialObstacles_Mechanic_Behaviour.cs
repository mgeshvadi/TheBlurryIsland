using Blurry.Application.CoreMechanics.Mechanics.Behaviour;
using Blurry.Application.Utility.StaticMethods;
using Blurry.Main.CoreMechanics.Mechanics.SocialObstacles_Mechanic.BehaviourData;
using Blurry.Main.CoreMechanics.Mechanics.SocialObstacles_Mechanic.Input;
using Blurry.Main.CoreMechanics.Mechanics.SocialObstacles_Mechanic.Presentation;
using UnityEngine;
using UnityEngine.Scripting;


namespace Blurry.Main.CoreMechanics.Mechanics.SocialObstacles_Mechanic.Behaviour
{
    [Preserve] [SocialObstacles_Mechanic_ReflectionCreationBundle]
    public sealed class SocialObstacles_Mechanic_Behaviour : AMechanic_Behaviour<ISocialObstacles_Mechanic_Presentation, SocialObstacles_Mechanic_BehaviourData>
    {
        private readonly SocialObstacles_InputSubject input;

        public SocialObstacles_Mechanic_Behaviour(ISocialObstacles_Mechanic_Presentation presentation, SocialObstacles_Mechanic_BehaviourData data) : base(presentation, data)
        {
            input = new SocialObstacles_InputSubject();

            Start_(); // TODO : If this is a controller in the first level it's start is not called if that's not ok, debug it and remove this line after that
        }

        protected override void Activate_()
        {
        }

        protected override void Start_()
        {
        }

        protected override void Stop_()
        {
        }

        protected override void Reset_()
        {
        }

        protected override void Restart_()
        {
        }

        protected override void Deactivate_()
        {
        }

        private float passedTimeFromLastWave;
        private float obstaclesWavesInterval;

        public override void Update(float deltaTime)
        {
            UpdateWavesInterval();

            passedTimeFromLastWave += deltaTime;
            if (passedTimeFromLastWave > obstaclesWavesInterval)
            {
                presentation.InstantiateObstacles(data.EachWaveObstaclesCount.RandomAmount);
                passedTimeFromLastWave = 0;
            }
        }

        private void UpdateWavesInterval()
        {
            obstaclesWavesInterval = Mathf.Lerp(data.WavesIntervalSecond.min, data.WavesIntervalSecond.max, input.InputValue);
            obstaclesWavesInterval = RandomnessUtilities.AddRandomness(obstaclesWavesInterval, data.ObstaclesWavesIntervalRandomnessNormalizedPercent);
        }

        public override void FixedUpdate(float fixedDeltaTime)
        {
            presentation.SetObstaclesSpeed(CalculateObstaclesSpeed());
        }

        private float CalculateObstaclesSpeed()
        {
            return Mathf.Lerp(data.Speed.min, data.Speed.max, input.InputValue);
        }
    }
}