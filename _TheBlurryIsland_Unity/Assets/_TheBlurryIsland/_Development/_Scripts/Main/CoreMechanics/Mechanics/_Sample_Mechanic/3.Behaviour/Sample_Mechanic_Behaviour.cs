using Blurry.Application.CoreMechanics.Mechanics.Behaviour;
using Blurry.Main.CoreMechanics.Mechanics.Sample_Mechanic.BehaviourData;
using Blurry.Main.CoreMechanics.Mechanics.Sample_Mechanic.Presentation;
using UnityEngine;
using UnityEngine.Scripting;


namespace Blurry.Main.CoreMechanics.Mechanics.Sample_Mechanic.Behaviour
{
    [Preserve] [Sample_Mechanic_ReflectionCreationBundle]
    public sealed class Sample_Mechanic_Behaviour : AMechanic_Behaviour<ISample_Mechanic_Presentation, Sample_Mechanic_BehaviourData>
    {
        public Sample_Mechanic_Behaviour(ISample_Mechanic_Presentation presentation, Sample_Mechanic_BehaviourData data) : base(presentation, data)
        {
        }

        protected override void Activate_()
        {
            Debug.Log($"{nameof(Sample_Mechanic_Behaviour)}:{nameof(Activate_)}");
        }

        protected override void Start_()
        {
            Debug.Log($"{nameof(Sample_Mechanic_Behaviour)}:{nameof(Start_)}");
        }

        protected override void Stop_()
        {
            Debug.Log($"{nameof(Sample_Mechanic_Behaviour)}:{nameof(Stop_)}");
        }

        protected override void Reset_()
        {
            Debug.Log($"{nameof(Sample_Mechanic_Behaviour)}:{nameof(Reset_)}");
        }

        protected override void Restart_()
        {
            Debug.Log($"{nameof(Sample_Mechanic_Behaviour)}:{nameof(Restart_)}");
        }

        protected override void Deactivate_()
        {
            Debug.Log($"{nameof(Sample_Mechanic_Behaviour)}:{nameof(Deactivate_)}");
        }

        public override void Update(float deltaTime)
        {
            Debug.Log($"{nameof(Sample_Mechanic_Behaviour)}:{nameof(Update)}");
        }

        public override void FixedUpdate(float fixedDeltaTime)
        {
            Debug.Log($"{nameof(Sample_Mechanic_Behaviour)}:{nameof(FixedUpdate)}");
        }
    }
}