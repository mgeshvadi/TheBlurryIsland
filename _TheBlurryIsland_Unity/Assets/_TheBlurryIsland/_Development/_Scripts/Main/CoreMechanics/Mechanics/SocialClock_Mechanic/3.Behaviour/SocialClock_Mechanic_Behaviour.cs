using System;
using Blurry.Application.CoreMechanics.Mechanics.Behaviour;
using Blurry.Main.CoreMechanics.Mechanics.SocialClock_Mechanic.BehaviourData;
using Blurry.Main.CoreMechanics.Mechanics.SocialClock_Mechanic.Input;
using Blurry.Main.CoreMechanics.Mechanics.SocialClock_Mechanic.Presentation;
using UnityEngine;
using UnityEngine.Scripting;


namespace Blurry.Main.CoreMechanics.Mechanics.SocialClock_Mechanic.Behaviour
{
    [Preserve] [SocialClock_Mechanic_ReflectionCreationBundle]
    public sealed class SocialClock_Mechanic_Behaviour : AMechanic_Behaviour<ISocialClock_Mechanic_Presentation, SocialClock_Mechanic_BehaviourData>
    {
        private bool shouldRecordTimePassing;
        private TimeSpan passedTime;
        private readonly SocialClock_InputSubject input;

        public SocialClock_Mechanic_Behaviour(ISocialClock_Mechanic_Presentation presentation, SocialClock_Mechanic_BehaviourData data) : base(presentation, data)
        {
            shouldRecordTimePassing = false;
            passedTime = new TimeSpan();
            input = new SocialClock_InputSubject();

            Start_(); // TODO : If this is a controller in the first level it's start is not called if that's not ok, debug it and remove this line after that
        }

        protected override void Activate_()
        {
        }

        protected override void Start_()
        {
            shouldRecordTimePassing = true;
        }

        protected override void Stop_()
        {
            shouldRecordTimePassing = false;
        }

        protected override void Reset_()
        {
            passedTime = new TimeSpan();
        }

        protected override void Restart_()
        {
        }

        protected override void Deactivate_()
        {
        }

        public override void Update(float deltaTime)
        {
            if (!shouldRecordTimePassing)
                return;

            float thisFramePassedTime = deltaTime * CalculateTimeSpeedBasedOnUserInput();
            AddPassedTime(thisFramePassedTime);

            presentation.VisualizeTime(data.InitialTimeOffset + passedTime);
        }

        private void AddPassedTime(float seconds)
        {
            passedTime += new TimeSpan(days: 0, hours: 0, minutes: 0, seconds: 0, (int) (seconds * 1000f));
        }

        private float CalculateTimeSpeedBasedOnUserInput()
        {
            return Mathf.Lerp(data.MinTimePassingSpeed, data.MaxTimePassingSpeed, input.InputValue);
        }

        public override void FixedUpdate(float fixedDeltaTime)
        {
        }
    }
}