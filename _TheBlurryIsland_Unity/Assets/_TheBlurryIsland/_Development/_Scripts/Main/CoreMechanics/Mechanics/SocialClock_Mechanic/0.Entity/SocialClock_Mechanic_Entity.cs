using Blurry.Application.CoreMechanics.Mechanics.Entity;
using UnityEngine;


namespace Blurry.Main.CoreMechanics.Mechanics.SocialClock_Mechanic.Entity
{
    [SocialClock_Mechanic_ReflectionCreationBundle]
    public sealed class SocialClock_Mechanic_Entity : AMechanic_Entity
    {
        [SerializeField] private Transform hourHandle;
        [SerializeField] private Transform minutesHandle;

        internal  Transform HourHandle => hourHandle;
        internal Transform MinutesHandle => minutesHandle;
    }
}