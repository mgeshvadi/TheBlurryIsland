using Blurry.Application.CoreMechanics.Mechanics.BehaviourData;
using Blurry.Main.CoreMechanics.Mechanics.Sample_Mechanic.Config;
using Blurry.Main.CoreMechanics.Mechanics.Sample_Mechanic.Entity;
using UnityEngine.Scripting;


namespace Blurry.Main.CoreMechanics.Mechanics.Sample_Mechanic.BehaviourData
{
    [Preserve] [Sample_Mechanic_ReflectionCreationBundle]
    public sealed class Sample_Mechanic_BehaviourData : AMechanic_BehaviourData<Sample_Mechanic_Entity, Sample_Mechanic_Config>
    {
        public Sample_Mechanic_BehaviourData(Sample_Mechanic_Entity entity, Sample_Mechanic_Config config) : base(entity, config)
        {
        }
    }
}