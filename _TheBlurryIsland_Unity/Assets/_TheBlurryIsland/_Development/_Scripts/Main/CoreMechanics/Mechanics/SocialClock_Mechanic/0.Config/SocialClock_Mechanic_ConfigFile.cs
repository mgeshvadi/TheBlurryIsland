using System;
using UnityEngine;
using Blurry.Application.CoreMechanics.Mechanics.Config;
using Blurry.Application.CoreMechanics.Mechanics.ConfigManagement;


namespace Blurry.Main.CoreMechanics.Mechanics.SocialClock_Mechanic.Config
{
    [Serializable]
    public sealed class SocialClock_Mechanic_Config : IMechanic_Config
    {
        internal enum Mode
        {
            Continues,
            Descrite
        }

        [SerializeField] private Mode mode;
        [SerializeField] private int initialTimeOffsetSeconds;
        [SerializeField] private float timePassingSpeedWhenNoUserInput;
        [SerializeField] private float timePassingSpeedWhenMaxUserInput;


        internal int InitialTimeOffsetSeconds => initialTimeOffsetSeconds;
        internal float TimePassingSpeedWhenNoUserInput => timePassingSpeedWhenNoUserInput;
        internal float TimePassingSpeedWhenMaxUserInput => timePassingSpeedWhenMaxUserInput;
        internal Mode TimePassingMode => mode;
    }

    [SocialClock_Mechanic_ReflectionCreationBundle]
    [CreateAssetMenu(fileName = nameof(SocialClock_Mechanic_ConfigFile), menuName = Mechanics_ConfigFileLoader.CONFIG_FILES_CONTEXT_MENU + nameof(SocialClock_Mechanic_ConfigFile))]
    public sealed class SocialClock_Mechanic_ConfigFile : AMechanic_ConfigFile<SocialClock_Mechanic_Config>
    {
    }
}