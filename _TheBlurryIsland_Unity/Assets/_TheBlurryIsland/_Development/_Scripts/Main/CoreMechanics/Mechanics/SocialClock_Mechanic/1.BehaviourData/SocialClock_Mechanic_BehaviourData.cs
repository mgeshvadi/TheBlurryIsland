using System;
using UnityEngine.Scripting;
using Blurry.Application.CoreMechanics.Mechanics.BehaviourData;
using Blurry.Main.CoreMechanics.Mechanics.SocialClock_Mechanic.Config;
using Blurry.Main.CoreMechanics.Mechanics.SocialClock_Mechanic.Entity;
using UnityEngine;


namespace Blurry.Main.CoreMechanics.Mechanics.SocialClock_Mechanic.BehaviourData
{
    [Preserve] [SocialClock_Mechanic_ReflectionCreationBundle]
    public sealed class SocialClock_Mechanic_BehaviourData : AMechanic_BehaviourData<SocialClock_Mechanic_Entity, SocialClock_Mechanic_Config>
    {
        internal TimeSpan InitialTimeOffset { get; }
        internal float MinTimePassingSpeed { get; }
        internal float MaxTimePassingSpeed { get; }

        public SocialClock_Mechanic_BehaviourData(SocialClock_Mechanic_Entity entity, SocialClock_Mechanic_Config config) : base(entity, config)
        {
            InitialTimeOffset = new TimeSpan(days: 0, hours: 0, minutes: 0, seconds: config.InitialTimeOffsetSeconds);

            if (config.TimePassingSpeedWhenMaxUserInput < config.TimePassingSpeedWhenNoUserInput)
                Debug.LogError($"Social clock expects a bigger value for {nameof(config.TimePassingSpeedWhenMaxUserInput)}:{config.TimePassingSpeedWhenMaxUserInput} than {nameof(config.TimePassingSpeedWhenNoUserInput)}:{config.TimePassingSpeedWhenNoUserInput}");

            MinTimePassingSpeed = config.TimePassingSpeedWhenNoUserInput;
            MaxTimePassingSpeed = config.TimePassingSpeedWhenMaxUserInput;
        }
    }
}