using Blurry.Application.CoreMechanics.MechanicControllers.Factory;


namespace Blurry.Main.CoreMechanics
{
    public class LayerInitializationsResult : Application.CoreMechanics.LayerInitializationsResult
    {
        public LayerInitializationsResult(IMechanicControllersFactory mechanicControllersFactory) : base(mechanicControllersFactory)
        {
        }
    }

    public static class LayerInitializer
    {
        public static LayerInitializationsResult Initialize()
        {
            var applicationSideResult = Application.CoreMechanics.LayerInitializer.Initialize();
            LayerInitializationsResult result = new LayerInitializationsResult(applicationSideResult.MechanicControllersFactory);
            return result;
        }
    }
}