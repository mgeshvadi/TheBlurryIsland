using Blurry.Application.CoreMechanics.MechanicControllers.BehaviourData;
using Blurry.Main.CoreMechanics.MechanicControllers.SocialClock_MechanicController.Config;
using Blurry.Main.CoreMechanics.MechanicControllers.SocialClock_MechanicController.Entity;
using UnityEngine.Scripting;


namespace Blurry.Main.CoreMechanics.MechanicControllers.SocialClock_MechanicController.BehaviourData
{
    [Preserve] [SocialClock_MechanicController_ReflectionCreationBundle]
    public sealed class SocialClock_MechanicController_BehaviourData : AMechanicController_BehaviourData<SocialClock_MechanicController_Entity, SocialClock_MechanicController_Config>
    {
        public SocialClock_MechanicController_BehaviourData(SocialClock_MechanicController_Entity entity, SocialClock_MechanicController_Config config) : base(entity, config)
        {
        }
    }
}