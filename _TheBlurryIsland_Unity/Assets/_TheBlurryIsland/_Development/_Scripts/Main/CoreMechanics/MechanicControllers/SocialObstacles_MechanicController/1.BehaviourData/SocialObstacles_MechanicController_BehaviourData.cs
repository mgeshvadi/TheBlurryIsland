using Blurry.Application.CoreMechanics.MechanicControllers.BehaviourData;
using Blurry.Main.CoreMechanics.MechanicControllers.SocialObstacles_MechanicController.Config;
using Blurry.Main.CoreMechanics.MechanicControllers.SocialObstacles_MechanicController.Entity;
using UnityEngine.Scripting;


namespace Blurry.Main.CoreMechanics.MechanicControllers.SocialObstacles_MechanicController.BehaviourData
{
    [Preserve] [SocialObstacles_MechanicController_ReflectionCreationBundle]
    public sealed class SocialObstacles_MechanicController_BehaviourData : AMechanicController_BehaviourData<SocialObstacles_MechanicController_Entity, SocialObstacles_MechanicController_Config>
    {
        public SocialObstacles_MechanicController_BehaviourData(SocialObstacles_MechanicController_Entity entity, SocialObstacles_MechanicController_Config config) : base(entity, config)
        {
        }
    }
}