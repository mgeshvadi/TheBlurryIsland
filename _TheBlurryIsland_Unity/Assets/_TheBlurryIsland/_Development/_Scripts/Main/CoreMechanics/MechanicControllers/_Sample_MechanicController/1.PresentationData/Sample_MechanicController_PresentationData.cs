using UnityEngine.Scripting;
using Blurry.Application.CoreMechanics.MechanicControllers.PresentationData;
using Blurry.Main.CoreMechanics.MechanicControllers.Sample_MechanicController.Config;
using Blurry.Main.CoreMechanics.MechanicControllers.Sample_MechanicController.Entity;


namespace Blurry.Main.CoreMechanics.MechanicControllers.Sample_MechanicController.PresentationData
{
    [Preserve] [Sample_MechanicController_ReflectionCreationBundle]
    public sealed class Sample_MechanicController_PresentationData : AMechanicController_PresentationData<Sample_MechanicController_Entity, Sample_MechanicController_Config>
    {
        public Sample_MechanicController_PresentationData(Sample_MechanicController_Entity entity, Sample_MechanicController_Config config) : base(entity, config)
        {
        }
    }
}