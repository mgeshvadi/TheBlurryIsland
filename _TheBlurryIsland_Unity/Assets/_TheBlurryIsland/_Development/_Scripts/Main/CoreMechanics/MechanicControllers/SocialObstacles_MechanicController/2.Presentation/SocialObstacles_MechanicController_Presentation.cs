using Blurry.Application.CoreMechanics.MechanicControllers.Presentation;
using Blurry.Main.CoreMechanics.MechanicControllers.SocialObstacles_MechanicController.PresentationData;
using UnityEngine.Scripting;


namespace Blurry.Main.CoreMechanics.MechanicControllers.SocialObstacles_MechanicController.Presentation
{
    public interface ISocialObstacles_MechanicController_Presentation : IMechanicController_Presentation
    {
    }

    [Preserve] [SocialObstacles_MechanicController_ReflectionCreationBundle]
    public sealed class SocialObstacles_MechanicController_Presentation : AMechanicController_Presentation<SocialObstacles_MechanicController_PresentationData> , ISocialObstacles_MechanicController_Presentation
    {
        public SocialObstacles_MechanicController_Presentation(SocialObstacles_MechanicController_PresentationData data) : base(data)
        {
        }
    }
}