using Blurry.Application.CoreMechanics.MechanicControllers.Entity;


namespace Blurry.Main.CoreMechanics.MechanicControllers.Sample_MechanicController.Entity
{
    [Sample_MechanicController_ReflectionCreationBundle]
    public sealed class Sample_MechanicController_Entity : AMechanicController_Entity
    {
    }
}