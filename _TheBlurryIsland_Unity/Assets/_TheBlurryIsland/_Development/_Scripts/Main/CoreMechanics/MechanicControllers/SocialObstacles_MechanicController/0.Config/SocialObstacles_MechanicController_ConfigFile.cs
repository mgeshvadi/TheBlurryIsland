using System;
using UnityEngine;
using Blurry.Application.CoreMechanics.MechanicControllers.Config;
using Blurry.Application.CoreMechanics.MechanicControllers.ConfigManagement;


namespace Blurry.Main.CoreMechanics.MechanicControllers.SocialObstacles_MechanicController.Config
{
    [Serializable]
    public sealed class SocialObstacles_MechanicController_Config : IMechanicController_Config
    {
        [SerializeField] private int SocialObstacles; // TODO: Do not hesitate to FUCK Unity! a empty config casing unity to lose our presets, after open/closing unity! Find a solution
    }

    [SocialObstacles_MechanicController_ReflectionCreationBundle]
    [CreateAssetMenu(fileName = nameof(SocialObstacles_MechanicController_ConfigFile), menuName = MechanicsControllers_ConfigFileLoader.CONFIG_FILES_CONTEXT_MENU + nameof(SocialObstacles_MechanicController_ConfigFile))]
    public sealed class SocialObstacles_MechanicController_ConfigFile : AMechanicController_ConfigFile<SocialObstacles_MechanicController_Config>
    {
    }
}