using UnityEngine.Scripting;
using Blurry.Application.CoreMechanics.MechanicControllers.PresentationData;
using Blurry.Main.CoreMechanics.MechanicControllers.SocialClock_MechanicController.Config;
using Blurry.Main.CoreMechanics.MechanicControllers.SocialClock_MechanicController.Entity;


namespace Blurry.Main.CoreMechanics.MechanicControllers.SocialClock_MechanicController.PresentationData
{
    [Preserve] [SocialClock_MechanicController_ReflectionCreationBundle]
    public sealed class SocialClock_MechanicController_PresentationData : AMechanicController_PresentationData<SocialClock_MechanicController_Entity, SocialClock_MechanicController_Config>
    {
        public SocialClock_MechanicController_PresentationData(SocialClock_MechanicController_Entity entity, SocialClock_MechanicController_Config config) : base(entity, config)
        {
        }
    }
}