using System.Collections.Generic;
using Blurry.Application.CoreMechanics.MechanicControllers.ControllerBehaviour;
using Blurry.Main.CoreMechanics.MechanicControllers.SocialObstacles_MechanicController.BehaviourData;
using Blurry.Main.CoreMechanics.MechanicControllers.SocialObstacles_MechanicController.Presentation;
using Blurry.Main.CoreMechanics.Mechanics.SocialObstacles_Mechanic.Behaviour;
using UnityEngine;
using UnityEngine.Scripting;


namespace Blurry.Main.CoreMechanics.MechanicControllers.SocialObstacles_MechanicController.ControllerBehaviour
{
    [Preserve] [SocialObstacles_MechanicController_ReflectionCreationBundle]
    public sealed class SocialObstacles_MechanicController_Behaviour : AMechanicController_Behaviour<ISocialObstacles_MechanicController_Presentation, SocialObstacles_MechanicController_BehaviourData, SocialObstacles_Mechanic_Behaviour>
    {
        public SocialObstacles_MechanicController_Behaviour(ISocialObstacles_MechanicController_Presentation presentation, SocialObstacles_MechanicController_BehaviourData data, List<SocialObstacles_Mechanic_Behaviour> behaviours) : base(presentation, data, behaviours)
        {
        }

        public SocialObstacles_MechanicController_Behaviour(ISocialObstacles_MechanicController_Presentation presentation, SocialObstacles_MechanicController_BehaviourData data, List<int> behaviour) : base(presentation, data, new List<SocialObstacles_Mechanic_Behaviour>())
        {
        }

        protected override void Activate__()
        {
            Debug.Log($"{nameof(SocialObstacles_MechanicController_Behaviour)}:{nameof(Activate__)}");
        }

        protected override void StartInner__()
        {
            Debug.Log($"{nameof(SocialObstacles_MechanicController_Behaviour)}:{nameof(StartInner__)}");
        }

        protected override void StopInner__()
        {
            Debug.Log($"{nameof(SocialObstacles_MechanicController_Behaviour)}:{nameof(StopInner__)}");
        }

        protected override void ResetInner__()
        {
            Debug.Log($"{nameof(SocialObstacles_MechanicController_Behaviour)}:{nameof(ResetInner__)}");
        }

        protected override void RestartInner__()
        {
            Debug.Log($"{nameof(SocialObstacles_MechanicController_Behaviour)}:{nameof(RestartInner__)}");
        }

        protected override void Deactivate__()
        {
            Debug.Log($"{nameof(SocialObstacles_MechanicController_Behaviour)}:{nameof(Deactivate__)}");
        }

        protected override void Update_(float deltaTime)
        {
            Debug.Log($"{nameof(SocialObstacles_MechanicController_Behaviour)}:{nameof(Update_)}");
        }

        protected override void FixedUpdate_(float fixedDeltaTime)
        {
            Debug.Log($"{nameof(SocialObstacles_MechanicController_Behaviour)}:{nameof(FixedUpdate_)}");
        }
    }
}