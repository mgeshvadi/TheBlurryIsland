using Blurry.Application.CoreMechanics.MechanicControllers.Presentation;
using Blurry.Main.CoreMechanics.MechanicControllers.SocialClock_MechanicController.PresentationData;
using UnityEngine.Scripting;


namespace Blurry.Main.CoreMechanics.MechanicControllers.SocialClock_MechanicController.Presentation
{
    public interface ISocialClock_MechanicController_Presentation : IMechanicController_Presentation
    {
    }

    [Preserve] [SocialClock_MechanicController_ReflectionCreationBundle]
    public sealed class SocialClock_MechanicController_Presentation : AMechanicController_Presentation<SocialClock_MechanicController_PresentationData> , ISocialClock_MechanicController_Presentation
    {
        public SocialClock_MechanicController_Presentation(SocialClock_MechanicController_PresentationData data) : base(data)
        {
        }
    }
}