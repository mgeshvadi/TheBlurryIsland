using Blurry.Application.CoreMechanics.MechanicControllers.BehaviourData;
using Blurry.Main.CoreMechanics.MechanicControllers.Sample_MechanicController.Config;
using Blurry.Main.CoreMechanics.MechanicControllers.Sample_MechanicController.Entity;
using UnityEngine.Scripting;


namespace Blurry.Main.CoreMechanics.MechanicControllers.Sample_MechanicController.BehaviourData
{
    [Preserve] [Sample_MechanicController_ReflectionCreationBundle]
    public sealed class Sample_MechanicController_BehaviourData : AMechanicController_BehaviourData<Sample_MechanicController_Entity, Sample_MechanicController_Config>
    {
        public Sample_MechanicController_BehaviourData(Sample_MechanicController_Entity entity, Sample_MechanicController_Config config) : base(entity, config)
        {
        }
    }
}