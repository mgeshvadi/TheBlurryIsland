using System;
using UnityEngine;
using Blurry.Application.CoreMechanics.MechanicControllers.Config;
using Blurry.Application.CoreMechanics.MechanicControllers.ConfigManagement;


namespace Blurry.Main.CoreMechanics.MechanicControllers.SocialClock_MechanicController.Config
{
    [Serializable]
    public sealed class SocialClock_MechanicController_Config : IMechanicController_Config
    {
        [SerializeField] private int socialClock;
    }

    [SocialClock_MechanicController_ReflectionCreationBundle]
    [CreateAssetMenu(fileName = nameof(SocialClock_MechanicController_ConfigFile), menuName = MechanicsControllers_ConfigFileLoader.CONFIG_FILES_CONTEXT_MENU + nameof(SocialClock_MechanicController_ConfigFile))]
    public sealed class SocialClock_MechanicController_ConfigFile : AMechanicController_ConfigFile<SocialClock_MechanicController_Config>
    {
    }
}