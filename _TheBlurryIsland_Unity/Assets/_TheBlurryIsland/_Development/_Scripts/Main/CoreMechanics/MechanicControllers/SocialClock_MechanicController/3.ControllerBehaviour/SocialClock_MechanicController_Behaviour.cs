using System.Collections.Generic;
using Blurry.Application.CoreMechanics.MechanicControllers.ControllerBehaviour;
using Blurry.Main.CoreMechanics.MechanicControllers.SocialClock_MechanicController.BehaviourData;
using Blurry.Main.CoreMechanics.MechanicControllers.SocialClock_MechanicController.Presentation;
using Blurry.Main.CoreMechanics.Mechanics.SocialClock_Mechanic.Behaviour;
using UnityEngine.Scripting;


namespace Blurry.Main.CoreMechanics.MechanicControllers.SocialClock_MechanicController.ControllerBehaviour
{
    [Preserve] [SocialClock_MechanicController_ReflectionCreationBundle]
    public sealed class SocialClock_MechanicController_Behaviour : AMechanicController_Behaviour<ISocialClock_MechanicController_Presentation, SocialClock_MechanicController_BehaviourData, SocialClock_Mechanic_Behaviour>
    {
        public SocialClock_MechanicController_Behaviour(ISocialClock_MechanicController_Presentation presentation, SocialClock_MechanicController_BehaviourData data, List<SocialClock_Mechanic_Behaviour> behaviours) : base(presentation, data, behaviours)
        {
        }

        public SocialClock_MechanicController_Behaviour(ISocialClock_MechanicController_Presentation presentation, SocialClock_MechanicController_BehaviourData data, List<int> behaviour) : base(presentation, data, new List<SocialClock_Mechanic_Behaviour>())
        {
        }

        protected override void Activate__()
        {
        }

        protected override void StartInner__()
        {
        }

        protected override void StopInner__()
        {
        }

        protected override void ResetInner__()
        {
        }

        protected override void RestartInner__()
        {
        }

        protected override void Deactivate__()
        {
        }

        protected override void Update_(float deltaTime)
        {
        }

        protected override void FixedUpdate_(float fixedDeltaTime)
        {
        }
    }
}