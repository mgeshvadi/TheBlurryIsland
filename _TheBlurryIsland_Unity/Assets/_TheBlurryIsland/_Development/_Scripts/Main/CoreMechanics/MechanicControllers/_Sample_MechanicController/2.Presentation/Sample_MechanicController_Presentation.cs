using Blurry.Application.CoreMechanics.MechanicControllers.Presentation;
using Blurry.Main.CoreMechanics.MechanicControllers.Sample_MechanicController.PresentationData;
using UnityEngine.Scripting;


namespace Blurry.Main.CoreMechanics.MechanicControllers.Sample_MechanicController.Presentation
{
    public interface ISample_MechanicController_Presentation : IMechanicController_Presentation
    {
    }

    [Preserve] [Sample_MechanicController_ReflectionCreationBundle]
    public sealed class Sample_MechanicController_Presentation : AMechanicController_Presentation<Sample_MechanicController_PresentationData> , ISample_MechanicController_Presentation
    {
        public Sample_MechanicController_Presentation(Sample_MechanicController_PresentationData data) : base(data)
        {
        }
    }
}