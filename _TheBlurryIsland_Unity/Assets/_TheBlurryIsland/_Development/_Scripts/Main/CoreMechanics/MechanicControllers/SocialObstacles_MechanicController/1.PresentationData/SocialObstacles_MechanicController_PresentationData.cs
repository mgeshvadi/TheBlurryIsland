using UnityEngine.Scripting;
using Blurry.Application.CoreMechanics.MechanicControllers.PresentationData;
using Blurry.Main.CoreMechanics.MechanicControllers.SocialObstacles_MechanicController.Config;
using Blurry.Main.CoreMechanics.MechanicControllers.SocialObstacles_MechanicController.Entity;


namespace Blurry.Main.CoreMechanics.MechanicControllers.SocialObstacles_MechanicController.PresentationData
{
    [Preserve] [SocialObstacles_MechanicController_ReflectionCreationBundle]
    public sealed class SocialObstacles_MechanicController_PresentationData : AMechanicController_PresentationData<SocialObstacles_MechanicController_Entity, SocialObstacles_MechanicController_Config>
    {
        public SocialObstacles_MechanicController_PresentationData(SocialObstacles_MechanicController_Entity entity, SocialObstacles_MechanicController_Config config) : base(entity, config)
        {
        }
    }
}