using System;
using UnityEngine;
using Blurry.Application.CoreMechanics.MechanicControllers.Config;
using Blurry.Application.CoreMechanics.MechanicControllers.ConfigManagement;


namespace Blurry.Main.CoreMechanics.MechanicControllers.Sample_MechanicController.Config
{
    [Serializable]
    public sealed class Sample_MechanicController_Config : IMechanicController_Config
    {
        [SerializeField] private int sample; // TODO: Do not hesitate to FUCK Unity! a empty config casing unity to lose our presets, after open/closing unity! Find a solution
    }

    [Sample_MechanicController_ReflectionCreationBundle]
    [CreateAssetMenu(fileName = nameof(Sample_MechanicController_ConfigFile), menuName = MechanicsControllers_ConfigFileLoader.CONFIG_FILES_CONTEXT_MENU + nameof(Sample_MechanicController_ConfigFile))]
    public sealed class Sample_MechanicController_ConfigFile : AMechanicController_ConfigFile<Sample_MechanicController_Config>
    {
    }
}