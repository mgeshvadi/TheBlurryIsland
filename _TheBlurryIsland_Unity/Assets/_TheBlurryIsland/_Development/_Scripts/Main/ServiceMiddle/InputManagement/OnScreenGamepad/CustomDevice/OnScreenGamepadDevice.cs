using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Controls;
using UnityEngine.InputSystem.Layouts;
using UnityEngine.InputSystem.LowLevel;
using UnityEngine.InputSystem.Utilities;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Blurry.Main.ServiceMiddle.InputManagement.OnScreenGamepad.CustomDevice
{
    public struct OnScreenGamepadState : IInputStateTypeInfo
    {
        public FourCC format => new FourCC('C', 'U', 'S', 'T');

        [InputControl(name = "leftStick", displayName = "Left Stick", layout = "Stick", usage = "Primary2DMotion", processors = "stickDeadzone")]
        public Vector2 lefttStick;

        [InputControl(name = "rightStick", displayName = "Right Stick", layout = "Stick", usage = "Primary2DMotion", processors = "stickDeadzone")]
        public Vector2 rightStick;
    }

    #if UNITY_EDITOR
    [InitializeOnLoad]
    #endif
    [InputControlLayout(stateType = typeof(OnScreenGamepadState))]
    public class OnScreenGamepad : InputDevice, IInputUpdateCallbackReceiver
    {
        #if UNITY_EDITOR
        static OnScreenGamepad()
        {
            Initialize();
        }
        #endif

        [RuntimeInitializeOnLoadMethod]
        private static void Initialize()
        {
            InputSystem.RegisterLayout<OnScreenGamepad>(
                matches: new InputDeviceMatcher()
                   .WithInterface("Custom"));
        }

        public StickControl leftStick { get; private set; }
        public StickControl rightStick { get; private set; }

        protected override void FinishSetup()
        {
            base.FinishSetup();

            leftStick = GetChildControl<StickControl>("leftStick");
            rightStick = GetChildControl<StickControl>("rightStick");
        }

        public static OnScreenGamepad current { get; private set; }

        public override void MakeCurrent()
        {
            base.MakeCurrent();
            current = this;
        }

        protected override void OnRemoved()
        {
            base.OnRemoved();
            if (current == this)
                current = null;
        }

        public void OnUpdate()
        {

        }
    }
}