﻿using System;
using UnityEngine;


namespace Blurry.Main.ServiceMiddle.InputManagement.OnScreenGamepad.Presentation
{
    public partial class OnScreenGamepadStick
    {
        [Serializable]
        public struct StickVisual
        {
            [SerializeField] private RectTransform visualRectTransform;
            [SerializeField] private bool showVisualOnStart;
            [SerializeField] private bool showVisualOnPointerDown;

            private Vector2 touchStartAnchoredPosition;
            private bool isActive;

            public void Init()
            {
                SetActiveness(showVisualOnStart, forceToUpdate: true);
            }

            public void HandlePointerDown(Vector2 touchDownPosition)
            {
                SetActiveness(showVisualOnPointerDown);
                SetPosition(touchDownPosition);
                touchStartAnchoredPosition = visualRectTransform.anchoredPosition;
            }

            public void HandleDrag(Vector2 dragAmount)
            {
                SetAnchoredPosition(touchStartAnchoredPosition + dragAmount);
            }

            public void HandlePointerUp()
            {
                SetAnchoredPosition(touchStartAnchoredPosition);
                SetActiveness(false);
            }

            private void SetActiveness(bool isActive, bool forceToUpdate = false)
            {
                if (this.isActive == isActive && !forceToUpdate)
                    return;

                this.isActive = isActive;
                visualRectTransform.gameObject.SetActive(isActive);
            }

            private void SetAnchoredPosition(Vector2 position)
            {
                if (isActive)
                    visualRectTransform.anchoredPosition = position;
            }

            private void SetPosition(Vector2 position)
            {
                if (isActive)
                    visualRectTransform.position = position;
            }
        }
    }
}