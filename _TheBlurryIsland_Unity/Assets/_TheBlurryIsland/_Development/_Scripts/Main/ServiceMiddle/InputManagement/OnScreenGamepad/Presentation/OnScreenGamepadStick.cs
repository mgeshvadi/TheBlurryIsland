﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem.Layouts;
using UnityEngine.InputSystem.OnScreen;
using UnityEngine.UI;


namespace Blurry.Main.ServiceMiddle.InputManagement.OnScreenGamepad.Presentation
{
    public partial class OnScreenGamepadStick : OnScreenControl, IPointerDownHandler, IPointerUpHandler, IDragHandler
    {
        private const int MAX_DRAG_AMOUNT = 50;

        [InputControl(layout = "Vector2")]
        [SerializeField] private string actionControlPath;
        [SerializeField] private StickVisual stickVisual;
        [Space]
        [SerializeField] private Text debugText; // Todo : remove this line After Input system is proven to be correct

        private RectTransform thisRectTransform;
        private Vector2 touchStartLocalPoint;

        protected override string controlPathInternal
        {
            get => actionControlPath;
            set => actionControlPath = value;
        }

        private void Awake()
        {
            stickVisual.Init();
            thisRectTransform = transform.GetComponent<RectTransform>();
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            touchStartLocalPoint = CalculateScreenPointToLocalPointInRectangle(eventData.position, eventData.pressEventCamera);
            stickVisual.HandlePointerDown(touchDownPosition: eventData.position);
        }

        public void OnDrag(PointerEventData eventData)
        {
            Vector2 currenTouchLocalPoint = CalculateScreenPointToLocalPointInRectangle(eventData.position, eventData.pressEventCamera);

            if (thisRectTransform.rect.Contains(currenTouchLocalPoint))
                HandleDrag(dragAmount: currenTouchLocalPoint - touchStartLocalPoint);
            else
                OnPointerUp(eventData);
        }

        private void HandleDrag(Vector2 dragAmount)
        {
            dragAmount = Vector2.ClampMagnitude(dragAmount, maxLength: MAX_DRAG_AMOUNT);
            SendValueToControl(new Vector2(dragAmount.x / MAX_DRAG_AMOUNT, dragAmount.y / MAX_DRAG_AMOUNT));

            stickVisual.HandleDrag(dragAmount);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            SendValueToControl(Vector2.zero);
            stickVisual.HandlePointerUp();
        }

        private Vector2 CalculateScreenPointToLocalPointInRectangle(Vector2 screenPoint, Camera camera)
        {
            RectTransformUtility.ScreenPointToLocalPointInRectangle(thisRectTransform, screenPoint, camera, out Vector2 result);
            return result;
        }

        private void SendValueToControl(Vector2 value)
        {
            debugText.text = $"Value sent to input: {value}"; // Todo : remove this line After Input system is proven to be correct
            base.SendValueToControl(value);
        }
    }
}