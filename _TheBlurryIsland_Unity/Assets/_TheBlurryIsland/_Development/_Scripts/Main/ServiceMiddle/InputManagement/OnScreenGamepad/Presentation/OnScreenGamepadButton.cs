﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem.Layouts;
using UnityEngine.InputSystem.OnScreen;


namespace Blurry.Main.ServiceMiddle.InputManagement.OnScreenGamepad.Presentation
{
    public class OnScreenGamepadButton : OnScreenControl, IPointerDownHandler, IPointerUpHandler, IDragHandler
    {
        [InputControl(layout = "Button")]
        [SerializeField]
        private string actionControlPath;

        private RectTransform thisRectTransform;

        private void Awake()
        {
            thisRectTransform = transform.GetComponent<RectTransform>();
        }

        // ReSharper disable once ConvertToAutoProperty
        protected override string controlPathInternal
        {
            get => actionControlPath;
            set => actionControlPath = value;
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            SendValueToControl(1.0f);
        }

        public void OnDrag(PointerEventData eventData)
        {
            var currentTouchLocalPoint = CalculateScreenPointToLocalPointInRectangle(eventData.position, eventData.pressEventCamera);
            if (!thisRectTransform.rect.Contains(currentTouchLocalPoint))
                OnPointerUp(eventData);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            SendValueToControl(0.0f);
        }

        private Vector2 CalculateScreenPointToLocalPointInRectangle(Vector2 screenPoint, Camera camera)
        {
            RectTransformUtility.ScreenPointToLocalPointInRectangle(thisRectTransform, screenPoint, camera, out Vector2 result);
            return result;
        }
    }
}