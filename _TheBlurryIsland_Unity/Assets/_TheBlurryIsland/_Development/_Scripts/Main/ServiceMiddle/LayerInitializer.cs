namespace Blurry.Main.ServiceMiddle
{
    public class LayerInitializationsResult : Application.ServiceMiddle.LayerInitializationsResult
    {
    }

    public static class LayerInitializer
    {
        public static LayerInitializationsResult Initialize()
        {
            var applicationSideResult = Application.ServiceMiddle.LayerInitializer.Initialize();
            LayerInitializationsResult result = new LayerInitializationsResult();
            return result;
        }
    }
}