namespace Blurry.Main.CharacterControllers
{
    public class LayerInitializationsResult : Application.CharacterControllers.LayerInitializationsResult
    {
    }

    public static class LayerInitializer
    {
        public static LayerInitializationsResult Initialize()
        {
            var applicationSideResult = Application.CharacterControllers.LayerInitializer.Initialize();
            LayerInitializationsResult result = new LayerInitializationsResult();
            return result;
        }
    }
}