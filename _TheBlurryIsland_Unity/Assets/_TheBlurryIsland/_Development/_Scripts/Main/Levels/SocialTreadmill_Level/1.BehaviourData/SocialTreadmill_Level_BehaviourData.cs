using Blurry.Application.Levels.BehaviourData;
using Blurry.Main.Levels.SocialTreadmill_Level.Config;
using Blurry.Main.Levels.SocialTreadmill_Level.Entity;
using UnityEngine.Scripting;


namespace Blurry.Main.Levels.SocialTreadmill_Level.BehaviourData
{
    [Preserve] [SocialTreadmill_Level_ReflectionCreationBundle]
    public sealed class SocialTreadmill_Level_BehaviourData : ALevel_BehaviourData<SocialTreadmill_Level_Entity, SocialTreadmill_Level_Config>
    {
        public SocialTreadmill_Level_BehaviourData(SocialTreadmill_Level_Entity entity, SocialTreadmill_Level_Config config) : base(entity, config)
        {
        }
    }
}