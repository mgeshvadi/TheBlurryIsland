using System;
using UnityEngine;
using Blurry.Application.Levels.Config;
using Blurry.Application.Levels.ConfigManagement;


namespace Blurry.Main.Levels.SocialTreadmill_Level.Config
{
    [Serializable]
    public sealed class SocialTreadmill_Level_Config : ILevel_Config
    {
        [SerializeField] private int socialTreadmill;
    }

    [SocialTreadmill_Level_ReflectionCreationBundle]
    [CreateAssetMenu(fileName = nameof(SocialTreadmill_Level_ConfigFile), menuName = Levels_ConfigFileLoader.CONFIG_FILES_CONTEXT_MENU + nameof(SocialTreadmill_Level_ConfigFile))]
    public sealed class SocialTreadmill_Level_ConfigFile : ALevel_ConfigFile<SocialTreadmill_Level_Config>
    {
    }
}