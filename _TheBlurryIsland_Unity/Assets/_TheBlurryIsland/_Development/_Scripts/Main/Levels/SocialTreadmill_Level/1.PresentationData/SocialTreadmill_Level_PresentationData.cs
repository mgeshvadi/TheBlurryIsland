using Blurry.Application.Levels.PresentationData;
using Blurry.Main.Levels.SocialTreadmill_Level.Config;
using Blurry.Main.Levels.SocialTreadmill_Level.Entity;
using UnityEngine.Scripting;


namespace Blurry.Main.Levels.SocialTreadmill_Level.PresentationData
{
    [Preserve] [SocialTreadmill_Level_ReflectionCreationBundle]
    public sealed class SocialTreadmill_Level_PresentationData : ALevel_PresentationData<SocialTreadmill_Level_Entity, SocialTreadmill_Level_Config>
    {
        public SocialTreadmill_Level_PresentationData(SocialTreadmill_Level_Entity entity, SocialTreadmill_Level_Config config) : base(entity, config)
        {
        }
    }
}