using Blurry.Application.Levels.Presentation;
using Blurry.Main.Levels.SocialTreadmill_Level.PresentationData;
using UnityEngine.Scripting;


namespace Blurry.Main.Levels.SocialTreadmill_Level.Presentation
{
    public interface ISocialTreadmill_Level_Presentation : ILevel_Presentation
    {
    }

    [Preserve] [SocialTreadmill_Level_ReflectionCreationBundle]
    internal sealed class SocialTreadmill_Level_Presentation : ALevel_Presentation<SocialTreadmill_Level_PresentationData>, ISocialTreadmill_Level_Presentation
    {
        public SocialTreadmill_Level_Presentation(SocialTreadmill_Level_PresentationData data) : base(data)
        {
        }
    }
}