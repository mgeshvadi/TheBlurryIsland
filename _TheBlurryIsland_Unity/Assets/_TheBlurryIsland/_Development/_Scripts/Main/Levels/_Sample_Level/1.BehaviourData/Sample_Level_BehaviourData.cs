using Blurry.Application.Levels.BehaviourData;
using Blurry.Main.Levels.Sample_Level.Config;
using Blurry.Main.Levels.Sample_Level.Entity;
using UnityEngine.Scripting;


namespace Blurry.Main.Levels.Sample_Level.BehaviourData
{
    [Preserve] [Sample_Level_ReflectionCreationBundle]
    public sealed class Sample_Level_BehaviourData : ALevel_BehaviourData<Sample_Level_Entity, Sample_Level_Config>
    {
        public Sample_Level_BehaviourData(Sample_Level_Entity entity, Sample_Level_Config config) : base(entity, config)
        {
        }
    }
}