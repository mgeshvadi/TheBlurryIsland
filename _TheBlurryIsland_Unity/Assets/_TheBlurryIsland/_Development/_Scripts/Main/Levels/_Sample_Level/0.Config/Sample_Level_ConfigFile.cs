using System;
using UnityEngine;
using Blurry.Application.Levels.Config;
using Blurry.Application.Levels.ConfigManagement;


namespace Blurry.Main.Levels.Sample_Level.Config
{
    [Serializable]
    public sealed class Sample_Level_Config : ILevel_Config
    {
        [SerializeField] private int sample;
    }

    [Sample_Level_ReflectionCreationBundle]
    [CreateAssetMenu(fileName = nameof(Sample_Level_ConfigFile), menuName = Levels_ConfigFileLoader.CONFIG_FILES_CONTEXT_MENU + nameof(Sample_Level_ConfigFile))]
    public sealed class Sample_Level_ConfigFile : ALevel_ConfigFile<Sample_Level_Config>
    {
    }
}