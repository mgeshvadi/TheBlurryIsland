using System;
using System.Collections.Generic;
using Blurry.Application.CoreMechanics.MechanicControllers.ControllerBehaviour;
using Blurry.Application.Levels.Behaviour;
using Blurry.Main.Levels.Sample_Level.BehaviourData;
using Blurry.Main.Levels.Sample_Level.Presentation;
using UnityEngine;
using UnityEngine.Scripting;


namespace Blurry.Main.Levels.Sample_Level.Behaviour
{
    [Preserve] [Sample_Level_ReflectionCreationBundle]
    public sealed class Sample_Level_Behaviour : ALevel_Behaviour<ISample_Level_Presentation, Sample_Level_BehaviourData>
    {
        public Sample_Level_Behaviour(ISample_Level_Presentation presentation, Sample_Level_BehaviourData data, Dictionary<Type, IMechanicController_Behaviour> behaviours) : base(presentation, data, behaviours)
        {
        }

        protected override void Activate__()
        {
            Debug.Log($"{nameof(Sample_Level_Behaviour)}:{nameof(Activate__)}");
        }

        protected override void StartInner__()
        {
            Debug.Log($"{nameof(Sample_Level_Behaviour)}:{nameof(StartInner__)}");
        }

        protected override void StopInner__()
        {
            Debug.Log($"{nameof(Sample_Level_Behaviour)}:{nameof(StopInner__)}");
        }

        protected override void ResetInner__()
        {
            Debug.Log($"{nameof(Sample_Level_Behaviour)}:{nameof(ResetInner__)}");
        }

        protected override void RestartInner__()
        {
            Debug.Log($"{nameof(Sample_Level_Behaviour)}:{nameof(RestartInner__)}");
        }

        protected override void Deactivate__()
        {
            Debug.Log($"{nameof(Sample_Level_Behaviour)}:{nameof(Deactivate__)}");
        }

        protected override void Update_(float deltaTime)
        {
            Debug.Log($"{nameof(Sample_Level_Behaviour)}:{nameof(Update_)}");
        }

        protected override void FixedUpdate_(float fixedDeltaTime)
        {
            Debug.Log($"{nameof(Sample_Level_Behaviour)}:{nameof(FixedUpdate_)}");
        }
    }
}