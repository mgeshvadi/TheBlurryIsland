using Blurry.Application.Levels.PresentationData;
using Blurry.Main.Levels.Sample_Level.Config;
using Blurry.Main.Levels.Sample_Level.Entity;
using UnityEngine.Scripting;


namespace Blurry.Main.Levels.Sample_Level.PresentationData
{
    [Preserve] [Sample_Level_ReflectionCreationBundle]
    public sealed class Sample_Level_PresentationData : ALevel_PresentationData<Sample_Level_Entity, Sample_Level_Config>
    {
        public Sample_Level_PresentationData(Sample_Level_Entity entity, Sample_Level_Config config) : base(entity, config)
        {
        }
    }
}