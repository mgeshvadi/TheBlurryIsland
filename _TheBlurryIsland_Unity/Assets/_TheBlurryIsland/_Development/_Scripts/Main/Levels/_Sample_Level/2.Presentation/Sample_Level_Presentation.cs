using Blurry.Application.Levels.Presentation;
using Blurry.Main.Levels.Sample_Level.PresentationData;
using UnityEngine.Scripting;


namespace Blurry.Main.Levels.Sample_Level.Presentation
{
    public interface ISample_Level_Presentation : ILevel_Presentation
    {
    }

    [Preserve] [Sample_Level_ReflectionCreationBundle]
    internal sealed class Sample_Level_Presentation : ALevel_Presentation<Sample_Level_PresentationData>, ISample_Level_Presentation
    {
        public Sample_Level_Presentation(Sample_Level_PresentationData data) : base(data)
        {
        }
    }
}