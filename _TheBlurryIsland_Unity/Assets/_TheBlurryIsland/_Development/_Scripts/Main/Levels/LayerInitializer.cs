using Blurry.Application.CoreMechanics.MechanicControllers.Factory;
using Blurry.Application.Levels.Factory;


namespace Blurry.Main.Levels
{
    public class LayerInitializationsResult : Application.Levels.LayerInitializationsResult
    {
        public LayerInitializationsResult(ILevelFactory levelFactory) : base(levelFactory)
        {
        }
    }

    public static class LayerInitializer
    {
        public static LayerInitializationsResult Initialize(IMechanicControllersFactory mechanicControllersFactory)
        {
            var applicationSideResult = Application.Levels.LayerInitializer.Initialize(mechanicControllersFactory);
            LayerInitializationsResult result = new LayerInitializationsResult(applicationSideResult.LevelFactory);
            return result;
        }
    }
}