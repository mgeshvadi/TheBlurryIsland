using Blurry.Application.God;
using Blurry.Application.God.Factory;
using Blurry.Main.God.Ultimate;


namespace Blurry.Main.Game.God
{
    public sealed class Ultimate_God : AGod
    {
        protected override AGodFactory Factory => new Ultimate_GodFactory();
    }
}