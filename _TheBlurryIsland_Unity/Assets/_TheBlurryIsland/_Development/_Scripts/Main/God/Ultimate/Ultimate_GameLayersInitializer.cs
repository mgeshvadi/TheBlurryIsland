// ReSharper disable UnusedVariable
// ReSharper disable RedundantNameQualifier

using Blurry.Application.God.Initialization;


namespace Blurry.Main.God.Ultimate
{
    public sealed class Ultimate_GameLayersInitializer : IGameLayersInitializer
    {
        public GameLayersInitializationResult InitializeAllLayers()
        {
            GameLayersInitializationResult initializationResult = new GameLayersInitializationResult();

            Application.Utility.LayerInitializationsResult utilityInitializationsResult = Application.Utility.LayerInitializer.Initialize();
            Application.UnityLow.LayerInitializationsResult unityLowInitializationsResult = Application.UnityLow.LayerInitializer.Initialize();
            Main.Base.LayerInitializationsResult baseInitializationsResult = Main.Base.LayerInitializer.Initialize();
            Application.Foundation.LayerInitializationsResult foundationInitializationsResult = Application.Foundation.LayerInitializer.Initialize();
            Application.UnityMiddle.LayerInitializationsResult unityMiddleInitializationsResult = Application.UnityMiddle.LayerInitializer.Initialize();

            Application.ServiceLow.LayerInitializationsResult serviceLowInitializationsResult = Application.ServiceLow.LayerInitializer.Initialize();
            Main.ServiceMiddle.LayerInitializationsResult serviceMiddleInitializationsResult = Main.ServiceMiddle.LayerInitializer.Initialize();

            Main.CoreBase.LayerInitializationsResult coreBaseInitializationsResult = Main.CoreBase.LayerInitializer.Initialize();
            Main.CharacterBehaviours.LayerInitializationsResult characterBehavioursInitializationsResult = Main.CharacterBehaviours.LayerInitializer.Initialize();
            Main.CharacterControllers.LayerInitializationsResult characterControllersInitializationsResult = Main.CharacterControllers.LayerInitializer.Initialize();
            Main.CoreMechanics.LayerInitializationsResult coreMechanicsInitializationsResult = Main.CoreMechanics.LayerInitializer.Initialize();
            Main.Levels.LayerInitializationsResult levelsInitializationsResult = Main.Levels.LayerInitializer.Initialize(coreMechanicsInitializationsResult.MechanicControllersFactory);
            Main.LevelManagement.LayerInitializationsResult levelManagementInitializationsResult = Main.LevelManagement.LayerInitializer.Initialize(levelsInitializationsResult.LevelFactory);


            Application.UI.LayerInitializationsResult uiInitializationsResult = Application.UI.LayerInitializer.Initialize();
            Application.Overlay.LayerInitializationsResult overlayInitializationsResult = Application.Overlay.LayerInitializer.Initialize();
            Application.Business.LayerInitializationsResult businessInitializationsResult = Application.Business.LayerInitializer.Initialize();

            initializationResult.BehavioursNeedingUpdate.Add(levelManagementInitializationsResult.LevelManagement);
            initializationResult.LevelManagement = levelManagementInitializationsResult.LevelManagement;

            return initializationResult;
        }
    }
}