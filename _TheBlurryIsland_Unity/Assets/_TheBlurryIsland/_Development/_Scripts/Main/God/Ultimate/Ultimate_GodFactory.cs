using UnityEngine.Scripting;
using Blurry.Application.God.Factory;
using Blurry.Application.God.Initialization;


namespace Blurry.Main.God.Ultimate
{
    [Preserve]
    public sealed class Ultimate_GodFactory : AGodFactory
    {
        public override IGameLayersInitializer CreateLayersInitializer()
        {
            return new Ultimate_GameLayersInitializer();
        }
    }
}