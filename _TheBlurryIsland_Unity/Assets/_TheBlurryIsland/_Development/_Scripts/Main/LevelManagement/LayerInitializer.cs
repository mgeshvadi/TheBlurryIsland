using Blurry.Application.LevelManagement.Base;
using Blurry.Application.Levels.Factory;
using Blurry.Main.LevelManagement.Ultimate;


namespace Blurry.Main.LevelManagement
{
    public class LayerInitializationsResult : Application.LevelManagement.LayerInitializationsResult
    {
        public LayerInitializationsResult(ILevelManagement levelManagement) : base(levelManagement)
        {
        }
    }

    public static class LayerInitializer
    {
        public static LayerInitializationsResult Initialize(ILevelFactory levelFactory)
        {
            var applicationSideResult = Application.LevelManagement.LayerInitializer.Initialize<Ultimate_LevelManagement_ConfigFile>(levelFactory);
            LayerInitializationsResult result = new LayerInitializationsResult(applicationSideResult.LevelManagement);
            return result;
        }
    }
}