using Blurry.Application.Foundation.ConfigFileManagement;
using Blurry.Application.CoreMechanics.MechanicControllers.ConfigManagement;
using Blurry.Application.CoreMechanics.MechanicControllers.Factory;
using Blurry.Application.CoreMechanics.Mechanics.ConfigManagement;
using Blurry.Application.CoreMechanics.Mechanics.Factory;


namespace Blurry.Application.CoreMechanics
{
    public class LayerInitializationsResult
    {
        public IMechanicControllersFactory MechanicControllersFactory { get; }

        public LayerInitializationsResult(IMechanicControllersFactory mechanicControllersFactory)
        {
            MechanicControllersFactory = mechanicControllersFactory;
        }
    }

    public static class LayerInitializer
    {
        public static LayerInitializationsResult MockInitialize()
        {
            return Initialize();
        }

        public static LayerInitializationsResult Initialize()
        {
            ConfigFileManagement.RegisterConfigFileLoader<Mechanics_ConfigFileLoader>(new Mechanics_ConfigFileLoader());
            ConfigFileManagement.RegisterConfigFileLoader<MechanicsControllers_ConfigFileLoader>(new MechanicsControllers_ConfigFileLoader());
            IMechanicsFactory mechanicsFactory = new MechanicsFactory();
            return new LayerInitializationsResult(new MechanicControllersFactory(mechanicsFactory));
        }
    }
}