using Blurry.Application.Foundation.Config;


namespace Blurry.Application.CoreMechanics.MechanicControllers.Config
{
    public interface IMechanicController_Config : IConfig
    {
    }

    public interface IMechanicController_ConfigFile : IConfig_File
    {

    }

    public abstract class AMechanicController_ConfigFile<TConfig> : AConfig_File<TConfig>, IMechanicController_ConfigFile where TConfig : IMechanicController_Config
    {
    }
}