using Blurry.Application.CoreMechanics.MechanicControllers.Config;
using Blurry.Application.CoreMechanics.MechanicControllers.Entity;
using Blurry.Application.Foundation.BehaviourData;


namespace Blurry.Application.CoreMechanics.MechanicControllers.BehaviourData
{
    public interface IMechanicController_BehaviourData : IBehaviour_Data
    {
    }

    public abstract class AMechanicController_BehaviourData<TEntity, TConfig> : ABehaviour_Data<TEntity, TConfig>, IMechanicController_BehaviourData where TEntity : IMechanicController_Entity
                                                                                                                                                     where TConfig : IMechanicController_Config
    {
        protected AMechanicController_BehaviourData(TEntity entity, TConfig config) : base(entity, config)
        {
        }
    }
}