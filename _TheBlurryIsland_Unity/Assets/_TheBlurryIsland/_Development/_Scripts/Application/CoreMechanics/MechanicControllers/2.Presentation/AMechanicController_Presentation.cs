using Blurry.Application.CoreMechanics.MechanicControllers.PresentationData;
using Blurry.Application.Foundation.Presentation;


namespace Blurry.Application.CoreMechanics.MechanicControllers.Presentation
{
    public interface IMechanicController_Presentation : IPresentation
    {
    }

    public abstract class AMechanicController_Presentation<TData> : APresentation<TData>, IMechanicController_Presentation where TData : IMechanicController_PresentationData
    {
        protected AMechanicController_Presentation(TData data) : base(data)
        {
        }
    }
}