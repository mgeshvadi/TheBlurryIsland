using Blurry.Application.Foundation.Entity;


namespace Blurry.Application.CoreMechanics.MechanicControllers.Entity
{
    public interface IMechanicController_Entity : IEntity
    {

    }

    public abstract class AMechanicController_Entity : AEntity, IMechanicController_Entity
    {
    }
}