using System;
using System.Collections.Generic;
using Blurry.Application.CoreMechanics.MechanicControllers.ControllerBehaviour;
using Blurry.Application.CoreMechanics.MechanicControllers.Entity;


namespace Blurry.Application.CoreMechanics.MechanicControllers.Factory
{
    public interface IMechanicControllersFactory
    {
        public Dictionary<Type, IMechanicController_Behaviour> CreateMechanicControllersBehavioursForThese(List<IMechanicController_Entity> mechanicControllerEntities);
    }
}