using System.Collections.Generic;
using Blurry.Application.CoreMechanics.Mechanics.Behaviour;
using Blurry.Application.Foundation.BehaviourController;
using Blurry.Application.CoreMechanics.MechanicControllers.BehaviourData;
using Blurry.Application.CoreMechanics.MechanicControllers.Presentation;


namespace Blurry.Application.CoreMechanics.MechanicControllers.ControllerBehaviour
{
    public interface IMechanicController_Behaviour : IBehaviourController_Behaviour
    {
    }

    public abstract class AMechanicController_Behaviour<TPresentation, TData, TBehaviours> : ABehaviourController_BehaviourList<TPresentation, TData, TBehaviours>, IMechanicController_Behaviour where TPresentation : IMechanicController_Presentation
                                                                                                                                                                                                  where TData : IMechanicController_BehaviourData
                                                                                                                                                                                                  where TBehaviours : IMechanic_Behaviour

    {
        protected AMechanicController_Behaviour(TPresentation presentation, TData data, List<TBehaviours> behaviours) : base(presentation, data, behaviours)
        {
        }
    }
}