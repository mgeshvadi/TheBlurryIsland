using Blurry.Application.Base.AssetManagement.Addresses;
using Blurry.Application.Base.AssetManagement.Base;
using Blurry.Application.CoreMechanics.MechanicControllers.Config;
using Blurry.Application.Foundation.ConfigFileManagement;
using Blurry.Application.Foundation.ConfigFileManagement.Base;


namespace Blurry.Application.CoreMechanics.MechanicControllers.ConfigManagement
{
    public sealed class MechanicsControllers_ConfigFileLoader : AConfigFileLoader<IMechanicController_ConfigFile>
    {
        public const string CONFIG_FILES_CONTEXT_MENU = Root_ConfigFileLoader.CONFIG_FILES_CONTEXT_MENU_BASE + "Mechanics/";

        protected override AssetFolder GetConfigAssetFolder()
        {
            return AssetFolders.ConfigFiles.mechanicsControllers;
        }
    }
}