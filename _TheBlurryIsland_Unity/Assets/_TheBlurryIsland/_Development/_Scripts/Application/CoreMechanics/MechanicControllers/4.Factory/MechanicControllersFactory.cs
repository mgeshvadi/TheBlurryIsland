using System;
using System.Collections.Generic;
using System.Reflection;
using Blurry.Application.CoreMechanics.MechanicControllers.BehaviourData;
using Blurry.Application.CoreMechanics.MechanicControllers.Config;
using Blurry.Application.CoreMechanics.MechanicControllers.ConfigManagement;
using Blurry.Application.CoreMechanics.MechanicControllers.ControllerBehaviour;
using Blurry.Application.CoreMechanics.MechanicControllers.Entity;
using Blurry.Application.CoreMechanics.MechanicControllers.Presentation;
using Blurry.Application.CoreMechanics.MechanicControllers.PresentationData;
using Blurry.Application.CoreMechanics.Mechanics.Entity;
using Blurry.Application.CoreMechanics.Mechanics.Factory;
using Blurry.Application.Foundation.ConfigFileManagement;
using Blurry.Application.Utility.Extensions;
using Blurry.Application.Utility.StaticMethods;


namespace Blurry.Application.CoreMechanics.MechanicControllers.Factory
{
    public class MechanicControllersFactory : IMechanicControllersFactory
    {
        private readonly struct MechanicControllerInfo
        {
            public Type PresentationType { get; }
            public Type BehaviourDataType { get; }
            public Type PresentationDataType { get; }
            public Type BehaviourType { get; }
            public Type ConfigFileType { get; }

            public MechanicControllerInfo(Type presentationType, Type behaviourDataType, Type presentationDataType, Type behaviourType, Type configFileType)
            {
                PresentationType = presentationType;
                BehaviourDataType = behaviourDataType;
                PresentationDataType = presentationDataType;
                BehaviourType = behaviourType;
                ConfigFileType = configFileType;
            }
        }

        private readonly IMechanicsFactory mechanicsFactory;

        public MechanicControllersFactory(IMechanicsFactory mechanicsFactory)
        {
            this.mechanicsFactory = mechanicsFactory;
        }

        public Dictionary<Type, IMechanicController_Behaviour> CreateMechanicControllersBehavioursForThese(List<IMechanicController_Entity> mechanicControllerEntities)
        {
            Dictionary<Type, IMechanicController_Behaviour> result = new Dictionary<Type, IMechanicController_Behaviour>(mechanicControllerEntities.Count);

            foreach (IMechanicController_Entity controllerEntity in mechanicControllerEntities)
            {
                MechanicControllerInfo controllerInfo = GetMechanicControllerInfoBasedOnIMechanicControllerEntityType(controllerEntity.GetType());
                IMechanicController_Behaviour controllerBehaviour = CreateMechanicControllerBehavioursForThis(controllerInfo, controllerEntity);
                result.Add(controllerInfo.BehaviourType, controllerBehaviour);
            }

            return result;
        }

        private IMechanicController_Behaviour CreateMechanicControllerBehavioursForThis(MechanicControllerInfo controllerInfo, IMechanicController_Entity controllerEntity)
        {
            IMechanicController_Config config = (IMechanicController_Config) ((IMechanicController_ConfigFile) ConfigFileManagement.GetConfigLoader<MechanicsControllers_ConfigFileLoader>().LoadConfigFile(controllerInfo.ConfigFileType)).GetNotCastedConfig();
            IMechanicController_PresentationData presentationData = (IMechanicController_PresentationData) Activator.CreateInstance(controllerInfo.PresentationDataType, controllerEntity, config);

            IMechanicController_Presentation presentation = (IMechanicController_Presentation) Activator.CreateInstance(controllerInfo.PresentationType, presentationData);
            IMechanicController_BehaviourData behaviourData = (IMechanicController_BehaviourData) Activator.CreateInstance(controllerInfo.BehaviourDataType, controllerEntity, config);
            dynamic mechanicBehaviours = mechanicsFactory.CreateMechanicsBehavioursForThese(controllerEntity.GetChildEntities<IMechanic_Entity>().ToList());

            if (mechanicBehaviours[0] == null)
                throw new Exception($"Mechanic controller with no entity found! controller type {controllerInfo.BehaviourType}.");

            dynamic mechanicBehavioursExplicitType = OtherUtilities.CreateDynamicList(mechanicBehaviours[0]);
            foreach (dynamic mechanicBehaviour in mechanicBehaviours)
                mechanicBehavioursExplicitType.Add(mechanicBehaviour);
            IMechanicController_Behaviour behaviour = (IMechanicController_Behaviour) Activator.CreateInstance(controllerInfo.BehaviourType, presentation, behaviourData, mechanicBehavioursExplicitType);

            return behaviour;
        }

        private MechanicControllerInfo GetMechanicControllerInfoBasedOnIMechanicControllerEntityType(Type controllerEntity) // TODO: Check this methode from performance point of view
        {
            var allTypes = Assembly.GetAssembly(controllerEntity).GetTypes().ToList();

            Type configFileType = null, behaviourDataType = null, presentationDataType = null, presentationType = null, behaviourType = null;
            foreach (Type type in allTypes)
            {
                if (!type.IsAEndImplementation() || !type.CompareReflectionCreationBundleAttributeTo(controllerEntity))
                    continue;

                if (type.IsImplementationOf(typeof(IMechanicController_ConfigFile)))
                    configFileType = type;
                else if (type.IsImplementationOf(typeof(IMechanicController_BehaviourData)))
                    behaviourDataType = type;
                else if (type.IsImplementationOf(typeof(IMechanicController_PresentationData)))
                    presentationDataType = type;
                else if (type.IsImplementationOf(typeof(IMechanicController_Presentation)))
                    presentationType = type;
                else if (type.IsImplementationOf(typeof(IMechanicController_Behaviour)))
                    behaviourType = type;
            }

            return new MechanicControllerInfo(presentationType, behaviourDataType, presentationDataType, behaviourType, configFileType);
        }
    }
}