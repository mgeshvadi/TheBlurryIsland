using Blurry.Application.CoreMechanics.MechanicControllers.Config;
using Blurry.Application.CoreMechanics.MechanicControllers.Entity;
using Blurry.Application.Foundation.PresentationData;


namespace Blurry.Application.CoreMechanics.MechanicControllers.PresentationData
{
    public interface IMechanicController_PresentationData : IPresentation_Data
    {
    }

    public abstract class AMechanicController_PresentationData<TEntity, TConfig> : APresentation_Data<TEntity, TConfig>, IMechanicController_PresentationData where TEntity : IMechanicController_Entity
                                                                                                                                                              where TConfig : IMechanicController_Config
    {
        protected AMechanicController_PresentationData(TEntity entity, TConfig config) : base(entity, config)
        {
        }
    }
}