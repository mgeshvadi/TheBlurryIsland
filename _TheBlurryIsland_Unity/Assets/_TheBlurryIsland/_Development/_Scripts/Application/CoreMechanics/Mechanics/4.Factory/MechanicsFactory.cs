using System;
using System.Collections.Generic;
using System.Reflection;
using Blurry.Application.CoreMechanics.Mechanics.Behaviour;
using Blurry.Application.CoreMechanics.Mechanics.BehaviourData;
using Blurry.Application.CoreMechanics.Mechanics.Config;
using Blurry.Application.CoreMechanics.Mechanics.ConfigManagement;
using Blurry.Application.CoreMechanics.Mechanics.Entity;
using Blurry.Application.CoreMechanics.Mechanics.Presentation;
using Blurry.Application.CoreMechanics.Mechanics.PresentationData;
using Blurry.Application.Foundation.ConfigFileManagement;
using Blurry.Application.Utility.Extensions;



namespace Blurry.Application.CoreMechanics.Mechanics.Factory
{
    public class MechanicsFactory : IMechanicsFactory
    {
        private readonly struct MechanicInfo
        {
            public Type PresentationType { get; }
            public Type BehaviourDataType { get; }
            public Type PresentationDataType { get; }
            public Type BehaviourType { get; }
            public Type ConfigFileType { get; }

            public MechanicInfo(Type presentationType, Type behaviourDataType, Type presentationDataType, Type behaviourType, Type configFileType)
            {
                PresentationType = presentationType;
                BehaviourDataType = behaviourDataType;
                PresentationDataType = presentationDataType;
                BehaviourType = behaviourType;
                ConfigFileType = configFileType;
            }
        }

        public List<IMechanic_Behaviour> CreateMechanicsBehavioursForThese(List<IMechanic_Entity> mechanicsEntities)
        {
            List<IMechanic_Behaviour> result = new List<IMechanic_Behaviour>(mechanicsEntities.Count);

            foreach (IMechanic_Entity mechanicsEntity in mechanicsEntities)
            {
                IMechanic_Behaviour mechanicBehaviour = CreateMechanicBehavioursForThis(mechanicsEntity);
                result.Add(mechanicBehaviour);
            }

            return result;
        }

        private IMechanic_Behaviour CreateMechanicBehavioursForThis(IMechanic_Entity mechanicEntity)
        {
            MechanicInfo mechanicInfo = GetMechanicInfoBasedOnIMechanicEntityType(mechanicEntity.GetType());

            IMechanic_Config config = (IMechanic_Config) ((IMechanic_ConfigFile) ConfigFileManagement.GetConfigLoader<Mechanics_ConfigFileLoader>().LoadConfigFile(mechanicInfo.ConfigFileType)).GetNotCastedConfig();
            IMechanic_PresentationData presentationData = (IMechanic_PresentationData) Activator.CreateInstance(mechanicInfo.PresentationDataType, mechanicEntity, config);

            IMechanic_Presentation presentation = (IMechanic_Presentation) Activator.CreateInstance(mechanicInfo.PresentationType, presentationData);
            IMechanic_BehaviourData behaviourData = (IMechanic_BehaviourData) Activator.CreateInstance(mechanicInfo.BehaviourDataType, mechanicEntity, config);

            IMechanic_Behaviour behaviour = (IMechanic_Behaviour) Activator.CreateInstance(mechanicInfo.BehaviourType, presentation, behaviourData);

            return behaviour;
        }

        private MechanicInfo GetMechanicInfoBasedOnIMechanicEntityType(Type mechanicEntityType) // TODO: Check this methode from performance point of view
        {
            var allTypes = Assembly.GetAssembly(mechanicEntityType).GetTypes().ToList();

            Type configFileType = null, behaviourDataType = null, presentationDataType = null, presentationType = null, behaviourType = null;
            foreach (Type type in allTypes)
            {
                if (!type.IsAEndImplementation() || !type.CompareReflectionCreationBundleAttributeTo(mechanicEntityType))
                    continue;

                if (type.IsImplementationOf(typeof(IMechanic_ConfigFile)))
                    configFileType = type;
                else if (type.IsImplementationOf(typeof(IMechanic_BehaviourData)))
                    behaviourDataType = type;
                else if (type.IsImplementationOf(typeof(IMechanic_PresentationData)))
                    presentationDataType = type;
                else if (type.IsImplementationOf(typeof(IMechanic_Presentation)))
                    presentationType = type;
                else if (type.IsImplementationOf(typeof(IMechanic_Behaviour)))
                    behaviourType = type;
            }

            return new MechanicInfo(presentationType, behaviourDataType, presentationDataType, behaviourType, configFileType);
        }
    }
}