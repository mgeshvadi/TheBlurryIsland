using Blurry.Application.CoreMechanics.Mechanics.BehaviourData;
using Blurry.Application.CoreMechanics.Mechanics.Presentation;
using Blurry.Application.Foundation.Behaviour;


namespace Blurry.Application.CoreMechanics.Mechanics.Behaviour
{
    public interface IMechanic_Behaviour : IBehaviour
    {
    }

    public abstract class AMechanic_Behaviour<TPresentation, TData> : ABehaviour<TPresentation, TData>, IMechanic_Behaviour where TPresentation : IMechanic_Presentation
                                                                                                                            where TData : IMechanic_BehaviourData
    {
        protected AMechanic_Behaviour(TPresentation presentation, TData data) : base(presentation, data)
        {
        }
    }
}