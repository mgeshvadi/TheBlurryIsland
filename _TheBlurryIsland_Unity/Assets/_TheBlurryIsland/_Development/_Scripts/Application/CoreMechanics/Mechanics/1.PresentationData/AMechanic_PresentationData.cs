using Blurry.Application.CoreMechanics.Mechanics.Config;
using Blurry.Application.CoreMechanics.Mechanics.Entity;
using Blurry.Application.Foundation.PresentationData;


namespace Blurry.Application.CoreMechanics.Mechanics.PresentationData
{
    public interface IMechanic_PresentationData : IPresentation_Data
    {
    }

    public abstract class AMechanic_PresentationData<TEntity, TConfig> : APresentation_Data<TEntity, TConfig>, IMechanic_PresentationData where TEntity : IMechanic_Entity
                                                                                                                                          where TConfig : IMechanic_Config
    {
        protected AMechanic_PresentationData(TEntity entity, TConfig config) : base(entity, config)
        {
        }
    }
}