using Blurry.Application.CoreMechanics.Mechanics.Config;
using Blurry.Application.CoreMechanics.Mechanics.Entity;
using Blurry.Application.Foundation.BehaviourData;


namespace Blurry.Application.CoreMechanics.Mechanics.BehaviourData
{
    public interface IMechanic_BehaviourData : IBehaviour_Data
    {
    }

    public abstract class AMechanic_BehaviourData<TEntity, TConfig> : ABehaviour_Data<TEntity, TConfig>, IMechanic_BehaviourData where TEntity : IMechanic_Entity
                                                                                                                                 where TConfig : IMechanic_Config
    {
        protected AMechanic_BehaviourData(TEntity entity, TConfig config) : base(entity, config)
        {
        }
    }
}