using Blurry.Application.Foundation.Entity;


namespace Blurry.Application.CoreMechanics.Mechanics.Entity
{
    public interface IMechanic_Entity : IEntity
    {

    }

    public abstract class AMechanic_Entity : AEntity, IMechanic_Entity
    {
    }
}