using Blurry.Application.Base.AssetManagement.Addresses;
using Blurry.Application.Base.AssetManagement.Base;
using Blurry.Application.Foundation.ConfigFileManagement;
using Blurry.Application.Foundation.ConfigFileManagement.Base;
using Blurry.Application.CoreMechanics.Mechanics.Config;


namespace Blurry.Application.CoreMechanics.Mechanics.ConfigManagement
{
    public sealed class Mechanics_ConfigFileLoader : AConfigFileLoader<IMechanic_ConfigFile>
    {
        public const string CONFIG_FILES_CONTEXT_MENU = Root_ConfigFileLoader.CONFIG_FILES_CONTEXT_MENU_BASE + "Mechanics/";

        protected override AssetFolder GetConfigAssetFolder()
        {
            return AssetFolders.ConfigFiles.mechanics;
        }
    }
}