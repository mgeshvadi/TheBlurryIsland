using System.Collections.Generic;
using Blurry.Application.CoreMechanics.Mechanics.Behaviour;
using Blurry.Application.CoreMechanics.Mechanics.Entity;


namespace Blurry.Application.CoreMechanics.Mechanics.Factory
{
    public interface IMechanicsFactory
    {
        public List<IMechanic_Behaviour> CreateMechanicsBehavioursForThese(List<IMechanic_Entity> mechanicsEntities);
    }
}