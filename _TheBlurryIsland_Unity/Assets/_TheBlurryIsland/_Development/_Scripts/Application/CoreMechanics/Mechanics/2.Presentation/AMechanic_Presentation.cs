using Blurry.Application.CoreMechanics.Mechanics.PresentationData;
using Blurry.Application.Foundation.Presentation;


namespace Blurry.Application.CoreMechanics.Mechanics.Presentation
{
    public interface IMechanic_Presentation : IPresentation
    {
    }

    public abstract class AMechanic_Presentation<TData> : APresentation<TData>, IMechanic_Presentation where TData : IMechanic_PresentationData
    {
        protected AMechanic_Presentation(TData data) : base(data)
        {
        }
    }
}