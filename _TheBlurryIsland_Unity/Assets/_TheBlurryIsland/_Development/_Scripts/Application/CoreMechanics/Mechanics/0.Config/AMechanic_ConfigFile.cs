using Blurry.Application.Foundation.Config;


namespace Blurry.Application.CoreMechanics.Mechanics.Config
{
    public interface IMechanic_Config : IConfig
    {
    }

    public interface IMechanic_ConfigFile : IConfig_File
    {

    }

    public abstract class AMechanic_ConfigFile<TConfig> : AConfig_File<TConfig>, IMechanic_ConfigFile where TConfig : IMechanic_Config
    {
    }
}