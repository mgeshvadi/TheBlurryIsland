using UnityEngine;


namespace Blurry.Application.Utility.StaticMethods
{
    public static class RandomnessUtilities
    {
        public static float AddRandomness(float value, float normalizedPercent)
        {
            float amplitude = normalizedPercent * value;
            return Random.Range(value - amplitude, value + amplitude);
        }
    }
}