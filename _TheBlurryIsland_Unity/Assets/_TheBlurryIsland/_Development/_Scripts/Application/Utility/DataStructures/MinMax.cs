using System;
using UnityEngine;
using Random = UnityEngine.Random;


namespace Blurry.Application.Utility.DataStructures
{
    // TODO: Maybe a generic min max?
    [Serializable]
    public struct IntMinMax
    {
        public int min;
        public int max;

        public int RandomAmount => Random.Range(min, max);

        public IntMinMax(int min, int max)
        {
            this.min = min;
            this.max = max;
        }
    }

    [Serializable]
    public struct FloatMinMax
    {
        public float min;
        public float max;

        public float RandomAmount => Random.Range(min, max);

        public FloatMinMax(float min, float max)
        {
            this.min = min;
            this.max = max;
        }
    }
}