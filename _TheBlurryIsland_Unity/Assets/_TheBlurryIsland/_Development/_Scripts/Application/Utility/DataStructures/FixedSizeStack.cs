using System;
using System.Collections;
using System.Collections.Generic;
using Blurry.Application.Utility.Extensions;


namespace Blurry.Application.Utility.DataStructures
{
    public class FixedSizeList<T> : IEnumerable<T>
    {
        private readonly int size;
        private List<T> elements;

        public FixedSizeList(int size)
        {
            this.size = size;
            elements = new List<T>(this.size);
        }

        public int Count => elements.Count;
        public int Capacity => elements.Capacity;

        public void PushToLast(T toPush, Action<T> onElementDrop = default)
        {
            if (elements.Count == size)
            {
                onElementDrop?.Invoke(First());
                elements.RemoveFirst();
            }
            elements.AddLast(toPush);
        }

        public void PushToFirst(T toPush, Action<T> onElementDrop = default)
        {
            if (elements.Count == size)
            {
                onElementDrop?.Invoke(Last());
                elements.RemoveLast();
            }
            elements.AddFirst(toPush);
        }

        public T First()
        {
            return elements.First();
        }

        public T Last()
        {
            return elements.Last();
        }

        public void RemoveFirst()
        {
            elements.RemoveFirst();
        }

        public void RemoveLast()
        {
            elements.RemoveLast();
        }

        public T this[int index]
        {
            get => elements[index];
        }

        public IEnumerator<T> GetEnumerator()
        {
            return elements.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Clear()
        {
            elements.Clear();
        }
    }
}