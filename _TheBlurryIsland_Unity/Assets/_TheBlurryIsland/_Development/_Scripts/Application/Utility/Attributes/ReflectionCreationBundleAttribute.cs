using System;


namespace Blurry.Application.Utility
{
    [AttributeUsage(validOn: AttributeTargets.Class)]
    public class ReflectionCreationBundleAttribute : Attribute
    {
    }
}