using System;


namespace Blurry.Application.Utility.Extensions
{
    public static class Array_Extensions
    {
        public static T First<T>(this T[] self)
        {
            return self[0];
        }

        public static T Last<T>(this T[] self)
        {
            return self[self.Length - 1];
        }

        public static TResult[] Select<TSource, TResult>(this TSource[] self, Func<TSource, TResult> selector)
        {
            TResult[] result = new TResult[self.Length];
            for (int i = 0; i < result.Length; i++)
                result[i] = selector.Invoke(self[i]);
            return result;
        }

        public static TResult[] ConvertTo<TSource, TResult>(this TSource[] self) where TResult : TSource
        {
            if (self == null)
                return (TResult[]) null;
            TResult[] result = new TResult[self.Length];
            for (int i = 0; i < result.Length; i++)
                result[i] = (TResult) self[i];
            return result;
        }
    }
}