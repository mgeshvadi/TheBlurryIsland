using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using Random = UnityEngine.Random;


namespace Blurry.Application.Utility.Extensions
{
    public static class List_Extensions
    {
        public static void RemoveFirst<T>(this List<T> self)
        {
            self.RemoveAt(0);
        }

        public static void RemoveLast<T>(this List<T> self)
        {
            self.RemoveAt(self.Count - 1);
        }

        public static void AddFirst<T>(this List<T> self, T toAdd)
        {
            self.Insert(0, toAdd);
        }

        public static void AddLast<T>(this List<T> self, T toAdd)
        {
            self.Add(toAdd);
        }

        public static T First<T>(this List<T> self)
        {
            return self[0];
        }

        public static T Last<T>(this List<T> self)
        {
            return self[self.Count - 1];
        }

        public static List<TResult> Select<TSource, TResult>(this List<TSource> self, Func<TSource, TResult> selector)
        {
            List<TResult> result = new List<TResult>(self.Count);
            foreach (TSource element in self)
                result.Add(selector.Invoke(element));
            return result;
        }

        public static List<TResult> ConvertTo<TSource, TResult>(this List<TSource> self) where TResult : TSource
        {
            if (self == null)
                return (List<TResult>) null;
            List<TResult> result = new List<TResult>(self.Count);
            foreach (var obj in self)
                result.Add((TResult) obj);
            return result;
        }

        public static void Shuffle<T>(this IList<T> list)
        {
            RNGCryptoServiceProvider provider = new RNGCryptoServiceProvider();
            int n = list.Count;
            while (n > 1)
            {
                byte[] box = new byte[1];
                do provider.GetBytes(box);
                while (!(box[0] < n * (Byte.MaxValue / n)));
                int k = (box[0] % n);
                n--;
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        public static T RandomElement<T>(this List<T> list) where T : class
        {
            return list.Count == 0 ? null : list[Random.Range(0, list.Count)];
        }

        public static List<T> RandomElements<T>(this List<T> list, int targetElementsCount) where T : class
        {
            list.Shuffle();
            return list.GetRange(0, targetElementsCount);
        }
    }
}