using UnityEngine;


namespace Blurry.Application.Utility.Extensions
{
    public static class Float_Extensions
    {
        public static float RemapTo(this float value, Vector2 oldRange, Vector2 newRange, bool inverseRemap = false)
        {
            value = Mathf.Clamp(value, oldRange.x, oldRange.y);
            return (inverseRemap ? newRange.y : newRange.x) + (inverseRemap ? -1 : 1) * (value - oldRange.x) * (newRange.y - newRange.x) / (oldRange.y - oldRange.x);
        }
    }
}