using System;
using System.Collections.Generic;


namespace Blurry.Application.Utility.Extensions
{
    public static class IEnumerable_Extensions
    {
        public static List<T> ToList<T>(this IEnumerable<T> self)
        {
            return new List<T>(self);
        }

        public static bool Any<T>(this IEnumerable<T> self, Func<T, bool> predicate)
        {
            foreach (T element in self)
                if (predicate(element))
                    return true;
            return false;
        }

        public static bool ContainsAll<T>(this IEnumerable<T> self, IEnumerable<T> toContain)
        {
            return new HashSet<T>(toContain).IsSubsetOf(self);
        }


    }
}