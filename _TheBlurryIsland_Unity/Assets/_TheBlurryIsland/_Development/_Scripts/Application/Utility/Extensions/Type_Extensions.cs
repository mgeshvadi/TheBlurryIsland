using System;
using System.Reflection;


namespace Blurry.Application.Utility.Extensions
{

    public static class Type_Extensions // TODO: who ever reading this note, you are responsible to refactor this shit (although I'm not sure if this is shit or not, so think about it beforehand)
    {
        public static bool IsEndImplementationOfInterfaceWithThisGerenicTypes(this Type sourceType, Type targetInterfaceType, params Type[] targetGenericTypes)
        {
            return sourceType.IsAEndImplementation()
                && sourceType.GetInterfaces().Any(i =>
                                                      i.IsGenericType
                                                   && i.GetGenericTypeDefinition() == targetInterfaceType
                                                   && i.GetGenericArguments().ContainsAll(targetGenericTypes));

        }

        public static bool IsEndImplementationOfInterfaceWithThisAttribute(this Type sourceType, Type targetInterfaceType, params Attribute[] targetAttributes)
        {
            return sourceType.IsImplementationOf(targetInterfaceType)
                && sourceType.GetCustomAttributes(inherit: true).ContainsAll(targetAttributes);
        }

        public static bool IsEndImplementationOfInterfaceWithThisParametersInConstructors(this Type sourceType, Type targetInterfaceType, params Type[] targetConstructorParameters) // TODO: Check this methode from performance point of view
        {
            return sourceType.IsImplementationOf(targetInterfaceType)
                && sourceType.GetConstructors().Any(constructor => constructor.GetParameters().Select(parameter => parameter.ParameterType).ContainsAll(targetConstructorParameters));
        }

        public static bool IsImplementationOf(this Type sourceType, Type targetInterfaceType)
        {
            return sourceType.IsAEndImplementation() && sourceType.GetInterfaces().Any(i => i == targetInterfaceType);
        }

        public static bool IsAEndImplementation(this Type type)
        {
            return type.IsAbstract == false && type.IsInterface == false;
        }

        public static bool CompareReflectionCreationBundleAttributeTo(this Type sourceType, Type toCompareWithType)
        {
            ReflectionCreationBundleAttribute sourceAttribute = sourceType.GetCustomAttribute<ReflectionCreationBundleAttribute>();
            ReflectionCreationBundleAttribute targetAttribute = toCompareWithType.GetCustomAttribute<ReflectionCreationBundleAttribute>();

            if (sourceAttribute != null && targetAttribute != null)
                return sourceAttribute.GetType() == targetAttribute.GetType();

            return false;
        }
    }
}