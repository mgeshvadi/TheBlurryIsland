using System;
using UnityEngine;
using System.Collections.Generic;
using Blurry.Application.ServiceLow.EventManagement.Base;


namespace Blurry.Application.ServiceLow.EventManagement
{
    public sealed class EventManager_Service : IEventManager_Service
    {
        private readonly Dictionary<Type, Dictionary<Type, IEventListener>> eventListenersToEventsMapping = new Dictionary<Type, Dictionary<Type, IEventListener>>();

        public void RegisterListenerForEvent<TEvent>(IEventListener listener) where TEvent : IEvent
        {
            GetEventReadyForListeningIfNeeded<TEvent>();
            if (!IsListenerRegisteredForEvent<TEvent>(listener))
                GetListenersOfEvent<TEvent>().Add(listener.GetType(), listener);
            else
                LogErrorFor(eventListenerType: listener.GetType(), eventType: typeof(TEvent), message: "Event Listener already registered For this event");
        }

        public void UnRegisterListenerCompletely(IEventListener listener)
        {
            foreach (Dictionary<Type, IEventListener> listeners in eventListenersToEventsMapping.Values)
                listeners.Remove(listener.GetType());
        }

        public void UnRegisterListenerOfEvent<TEvent>(IEventListener listener) where TEvent : IEvent
        {
            if (IsListenerRegisteredForEvent<TEvent>(listener))
            {
                GetListenersOfEvent<TEvent>().Remove(listener.GetType());
                RemoveEventListeningIfNeeded<TEvent>();
            }
            else
                LogErrorFor(eventListenerType: listener.GetType(), eventType: typeof(TEvent), message: "Event Listener already unregistered or never registered");
        }

        public bool IsListenerRegisteredForEvent<TEvent>(IEventListener listener) where TEvent : IEvent
        {
            return IsAnyListenerRegisteredForEvent<TEvent>() && GetListenersOfEvent<TEvent>().ContainsKey(listener.GetType());
        }

        public void PropagateEvent<TEvent>(object sender) where TEvent : IEvent
        {
            if (!IsAnyListenerRegisteredForEvent<TEvent>())
                return;
            Dictionary<Type, IEventListener>.ValueCollection listeners = GetListenersOfEvent<TEvent>().Values;
            foreach (IEventListener listener in listeners)
                listener.HandleEventOccurrence<TEvent>(sender);
        }

        private void GetEventReadyForListeningIfNeeded<TEvent>() where TEvent : IEvent
        {
            if (!IsAnyListenerRegisteredForEvent<TEvent>())
                eventListenersToEventsMapping.Add(typeof(TEvent), new Dictionary<Type, IEventListener>());
        }

        private void RemoveEventListeningIfNeeded<TEvent>() where TEvent : IEvent
        {
            if (GetListenersOfEvent<TEvent>().Count == 0)
                eventListenersToEventsMapping.Remove(typeof(TEvent));
        }

        private bool IsAnyListenerRegisteredForEvent<TEvent>() where TEvent : IEvent
        {
            return eventListenersToEventsMapping.ContainsKey(typeof(TEvent)) && GetListenersOfEvent<TEvent>().Count != 0;
        }

        private Dictionary<Type, IEventListener> GetListenersOfEvent<TEvent>() where TEvent : IEvent
        {
            return eventListenersToEventsMapping[typeof(TEvent)];
        }

        private void LogErrorFor(Type eventListenerType, Type eventType, string message)
        {
            // todo : use our logging system
            Debug.LogError($"{message}. {nameof(IEventListener)} type: {eventListenerType}. this {nameof(IEvent)} type: {eventType}");
        }
    }
}