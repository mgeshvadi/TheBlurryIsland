using Blurry.Application.ServiceLow.EventManagement.Base;


namespace Blurry.Application.ServiceLow.EventManagement
{
    public sealed class EventManagementFactory
    {
        public IEventManager_Service CreateEventManagerService()
        {
            return new EventManager_Service();
        }
    }
}