using Blurry.Application.Base.ServiceManagement.Base;


namespace Blurry.Application.ServiceLow.EventManagement.Base
{
    public interface IEventManager_Service : IMicroService
    {
        public void RegisterListenerForEvent<TEvent>(IEventListener listener) where TEvent : IEvent;

        public void UnRegisterListenerCompletely(IEventListener listener);

        public void UnRegisterListenerOfEvent<TEvent>(IEventListener listener) where TEvent : IEvent;

        public bool IsListenerRegisteredForEvent<TEvent>(IEventListener listener) where TEvent : IEvent;

        public void PropagateEvent<TEvent>(object sender) where TEvent : IEvent;
    }
}