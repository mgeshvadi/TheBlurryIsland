using Blurry.Application.Base.ServiceManagement;


namespace Blurry.Application.ServiceLow.EventManagement.Base
{
    public abstract class AEventListener : IEventListener
    {
        ~AEventListener() // TODO : Check if its working
        {
            ServiceManagement.ActiveServiceLocator.Find<IEventManager_Service>().UnRegisterListenerCompletely(this);
        }

        public abstract void HandleEventOccurrence<TEvent>(object sender) where TEvent : IEvent;
    }
}