namespace Blurry.Application.ServiceLow.EventManagement.Base
{
    public interface IEventListener
    {
        void HandleEventOccurrence<TEvent>(object sender) where TEvent : IEvent;
    }
}