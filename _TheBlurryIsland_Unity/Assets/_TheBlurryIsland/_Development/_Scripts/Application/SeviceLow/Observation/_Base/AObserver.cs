using Blurry.Application.Base.ServiceManagement;


namespace Blurry.Application.ServiceLow.Observation.Base
{
    public abstract class AObserver : IObserver
    {
        ~AObserver() // TODO : Check if its working
        {
            ServiceManagement.ActiveServiceLocator.Find<IObservationManager_Service>().UnRegisterObserverCompletely(this);
        }

        public abstract void HandleSubjectOccurrence(ISubject subject, object subjectOwner);
    }
}