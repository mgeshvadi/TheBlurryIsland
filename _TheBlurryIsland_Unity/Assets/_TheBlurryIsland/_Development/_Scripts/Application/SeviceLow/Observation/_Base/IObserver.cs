namespace Blurry.Application.ServiceLow.Observation.Base
{
    public interface IObserver
    {
        void HandleSubjectOccurrence(ISubject subject, object subjectOwner);
    }
}