using Blurry.Application.ServiceLow.Observation.Base;


namespace Blurry.Application.ServiceLow.Observation
{
    public sealed class ObservationFactory
    {
        public IObservationManager_Service CreateObservationManagerService()
        {
            return new ObservationManager_Service();
        }
    }
}