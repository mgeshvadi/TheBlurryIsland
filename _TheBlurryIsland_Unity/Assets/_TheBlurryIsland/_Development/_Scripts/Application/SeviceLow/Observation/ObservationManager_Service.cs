using System;
using System.Collections.Generic;
using UnityEngine;
using Blurry.Application.ServiceLow.Observation.Base;


namespace Blurry.Application.ServiceLow.Observation
{
    public sealed class ObservationManager_Service : IObservationManager_Service
    {
        private readonly Dictionary<Type, Dictionary<Type, IObserver>> observersToSubjectsMapping = new Dictionary<Type, Dictionary<Type, IObserver>>();

        public void RegisterObserverForSubject<TSubject>(IObserver observer) where TSubject : ISubject
        {
            GetSubjectReadyForObservationIfNeeded<TSubject>();
            if (!IsObserverRegisteredForSubject<TSubject>(observer))
                GetObserversForSubject<TSubject>().Add(observer.GetType(), observer);
            else
                LogErrorFor(observerType: observer.GetType(), subjectType: typeof(TSubject), message: "Observer already registered For this subject");
        }

        public void UnRegisterObserverCompletely(IObserver observer)
        {
            foreach (Dictionary<Type, IObserver> observers in observersToSubjectsMapping.Values)
                observers.Remove(observer.GetType());
        }

        public void UnRegisterObserverForSubject<TSubject>(IObserver observer) where TSubject : ISubject
        {
            if (IsObserverRegisteredForSubject<TSubject>(observer))
            {
                GetObserversForSubject<TSubject>().Remove(observer.GetType());
                RemoveSubjectObservingIfNeeded<TSubject>();
            }
            else
                LogErrorFor(observerType: observer.GetType(), subjectType: typeof(TSubject), message: "Observer already unregistered or never registered");
        }

        public bool IsObserverRegisteredForSubject<TSubject>(IObserver observer) where TSubject : ISubject
        {
            return IsAnyObserverRegisteredForSubject<TSubject>() && GetObserversForSubject<TSubject>().ContainsKey(observer.GetType());
        }

        public void NotifyObserversAboutSubjectHappening<TSubject>(TSubject subject, object subjectOwner) where TSubject : ISubject
        {
            if (!IsAnyObserverRegisteredForSubject<TSubject>())
                return;
            Dictionary<Type, IObserver>.ValueCollection observers = GetObserversForSubject<TSubject>().Values;
            foreach (IObserver observer in observers)
                observer.HandleSubjectOccurrence(subject, subjectOwner);
        }

        private void GetSubjectReadyForObservationIfNeeded<TSubject>() where TSubject : ISubject
        {
            if (!IsAnyObserverRegisteredForSubject<TSubject>())
                observersToSubjectsMapping.Add(typeof(TSubject), new Dictionary<Type, IObserver>());
        }

        private void RemoveSubjectObservingIfNeeded<TSubject>() where TSubject : ISubject
        {
            if (GetObserversForSubject<TSubject>().Count == 0)
                observersToSubjectsMapping.Remove(typeof(TSubject));
        }

        private bool IsAnyObserverRegisteredForSubject<TSubject>() where TSubject : ISubject
        {
            return observersToSubjectsMapping.ContainsKey(typeof(TSubject)) && GetObserversForSubject<TSubject>().Count != 0;
        }

        private Dictionary<Type, IObserver> GetObserversForSubject<TSubject>() where TSubject : ISubject
        {
            return observersToSubjectsMapping[typeof(TSubject)];
        }

        private void LogErrorFor(Type observerType, Type subjectType, string message)
        {
            // todo : use our logging system
            Debug.LogError($"{message}. {nameof(IObserver)} type: {observerType}. this {nameof(ISubject)} type: {subjectType}");
        }
    }
}