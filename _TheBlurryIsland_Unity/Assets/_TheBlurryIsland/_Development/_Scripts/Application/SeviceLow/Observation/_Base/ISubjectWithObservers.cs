using System.Collections.Generic;


namespace Blurry.Application.ServiceLow.Observation.Base
{
    public interface ISubjectWithObservers : ISubject
    {
        public void AddObservers(IObserver observer);
        public void RemoveObservers(IObserver observer);
    }

    public abstract class ASubjectWithObservers : ISubjectWithObservers
    {
        protected readonly HashSet<IObserver> observers = new HashSet<IObserver>();

        public void AddObservers(IObserver observer)
        {
            observers.Add(observer);
        }

        public void RemoveObservers(IObserver observer)
        {
            observers.Remove(observer);
        }

        protected void NotifyObservers()
        {
            foreach (IObserver observer in observers)
                observer.HandleSubjectOccurrence(subject: this, subjectOwner: this);
        }
    }
}