using Blurry.Application.Base.ServiceManagement.Base;


namespace Blurry.Application.ServiceLow.Observation.Base
{
    public interface IObservationManager_Service : IMicroService
    {
        public void RegisterObserverForSubject<TSubject>(IObserver observer) where TSubject : ISubject;

        public void UnRegisterObserverCompletely(IObserver observer);

        public void UnRegisterObserverForSubject<TSubject>(IObserver observer) where TSubject : ISubject;

        public bool IsObserverRegisteredForSubject<TSubject>(IObserver observer) where TSubject : ISubject;

        public void NotifyObserversAboutSubjectHappening<TSubject>(TSubject subject, object subjectOwner) where TSubject : ISubject;
    }
}