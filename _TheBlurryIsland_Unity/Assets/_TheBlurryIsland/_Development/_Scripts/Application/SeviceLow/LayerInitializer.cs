using Blurry.Application.Base.ServiceManagement;
using Blurry.Application.ServiceLow.EventManagement;
using Blurry.Application.ServiceLow.EventManagement.Base;
using Blurry.Application.ServiceLow.Observation;
using Blurry.Application.ServiceLow.Observation.Base;


namespace Blurry.Application.ServiceLow
{
    public readonly struct LayerInitializationsResult
    {
    }

    public static class LayerInitializer
    {
        public static LayerInitializationsResult MockInitialize()
        {
            return Initialize();
        }

        public static LayerInitializationsResult Initialize()
        {
            ServiceManagement.ActiveServiceLocator.Register<IEventManager_Service>(new EventManagementFactory().CreateEventManagerService());
            ServiceManagement.ActiveServiceLocator.Register<IObservationManager_Service>(new ObservationFactory().CreateObservationManagerService());

            return new LayerInitializationsResult();
        }
    }
}