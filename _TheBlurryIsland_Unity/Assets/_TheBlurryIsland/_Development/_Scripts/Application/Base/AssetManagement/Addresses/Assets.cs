using Blurry.Application.Base.AssetManagement.Base;


namespace Blurry.Application.Base.AssetManagement.Addresses
{
    public static class Assets
    {
        public static class Textures
        {
            public static Asset sample = new Asset(path: "Sample/SampleTexture");
        }
    }
}