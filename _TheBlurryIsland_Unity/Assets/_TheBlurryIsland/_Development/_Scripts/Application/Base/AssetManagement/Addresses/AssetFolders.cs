using Blurry.Application.Base.AssetManagement.Base;


namespace Blurry.Application.Base.AssetManagement.Addresses
{
    public static class AssetFolders
    {
        public static class Textures
        {
            public static AssetFolder sample = new AssetFolder(path: "Sample/");
        }

        public static class ConfigFiles
        {
            public static AssetFolder root = new AssetFolder(path: "ConfigFiles/");

            public static AssetFolder characterBehaviours = new AssetFolder(path: root + "CharacterBehaviours/");
            public static AssetFolder mechanics = new AssetFolder(path: root.Path + "CoreMechanics/");
            public static AssetFolder mechanicsControllers = new AssetFolder(path: root.Path + "CoreMechanics/");
            public static AssetFolder levels = new AssetFolder(path: root.Path + "Levels/");
            public static AssetFolder levelManagement = new AssetFolder(path: root.Path + "LevelManagement/");
        }

        public static class Levels
        {
            public static AssetFolder root = new AssetFolder(path: "Levels/");
        }
    }
}