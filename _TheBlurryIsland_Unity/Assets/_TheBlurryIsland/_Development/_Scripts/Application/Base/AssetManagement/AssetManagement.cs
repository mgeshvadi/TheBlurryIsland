using Blurry.Application.Base.AssetManagement.Base;


namespace Blurry.Application.Base.AssetManagement
{
    public static class AssetManagement
    {
        public static IAssetLoader ActiveAssetLoader { private set; get; }
        public static IAsyncAssetLoader ActiveAsyncAssetLoader { private set; get; }
        public static bool IsInitialized { private set; get; }

        public static void Initialize(IAssetLoader assetLoader, IAsyncAssetLoader asyncAssetLoader)
        {
            if (IsInitialized)
                return;
            SetActiveAssetLoaderTo(assetLoader, asyncAssetLoader);
            IsInitialized = true;
        }

        public static void SetActiveAssetLoaderTo(IAssetLoader assseLoader, IAsyncAssetLoader asyncAssetLoader)
        {
            ActiveAssetLoader = assseLoader;
            ActiveAsyncAssetLoader = asyncAssetLoader;
        }
    }
}