namespace Blurry.Application.Base.AssetManagement.Base
{
    public readonly struct Asset
    {
        public string Path { get; }

        public Asset(string path)
        {
            Path = path;
        }
    }
}