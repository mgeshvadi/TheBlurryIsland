namespace Blurry.Application.Base.AssetManagement.Base
{
    public interface IAssetManagementFactory
    {
        public IAssetLoader CreateActiveAssetLoader();
        public IAsyncAssetLoader CreateActiveAsyncAssetLoader();
    }
}