using Object = UnityEngine.Object;


namespace Blurry.Application.Base.AssetManagement.Base
{
    public interface IAsyncAssetLoader
    {
        public IAssetRequest<T> Load<T>(Asset asset) where T : Object;

        public IAssetRequest Load(Asset asset);

        public IAssetRequest<T>[] LoadAll<T>(AssetFolder assetFolder) where T : Object;

        public IAssetRequest[] LoadAll(AssetFolder assetFolder);
    }
}