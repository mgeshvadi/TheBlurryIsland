using Blurry.Application.Base.AssetManagement.Base;
using UnityEngine;


namespace Blurry.Application.Base.AssetManagement.Mock
{
    public sealed class Mock_AsyncAssetLoader : IAsyncAssetLoader
    {
        public IAssetRequest<T> Load<T>(Asset asset) where T : Object
        {
            return null;
        }

        public IAssetRequest Load(Asset asset)
        {
            return null;
        }

        public IAssetRequest<T>[] LoadAll<T>(AssetFolder assetFolder) where T : Object
        {
            return new IAssetRequest<T>[0];
        }

        public IAssetRequest[] LoadAll(AssetFolder assetFolder)
        {
            return new IAssetRequest[0];
        }
    }
}