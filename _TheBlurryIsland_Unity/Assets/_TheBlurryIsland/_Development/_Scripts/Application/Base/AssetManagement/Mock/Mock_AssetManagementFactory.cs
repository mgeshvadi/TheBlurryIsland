using Blurry.Application.Base.AssetManagement.Base;

namespace Blurry.Application.Base.AssetManagement.Mock
{
    public sealed class Mock_AssetManagementFactory : IAssetManagementFactory
    {
        public IAssetLoader CreateActiveAssetLoader()
        {
            return new Mock_AssetLoader();
        }

        public IAsyncAssetLoader CreateActiveAsyncAssetLoader()
        {
            return new Mock_AsyncAssetLoader();
        }
    }
}
