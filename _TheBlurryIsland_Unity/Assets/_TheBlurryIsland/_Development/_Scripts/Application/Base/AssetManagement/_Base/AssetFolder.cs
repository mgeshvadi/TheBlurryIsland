namespace Blurry.Application.Base.AssetManagement.Base
{
    public readonly struct AssetFolder
    {
        public string Path { get; }

        public AssetFolder(string path)
        {
            Path = path;
        }
    }
}