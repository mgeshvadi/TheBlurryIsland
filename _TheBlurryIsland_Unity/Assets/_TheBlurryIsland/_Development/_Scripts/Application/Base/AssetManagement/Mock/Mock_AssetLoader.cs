using System;
using Blurry.Application.Base.AssetManagement.Base;
using Blurry.Application.Utility.Extensions;
using UnityEngine;
using Object = UnityEngine.Object;


namespace Blurry.Application.Base.AssetManagement.Mock
{
    // TODO: Okay this is the same as UnityResources_AssetLoader maybe merge them. I don't know just think about this fucking shit crap mess and act about it. I'm talking to you dear reader, yes you!
    public sealed class Mock_AssetLoader : IAssetLoader
    {
        public T Load<T>(Asset asset) where T : Object
        {
            return (T) Load(asset, typeof(T));
        }

        public T Load<T>(Asset asset, Type type) where T : Object
        {
            return (T) Load(asset, type);
        }

        public Object Load(Asset asset)
        {
            return Load(asset, typeof(Object));
        }

        public Object Load(Asset asset, Type type)
        {
            return Resources.Load(asset.Path, type);
        }


        public T[] LoadAll<T>(AssetFolder assetFolder) where T : Object
        {
            return LoadAll(assetFolder, typeof(T)).ConvertTo<Object, T>();
        }

        public T[] LoadAll<T>(AssetFolder assetFolder, Type type) where T : Object
        {
            return LoadAll(assetFolder, type).ConvertTo<Object, T>();
        }

        public Object[] LoadAll(AssetFolder assetFolder)
        {
            return LoadAll(assetFolder, typeof(Object));
        }

        public Object[] LoadAll(AssetFolder assetFolder, Type type)
        {
            return Resources.LoadAll(assetFolder.Path, type);
        }
    }
}