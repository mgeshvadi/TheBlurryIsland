using UnityEngine;


namespace Blurry.Application.Base.AssetManagement.Base
{
    public interface IAssetRequest
    {
        public Object Asset { get; }
    }

    public interface IAssetRequest<T> where T : Object
    {
        public T Asset { get; }
    }
}