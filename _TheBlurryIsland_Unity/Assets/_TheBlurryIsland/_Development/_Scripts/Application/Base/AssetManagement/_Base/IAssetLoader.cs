using System;
using Object = UnityEngine.Object;


namespace Blurry.Application.Base.AssetManagement.Base
{
    public interface IAssetLoader
    {
        public T Load<T>(Asset asset) where T : Object;
        public T Load<T>(Asset asset, Type type) where T : Object;
        public Object Load(Asset asset, Type type);
        public Object Load(Asset asset);

        public T[] LoadAll<T>(AssetFolder assetFolder) where T : Object;
        public T[] LoadAll<T>(AssetFolder assetFolder, Type type) where T : Object;
        public Object[] LoadAll(AssetFolder assetFolder);
        public Object[] LoadAll(AssetFolder assetFolder, Type type);
    }
}