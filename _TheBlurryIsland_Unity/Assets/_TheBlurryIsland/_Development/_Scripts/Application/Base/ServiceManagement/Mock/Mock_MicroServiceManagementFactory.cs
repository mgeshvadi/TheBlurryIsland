using Blurry.Application.Base.ServiceManagement.Base;


namespace Blurry.Application.Base.ServiceManagement.Mock
{
    public sealed class Mock_MicroServiceManagementFactory : IMicroServiceManagementFactory
    {
        public IMicroServiceLocator CreateActiveServiceLocator()
        {
            return new Mock_MicroServiceLocator();
        }
    }
}