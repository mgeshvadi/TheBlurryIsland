using Blurry.Application.Base.ServiceManagement.Base;


namespace Blurry.Application.Base.ServiceManagement
{
    public static class ServiceManagement
    {
        public static IMicroServiceLocator ActiveServiceLocator { private set; get; }
        public static bool IsInitialized { private set; get; }

        public static void Initialize(IMicroServiceLocator serviceLocator)
        {
            if (IsInitialized)
                return;
            ChangeActiveServiceLocatorTo(serviceLocator);
            IsInitialized = true;
        }

        public static void ChangeActiveServiceLocatorTo(IMicroServiceLocator microServiceLocator)
        {
            ActiveServiceLocator = microServiceLocator;
        }
    }
}