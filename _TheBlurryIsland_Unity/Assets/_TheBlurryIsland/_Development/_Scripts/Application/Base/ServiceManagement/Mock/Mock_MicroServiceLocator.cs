using System;
using System.Collections.Generic;
using Blurry.Application.Base.ServiceManagement.Base;


namespace Blurry.Application.Base.ServiceManagement.Mock
{
    public sealed class Mock_MicroServiceLocator : IMicroServiceLocator
    {
        private readonly Dictionary<Type, IMicroService> microServices = new Dictionary<Type, IMicroService>();

        public void Register<T>(IMicroService microService) where T : IMicroService
        {
            microServices.Add(typeof(T), microService);
        }

        public void UnRegister<T>() where T : IMicroService
        {
            microServices.Remove(typeof(T));
        }

        public void Replace<T>(T service) where T : IMicroService
        {
            Register<T>(service);
        }

        public bool Has<T>() where T : IMicroService
        {
            return microServices.ContainsKey(typeof(T));
        }

        public T Find<T>() where T : IMicroService
        {
            return (T) microServices[typeof(T)];
        }
    }
}