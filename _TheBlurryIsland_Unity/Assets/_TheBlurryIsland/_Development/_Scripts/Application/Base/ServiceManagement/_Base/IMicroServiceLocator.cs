namespace Blurry.Application.Base.ServiceManagement.Base
{
    public interface IMicroServiceLocator
    {
        public void Register<T>(IMicroService microService) where T : IMicroService;
        public void UnRegister<T>() where T : IMicroService;
        public void Replace<T>(T service) where T : IMicroService;
        public bool Has<T>() where T : IMicroService;
        public T Find<T>() where T : IMicroService;
    }
}