namespace Blurry.Application.Base.ServiceManagement.Base
{
    public interface IMicroServiceManagementFactory
    {
        public IMicroServiceLocator CreateActiveServiceLocator();
    }
}