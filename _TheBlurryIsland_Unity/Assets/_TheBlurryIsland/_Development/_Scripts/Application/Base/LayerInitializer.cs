using Blurry.Application.Base.AssetManagement.Base;
using Blurry.Application.Base.AssetManagement.Mock;
using Blurry.Application.Base.ServiceManagement.Base;
using Blurry.Application.Base.ServiceManagement.Mock;


namespace Blurry.Application.Base
{
    public class LayerInitializationsResult
    {
    }

    public static class LayerInitializer
    {
        public static LayerInitializationsResult MockInitialize()
        {
            return Initialize(new Mock_MicroServiceManagementFactory(), new Mock_AssetManagementFactory());
        }

        public static LayerInitializationsResult Initialize(IMicroServiceManagementFactory microServiceManagementFactory, IAssetManagementFactory assetManagementFactory)
        {
            IMicroServiceLocator serviceLocator = microServiceManagementFactory.CreateActiveServiceLocator();
            ServiceManagement.ServiceManagement.Initialize(serviceLocator);

            AssetManagement.AssetManagement.Initialize(assetManagementFactory.CreateActiveAssetLoader(), assetManagementFactory.CreateActiveAsyncAssetLoader());

            return new LayerInitializationsResult();
        }
    }
}