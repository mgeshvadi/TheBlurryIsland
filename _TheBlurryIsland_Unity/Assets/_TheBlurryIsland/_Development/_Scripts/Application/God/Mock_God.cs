using Blurry.Application.God.Factory;
using Blurry.Application.God.Mock;


namespace Blurry.Application.God
{
    public sealed class Mock_God : AGod
    {
        protected override AGodFactory Factory => new Mock_GodFactory();
    }
}