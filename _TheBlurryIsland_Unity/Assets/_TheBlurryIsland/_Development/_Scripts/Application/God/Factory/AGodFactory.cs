using System.Collections.Generic;
using Blurry.Application.Foundation.Behaviour;
using Blurry.Application.God.Initialization;
using Blurry.Application.God.GameLoop;


namespace Blurry.Application.God.Factory
{
    public abstract class AGodFactory : IGodFactory
    {
        public abstract IGameLayersInitializer CreateLayersInitializer();

        public IGameLoopUpdater CreateGameLoopUpdater(List<IBasicBehaviour> updatables)
        {
            return new GameLoopUpdater(updatables);
        }
    }
}