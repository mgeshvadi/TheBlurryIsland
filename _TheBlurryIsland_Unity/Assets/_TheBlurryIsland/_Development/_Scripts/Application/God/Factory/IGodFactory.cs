using System.Collections.Generic;
using Blurry.Application.Foundation.Behaviour;
using Blurry.Application.God.Initialization;
using Blurry.Application.God.GameLoop;


namespace Blurry.Application.God.Factory
{
    public interface IGodFactory
    {
        public IGameLayersInitializer CreateLayersInitializer();
        public IGameLoopUpdater CreateGameLoopUpdater(List<IBasicBehaviour> updatables);
    }
}