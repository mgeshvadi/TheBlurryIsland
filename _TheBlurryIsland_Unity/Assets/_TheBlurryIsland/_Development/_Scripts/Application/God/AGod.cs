using UnityEngine;
using Blurry.Application.God.Factory;
using Blurry.Application.God.Initialization;
using Blurry.Application.God.GameLoop;


namespace Blurry.Application.God
{
    public abstract class AGod : MonoBehaviour
    {
        protected abstract AGodFactory Factory { get; }

        private IGameLoopUpdater gameLoopUpdater;

        private void Awake()
        {
            DontDestroyOnLoad(gameObject);

            IGameLayersInitializer initializer = Factory.CreateLayersInitializer();
            var initializationResult = initializer.InitializeAllLayers();

            gameLoopUpdater = Factory.CreateGameLoopUpdater(initializationResult.BehavioursNeedingUpdate);

            initializationResult.LevelManagement.GoToLevel(initializationResult.LevelManagement.GetAllLevelSourceEntityTypes()[0]);
        }

        private void Update()
        {
            gameLoopUpdater.Update(Time.deltaTime);
        }

        private void FixedUpdate()
        {
            gameLoopUpdater.FixedUpdate(Time.fixedDeltaTime);
        }
    }
}