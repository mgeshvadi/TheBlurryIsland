using System.Collections.Generic;
using Blurry.Application.Foundation.Behaviour;


namespace Blurry.Application.God.GameLoop
{
    public class GameLoopUpdater : IGameLoopUpdater
    {
        private readonly List<IBasicBehaviour> updatables;

        public GameLoopUpdater(List<IBasicBehaviour> updatables)
        {
            this.updatables = updatables;
        }

        public void Update(float deltaTime)
        {
            foreach (IBasicBehaviour updatable in updatables)
                updatable.Update(deltaTime);
        }

        public void FixedUpdate(float fixedDeltaTime)
        {
            foreach (IBasicBehaviour updatable in updatables)
                updatable.FixedUpdate(fixedDeltaTime);
        }
    }
}