using System.Collections.Generic;
using Blurry.Application.Foundation.Behaviour;
using Blurry.Application.LevelManagement.Base;


namespace Blurry.Application.God.Initialization
{
    public class GameLayersInitializationResult
    {
        public List<IBasicBehaviour> BehavioursNeedingUpdate { get; } = new List<IBasicBehaviour>();
        public ILevelManagement LevelManagement { get;  set; }
    }
}