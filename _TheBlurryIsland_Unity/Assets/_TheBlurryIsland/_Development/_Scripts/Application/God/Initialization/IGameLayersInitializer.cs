namespace Blurry.Application.God.Initialization
{
    public interface IGameLayersInitializer
    {
        public GameLayersInitializationResult InitializeAllLayers();
    }
}