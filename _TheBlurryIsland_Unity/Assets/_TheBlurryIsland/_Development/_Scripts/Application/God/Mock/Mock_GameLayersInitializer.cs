// ReSharper disable UnusedVariable

using Blurry.Application.God.Initialization;


namespace Blurry.Application.God.Mock
{
    public sealed class Mock_GameLayersInitializer : IGameLayersInitializer
    {
        public GameLayersInitializationResult InitializeAllLayers()
        {
            GameLayersInitializationResult initializationResult = new GameLayersInitializationResult();

            Utility.LayerInitializationsResult utilityInitializationsResult = Utility.LayerInitializer.MockInitialize();
            UnityLow.LayerInitializationsResult unityLowInitializationsResult = UnityLow.LayerInitializer.MockInitialize();
            Base.LayerInitializationsResult baseInitializationsResult = Base.LayerInitializer.MockInitialize();
            Foundation.LayerInitializationsResult foundationInitializationsResult = Foundation.LayerInitializer.MockInitialize();
            UnityMiddle.LayerInitializationsResult unityMiddleInitializationsResult = UnityMiddle.LayerInitializer.MockInitialize();

            ServiceLow.LayerInitializationsResult serviceLowInitializationsResult = ServiceLow.LayerInitializer.MockInitialize();
            ServiceMiddle.LayerInitializationsResult serviceMiddleInitializationsResult = ServiceMiddle.LayerInitializer.MockInitialize();

            CoreBase.LayerInitializationsResult coreBaseInitializationsResult = CoreBase.LayerInitializer.MockInitialize();
            CharacterBehaviours.LayerInitializationsResult characterBehavioursInitializationsResult = CharacterBehaviours.LayerInitializer.MockInitialize();
            CharacterControllers.LayerInitializationsResult characterControllersInitializationsResult = CharacterControllers.LayerInitializer.MockInitialize();
            CoreMechanics.LayerInitializationsResult coreMechanicsInitializationsResult = CoreMechanics.LayerInitializer.MockInitialize();
            Levels.LayerInitializationsResult levelsInitializationsResult = Levels.LayerInitializer.MockInitialize(coreMechanicsInitializationsResult.MechanicControllersFactory);
            LevelManagement.LayerInitializationsResult levelManagementInitializationsResult = LevelManagement.LayerInitializer.MockInitialize(levelsInitializationsResult.LevelFactory);


            UI.LayerInitializationsResult uiInitializationsResult = UI.LayerInitializer.MockInitialize();
            Overlay.LayerInitializationsResult overlayInitializationsResult = Overlay.LayerInitializer.MockInitialize();
            Business.LayerInitializationsResult businessInitializationsResult = Business.LayerInitializer.MockInitialize();

            initializationResult.BehavioursNeedingUpdate.Add(levelManagementInitializationsResult.LevelManagement);
            initializationResult.LevelManagement = levelManagementInitializationsResult.LevelManagement;

            return initializationResult;
        }
    }
}