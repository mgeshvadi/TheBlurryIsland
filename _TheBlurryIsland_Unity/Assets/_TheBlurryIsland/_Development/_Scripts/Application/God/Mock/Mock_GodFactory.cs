using UnityEngine.Scripting;
using Blurry.Application.God.Factory;
using Blurry.Application.God.Initialization;


namespace Blurry.Application.God.Mock
{
    [Preserve]
    public sealed class Mock_GodFactory : AGodFactory
    {
        public override IGameLayersInitializer CreateLayersInitializer()
        {
            return new Mock_GameLayersInitializer();
        }
    }
}