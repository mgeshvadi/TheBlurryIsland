using Blurry.Application.CharacterBehaviours.PresentationData;
using Blurry.Application.Foundation.Presentation;


namespace Blurry.Application.CharacterBehaviours.Presentation
{
    public interface ICharacterBehaviour_Presentation : IPresentation
    {
    }

    public abstract class ACharacterBehaviour_Presentation<TData> : APresentation<TData>, ICharacterBehaviour_Presentation where TData : ICharacterBehaviour_PresentationData
    {
        protected ACharacterBehaviour_Presentation(TData data) : base(data)
        {
        }
    }
}