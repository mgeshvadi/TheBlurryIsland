using Blurry.Application.Foundation.Config;


namespace Blurry.Application.CharacterBehaviours.Config
{
    public interface ICharacterBehaviour_Config : IConfig
    {
    }

    public interface ICharacterBehaviour_ConfigFile : IConfig_File
    {

    }

    public abstract class ACharacterBehaviour_ConfigFile<TConfig> : AConfig_File<TConfig>, ICharacterBehaviour_ConfigFile where TConfig : ICharacterBehaviour_Config
    {
    }
}