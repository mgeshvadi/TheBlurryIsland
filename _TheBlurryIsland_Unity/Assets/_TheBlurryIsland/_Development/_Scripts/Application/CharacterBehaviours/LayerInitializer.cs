using Blurry.Application.Foundation.ConfigFileManagement;
using Blurry.Application.CharacterBehaviours.ConfigManagement;


namespace Blurry.Application.CharacterBehaviours
{
    public class LayerInitializationsResult
    {
    }

    public static class LayerInitializer
    {
        public static LayerInitializationsResult MockInitialize()
        {
            return Initialize();
        }

        public static LayerInitializationsResult Initialize()
        {
            ConfigFileManagement.RegisterConfigFileLoader<CharacterBehaviours_ConfigFileLoader>(new CharacterBehaviours_ConfigFileLoader());
            return new LayerInitializationsResult();
        }
    }
}