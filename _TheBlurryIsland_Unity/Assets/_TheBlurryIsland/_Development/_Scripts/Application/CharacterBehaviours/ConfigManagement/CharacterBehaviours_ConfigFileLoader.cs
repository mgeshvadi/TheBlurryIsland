using Blurry.Application.Base.AssetManagement.Addresses;
using Blurry.Application.Base.AssetManagement.Base;
using Blurry.Application.CharacterBehaviours.Config;
using Blurry.Application.Foundation.ConfigFileManagement;
using Blurry.Application.Foundation.ConfigFileManagement.Base;


namespace Blurry.Application.CharacterBehaviours.ConfigManagement
{
    public class CharacterBehaviours_ConfigFileLoader : AConfigFileLoader<ICharacterBehaviour_ConfigFile>
    {
        public const string CONFIG_FILES_CONTEXT_MENU = Root_ConfigFileLoader.CONFIG_FILES_CONTEXT_MENU_BASE + "CharacterBehaviours/";

        protected override AssetFolder GetConfigAssetFolder()
        {
            return AssetFolders.ConfigFiles.characterBehaviours;
        }
    }
}