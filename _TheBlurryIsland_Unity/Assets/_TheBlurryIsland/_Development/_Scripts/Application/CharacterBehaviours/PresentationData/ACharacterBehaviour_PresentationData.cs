using Blurry.Application.CharacterBehaviours.Config;
using Blurry.Application.Foundation.PresentationData;
using Blurry.Application.UnityMiddle.Animator;


namespace Blurry.Application.CharacterBehaviours.PresentationData
{
    public interface ICharacterBehaviour_PresentationData : IPresentation_Data
    {
        public AnimatorHandler Animator { get; set; }
    }

    public abstract class ACharacterBehaviour_PresentationData<TConfig> : APresentation_Data<TConfig>, ICharacterBehaviour_PresentationData where TConfig : ICharacterBehaviour_Config
    {
        public AnimatorHandler Animator { get; set; }

        protected ACharacterBehaviour_PresentationData(TConfig config) : base(config)
        {
        }
    }
}