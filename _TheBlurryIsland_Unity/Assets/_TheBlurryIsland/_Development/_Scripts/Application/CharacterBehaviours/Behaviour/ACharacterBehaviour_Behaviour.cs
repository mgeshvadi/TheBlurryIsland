using Blurry.Application.Foundation.Behaviour;
using Blurry.Application.CharacterBehaviours.BehaviourData;
using Blurry.Application.CharacterBehaviours.Presentation;


namespace Blurry.Application.CharacterBehaviours.Behaviour
{
    public interface ICharacterBehaviour_Behaviour : IBehaviour
    {
    }

    public abstract class ACharacterBehaviour_Behaviour<TPresentation, TData> : ABehaviour<TPresentation, TData>, ICharacterBehaviour_Behaviour where TPresentation : ICharacterBehaviour_Presentation
                                                                                                                                                where TData : ICharacterBehaviour_BehaviourData
    {
        protected ACharacterBehaviour_Behaviour(TPresentation presentation, TData data) : base(presentation, data)
        {
        }
    }
}