using Blurry.Application.CharacterBehaviours.Config;
using Blurry.Application.Foundation.BehaviourData;


namespace Blurry.Application.CharacterBehaviours.BehaviourData
{
    public interface ICharacterBehaviour_BehaviourData : IBehaviour_Data
    {
    }

    public abstract class ACharacterBehaviour_BehaviourData<TConfig> : ABehaviour_Data<TConfig>, ICharacterBehaviour_BehaviourData where TConfig : ICharacterBehaviour_Config
    {
        protected ACharacterBehaviour_BehaviourData(TConfig config) : base(config)
        {
        }
    }
}