namespace Blurry.Application.CoreBase.Conditions.Base
{
    public interface ICondition
    {
        public bool IsSatisfied();
    }
}
