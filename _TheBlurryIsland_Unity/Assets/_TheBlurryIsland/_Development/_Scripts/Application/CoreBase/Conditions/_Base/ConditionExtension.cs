using System.Collections.Generic;
using System.Linq;


namespace Blurry.Application.CoreBase.Conditions.Base
{
    public static class ConditionExtension
    {
        public static bool AreAllSatisfied<T>(this IEnumerable<T> conditions) where T : ICondition
        {
            return conditions.All(condition => condition.IsSatisfied());
        }

        public static bool IsAnySatisfied<T>(this IEnumerable<T> conditions) where T : ICondition
        {
            return conditions.Any(condition => condition.IsSatisfied());
        }
    }
}