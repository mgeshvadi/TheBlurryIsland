using Blurry.Application.Base.ServiceManagement;
using Blurry.Application.ServiceMiddle.InputManagement;
using Blurry.Application.ServiceMiddle.InputManagement.Base;


namespace Blurry.Application.ServiceMiddle
{
    public class LayerInitializationsResult
    {
    }

    public static class LayerInitializer
    {
        public static LayerInitializationsResult MockInitialize()
        {
            return Initialize();
        }

        public static LayerInitializationsResult Initialize()
        {
            ServiceManagement.ActiveServiceLocator.Register<IInputManagement_Service>(new InputManagementFactory().CreateInputManagerService());

            return new LayerInitializationsResult();
        }
    }
}