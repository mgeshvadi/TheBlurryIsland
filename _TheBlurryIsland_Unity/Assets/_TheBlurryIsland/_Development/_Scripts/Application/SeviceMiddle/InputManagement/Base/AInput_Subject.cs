using System;
using UnityEngine.InputSystem;
using Blurry.Application.Base.ServiceManagement;
using Blurry.Application.ServiceLow.Observation.Base;


namespace Blurry.Application.ServiceMiddle.InputManagement.Base
{
    public interface IInput_Subject<out TInputValue> : ISubjectWithObservers where TInputValue : struct
    {
        public bool IsEnable { set; get; }
        public TInputValue InputValue { get; }
    }

    public abstract class AInput_Subject<TInputValue> : ASubjectWithObservers, IInput_Subject<TInputValue> where TInputValue : struct
    {
        public bool IsEnable { set; get; }
        public TInputValue InputValue { protected set; get; }

        private Action<InputAction.CallbackContext> performAction;

        protected AInput_Subject()
        {
            IsEnable = true;
            GetActivated();
        }

        ~AInput_Subject()
        {
            GetDeactivated();
        }

        private void GetActivated()
        {
            performAction = GetCorrespondingInputAction(GetInputController()).AddListenerToPerformed<TInputValue>(value =>
            {
                if (!IsEnable)
                    return;
                InputValue = value;
                NotifyObservers();
            });
        }

        private void GetDeactivated()
        {
            GetCorrespondingInputAction(GetInputController()).RemoveListenerFromPerformed(performAction);
            performAction = null;
        }

        private InputController GetInputController()
        {
            return ServiceManagement.ActiveServiceLocator.Find<IInputManagement_Service>().InputController;
        }

        protected abstract InputAction GetCorrespondingInputAction(InputController inputController);
    }
}