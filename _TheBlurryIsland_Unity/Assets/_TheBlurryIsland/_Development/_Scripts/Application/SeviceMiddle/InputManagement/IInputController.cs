using System;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Scripting;


namespace Blurry.Application.ServiceMiddle.InputManagement
{
    public sealed class InputController : UnityInput_ConfigFile
    {
    }

    public static class InputControllerExtension
    {
        public static Action<InputAction.CallbackContext> AddListenerToPerformed<TValue>(this InputAction self, Action<TValue> callback) where TValue : struct
        {
            Action<InputAction.CallbackContext> perform = Perform;
            self.performed += perform;

            return perform;

            void Perform(InputAction.CallbackContext context)
            {
                if (context.valueType != typeof(TValue))
                {
                    Debug.LogError(""); // TODO : Out logger
                    return;
                }
                TValue value = context.ReadValue<TValue>();
                callback.Invoke(value);
            }
        }

        public static void RemoveListenerFromPerformed(this InputAction self, Action<InputAction.CallbackContext> toRemove)
        {
            self.performed -= toRemove;
        }
    }
}