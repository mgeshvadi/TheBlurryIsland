using Blurry.Application.ServiceMiddle.InputManagement.Base;


namespace Blurry.Application.ServiceMiddle.InputManagement
{
    public class InputManagementFactory
    {
        public IInputManagement_Service CreateInputManagerService()
        {
            return new InputManagement_Service();
        }
    }
}