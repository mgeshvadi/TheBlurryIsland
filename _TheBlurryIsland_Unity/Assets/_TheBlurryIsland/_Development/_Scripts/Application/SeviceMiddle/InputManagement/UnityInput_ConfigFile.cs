// GENERATED AUTOMATICALLY FROM 'Assets/_TheBlurryIsland/_Development/Other/UnityInput/UnityInput_ConfigFile.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

namespace Blurry.Application.ServiceMiddle.InputManagement
{
    public class @UnityInput_ConfigFile : IInputActionCollection, IDisposable
    {
        public InputActionAsset asset { get; }
        public @UnityInput_ConfigFile()
        {
            asset = InputActionAsset.FromJson(@"{
    ""name"": ""UnityInput_ConfigFile"",
    ""maps"": [
        {
            ""name"": ""CoreMechanics"",
            ""id"": ""9764c253-f0ab-48a2-a5c9-18878a6d641d"",
            ""actions"": [
                {
                    ""name"": ""SocialClock"",
                    ""type"": ""PassThrough"",
                    ""id"": ""30d991df-b587-4a65-9a70-eed0d4b4da6c"",
                    ""expectedControlType"": ""Analog"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""SocialObstacles"",
                    ""type"": ""PassThrough"",
                    ""id"": ""5d1e9859-5b87-44d6-863f-ac0dc5c7201c"",
                    ""expectedControlType"": ""Analog"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""ArrowKeys"",
                    ""id"": ""f8f1feda-e06b-4073-83f4-e5b72fde3f66"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""SocialClock"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""109e8924-77bb-4836-a9a9-5c34c6966c8d"",
                    ""path"": ""<Keyboard>/leftArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""SocialClock"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""0d7e422b-e2ec-4cd9-95a8-e5a0bd39363f"",
                    ""path"": ""<Keyboard>/rightArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""SocialClock"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""ArrowKeys"",
                    ""id"": ""96cd204f-efb7-4c2f-a14c-755284ab3d29"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""SocialObstacles"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""ac400c97-bf44-4c77-a65e-43b7aeb8252e"",
                    ""path"": ""<Keyboard>/leftArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""SocialObstacles"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""5b0d28db-6466-4ecb-a412-95b4190aa65a"",
                    ""path"": ""<Keyboard>/rightArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""PC"",
                    ""action"": ""SocialObstacles"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                }
            ]
        }
    ],
    ""controlSchemes"": [
        {
            ""name"": ""PC"",
            ""bindingGroup"": ""PC"",
            ""devices"": [
                {
                    ""devicePath"": ""<Keyboard>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        },
        {
            ""name"": ""Mobile"",
            ""bindingGroup"": ""Mobile"",
            ""devices"": [
                {
                    ""devicePath"": ""<OnScreenGamepad>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        }
    ]
}");
            // CoreMechanics
            m_CoreMechanics = asset.FindActionMap("CoreMechanics", throwIfNotFound: true);
            m_CoreMechanics_SocialClock = m_CoreMechanics.FindAction("SocialClock", throwIfNotFound: true);
            m_CoreMechanics_SocialObstacles = m_CoreMechanics.FindAction("SocialObstacles", throwIfNotFound: true);
        }

        public void Dispose()
        {
            UnityEngine.Object.Destroy(asset);
        }

        public InputBinding? bindingMask
        {
            get => asset.bindingMask;
            set => asset.bindingMask = value;
        }

        public ReadOnlyArray<InputDevice>? devices
        {
            get => asset.devices;
            set => asset.devices = value;
        }

        public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

        public bool Contains(InputAction action)
        {
            return asset.Contains(action);
        }

        public IEnumerator<InputAction> GetEnumerator()
        {
            return asset.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Enable()
        {
            asset.Enable();
        }

        public void Disable()
        {
            asset.Disable();
        }

        // CoreMechanics
        private readonly InputActionMap m_CoreMechanics;
        private ICoreMechanicsActions m_CoreMechanicsActionsCallbackInterface;
        private readonly InputAction m_CoreMechanics_SocialClock;
        private readonly InputAction m_CoreMechanics_SocialObstacles;
        public struct CoreMechanicsActions
        {
            private @UnityInput_ConfigFile m_Wrapper;
            public CoreMechanicsActions(@UnityInput_ConfigFile wrapper) { m_Wrapper = wrapper; }
            public InputAction @SocialClock => m_Wrapper.m_CoreMechanics_SocialClock;
            public InputAction @SocialObstacles => m_Wrapper.m_CoreMechanics_SocialObstacles;
            public InputActionMap Get() { return m_Wrapper.m_CoreMechanics; }
            public void Enable() { Get().Enable(); }
            public void Disable() { Get().Disable(); }
            public bool enabled => Get().enabled;
            public static implicit operator InputActionMap(CoreMechanicsActions set) { return set.Get(); }
            public void SetCallbacks(ICoreMechanicsActions instance)
            {
                if (m_Wrapper.m_CoreMechanicsActionsCallbackInterface != null)
                {
                    @SocialClock.started -= m_Wrapper.m_CoreMechanicsActionsCallbackInterface.OnSocialClock;
                    @SocialClock.performed -= m_Wrapper.m_CoreMechanicsActionsCallbackInterface.OnSocialClock;
                    @SocialClock.canceled -= m_Wrapper.m_CoreMechanicsActionsCallbackInterface.OnSocialClock;
                    @SocialObstacles.started -= m_Wrapper.m_CoreMechanicsActionsCallbackInterface.OnSocialObstacles;
                    @SocialObstacles.performed -= m_Wrapper.m_CoreMechanicsActionsCallbackInterface.OnSocialObstacles;
                    @SocialObstacles.canceled -= m_Wrapper.m_CoreMechanicsActionsCallbackInterface.OnSocialObstacles;
                }
                m_Wrapper.m_CoreMechanicsActionsCallbackInterface = instance;
                if (instance != null)
                {
                    @SocialClock.started += instance.OnSocialClock;
                    @SocialClock.performed += instance.OnSocialClock;
                    @SocialClock.canceled += instance.OnSocialClock;
                    @SocialObstacles.started += instance.OnSocialObstacles;
                    @SocialObstacles.performed += instance.OnSocialObstacles;
                    @SocialObstacles.canceled += instance.OnSocialObstacles;
                }
            }
        }
        public CoreMechanicsActions @CoreMechanics => new CoreMechanicsActions(this);
        private int m_PCSchemeIndex = -1;
        public InputControlScheme PCScheme
        {
            get
            {
                if (m_PCSchemeIndex == -1) m_PCSchemeIndex = asset.FindControlSchemeIndex("PC");
                return asset.controlSchemes[m_PCSchemeIndex];
            }
        }
        private int m_MobileSchemeIndex = -1;
        public InputControlScheme MobileScheme
        {
            get
            {
                if (m_MobileSchemeIndex == -1) m_MobileSchemeIndex = asset.FindControlSchemeIndex("Mobile");
                return asset.controlSchemes[m_MobileSchemeIndex];
            }
        }
        public interface ICoreMechanicsActions
        {
            void OnSocialClock(InputAction.CallbackContext context);
            void OnSocialObstacles(InputAction.CallbackContext context);
        }
    }
}
