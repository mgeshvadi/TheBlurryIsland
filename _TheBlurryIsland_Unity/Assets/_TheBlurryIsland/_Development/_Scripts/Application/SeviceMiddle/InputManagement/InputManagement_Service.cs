using Blurry.Application.ServiceMiddle.InputManagement.Base;


namespace Blurry.Application.ServiceMiddle.InputManagement
{
    public sealed class InputManagement_Service : IInputManagement_Service
    {
        public InputController InputController { get; }

        public InputManagement_Service()
        {
            InputController = new InputController();
            InputController.Enable();
        }
    }
}