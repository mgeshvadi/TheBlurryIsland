using Blurry.Application.Base.ServiceManagement.Base;


namespace Blurry.Application.ServiceMiddle.InputManagement.Base
{
    public interface IInputManagement_Service : IMicroService
    {
        public InputController InputController { get; }
    }
}