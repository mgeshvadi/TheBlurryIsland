namespace Blurry.Application.CharacterControllers
{
    public class LayerInitializationsResult
    {
    }

    public static class LayerInitializer
    {
        public static LayerInitializationsResult MockInitialize()
        {
            return Initialize();
        }

        public static LayerInitializationsResult Initialize()
        {
            return new LayerInitializationsResult();
        }
    }
}