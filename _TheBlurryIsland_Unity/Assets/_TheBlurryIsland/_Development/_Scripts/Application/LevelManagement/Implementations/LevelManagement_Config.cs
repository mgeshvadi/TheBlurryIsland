using System;
using System.Collections.Generic;
using UnityEngine;
using Blurry.Application.Utility.Extensions;
using Blurry.Application.Foundation.Config;
using Blurry.Application.Levels.Entity;
using PandasCanPlay.Attributes;


namespace Blurry.Application.LevelManagement.Implementations
{
    [Serializable]
    public class LevelManagement_Config : IConfig
    {
        [SerializeField] private int levelsToKeepLoadedFromEachSideCount;
        [TypeAttribute(typeof(ILevel_Entity), false)]
        [SerializeField] private List<string> levels;

        public int LevelsToKeepLoadedFromEachSideCount => levelsToKeepLoadedFromEachSideCount;

        public List<Type> GetAllLevelsEntitiesTypes()
        {
            return levels.Select(level => Type.GetType(level));
        }
    }
}