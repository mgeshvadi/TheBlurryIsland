using Blurry.Application.LevelManagement.Base;


namespace Blurry.Application.LevelManagement.Implementations
{
    public class CurrentLevelChangeNeedDetector : ICurrentLevelChangeNeedDetector
    {
        public CurrentLevelChangeNeedSubject LevelChangeNeed { get; }

        public CurrentLevelChangeNeedDetector()
        {
            LevelChangeNeed = new CurrentLevelChangeNeedSubject();
        }
    }
}