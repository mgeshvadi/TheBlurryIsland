using Blurry.Application.Base.AssetManagement.Addresses;
using Blurry.Application.Base.AssetManagement.Base;
using Blurry.Application.Foundation.ConfigFileManagement;
using Blurry.Application.Foundation.ConfigFileManagement.Base;
using Blurry.Application.LevelManagement.Base;


namespace Blurry.Application.LevelManagement.Implementations
{
    public interface ILevelManagement_ConfigFileLoader : IConfigFileLoader<ILevelManagement_ConfigFile>
    {
    }

    public class LevelManagement_ConfigFileLoader : AConfigFileLoader<ILevelManagement_ConfigFile>, ILevelManagement_ConfigFileLoader
    {
        public const string CONFIG_FILES_CONTEXT_MENU = Root_ConfigFileLoader.CONFIG_FILES_CONTEXT_MENU_BASE + "LevelManagement/";

        protected override AssetFolder GetConfigAssetFolder()
        {
            return AssetFolders.ConfigFiles.levelManagement;
        }
    }
}