using System;
using System.Collections.Generic;
using Blurry.Application.LevelManagement.Base;
using Blurry.Application.Levels.Behaviour;
using Blurry.Application.Levels.Entity;
using Blurry.Application.Levels.Factory;
using UnityEngine;


namespace Blurry.Application.LevelManagement.Implementations
{
    public class LevelSourceEntitiesPool : ILevelSourceEntitiesPool
    {
        private readonly ILevelFactory levelFactory;

        public List<Type> AllLevelsSourceEntitiesTypes { get; }

        public LevelSourceEntitiesPool(List<Type> allLevelsSourceEntitiesTypes, ILevelFactory levelFactory)
        {
            this.AllLevelsSourceEntitiesTypes = allLevelsSourceEntitiesTypes;
            this.levelFactory = levelFactory;
        }

        public bool DoesLevelExist(Type levelEntityType)
        {
            return DoesLevelExist(AllLevelsSourceEntitiesTypes.IndexOf(levelEntityType));
        }

        public bool DoesLevelExist(int levelIndex)
        {
            return 0 <= levelIndex && levelIndex < AllLevelsSourceEntitiesTypes.Count;
        }

        public InstantiatedLevel InstantiateLevel(int levelIndex)
        {
            Type toInstantiateLevelEntityType = AllLevelsSourceEntitiesTypes[levelIndex];
            return InstantiateLevel(toInstantiateLevelEntityType);
        }

        public InstantiatedLevel InstantiateLevel(Type toInstantiateLevelEntityType)
        {
            ILevel_Entity instantiatedEntity = levelFactory.InstantiateLevel(toInstantiateLevelEntityType, Vector3.zero, null); // TODO: Handle the position and parents of instantiated levels
            ILevel_Behaviour instantiatedBehaviour = levelFactory.CreateLevelBehaviourFor(instantiatedEntity);

            return new InstantiatedLevel(instantiatedEntity, instantiatedBehaviour);
        }

        public List<InstantiatedLevel> InstantiateLevels(int initialIndex, int count)
        {
            List<InstantiatedLevel> result = new List<InstantiatedLevel>();
            for (int i = 0; i < count; i++)
            {
                if (DoesLevelExist(i + initialIndex))
                    result.Add(InstantiateLevel(i));
            }
            return result;
        }
    }
}