using System;
using System.Collections.Generic;
using Blurry.Application.LevelManagement.Base;
using Blurry.Application.ServiceLow.Observation.Base;
using UnityEngine;


namespace Blurry.Application.LevelManagement.Implementations
{
    public class LevelManager : ILevelManager
    {
        private readonly ILoadedLevelsManager loadedLevelsManager;
        private readonly ILevelSourceEntitiesPool levelSourceEntitiesPool;
        private readonly int levelsToKeepLoadedFromEachSideCount;

        private int currentLevelIndex;

        public LevelManager(int levelsToKeepLoadedFromEachSideCount, ILoadedLevelsManager loadedLevelsManager, ILevelSourceEntitiesPool levelSourceEntitiesPool)
        {
            this.loadedLevelsManager = loadedLevelsManager;
            this.levelSourceEntitiesPool = levelSourceEntitiesPool;
            this.levelsToKeepLoadedFromEachSideCount = levelsToKeepLoadedFromEachSideCount;
        }

        public ILevelSourceEntitiesPool GetLevelsSourceEntitiesPool()
        {
            return levelSourceEntitiesPool;
        }

        public void ForceChangeCurrentLevelTo(Type targetLevelEntityType)
        {
            if (!levelSourceEntitiesPool.DoesLevelExist(targetLevelEntityType))
            {
                Debug.LogError($"Trying for Force changing current to a non existing level. requested level entity type: {targetLevelEntityType}");
                return;
            }

            currentLevelIndex = levelSourceEntitiesPool.AllLevelsSourceEntitiesTypes.IndexOf(targetLevelEntityType);
            InstantiatedLevel currentLevel = levelSourceEntitiesPool.InstantiateLevel(targetLevelEntityType);

            List<InstantiatedLevel> backwardLevels = levelSourceEntitiesPool.InstantiateLevels(initialIndex: currentLevelIndex - levelsToKeepLoadedFromEachSideCount, count: levelsToKeepLoadedFromEachSideCount);
            List<InstantiatedLevel> forwardLevels = levelSourceEntitiesPool.InstantiateLevels(initialIndex: currentLevelIndex + 1, count: levelsToKeepLoadedFromEachSideCount);

            loadedLevelsManager.ForceResetTo(backwardLevels, currentLevel, forwardLevels);
        }

        public void HandleSubjectOccurrence(ISubject subject, object subjectOwner)
        {
            switch (subject)
            {
                case CurrentLevelChangeNeedSubject {Direction: LevelChangeDirection.ToForward}:
                    if (loadedLevelsManager.CanGoForward())
                        MoveToNextLevel();
                    break;
                case CurrentLevelChangeNeedSubject {Direction: LevelChangeDirection.ToBackward}:
                    if (loadedLevelsManager.CanGoBackward())
                        MoveToPreviousLevel();
                    break;
            }
        }

        private void MoveToNextLevel()
        {
            currentLevelIndex++;
            int levelToLoadToForwardIndex = currentLevelIndex + levelsToKeepLoadedFromEachSideCount;
            if (levelSourceEntitiesPool.DoesLevelExist(levelToLoadToForwardIndex))
            {
                InstantiatedLevel instantiatedLevel = levelSourceEntitiesPool.InstantiateLevel(currentLevelIndex);
                loadedLevelsManager.MoveCurrentLevelToForward(instantiatedLevel.Entity, instantiatedLevel.Behaviour);
            }
            else
                loadedLevelsManager.MoveCurrentLevelToForwardWithoutFurtherLoading();
        }

        private void MoveToPreviousLevel()
        {
            currentLevelIndex--;
            int levelToLoadToBackwardIndex = currentLevelIndex - levelsToKeepLoadedFromEachSideCount;
            if (levelSourceEntitiesPool.DoesLevelExist(levelToLoadToBackwardIndex))
            {
                InstantiatedLevel instantiatedLevel = levelSourceEntitiesPool.InstantiateLevel(currentLevelIndex);
                loadedLevelsManager.MoveCurrentLevelToBackward(instantiatedLevel.Entity, instantiatedLevel.Behaviour);
            }
            else
                loadedLevelsManager.MoveCurrentLevelToBackwardWithoutFurtherLoading();
        }

        public void Update(float deltaTime)
        {
            loadedLevelsManager.Update(deltaTime);
        }

        public void FixedUpdate(float fixedDeltaTime)
        {
            loadedLevelsManager.FixedUpdate(fixedDeltaTime);
        }
    }
}