using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using Blurry.Application.LevelManagement.Base;
using Blurry.Application.Levels.Behaviour;
using Blurry.Application.Levels.Entity;
using Blurry.Application.Utility.DataStructures;
using static UnityEngine.Object;


namespace Blurry.Application.LevelManagement.Implementations
{
    public class LoadedLevelsManager : ILoadedLevelsManager
    {
        private class LoadedLevel
        {
            public ILevel_Entity Entity { get; }
            public ILevel_Behaviour Behaviour { get; }

            public LoadedLevel(ILevel_Entity entity, ILevel_Behaviour behaviour)
            {
                Entity = entity;
                Behaviour = behaviour;
            }
        }

        private LoadedLevel currentFocusedLevel;
        private readonly FixedSizeList<LoadedLevel> forwardLoadedLevels;
        private readonly FixedSizeList<LoadedLevel> backwardLoadedLevels;

        public LoadedLevelsManager(int levelsToKeepLoadedFromEachSideCount)
        {
            currentFocusedLevel = null;
            forwardLoadedLevels = new FixedSizeList<LoadedLevel>(levelsToKeepLoadedFromEachSideCount);
            backwardLoadedLevels = new FixedSizeList<LoadedLevel>(levelsToKeepLoadedFromEachSideCount);
        }

        public void ForceResetTo(List<InstantiatedLevel> instantiatedLevelsForBackwardOfCurrentLevel, InstantiatedLevel instantiatedLevelForCurrentLevel, List<InstantiatedLevel> instantiatedLevelsForForwardOfCurrentLevel)
        {
            Reset();

            currentFocusedLevel = new LoadedLevel(instantiatedLevelForCurrentLevel.Entity, instantiatedLevelForCurrentLevel.Behaviour);
            for (int i = 0; i < Math.Min(forwardLoadedLevels.Capacity, instantiatedLevelsForForwardOfCurrentLevel.Count); i++)
            {
                InstantiatedLevel level = instantiatedLevelsForForwardOfCurrentLevel[i];
                forwardLoadedLevels.PushToLast(new LoadedLevel(level.Entity, level.Behaviour));
            }
            for (int i = 0; i < Math.Min(backwardLoadedLevels.Capacity, instantiatedLevelsForBackwardOfCurrentLevel.Count); i++)
            {
                InstantiatedLevel level = instantiatedLevelsForForwardOfCurrentLevel[i];
                backwardLoadedLevels.PushToLast(new LoadedLevel(level.Entity, level.Behaviour));
            }
        }

        private void Reset()
        {
            currentFocusedLevel?.Behaviour.GetStopped();
            currentFocusedLevel?.Behaviour.GetDeactivated();
            Destroy(currentFocusedLevel?.Entity.GameObject);
            foreach (LoadedLevel level in backwardLoadedLevels)
                level.Behaviour.GetDeactivated();
            foreach (LoadedLevel level in forwardLoadedLevels)
                level.Behaviour.GetDeactivated();

            currentFocusedLevel = null;
            backwardLoadedLevels.Clear();
            forwardLoadedLevels.Clear();
        }

        public bool CanGoForward()
        {
            return forwardLoadedLevels.Count != 0;
        }

        public bool CanGoBackward()
        {
            return backwardLoadedLevels.Count != 0;
        }

        public void MoveCurrentLevelToForward(ILevel_Entity toAddToForwardLevelEntity, ILevel_Behaviour toAddToForwardLevelBehaviour)
        {
            MoveCurrentLevelToForward(new LoadedLevel(toAddToForwardLevelEntity, toAddToForwardLevelBehaviour));
        }

        public void MoveCurrentLevelToForwardWithoutFurtherLoading()
        {
            MoveCurrentLevelToBackward(null);
        }

        private void MoveCurrentLevelToForward([CanBeNull] LoadedLevel toAddToForwardLoadedLevels)
        {
            backwardLoadedLevels.PushToLast(currentFocusedLevel, onElementDrop: level => UnloadLevel(level));

            ChangeCurrentFocusedLevel(forwardLoadedLevels.First());
            forwardLoadedLevels.RemoveFirst();
            if (toAddToForwardLoadedLevels == null)
                return;
            forwardLoadedLevels.PushToLast(toAddToForwardLoadedLevels);
            toAddToForwardLoadedLevels.Behaviour.GetActivated();
        }

        public void MoveCurrentLevelToBackward(ILevel_Entity toAddToBackwardLevelEntity, ILevel_Behaviour toAddToBackwardLevelBehaviour)
        {
            MoveCurrentLevelToBackward(new LoadedLevel(toAddToBackwardLevelEntity, toAddToBackwardLevelBehaviour));
        }

        public void MoveCurrentLevelToBackwardWithoutFurtherLoading()
        {
            MoveCurrentLevelToBackward(null);
        }

        private void MoveCurrentLevelToBackward([CanBeNull] LoadedLevel toAddToBackwardLoadedLevels)
        {
            forwardLoadedLevels.PushToFirst(currentFocusedLevel, onElementDrop: level => UnloadLevel(level));

            ChangeCurrentFocusedLevel(backwardLoadedLevels.Last());
            backwardLoadedLevels.RemoveLast();
            if (toAddToBackwardLoadedLevels == null)
                return;
            backwardLoadedLevels.PushToFirst(toAddToBackwardLoadedLevels);
            toAddToBackwardLoadedLevels.Behaviour.GetActivated();
        }

        private void UnloadLevel(LoadedLevel toUnloadLevel)
        {
            toUnloadLevel.Behaviour.GetDeactivated();
            Destroy(toUnloadLevel.Entity.GameObject);
        }

        private void ChangeCurrentFocusedLevel(LoadedLevel to)
        {
            currentFocusedLevel.Behaviour.GetStopped();
            currentFocusedLevel = to;
            currentFocusedLevel.Behaviour.GetStarted();
        }

        public void Update(float deltaTime)
        {
            foreach (LoadedLevel level in backwardLoadedLevels)
                level.Behaviour.Update(deltaTime);
            currentFocusedLevel.Behaviour.Update(deltaTime);
            foreach (LoadedLevel level in forwardLoadedLevels)
                level.Behaviour.Update(deltaTime);
        }

        public void FixedUpdate(float fixedDeltaTime)
        {
            foreach (LoadedLevel level in backwardLoadedLevels)
                level.Behaviour.FixedUpdate(fixedDeltaTime);
            currentFocusedLevel.Behaviour.FixedUpdate(fixedDeltaTime);
            foreach (LoadedLevel level in forwardLoadedLevels)
                level.Behaviour.FixedUpdate(fixedDeltaTime);
        }
    }
}