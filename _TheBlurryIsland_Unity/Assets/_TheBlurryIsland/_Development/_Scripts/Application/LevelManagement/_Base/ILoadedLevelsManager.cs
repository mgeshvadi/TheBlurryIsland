using System.Collections.Generic;
using Blurry.Application.Foundation.Behaviour;
using Blurry.Application.Levels.Behaviour;
using Blurry.Application.Levels.Entity;


namespace Blurry.Application.LevelManagement.Base
{
    public interface ILoadedLevelsManager : IBasicBehaviour
    {
        public void ForceResetTo(List<InstantiatedLevel> instantiatedLevelsForBackwardOfCurrentLevel, InstantiatedLevel instantiatedLevelForCurrentLevel, List<InstantiatedLevel> instantiatedLevelsForForwardOfCurrentLevel);

        public bool CanGoForward();
        public bool CanGoBackward();

        public void MoveCurrentLevelToForward(ILevel_Entity toAddToForwardLevelEntity, ILevel_Behaviour toAddToForwardLevelBehaviour);
        public void MoveCurrentLevelToForwardWithoutFurtherLoading();

        public void MoveCurrentLevelToBackward(ILevel_Entity toAddToBackwardLevelEntity, ILevel_Behaviour toAddToBackwardLevelBehaviour);
        public void MoveCurrentLevelToBackwardWithoutFurtherLoading();

    }
}