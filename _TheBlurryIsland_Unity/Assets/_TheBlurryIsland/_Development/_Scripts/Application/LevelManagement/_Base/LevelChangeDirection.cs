namespace Blurry.Application.LevelManagement.Base
{
    public  enum LevelChangeDirection
    {
        ToForward,
        ToBackward
    }
}