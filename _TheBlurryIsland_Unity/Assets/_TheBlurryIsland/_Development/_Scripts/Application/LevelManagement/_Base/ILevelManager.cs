using System;
using Blurry.Application.Foundation.Behaviour;
using Blurry.Application.ServiceLow.Observation.Base;


namespace Blurry.Application.LevelManagement.Base
{
    public interface ILevelManager : IObserver, IBasicBehaviour
    {
        public ILevelSourceEntitiesPool GetLevelsSourceEntitiesPool();
        public void ForceChangeCurrentLevelTo(Type targetLevelEntityType);
    }
}