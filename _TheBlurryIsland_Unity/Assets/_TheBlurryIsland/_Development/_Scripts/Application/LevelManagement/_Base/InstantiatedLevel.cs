using Blurry.Application.Levels.Behaviour;
using Blurry.Application.Levels.Entity;


namespace Blurry.Application.LevelManagement.Base
{
    public struct InstantiatedLevel
    {
        public ILevel_Entity Entity { get; }
        public ILevel_Behaviour Behaviour { get; }

        public InstantiatedLevel(ILevel_Entity entity, ILevel_Behaviour behaviour)
        {
            Entity = entity;
            Behaviour = behaviour;
        }
    }
}