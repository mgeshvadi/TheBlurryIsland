using Blurry.Application.ServiceLow.Observation.Base;


namespace Blurry.Application.LevelManagement.Base
{
    public class CurrentLevelChangeNeedSubject : ASubjectWithObservers
    {
        public LevelChangeDirection Direction { get; private set; }

        public void NotifyObservers(LevelChangeDirection direction)
        {
            Direction = direction;
            base.NotifyObservers();
        }
    }
}