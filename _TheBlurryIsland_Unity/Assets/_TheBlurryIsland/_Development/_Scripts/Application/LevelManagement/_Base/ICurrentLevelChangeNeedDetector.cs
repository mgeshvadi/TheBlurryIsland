using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Blurry.Application.LevelManagement.Base
{
    public interface ICurrentLevelChangeNeedDetector
    {
        public CurrentLevelChangeNeedSubject LevelChangeNeed { get; }
    }
}