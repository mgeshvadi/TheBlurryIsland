using System;
using System.Collections.Generic;
using Blurry.Application.Foundation.Behaviour;
using Blurry.Application.Foundation.Factory;


namespace Blurry.Application.LevelManagement.Base
{
    public interface ILevelManagement : IBasicBehaviour, IFactorable
    {
        public List<Type> GetAllLevelSourceEntityTypes();
        public void GoToLevel(Type levelEntityType);
    }
}