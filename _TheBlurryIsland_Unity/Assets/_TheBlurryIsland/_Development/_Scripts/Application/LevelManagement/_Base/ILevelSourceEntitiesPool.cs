using System;
using System.Collections.Generic;


namespace Blurry.Application.LevelManagement.Base
{
    public interface ILevelSourceEntitiesPool
    {
        public List<Type> AllLevelsSourceEntitiesTypes { get; }

        public bool DoesLevelExist(Type levelEntityType);
        public bool DoesLevelExist(int levelIndex);

        public InstantiatedLevel InstantiateLevel(Type toInstantiateLevelEntityType);
        public InstantiatedLevel InstantiateLevel(int levelIndex);
        public List<InstantiatedLevel> InstantiateLevels(int initialIndex, int count);
    }
}