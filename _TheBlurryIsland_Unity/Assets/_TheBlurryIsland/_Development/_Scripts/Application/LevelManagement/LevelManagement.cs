using System;
using System.Collections.Generic;
using Blurry.Application.LevelManagement.Base;
using UnityEngine.Scripting;


namespace Blurry.Application.LevelManagement
{
    public class LevelManagement : ILevelManagement
    {
        [Preserve]
        private readonly ICurrentLevelChangeNeedDetector currentLevelChangeNeedDetector;
        private readonly ILevelManager levelManager;


        public LevelManagement(ILevelManager levelManager, ICurrentLevelChangeNeedDetector currentLevelChangeNeedDetector)
        {
            this.levelManager = levelManager;
            this.currentLevelChangeNeedDetector = currentLevelChangeNeedDetector;

            this.currentLevelChangeNeedDetector.LevelChangeNeed.AddObservers(levelManager);
        }

        public List<Type> GetAllLevelSourceEntityTypes()
        {
            return levelManager.GetLevelsSourceEntitiesPool().AllLevelsSourceEntitiesTypes;
        }

        public void GoToLevel(Type levelEntityType)
        {
            levelManager.ForceChangeCurrentLevelTo(levelEntityType);
        }

        public void Update(float deltaTime)
        {
            levelManager.Update(deltaTime);
        }

        public void FixedUpdate(float fixedDeltaTime)
        {
            levelManager.FixedUpdate(fixedDeltaTime);
        }
    }
}