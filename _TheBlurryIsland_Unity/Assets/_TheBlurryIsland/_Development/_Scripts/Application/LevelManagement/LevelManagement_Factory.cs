using System;
using System.Collections.Generic;
using Blurry.Application.LevelManagement.Base;
using Blurry.Application.LevelManagement.Implementations;
using Blurry.Application.Levels.Factory;


namespace Blurry.Application.LevelManagement
{
    public class LevelManagement_Factory
    {
        public ILevelManagement CreateLevelManagement(LevelManagement_Config config, ILevelFactory levelFactory)
        {
            List<Type> allLevelsEntitiesTypes = config.GetAllLevelsEntitiesTypes();
            int levelsToKeepLoadedFromEachSideCount = config.LevelsToKeepLoadedFromEachSideCount;

            ILoadedLevelsManager loadedLevelsManager = new LoadedLevelsManager(levelsToKeepLoadedFromEachSideCount);
            ILevelSourceEntitiesPool sourceEntitiesPool = new LevelSourceEntitiesPool(allLevelsEntitiesTypes, levelFactory);
            ILevelManager levelManager = new LevelManager(levelsToKeepLoadedFromEachSideCount, loadedLevelsManager, sourceEntitiesPool);

            ICurrentLevelChangeNeedDetector currentLevelChangeNeedDetector = new CurrentLevelChangeNeedDetector();

            return new LevelManagement(levelManager, currentLevelChangeNeedDetector);
        }
    }
}