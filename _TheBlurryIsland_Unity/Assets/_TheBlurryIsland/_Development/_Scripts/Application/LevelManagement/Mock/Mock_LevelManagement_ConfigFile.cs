using Blurry.Application.Foundation.Config;
using Blurry.Application.LevelManagement.Base;
using Blurry.Application.LevelManagement.Implementations;
using UnityEngine;


namespace Blurry.Application.LevelManagement.Mock
{
    [CreateAssetMenu(fileName = nameof(Mock_LevelManagement_ConfigFile), menuName = LevelManagement_ConfigFileLoader.CONFIG_FILES_CONTEXT_MENU + nameof(Mock_LevelManagement_ConfigFile))]
    public class Mock_LevelManagement_ConfigFile : AConfig_File<LevelManagement_Config>, ILevelManagement_ConfigFile
    {
    }
}