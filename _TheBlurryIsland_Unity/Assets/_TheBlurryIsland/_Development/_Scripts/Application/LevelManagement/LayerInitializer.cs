using Blurry.Application.Foundation.ConfigFileManagement;
using Blurry.Application.LevelManagement.Base;
using Blurry.Application.LevelManagement.Implementations;
using Blurry.Application.LevelManagement.Mock;
using Blurry.Application.Levels.Factory;
using UnityEngine;


namespace Blurry.Application.LevelManagement
{
    public class LayerInitializationsResult
    {
        public ILevelManagement LevelManagement { get; }

        public LayerInitializationsResult(ILevelManagement levelManagement)
        {
            LevelManagement = levelManagement;
        }
    }

    public static class LayerInitializer
    {
        public static LayerInitializationsResult MockInitialize(ILevelFactory levelFactory)
        {
            return Initialize<Mock_LevelManagement_ConfigFile>(levelFactory);
        }

        public static LayerInitializationsResult Initialize<T>(ILevelFactory levelFactory) where T : ScriptableObject, ILevelManagement_ConfigFile
        {
            ConfigFileManagement.RegisterConfigFileLoader<ILevelManagement_ConfigFileLoader>(new LevelManagement_ConfigFileLoader());

            ILevelManagement_ConfigFile configFile = ConfigFileManagement.GetConfigLoader<ILevelManagement_ConfigFileLoader>().LoadConfigFile<T>();
            LevelManagement_Config config = configFile.GetNotCastedConfig() as LevelManagement_Config;
            ILevelManagement levelManagement = new LevelManagement_Factory().CreateLevelManagement(config, levelFactory);
            return new LayerInitializationsResult(levelManagement);
        }
    }
}