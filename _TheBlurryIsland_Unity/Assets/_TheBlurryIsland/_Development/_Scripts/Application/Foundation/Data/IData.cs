using Blurry.Application.Foundation.Factory;


namespace Blurry.Application.Foundation.Data
{
    public interface IData : IFactorable, IFactoryNeed
    {
    }
}