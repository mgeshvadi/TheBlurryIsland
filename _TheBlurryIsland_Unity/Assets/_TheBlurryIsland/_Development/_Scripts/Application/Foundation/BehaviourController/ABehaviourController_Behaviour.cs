using System;
using Blurry.Application.Foundation.BehaviourData;
using Blurry.Application.Foundation.Behaviour;
using Blurry.Application.Foundation.Presentation;


namespace Blurry.Application.Foundation.BehaviourController
{
    public abstract class ABehaviourController_Behaviour<TPresentation, TData, TBehaviours> : ABehaviour<TPresentation, TData>, IBehaviourController_Behaviour where TPresentation : IPresentation
                                                                                                                                                               where TData : IBehaviour_Data
                                                                                                                                                               where TBehaviours : IBehaviour
    {
        protected ABehaviourController_Behaviour(TPresentation presentation, TData data) : base(presentation, data)
        {
        }

        protected override void Activate_()
        {
            DoForEachBehaviour(behaviour => behaviour.GetActivated());
            Activate__();
        }

        protected override void Start_()
        {
            DoForEachBehaviour(behaviour => behaviour.GetStarted());
            StartInner__();
        }

        protected override void Stop_()
        {
            DoForEachBehaviour(behaviour => behaviour.GetStopped());
            StopInner__();
        }

        protected override void Reset_()
        {
            DoForEachBehaviour(behaviour => behaviour.GetReset());
            ResetInner__();
        }

        protected override void Restart_()
        {
            DoForEachBehaviour(behaviour => behaviour.GetRestarted());
            RestartInner__();
        }

        protected override void Deactivate_()
        {
            DoForEachBehaviour(behaviour => behaviour.GetDeactivated());
            Deactivate__();
        }

        public override void Update(float deltaTime)
        {
            DoForEachBehaviour(behaviour => behaviour.Update(deltaTime));
            Update_(deltaTime);
        }

        public override void FixedUpdate(float fixedDeltaTime)
        {
            DoForEachBehaviour(behaviour => behaviour.FixedUpdate(fixedDeltaTime));
            FixedUpdate_(fixedDeltaTime);
        }

        protected abstract void DoForEachBehaviour(Action<TBehaviours> action);

        protected abstract void Activate__();
        protected abstract void StartInner__();
        protected abstract void StopInner__();
        protected abstract void ResetInner__();
        protected abstract void RestartInner__();
        protected abstract void Deactivate__();

        protected abstract void Update_(float deltaTime);
        protected abstract void FixedUpdate_(float fixedDeltaTime);
    }
}