using System;
using System.Collections.Generic;
using Blurry.Application.Foundation.BehaviourData;
using Blurry.Application.Foundation.Presentation;
using Blurry.Application.Foundation.Behaviour;


namespace Blurry.Application.Foundation.BehaviourController
{
    public abstract class ABehaviourController_BehaviourList<TPresentation, TData, TBehaviours> : ABehaviourController_Behaviour<TPresentation, TData, TBehaviours> where TPresentation : IPresentation
                                                                                                                                                                    where TData : IBehaviour_Data
                                                                                                                                                                    where TBehaviours : IBehaviour
    {
        protected readonly List<TBehaviours> behaviours;

        protected ABehaviourController_BehaviourList(TPresentation presentation, TData data, List<TBehaviours> behaviours) : base(presentation, data)
        {
            this.behaviours = behaviours;
        }

        protected override void DoForEachBehaviour(Action<TBehaviours> action)
        {
            foreach (TBehaviours behaviour in behaviours)
                action.Invoke(behaviour);
        }
    }
}