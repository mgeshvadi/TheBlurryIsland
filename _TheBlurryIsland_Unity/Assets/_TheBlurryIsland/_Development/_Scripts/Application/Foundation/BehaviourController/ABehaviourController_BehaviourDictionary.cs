using System;
using System.Collections.Generic;
using Blurry.Application.Foundation.BehaviourData;
using Blurry.Application.Foundation.Presentation;
using Blurry.Application.Foundation.Behaviour;


namespace Blurry.Application.Foundation.BehaviourController
{
    public abstract class ABehaviourController_BehaviourDictionary<TPresentation, TData, TBehaviours> : ABehaviourController_Behaviour<TPresentation, TData, TBehaviours> where TPresentation : IPresentation
                                                                                                                                                                          where TData : IBehaviour_Data
                                                                                                                                                                          where TBehaviours : IBehaviour
    {
        protected readonly Dictionary<Type, TBehaviours> behaviours; // TODO: Maybe wrapping this into our own class, helping to specify the meaning of type here

        protected ABehaviourController_BehaviourDictionary(TPresentation presentation, TData data, Dictionary<Type, TBehaviours> behaviours) : base(presentation, data)
        {
            this.behaviours = behaviours;
        }

        protected override void DoForEachBehaviour(Action<TBehaviours> action)
        {
            foreach (KeyValuePair<Type, TBehaviours> behaviour in behaviours)
                action.Invoke(behaviour.Value);
        }
    }
}