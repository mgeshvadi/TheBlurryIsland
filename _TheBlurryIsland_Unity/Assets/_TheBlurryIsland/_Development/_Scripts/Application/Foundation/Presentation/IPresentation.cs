using Blurry.Application.Foundation.Factory;


namespace Blurry.Application.Foundation.Presentation
{
    public interface IPresentation : IFactorable
    {
    }
}