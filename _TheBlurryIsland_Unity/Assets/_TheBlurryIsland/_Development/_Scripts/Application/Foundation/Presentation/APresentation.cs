using Blurry.Application.Foundation.PresentationData;


namespace Blurry.Application.Foundation.Presentation
{
    public abstract class APresentation<TData> : IPresentation where TData : IPresentation_Data
    {
        protected readonly TData data;

        protected APresentation(TData data)
        {
            this.data = data;
        }
    }
}