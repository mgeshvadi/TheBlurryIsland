using Blurry.Application.Foundation.Factory;


namespace Blurry.Application.Foundation.Config
{
    public interface IConfig : IFactoryNeed
    {
    }
}