using System.Collections.Generic;
using UnityEngine;


namespace Blurry.Application.Foundation.Config
{
    public interface IConfig_File
    {
        public IConfig GetNotCastedConfig();
    }

    public abstract class AConfig_File<TConfig> : ScriptableObject, IConfig_File where TConfig : IConfig
    {
        [SerializeField] private int activePresetIndex = 0;
        [SerializeField] private List<TConfig> presets = new List<TConfig>();

        public TConfig GetConfig()
        {
            if (presets.Count == 0)
                Debug.LogError($"Config of {GetType()} type has no presets!", this);
            return presets[activePresetIndex];
        }

        public IConfig GetNotCastedConfig()
        {
            return GetConfig();
        }
    }
}