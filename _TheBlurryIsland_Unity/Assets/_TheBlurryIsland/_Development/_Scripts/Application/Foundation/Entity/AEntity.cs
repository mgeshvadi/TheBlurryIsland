using UnityEngine;
using Blurry.Application.Foundation.Factory;


namespace Blurry.Application.Foundation.Entity
{
    public interface IEntity : IFactoryNeed
    {
        public GameObject GameObject { get; }
        public TTargetsEntity[] GetChildEntities<TTargetsEntity>() where TTargetsEntity : IEntity;
    }

    public abstract class AEntity : MonoBehaviour, IEntity
    {
        public GameObject GameObject => gameObject;

        public TTargetsEntity[] GetChildEntities<TTargetsEntity>() where TTargetsEntity : IEntity
        {
            return GetComponentsInChildren<TTargetsEntity>();
        }

        #if UNITY_EDITOR
        private void Reset()
        {
            if (gameObject.activeInHierarchy)
                gameObject.name = GetType().Name;
        }
        #endif
    }
}