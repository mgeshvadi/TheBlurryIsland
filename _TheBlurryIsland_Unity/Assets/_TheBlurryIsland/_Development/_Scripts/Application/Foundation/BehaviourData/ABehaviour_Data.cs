using Blurry.Application.Foundation.Config;
using Blurry.Application.Foundation.Entity;


namespace Blurry.Application.Foundation.BehaviourData
{
    public abstract class ABehaviour_Data<TConfig> : IBehaviour_Data where TConfig : IConfig
    {
        protected ABehaviour_Data(TConfig config)
        {
        }
    }

    public abstract class ABehaviour_Data<TEntity, TConfig> : IBehaviour_Data where TEntity : IEntity
                                                                              where TConfig : IConfig
    {
        protected ABehaviour_Data(TEntity entity, TConfig config)
        {
        }
    }
}