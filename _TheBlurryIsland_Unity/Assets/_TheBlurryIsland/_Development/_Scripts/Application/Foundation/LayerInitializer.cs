using Blurry.Application.Foundation.ConfigFileManagement;


namespace Blurry.Application.Foundation
{
    public readonly struct LayerInitializationsResult
    {
    }

    public static class LayerInitializer
    {
        public static LayerInitializationsResult MockInitialize()
        {
            return Initialize();
        }

        public static LayerInitializationsResult Initialize()
        {
            ConfigFileManagement.ConfigFileManagement.Initialize();
            ConfigFileManagement.ConfigFileManagement.RegisterConfigFileLoader<Root_ConfigFileLoader>(new Root_ConfigFileLoader());

            return new LayerInitializationsResult();
        }
    }
}