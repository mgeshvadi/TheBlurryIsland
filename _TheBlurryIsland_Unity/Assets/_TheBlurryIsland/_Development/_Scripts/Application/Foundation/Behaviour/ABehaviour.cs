using System;
using UnityEngine;
using Blurry.Application.Foundation.BehaviourData;
using Blurry.Application.Foundation.Presentation;


namespace Blurry.Application.Foundation.Behaviour
{
    public abstract class ABehaviour<TPresentation, TData> : IBehaviour where TPresentation : IPresentation
                                                                        where TData : IBehaviour_Data
    {
        protected readonly TPresentation presentation;
        protected readonly TData data;

        private bool isActive;
        private bool isStarted;

        protected ABehaviour(TPresentation presentation, TData data)
        {
            this.presentation = presentation;
            this.data = data;
        }

        public void GetActivated()
        {
            if (CanGetActivated())
                Activate();
            else
                LogAboutBehavior(LogType.Error, "Couldn't Get Activated"); // todo: Even move these strings to Log System
        }

        public void GetStarted()
        {
            if (CanGetStarted())
                Start();
            else
                LogAboutBehavior(LogType.Error, "Couldn't Get Started");
        }

        public void GetStopped()
        {
            if (CanGetStopped())
                Stop();
            else
                LogAboutBehavior(LogType.Error, "Couldn't Get Stopped");
        }

        public void GetRestarted()
        {
            if (CanGetRestarted())
                Restart();
            else
                LogAboutBehavior(LogType.Error, "Couldn't Get ReStarted");
        }

        public void GetDeactivated()
        {
            if (CanGetDeactivated())
                Deactivate();
            else
                LogAboutBehavior(LogType.Error, "Couldn't Get Deactivated");
        }

        private bool CanGetActivated() { return !isActive; }
        private bool CanGetStarted() { return isActive && !isStarted; }
        private bool CanGetStopped() { return isActive && isStarted; }
        private bool CanGetRestarted() { return isActive; }
        private bool CanGetDeactivated() { return isActive; }

        private void Activate()
        {
            isActive = true;
            Activate_();
            LogAboutBehavior(LogType.Normal, "Activated");
        }

        private void Start()
        {
            isStarted = true;
            Start_();
            LogAboutBehavior(LogType.Normal, "Got Started");
        }

        private void Stop()
        {
            isStarted = false;
            Stop_();
            LogAboutBehavior(LogType.Normal, "Got Stopped");
        }

        private void Restart()
        {
            Stop();
            GetReset();
            Restart_();
            LogAboutBehavior(LogType.Normal, "Got ReStarted");
            Start();
        }

        public void GetReset()
        {
            Reset_();
            LogAboutBehavior(LogType.Normal, "Got Reseted");
        }

        private void Deactivate()
        {
            isActive = false;
            Deactivate_();
            LogAboutBehavior(LogType.Normal, "Got Deactivated");
        }

        private enum LogType // TODO : Move this to loggerSystem
        {
            Error,
            Warning,
            Normal
        }

        private void LogAboutBehavior(LogType logType, string message)
        {
            message = $"System Of Type {GetType()} {message}";
            switch (logType)
            {
                case LogType.Error:
                    Debug.LogError(message);
                    break;
                case LogType.Warning:
                    Debug.LogWarning(message);
                    break;
                case LogType.Normal:
                    Debug.Log(message);
                    break;
                default: throw new ArgumentOutOfRangeException(nameof(logType), logType, null);
            }
        }

        protected abstract void Activate_();
        protected abstract void Start_();
        protected abstract void Stop_();
        protected abstract void Reset_();
        protected abstract void Restart_();
        protected abstract void Deactivate_();

        public abstract void Update(float deltaTime);
        public abstract void FixedUpdate(float fixedDeltaTime);
    }
}