using Blurry.Application.Foundation.Factory;


namespace Blurry.Application.Foundation.Behaviour
{
    public interface IBasicBehaviour
    {
        public void Update(float deltaTime);
        public void FixedUpdate(float fixedDeltaTime);
    }

    public interface IBehaviour : IBasicBehaviour, IFactorable
    {
        public void GetActivated();
        public void GetStarted();
        public void GetStopped();
        public void GetRestarted();
        public void GetDeactivated();

        public void GetReset();
    }
}