namespace Blurry.Application.Foundation.Factory
{
    public interface IFactory<out TFactorable> where TFactorable : IFactorable
    {
        public TFactorable Create();
    }

    public interface IFactory<out TFactorable, in TFactoryNeed> where TFactorable : IFactorable
                                                                where TFactoryNeed : IFactoryNeed
    {
        public TFactorable Create(TFactoryNeed factoryNeed);
    }

    public interface IFactory<out TFactorable, in TFactoryNeedOne, in TFactoryNeedTwo> where TFactorable : IFactorable
                                                                                       where TFactoryNeedOne : IFactoryNeed
                                                                                       where TFactoryNeedTwo : IFactoryNeed
    {
        public TFactorable Create(TFactoryNeedOne factoryNeedOne, TFactoryNeedTwo factoryNeedTwo);
    }
}