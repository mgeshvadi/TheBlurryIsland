using Blurry.Application.Foundation.Config;
using Blurry.Application.Foundation.Entity;


namespace Blurry.Application.Foundation.PresentationData
{
    public abstract class APresentation_Data<TConfig> : IPresentation_Data where TConfig : IConfig
    {
        protected APresentation_Data(TConfig config)
        {
        }
    }

    public abstract class APresentation_Data<TEntity, TConfig> : IPresentation_Data where TEntity : IEntity
                                                                                    where TConfig : IConfig
    {
        protected APresentation_Data(TEntity entity, TConfig config)
        {
        }
    }
}