using System;
using UnityEngine;
using Blurry.Application.Base.AssetManagement;
using Blurry.Application.Base.AssetManagement.Base;
using Blurry.Application.Foundation.Config;


namespace Blurry.Application.Foundation.ConfigFileManagement.Base
{
    public interface IConfigFileLoader
    {
    }

    public interface IConfigFileLoader<in TConfigFile> : IConfigFileLoader where TConfigFile : IConfig_File
    {
        public TTargetConfigFile LoadConfigFile<TTargetConfigFile>() where TTargetConfigFile : ScriptableObject, TConfigFile;
    }

    public abstract class AConfigFileLoader<TConfigFile> : IConfigFileLoader<TConfigFile> where TConfigFile : IConfig_File
    {
        public TTargetConfigFile LoadConfigFile<TTargetConfigFile>() where TTargetConfigFile : ScriptableObject, TConfigFile
        {
            return (TTargetConfigFile) LoadConfigFile(GetConfigAssetFolder(), typeof(TTargetConfigFile));
        }

        public ScriptableObject LoadConfigFile(Type targetConfigFileType)
        {
            return LoadConfigFile(GetConfigAssetFolder(), targetConfigFileType);
        }

        private ScriptableObject LoadConfigFile(AssetFolder configFolder, Type targetConfigFileType)
        {
            string configFilePath = configFolder.Path + targetConfigFileType.Name;
            var result = AssetManagement.ActiveAssetLoader.Load(new Asset(configFilePath), targetConfigFileType);
            if (result == null)
                Debug.LogError($"Config file for {targetConfigFileType} not found. Searched in {configFilePath}");
            return (ScriptableObject) result;
        }

        protected abstract AssetFolder GetConfigAssetFolder();
    }
}