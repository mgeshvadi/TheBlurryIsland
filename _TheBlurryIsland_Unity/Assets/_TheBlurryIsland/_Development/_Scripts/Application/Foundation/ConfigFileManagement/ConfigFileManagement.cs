using System;
using System.Collections.Generic;
using Blurry.Application.Foundation.ConfigFileManagement.Base;


namespace Blurry.Application.Foundation.ConfigFileManagement
{
    public static class ConfigFileManagement
    {
        public static bool IsInitialized { private set; get; }
        private static Dictionary<Type, IConfigFileLoader> configFileLoaders;

        public static void Initialize()
        {
            if (IsInitialized)
                return;

            configFileLoaders = new Dictionary<Type, IConfigFileLoader>();
            IsInitialized = true;
        }

        public static void RegisterConfigFileLoader<TConfigFileLoader>(IConfigFileLoader configFileLoader) where TConfigFileLoader : IConfigFileLoader
        {
            configFileLoaders.Add(typeof(TConfigFileLoader), configFileLoader);
        }

        public static TConfigFileLoader GetConfigLoader<TConfigFileLoader>() where TConfigFileLoader : IConfigFileLoader
        {
            if (!IsConfigFileLoaderRegistered<TConfigFileLoader>())
                throw new Exception($"Loader Of type: {typeof(TConfigFileLoader)} is not registered.");
            return (TConfigFileLoader) configFileLoaders[typeof(TConfigFileLoader)];
        }

        private static bool IsConfigFileLoaderRegistered<TConfigFileLoader>() where TConfigFileLoader : IConfigFileLoader
        {
            return configFileLoaders.ContainsKey(typeof(TConfigFileLoader));
        }
    }
}