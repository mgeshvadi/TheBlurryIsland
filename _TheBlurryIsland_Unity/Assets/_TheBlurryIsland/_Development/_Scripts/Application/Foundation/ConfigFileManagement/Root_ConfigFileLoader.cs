using Blurry.Application.Base.AssetManagement.Addresses;
using Blurry.Application.Base.AssetManagement.Base;
using Blurry.Application.Foundation.Config;
using Blurry.Application.Foundation.ConfigFileManagement.Base;


namespace Blurry.Application.Foundation.ConfigFileManagement
{
    public sealed class Root_ConfigFileLoader : AConfigFileLoader<IConfig_File>
    {
        public const string CONFIG_FILES_CONTEXT_MENU_BASE = "Blurry/Resources/ConfigFiles/";

        protected override AssetFolder GetConfigAssetFolder()
        {
            return AssetFolders.ConfigFiles.root;
        }
    }
}