using Blurry.Application.CoreMechanics.MechanicControllers.Factory;
using Blurry.Application.Foundation.ConfigFileManagement;
using Blurry.Application.Levels.ConfigManagement;
using Blurry.Application.Levels.Factory;


namespace Blurry.Application.Levels
{
    public class LayerInitializationsResult
    {
        public ILevelFactory LevelFactory { get; }

        public LayerInitializationsResult(ILevelFactory levelFactory)
        {
            LevelFactory = levelFactory;
        }
    }

    public static class LayerInitializer
    {
        public static LayerInitializationsResult MockInitialize(IMechanicControllersFactory mechanicControllersFactory)
        {
            return Initialize(mechanicControllersFactory);
        }

        public static LayerInitializationsResult Initialize(IMechanicControllersFactory mechanicControllersFactory)
        {
            ConfigFileManagement.RegisterConfigFileLoader<Levels_ConfigFileLoader>(new Levels_ConfigFileLoader());
            return new LayerInitializationsResult(new LevelFactory(mechanicControllersFactory));
        }
    }
}