using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using Object = UnityEngine.Object;
using Blurry.Application.Base.AssetManagement;
using Blurry.Application.Base.AssetManagement.Addresses;
using Blurry.Application.Base.AssetManagement.Base;
using Blurry.Application.CoreMechanics.MechanicControllers.ControllerBehaviour;
using Blurry.Application.CoreMechanics.MechanicControllers.Entity;
using Blurry.Application.CoreMechanics.MechanicControllers.Factory;
using Blurry.Application.Foundation.ConfigFileManagement;
using Blurry.Application.Levels.Behaviour;
using Blurry.Application.Levels.BehaviourData;
using Blurry.Application.Levels.Config;
using Blurry.Application.Levels.ConfigManagement;
using Blurry.Application.Levels.Entity;
using Blurry.Application.Levels.Presentation;
using Blurry.Application.Levels.PresentationData;
using Blurry.Application.Utility.Extensions;


namespace Blurry.Application.Levels.Factory
{
    // TODO: `LevelsFactory` and `MechanicControllersFactory` and `MechanicsFactory` are really similar maybe factoring them and preventing DRY code? (Do not repeat yourself)
    public class LevelFactory : ILevelFactory
    {
        private readonly struct LevelInfo
        {
            public Type PresentationType { get; }
            public Type BehaviourDataType { get; }
            public Type PresentationDataType { get; }
            public Type BehaviourType { get; }
            public Type ConfigFileType { get; }

            public LevelInfo(Type presentationType, Type behaviourDataType, Type presentationDataType, Type behaviourType, Type configFileType)
            {
                PresentationType = presentationType;
                BehaviourDataType = behaviourDataType;
                PresentationDataType = presentationDataType;
                BehaviourType = behaviourType;
                ConfigFileType = configFileType;
            }
        }

        private readonly IMechanicControllersFactory mechanicControllersFactory;

        public LevelFactory(IMechanicControllersFactory mechanicControllersFactory)
        {
            this.mechanicControllersFactory = mechanicControllersFactory;
        }

        public ILevel_Entity InstantiateLevel(Type levelEntityType, Vector3 position, Transform parent)
        {
            ILevel_Entity result = null;
            if (typeof(ILevel_Entity).IsAssignableFrom(levelEntityType))
            {
                Object source = AssetManagement.ActiveAssetLoader.Load(new Asset(path: AssetFolders.Levels.root.Path + levelEntityType.Name));
                result = ((GameObject) Object.Instantiate(source, position, Quaternion.identity, parent)).gameObject.GetComponent<ILevel_Entity>();
            }
            else
                Debug.LogError($"Telling my to Instantiate a level with entity type of {levelEntityType} whereas this type should be implementing {nameof(ILevel_Entity)}"); // todo : use our logger

            return result;
        }


        public ILevel_Behaviour CreateLevelBehaviourFor(ILevel_Entity levelEntity)
        {
            LevelInfo levelInfo = GetLevelInfoBasedOnLevelEntityType(levelEntity.GetType());

            ILevel_Config config = (ILevel_Config) ((ILevel_ConfigFile) ConfigFileManagement.GetConfigLoader<Levels_ConfigFileLoader>().LoadConfigFile(levelInfo.ConfigFileType)).GetNotCastedConfig();
            ILevel_PresentationData presentationData = (ILevel_PresentationData) Activator.CreateInstance(levelInfo.PresentationDataType, levelEntity, config);

            ILevel_Presentation presentation = (ILevel_Presentation) Activator.CreateInstance(levelInfo.PresentationType, presentationData);
            ILevel_BehaviourData behaviourData = (ILevel_BehaviourData) Activator.CreateInstance(levelInfo.BehaviourDataType, levelEntity, config);
            Dictionary<Type, IMechanicController_Behaviour> mechanicControllerBehaviours = mechanicControllersFactory.CreateMechanicControllersBehavioursForThese(levelEntity.GetChildEntities<IMechanicController_Entity>().ToList());

            ILevel_Behaviour behaviour = (ILevel_Behaviour) Activator.CreateInstance(levelInfo.BehaviourType, presentation, behaviourData, mechanicControllerBehaviours);

            return behaviour;
        }

        private LevelInfo GetLevelInfoBasedOnLevelEntityType(Type levelEntityType) // TODO: Check this methode from performance point of view
        {
            var allTypes = Assembly.GetAssembly(levelEntityType).GetTypes().ToList();

            Type configFileType = null, behaviourDataType = null, presentationDataType = null, presentationType = null, behaviourType = null;
            foreach (Type type in allTypes)
            {
                if (!type.IsAEndImplementation() || !type.CompareReflectionCreationBundleAttributeTo(levelEntityType))
                    continue;

                if (type.IsImplementationOf(typeof(ILevel_ConfigFile)))
                    configFileType = type;
                else if (type.IsImplementationOf(typeof(ILevel_BehaviourData)))
                    behaviourDataType = type;
                else if (type.IsImplementationOf(typeof(ILevel_PresentationData)))
                    presentationDataType = type;
                else if (type.IsImplementationOf(typeof(ILevel_Presentation)))
                    presentationType = type;
                else if (type.IsImplementationOf(typeof(ILevel_Behaviour)))
                    behaviourType = type;
            }

            return new LevelInfo(presentationType, behaviourDataType, presentationDataType, behaviourType, configFileType);
        }
    }
}