using System;
using UnityEngine;
using Blurry.Application.Levels.Behaviour;
using Blurry.Application.Levels.Entity;


namespace Blurry.Application.Levels.Factory
{
    public interface ILevelFactory
    {
        public ILevel_Entity InstantiateLevel(Type levelEntityType, Vector3 position, Transform parent);
        public ILevel_Behaviour CreateLevelBehaviourFor(ILevel_Entity levelEntity);
    }
}