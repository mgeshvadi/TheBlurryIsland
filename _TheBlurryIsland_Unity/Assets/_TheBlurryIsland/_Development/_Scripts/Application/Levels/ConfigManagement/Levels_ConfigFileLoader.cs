using Blurry.Application.Base.AssetManagement.Addresses;
using Blurry.Application.Base.AssetManagement.Base;
using Blurry.Application.Foundation.ConfigFileManagement;
using Blurry.Application.Foundation.ConfigFileManagement.Base;
using Blurry.Application.Levels.Config;


namespace Blurry.Application.Levels.ConfigManagement
{
    public sealed class Levels_ConfigFileLoader : AConfigFileLoader<ILevel_ConfigFile>
    {
        public const string CONFIG_FILES_CONTEXT_MENU = Root_ConfigFileLoader.CONFIG_FILES_CONTEXT_MENU_BASE + "Levels/";

        protected override AssetFolder GetConfigAssetFolder()
        {
            return AssetFolders.ConfigFiles.levels;
        }
    }
}