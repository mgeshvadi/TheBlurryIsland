using Blurry.Application.Foundation.BehaviourData;
using Blurry.Application.Levels.Config;
using Blurry.Application.Levels.Entity;


namespace Blurry.Application.Levels.BehaviourData
{
    public interface ILevel_BehaviourData : IBehaviour_Data
    {
    }

    public abstract class ALevel_BehaviourData<TEntity, TConfig> : ABehaviour_Data<TEntity, TConfig>, ILevel_BehaviourData where TEntity : ILevel_Entity
                                                                                                                           where TConfig : ILevel_Config
    {
        protected ALevel_BehaviourData(TEntity entity, TConfig config) : base(entity, config)
        {
        }
    }
}