using System;
using System.Collections.Generic;
using Blurry.Application.CoreMechanics.MechanicControllers.ControllerBehaviour;
using Blurry.Application.Foundation.BehaviourController;
using Blurry.Application.Levels.BehaviourData;
using Blurry.Application.Levels.Presentation;


namespace Blurry.Application.Levels.Behaviour
{
    public interface ILevel_Behaviour : IBehaviourController_Behaviour
    {
    }

    public abstract class ALevel_Behaviour<TPresentation, TData> : ABehaviourController_BehaviourDictionary<TPresentation, TData, IMechanicController_Behaviour>, ILevel_Behaviour where TPresentation : ILevel_Presentation
                                                                                                                                                                                   where TData : ILevel_BehaviourData

    {
        protected ALevel_Behaviour(TPresentation presentation, TData data, Dictionary<Type, IMechanicController_Behaviour> behaviours) : base(presentation, data, behaviours)
        {
        }

        protected T GetLevelMechanicController<T>() where T : class, IMechanicController_Behaviour
        {
            return behaviours[typeof(T)] as T;
        }
    }
}