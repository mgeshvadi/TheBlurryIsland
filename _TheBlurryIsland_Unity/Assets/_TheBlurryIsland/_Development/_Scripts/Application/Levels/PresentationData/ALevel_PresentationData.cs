using Blurry.Application.Foundation.PresentationData;
using Blurry.Application.Levels.Config;
using Blurry.Application.Levels.Entity;


namespace Blurry.Application.Levels.PresentationData
{
    public interface ILevel_PresentationData : IPresentation_Data
    {
    }

    public abstract class ALevel_PresentationData<TEntity, TConfig> : APresentation_Data<TEntity, TConfig>, ILevel_PresentationData where TEntity : ILevel_Entity
                                                                                                                                    where TConfig : ILevel_Config
    {
        protected ALevel_PresentationData(TEntity entity, TConfig config) : base(entity, config)
        {
        }
    }
}