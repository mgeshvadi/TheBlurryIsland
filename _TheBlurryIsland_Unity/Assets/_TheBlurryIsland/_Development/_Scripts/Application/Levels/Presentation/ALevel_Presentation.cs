using Blurry.Application.Foundation.Presentation;
using Blurry.Application.Levels.PresentationData;


namespace Blurry.Application.Levels.Presentation
{
    public interface ILevel_Presentation : IPresentation
    {
    }

    public abstract class ALevel_Presentation<TData> : APresentation<TData>, ILevel_Presentation where TData : ILevel_PresentationData
    {
        protected ALevel_Presentation(TData data) : base(data)
        {
        }
    }
}