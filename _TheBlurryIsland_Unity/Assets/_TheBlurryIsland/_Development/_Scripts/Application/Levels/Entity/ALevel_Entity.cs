using Blurry.Application.Foundation.Entity;


namespace Blurry.Application.Levels.Entity
{
    public interface ILevel_Entity : IEntity
    {
    }

    public abstract class ALevel_Entity : AEntity, ILevel_Entity
    {
        #if UNITY_EDITOR
        private void Reset()
        {
            if (gameObject.activeInHierarchy)
                gameObject.name = GetType().Name;
        }
        #endif
    }
}