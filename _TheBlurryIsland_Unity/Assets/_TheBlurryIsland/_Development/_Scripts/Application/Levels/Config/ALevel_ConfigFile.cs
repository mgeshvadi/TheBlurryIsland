using Blurry.Application.Foundation.Config;


namespace Blurry.Application.Levels.Config
{
    public interface ILevel_Config : IConfig
    {
    }

    public interface ILevel_ConfigFile : IConfig_File
    {

    }

    public abstract class ALevel_ConfigFile<TConfig> : AConfig_File<TConfig>, ILevel_ConfigFile where TConfig : ILevel_Config
    {
    }
}