﻿using PandasCanPlay.Utility;
using UnityEditor;
using UnityEngine;


namespace PandasCanPlay.Editor.Drawers
{
    [CustomPropertyDrawer(typeof(ButtonAttribute))]
    public class ButtonAttributeDrawer : PropertyDrawer
    {
        static object[] emptyParamters = new object[0];

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            ButtonAttribute buttonAtr = attribute as ButtonAttribute;
            var callBack = property.serializedObject.targetObject.GetType().GetMethod(buttonAtr.callBackName);
            var buttonAction = fieldInfo.GetValue(property.serializedObject.targetObject) as ButtonAction;

            if (GUI.Button(position, buttonAtr.name))
                if (callBack != null)
                    callBack.Invoke(property.serializedObject.targetObject, emptyParamters);
        }
    }
}