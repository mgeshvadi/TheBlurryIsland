﻿using UnityEngine;


namespace PandasCanPlay.Attributes
{
    public class ArrayElementTitleAttribute : PropertyAttribute
    {
        public string targetMember;

        public ArrayElementTitleAttribute(string targetMember)
        {
            this.targetMember = targetMember;
        }
    }
}