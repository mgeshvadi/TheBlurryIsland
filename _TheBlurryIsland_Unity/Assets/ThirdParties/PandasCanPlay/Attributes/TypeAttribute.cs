﻿using UnityEngine;
using System;


namespace PandasCanPlay.Attributes
{
    public class TypeAttribute : PropertyAttribute
    {
        public Type[] types;
        public bool includeAbstracts;
        public bool showPartialName;

        public TypeAttribute(Type type, bool includeAbstracts, bool showPartialName) : this(new Type[] {type}, includeAbstracts, showPartialName)
        {

        }

        public TypeAttribute(Type[] types, bool includeAbstracts, bool showPartialName)
        {
            this.types = types;
            this.includeAbstracts = includeAbstracts;
            this.showPartialName = showPartialName;
        }

        public TypeAttribute(Type type, bool includeAbstracts) : this(type, includeAbstracts, true)
        {
        }
    }
}