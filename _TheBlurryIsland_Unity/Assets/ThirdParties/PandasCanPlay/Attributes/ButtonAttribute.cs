﻿using System;
using UnityEngine;


namespace PandasCanPlay.Utility
{
    [Serializable]
    public class ButtonAction
    {
        public Action action;

        public ButtonAction(Action action)
        {
            this.action = action;
        }
    }

    public class ButtonAttribute : PropertyAttribute
    {
        public string name;
        public string callBackName;

        public ButtonAttribute(string name, string callBackName)
        {
            this.name = name;
            this.callBackName = callBackName;
        }
    }
}